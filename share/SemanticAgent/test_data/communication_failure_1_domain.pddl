(define (domain dm)
    (:requirements
        :strips
        :typing
        :negative-preconditions
        :constraints)
    (:types
        qiknowledge_event_communication_act
        social_agent)
    (:constants
        qiknowledge_event_event_ddf860ba - qiknowledge_event_communication_act
        qiknowledge_event_event_679eab36 - qiknowledge_event_communication_act)
    (:predicates
        (qiknowledge_event_responds_to
            ?input - qiknowledge_event_communication_act
            ?response - qiknowledge_event_communication_act)
        (was_responded_to ?c - qiknowledge_event_communication_act)
        (colocated ?p1 - social_agent ?p2 - social_agent)
        (was_misunderstood ?input - qiknowledge_event_communication_act))
    (:action action_suggestion_1
        :precondition
            (not (was_responded_to qiknowledge_event_event_ddf860ba))
        :effect
            (and
                (qiknowledge_event_responds_to qiknowledge_event_event_679eab36 qiknowledge_event_event_ddf860ba)
                (was_responded_to qiknowledge_event_event_ddf860ba)))
    (:action action_d_sol_je_ne_comprends_pas_
        :precondition
            (not (was_responded_to qiknowledge_event_event_ddf860ba))
        :effect
            (and
                (qiknowledge_event_responds_to qiknowledge_event_event_679eab36 qiknowledge_event_event_ddf860ba)
                (was_responded_to qiknowledge_event_event_ddf860ba)
                (was_misunderstood qiknowledge_event_event_ddf860ba))))
