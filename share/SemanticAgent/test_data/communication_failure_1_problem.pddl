(define (problem pb)
	(:domain dm)
	(:objects
		qiknowledge_event_event_ddf860ba - qiknowledge_event_communication_act
		qiknowledge_event_event_679eab36 - qiknowledge_event_communication_act)
	(:init
		)
	(:goal
		(and
			(was_responded_to qiknowledge_event_event_ddf860ba)
			(not (was_misunderstood qiknowledge_event_event_ddf860ba)))))