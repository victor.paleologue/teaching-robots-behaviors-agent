#include <gmock/gmock.h>
#include <qi/anymodule.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "test_agent.hpp"

class ModuleTest: public CommonSetup {
protected:
  ModuleTest()
    : CommonSetup()
  {
    auto module = qi::import("semanticagent_module");
    auto agent = module.call<qi::semantic::agent::AgentPtr>("SemanticAgent", _server);
    _serviceId = _server->registerService("SemanticAgent", agent).value();
    _agentRemote = _client->service("SemanticAgent").value();
  }

  virtual ~ModuleTest()
  {
    _agentRemote.reset();
    if (_serviceId > 0)
      _server->unregisterService(_serviceId).value();
  }

  int _serviceId = 0;
  qi::semantic::agent::AgentPtr _agentRemote;
};

TEST_F(ModuleTest, loadAndRegister)
{
}

TEST_F(ModuleTest, dialogSpeechExtractor)
{
  ASSERT_NO_THROW(_agentRemote->makeFakeSpeechExtractor());
}
