#ifndef SEMANTICAGENT_TEST_FUTURE_IO_HPP
#define SEMANTICAGENT_TEST_FUTURE_IO_HPP

#include <future>
#include <ostream>
#include <string>
#include <qi/future.hpp>


/// A helper to log the state of a future.
template <typename T>
std::string futureToString(qi::Future<T> future)
{
  switch (future.wait(0))
  {
  case qi::FutureState_None:
    return "invalid future";
  case qi::FutureState_Running:
    return "future is running";
  case qi::FutureState_FinishedWithValue:
    return "future has value"; // KLUDGE: serializing the value in JSON would break compilation
  case qi::FutureState_FinishedWithError:
  {
    std::stringstream ss;
    ss << "future has error: " << future.error();
    return ss.str();
  }
  case qi::FutureState_Canceled:
    return "future was canceled";
  }

  QI_ASSERT(false && "unexpected future state");
  throw std::runtime_error("unexpected future state");
}


/// Extract to stream.
template <typename T>
std::ostream& operator<<(std::ostream& out, qi::Future<T> future)
{
  out << futureToString(future);
  return out;
}


/// A helper to log the status of a standard future.
std::string futureStatusToString(const std::future_status& status);


#endif // SEMANTICAGENT_TEST_FUTURE_IO_HPP
