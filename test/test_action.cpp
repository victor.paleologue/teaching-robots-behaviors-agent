#include <functional>
#include <future>
#include <memory>
#include <gmock/gmock.h>
#include <qi/future.hpp>
#include <qi/context/commonerrormessages.hpp>
#include <qi/signalspy.hpp>
#include "../src/behaviormodel/action.hpp"
#include "../src/utility/disposable.hpp"
#include "debug_log_enabler.hpp"
#include "utility.hpp"

using namespace qi::semantic::agent;

class ActionTest: public FocusSetup
{
protected:
  DebugLogEnabler _actionDebug{"SemanticAgent.Action"};
  DebugLogEnabler _contextDebug{"qi.context"};
};

TEST_F(ActionTest, ActionMadeFromFunctionRunsAndFinishesProperly)
{
  std::promise<void> startedPromise;
  auto task = std::make_unique<qi::semantic::agent::FunctionAction>(
        "To do nothing", [&startedPromise]
  {
    startedPromise.set_value();
    return qi::Future<void>{nullptr};
  });

  qi::SignalSpy taskSpy{task->started};
  auto running = task->run();
  EXPECT_TRUE(taskSpy.waitUntil(1, usualTimeout).value());

  auto startedStatus = startedPromise.get_future().wait_for(stdUsualTimeout);
  EXPECT_EQ(std::future_status::ready, startedStatus);

  auto runningStatus = running.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, runningStatus);
}


TEST_F(ActionTest, ActionDropsFunctionWhenDestroyed)
{
  std::future<void> destroyed;

  {
    auto destroyedPromise = std::shared_ptr<std::promise<void>>(
            new std::promise<void>(), [](std::promise<void>* p)
    {
      p->set_value();
      delete p;
    });
    destroyed = destroyedPromise->get_future();

    auto task = std::make_unique<qi::semantic::agent::FunctionAction>(
          "To do nothing", [destroyedPromise]{ return qi::Future<void>{nullptr}; });
  }

  auto status = destroyed.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
}


TEST_F(ActionTest, ActionDropsFunctionWhenDestroyedWhileRunning)
{
  // We will track every copy of the function, and its final destruction.
  DebugLogEnabler debugCopies("SemanticAgent.Test");
  std::shared_future<void> destroyed;
  qi::Promise<void> runningPromise;
  qi::Future<void> running;

  {
    CopyTracker functionTracker;
    destroyed = functionTracker.chainDestroyed();

    // This task is cancellable.
    runningPromise = qi::Promise<void>([](qi::Promise<void> p){ p.setCanceled(); });
    auto task = std::make_unique<FunctionAction>(
          "To do nothing", [functionTracker, runningPromise]
    {
      return runningPromise.future();
    });

    // Start task and wait for started.
    auto starting = waitForSignal(task->started);
    running = task->run();
    auto startingStatus = starting.waitFor(usualTimeout);
    ASSERT_EQ(qi::FutureState_FinishedWithValue, startingStatus) << futureToString(starting);
  }

  // It is cancelled at destruction, and the function is dropped.
  auto runningStatus = running.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithError, runningStatus)
      << futureToString(running);
  EXPECT_EQ(qi::error::actionDestroyed, running.error());
  EXPECT_TRUE(runningPromise.future().isCanceled());

  auto status = destroyed.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
}


TEST_F(ActionTest, ActionDropsFunctionWhenDestroyedAfterRunning)
{
  std::future<void> destroyed;

  {
    auto destroyedPromise = std::shared_ptr<std::promise<void>>(
            new std::promise<void>(), [](std::promise<void>* p)
    {
      p->set_value();
      delete p;
    });
    destroyed = destroyedPromise->get_future();

    auto task = std::make_unique<FunctionAction>(
          "To do nothing", [destroyedPromise]{ return qi::Future<void>{nullptr}; });
    task->run();
  }

  auto status = destroyed.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
}


TEST_F(ActionTest, WaitForStarted)
{
  qi::Promise<void> p{[&](qi::Promise<void> pp){ pp.setCanceled(); }};
  auto task = std::make_unique<qi::semantic::agent::FunctionAction>("", [p]{ return p.future(); });
  auto running = task->run();
  auto starting = task->waitForStarted();
  auto status = starting.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, status) << futureToString(starting);
  auto runningStatus = running.waitFor(qi::Seconds{0});
  EXPECT_EQ(qi::FutureState_Running, runningStatus) << futureToString(running);
}


TEST_F(ActionTest, ActionMadeFromCancellableFunctionIsCancellable)
{
  std::promise<void> cancelCallbackPromise;
  auto action = std::make_unique<qi::semantic::agent::FunctionAction>(
        "To do nothing actively", [&cancelCallbackPromise]
  {
    qi::Promise<void> actionPromise;
    actionPromise.setOnCancel([&cancelCallbackPromise, actionPromise](qi::Promise<void> p)
    {
      cancelCallbackPromise.set_value();
      p.setCanceled();
    });
    return actionPromise.future();
  });

  // Run and wait for started.
  // This ensures the behavior underneath is actually run, otherwise,
  // there is no guarantee the cancellation callback will be called.
  auto running = action->run();
  auto startingStatus = action->waitForStarted().wait(usualTimeout);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, startingStatus);

  // Cancel and check the callback was called.
  running.cancel();

  auto cancellingStatus = cancelCallbackPromise.get_future().wait_for(stdUsualTimeout);
  EXPECT_EQ(std::future_status::ready, cancellingStatus);

  auto runningStatus = running.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_Canceled, runningStatus);
}


TEST_F(ActionTest, PDDLAwareQiActionChecksContext)
{
  EXPECT_CALL(*_focusService, check(testing::_)).Times(1);
  auto action = qi::semantic::agent::makeQiAction(_client, "dummy", dummyActionFunction);
  action->context.set(_contextObject).value();
  auto starting = waitForSignal(action->started);
  action->run();
  EXPECT_EQ(qi::FutureState_FinishedWithValue, starting.waitFor(usualTimeout));
}


TEST_F(ActionTest, PDDLAwareQiActionWithNoContextFailsAndDoesNotCallFunction)
{
  std::atomic<bool> called(false);
  auto function = [&](qi::context::ContextPtr)
  {
    called = true;
    return qi::Future<void>(nullptr);
  };

  auto action = qi::semantic::agent::makeQiAction(_client, "dummy", function);
  auto starting = waitForSignal(action->started);
  ASSERT_ANY_THROW(action->run());
  EXPECT_EQ(qi::FutureState_Running, starting.waitFor(usualTimeout));
  EXPECT_FALSE(called.load());
}


TEST_F(ActionTest, PDDLAwareQiActionReportsExceptionFromFunction)
{
  const auto session = qi::makeSession();

  auto function = [](qi::context::ContextPtr) -> qi::Future<void>
  {
    throw IntendedException();
  };
  auto action = qi::semantic::agent::makeQiAction(_client, "dummy", function);

  EXPECT_CALL(*_focusService, check(testing::_)).Times(1);
  action->context.set(_contextObject).value();
  auto starting = waitForSignal(action->started);
  ASSERT_ANY_THROW(action->run()); // the exception type may be altered
  EXPECT_EQ(qi::FutureState_Running, starting.waitFor(usualTimeout));
}


TEST_F(ActionTest, ChainingActionWithSimpleFunction)
{
  std::vector<int> operations;
  const std::vector<int> expectedOperations{1, 2};

  auto function = [&](qi::context::ContextPtr) -> qi::Future<void>
  {
    operations.push_back(1);
    return qi::Future<void>(nullptr);
  };

  auto action = qi::semantic::agent::makeQiAction(_client, "dummy", function);

  auto continuation = [&](qi::Future<void> f)
  {
    EXPECT_TRUE(f.hasValue());
    operations.push_back(2);
    return qi::Future<void>(nullptr);
  };

  auto continuedAction = qi::semantic::agent::then(_client, action, continuation);
  EXPECT_EQ(action->name.get().value(), continuedAction->name.get().value());

  continuedAction->context.set(_contextObject).value();

  auto starting = waitForSignal(continuedAction->started);
  ASSERT_NO_THROW(continuedAction->run()); // the exception type may be altered
  EXPECT_EQ(qi::FutureState_FinishedWithValue, starting.wait(0));

  EXPECT_EQ(expectedOperations, operations);
}

