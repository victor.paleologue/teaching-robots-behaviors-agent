#include <future>
#include <gmock/gmock.h>
#include <boost/algorithm/string.hpp>
#include <qi/future.hpp>
#include <qi/type/dynamicobjectbuilder.hpp>
#include <qi/semantic/agent/gmock/agent.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include "../src/behaviormodel/action_pool.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "../src/utility/owner.hpp"
#include "../src/utility/semantic_helpers.hpp"
#include "test_pddl.hpp"
#include "debug_log_enabler.hpp"
#include "utility.hpp"

using namespace qi::semantic::agent;

class ActionPoolTest: public testing::Test
{
protected:
  DebugLogEnabler _actionDebug{"SemanticAgent.ActionPool"};
  DebugLogEnabler _pddlDebug{"SemanticAgent.PDDL"};
};

TEST_F(ActionPoolTest, ActionMadeFromFunctionRunsAndFinishesProperly)
{
  auto mockFactory = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock>();

  // This action only accepts cooked food and makes it eaten by the performing agent.
  mockFactory->pddl.set(actionPddl1).value();

  ActionPool pool;
  EXPECT_TRUE(pool.pddl.get().value().domain.empty());
  auto owner = pool.addFactory(mockFactory).value();
  EXPECT_FALSE(pool.pddl.get().value().domain.empty());
  owner->release();
  EXPECT_TRUE(pool.pddl.get().value().domain.empty())
      << "Found PDDL data: \"" << pool.pddl.get().value().domain << "\"";
}


TEST_F(ActionPoolTest, PoolOnlyAcceptActualFactories)
{
  qi::DynamicObjectBuilder dob;
  auto notAFactory = dob.object();
  ActionPool pool;
  EXPECT_TRUE(pool.pddl.get().value().domain.empty());
  EXPECT_ANY_THROW(pool.addFactory(notAFactory).value());
  EXPECT_TRUE(pool.pddl.get().value().domain.empty());
}


TEST_F(ActionPoolTest, DomainIsUpdatedWhenBehaviorSuggestsAction)
{
  auto mockBehavior = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiBehaviorGMock>();
  ActionPool pool;
  auto owner = pool.addBehavior(mockBehavior).value();
  EXPECT_TRUE(pool.pddl.get().value().domain.empty());

  // Suggesting an action.
  auto mockAction = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionGMock>();
  mockAction->pddl.set(actionPddl1).value();
  auto updatingDomain = waitForSignal(pool.pddl);
  mockBehavior->actionSuggestions.set({qi::AnyObject(mockAction)}).value();

  // The domain should be updated with one action.
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingDomain.waitFor(usualTimeout))
      << futureToString(updatingDomain);
  auto pddl = std::get<0>(updatingDomain.value());
  EXPECT_FALSE(pddl.domain.empty());

  auto description = qi::semantic::agent::parsePddlDomainAndProblem(
        pddl.domain + pddl.problem);
  EXPECT_EQ(1u, description->domain->actions.size());
  EXPECT_FALSE(description->problem);

  // Then release, there should be no action remaining in the domain.
  owner->release();
  pddl = pool.pddl.get().value();
  EXPECT_TRUE(pddl.domain.empty())
      << "Found PDDL data: \"" << pddl.domain << "\"";
}


TEST_F(ActionPoolTest, ProblemIsNotGeneratedForNonParametricActionSuggestion)
{
  auto mockBehavior = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiBehaviorGMock>();
  ActionPool pool;
  auto owner = pool.addBehavior(mockBehavior).value();
  EXPECT_TRUE(pool.pddl.get().value().domain.empty());

  // Suggesting an action.
  auto mockAction = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionGMock>();
  mockAction->pddl.set(actionPddl3).value();
  auto updatingPddl = waitForSignal(pool.pddl);
  mockBehavior->actionSuggestions.set({qi::AnyObject(mockAction)}).value();

  // The domain should be updated with one action.
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingPddl.waitFor(usualTimeout))
      << futureToString(updatingPddl);
  auto pddl = std::get<0>(updatingPddl.value());
  EXPECT_FALSE(pddl.domain.empty());

  auto description = qi::semantic::agent::parsePddlDomainAndProblem(pddl.domain + pddl.problem);
  EXPECT_EQ(1u, description->domain->actions.size());
  ASSERT_FALSE(description->problem);

  // Then release, there should be no action remaining in the domain.
  owner->release();
  pddl = pool.pddl.get().value();
  EXPECT_TRUE(pddl.domain.empty())
      << "Found PDDL data: \"" << pddl.domain << "\"";
}


TEST_F(ActionPoolTest, DomainIsUpdatedWhenProblemSourceIsAddedAndRemoved)
{
  ActionPool pool;

  auto mockProblemSource =
      std::make_shared<StoredProp<std::shared_ptr<const pddl::ast::Description>>>();
  auto problem = parsePddlDomainAndProblem(problem1);
  mockProblemSource->set(std::move(problem));

  auto updatingDomain = waitForSignal(pool.pddl);
  auto owner = pool.addProblemSource(mockProblemSource).value();

  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingDomain.waitFor(usualTimeout))
      << futureToString(updatingDomain);
  auto pddlDomain = std::get<0>(updatingDomain.value());
  EXPECT_FALSE(pddlDomain.problem.empty());

  updatingDomain = waitForSignal(pool.pddl);
  owner->release();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingDomain.waitFor(usualTimeout))
      << futureToString(updatingDomain);
  pddlDomain = std::get<0>(updatingDomain.value());
  EXPECT_TRUE(pddlDomain.problem.empty());
}


TEST_F(ActionPoolTest, DomainIsUpdatedWhenProblemSourceChanges)
{
  ActionPool pool;

  auto mockProblemSource =
      std::make_shared<StoredProp<std::shared_ptr<const pddl::ast::Description>>>();
  auto problem = parsePddlDomainAndProblem(problem1);

  auto updatingDomain = waitForSignal(pool.pddl);
  auto owner = pool.addProblemSource(mockProblemSource).value();

  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingDomain.waitFor(usualTimeout))
      << futureToString(updatingDomain);
  auto pddlDomain = std::get<0>(updatingDomain.value());
  EXPECT_TRUE(pddlDomain.problem.empty());

  updatingDomain = waitForSignal(pool.pddl);
  mockProblemSource->set(std::move(problem));
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingDomain.waitFor(usualTimeout))
      << futureToString(updatingDomain);
  pddlDomain = std::get<0>(updatingDomain.value());
  EXPECT_FALSE(pddlDomain.problem.empty());
}


TEST_F(ActionPoolTest, MergesDomainWithSeparateProblem)
{
  ActionPool pool;

  auto mockFactory = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock>();
  mockFactory->pddl.set(actionPddl1).value();

  auto updatingPDDL = waitForSignal(pool.pddl);
  auto factoryOwner = pool.addFactory(mockFactory).value();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingPDDL.waitFor(usualTimeout))
      << futureToString(updatingPDDL);
  auto pddl = std::get<0>(updatingPDDL.value());
  EXPECT_FALSE(pddl.domain.empty());
  EXPECT_TRUE(pddl.problem.empty());

  auto mockProblemSource =
      std::make_shared<StoredProp<std::shared_ptr<const pddl::ast::Description>>>();
  auto problem = parsePddlDomainAndProblem(problem1);
  mockProblemSource->set(std::move(problem));

  updatingPDDL = waitForSignal(pool.pddl);
  auto problemOwner = pool.addProblemSource(mockProblemSource).value();

  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingPDDL.waitFor(usualTimeout))
      << futureToString(updatingPDDL);
  pddl = std::get<0>(updatingPDDL.value());
  EXPECT_FALSE(pddl.domain.empty());
  EXPECT_FALSE(pddl.problem.empty());
}


TEST_F(ActionPoolTest, GatheringSemanticTemplate)
{
  ActionPool pool;

  auto mockFactory = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock>();
  mockFactory->pddl.set(actionPddl4).value();
  std::map<std::string, SemanticTemplate> semanticTemplates;
  semanticTemplates["en"] = semanticTemplate4;
  mockFactory->semanticTemplates.set(semanticTemplates);

  auto updatingPDDL = waitForSignal(pool.pddl);
  auto factoryOwner = pool.addFactory(mockFactory).value();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingPDDL.waitFor(usualTimeout))
      << futureToString(updatingPDDL);
  auto pddl = std::get<0>(updatingPDDL.value());
  EXPECT_FALSE(pddl.domain.empty());
  EXPECT_TRUE(pddl.problem.empty());

  auto semanticContext = qi::semantic::agent::makeSemanticMemoryContext();
  auto semanticExpression =
      qi::semantic::agent::phraseToSemanticExpression(*semanticContext, englishPhrase("locate the leek"));
  auto foundFactories = pool.findActionFactory(semanticExpression, englishLocale).value();
  ASSERT_EQ(1u, foundFactories.size());
  const auto& foundFactory = foundFactories[0];
  const auto& objectExpression = foundFactory.semanticSlots.at({"objectToLocate"});
  const auto objectPhrase = qi::semantic::agent::phraseFromSemanticExpression(
        *semanticContext, *objectExpression, englishLocale);
  // This may fail because of https://gitlab.aldebaran.lan/naoqi/mind/issues/24
  EXPECT_EQ("the leek", boost::algorithm::to_lower_copy(objectPhrase.text));
}
