#include <chrono>
#include <gtest/gtest.h>
#include <qi/jsoncodec.hpp>
#include "../src/utility/propertyset.hpp"

using namespace qi::semantic::agent;

TEST(PropertySetTest, EmptyAtInitialization)
{
  StoredPropertySet<int> propEdit;
  PropertySet<int>& prop = propEdit;
  EXPECT_EQ(0u, prop.get().size());
}


TEST(PropertySetTest, InsertAbsent)
{
  StoredPropertySet<int> propEdit;
  PropertySet<int>& prop = propEdit;
  prop.connect([](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(1u, change.value.size());
    EXPECT_EQ(1u, change.inserted.size());
    EXPECT_EQ(0u, change.removed.size());
  });
  propEdit.insert(42);
}


TEST(PropertySetTest, InsertExisting)
{
  StoredPropertySet<int> propEdit;
  propEdit.insert(42);

  PropertySet<int>& prop = propEdit;
  prop.connect([](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.insert(42);
}


TEST(PropertySetTest, RemoveExisting)
{
  StoredPropertySet<int> propEdit;
  propEdit.insert(42);

  PropertySet<int>& prop = propEdit;
  prop.connect([](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(0u, change.value.size());
    EXPECT_EQ(0u, change.inserted.size());
    EXPECT_EQ(1u, change.removed.size());
  });
  propEdit.remove(42);
}


TEST(PropertySetTest, RemoveAbsent)
{
  StoredPropertySet<int> propEdit;
  PropertySet<int>& prop = propEdit;
  prop.connect([](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.remove(42);
}


TEST(PropertySetTest, SetFromEmpty)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(set, change.value);
    EXPECT_EQ(set.size(), change.inserted.size());
    EXPECT_EQ(0u, change.removed.size());
  });
  propEdit.set(set);
}


TEST(PropertySetTest, SetToEmpty)
{
  StoredPropertySet<int> propEdit;
  auto set = std::set<int>{1, 2, 3};
  propEdit.set(set);

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(0u, change.value.size());
    EXPECT_EQ(0u, change.inserted.size());
    EXPECT_EQ(set.size(), change.removed.size());
  });
  propEdit.set(std::set<int>{});
}


TEST(PropertySetTest, SetIntersecting)
{
  StoredPropertySet<int> propEdit;
  auto set1 = std::set<int>{1, 2, 3};
  auto set2 = std::set<int>{2, 3, 4};
  propEdit.set(set1);

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(set2, change.value);
    EXPECT_EQ(1u, change.inserted.size());
    EXPECT_EQ(1u, change.removed.size());
  });
  propEdit.set(set2);
}


TEST(PropertySetTest, SetNotIntersecting)
{
  StoredPropertySet<int> propEdit;
  auto set1 = std::set<int>{1, 2, 3};
  auto set2 = std::set<int>{4, 5, 6};
  propEdit.set(set1);

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(set2, change.value);
    EXPECT_EQ(set2.size(), change.inserted.size());
    EXPECT_EQ(set1.size(), change.removed.size());
  });
  propEdit.set(set2);
}


TEST(PropertySetTest, SetIdentical)
{
  StoredPropertySet<int> propEdit;
  auto set1 = std::set<int>{1, 2, 3};
  propEdit.set(set1);

  PropertySet<int>& prop = propEdit;
  prop.connect([](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.set(set1);
}


TEST(PropertySetTest, Clear)
{
  StoredPropertySet<int> propEdit;
  auto set = std::set<int>{1, 2, 3};
  propEdit.set(set);

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(0u, change.value.size());
    EXPECT_EQ(0u, change.inserted.size());
    EXPECT_EQ(set.size(), change.removed.size());
  });
  propEdit.clear();
}


TEST(PropertySetTest, ClearEmpty)
{
  StoredPropertySet<int> propEdit;

  PropertySet<int>& prop = propEdit;
  prop.connect([](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.clear();
}


TEST(PropertySetTest, ApplyInsertsAndRemoves)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3};
  propEdit.set(set);

  const std::set<int> newSet{1, 3, 4};
  const std::set<int> inserted{*newSet.find(4)};
  const std::set<int> removed{*set.find(2)};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(newSet.size(), change.value.size());
    EXPECT_EQ(inserted.size(), change.inserted.size());
    EXPECT_EQ(removed.size(), change.removed.size());
  });
  propEdit.applyDelta(inserted, removed);
}


TEST(PropertySetTest, ApplyInsertsAndRemovesOnlyWhenNeeded)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3, 4, 5};
  propEdit.set(set);

  const std::set<int> newSet{1, 3, 6, 7};
  const std::set<int> inserted{*set.find(4), *newSet.find(6)};
  const std::set<int> removed{*set.find(2), *newSet.find(7)};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(set.size(), change.value.size());
    EXPECT_EQ(inserted.size() - 1, change.inserted.size());
    EXPECT_EQ(removed.size() - 1, change.removed.size());
  });
  propEdit.applyDelta(inserted, removed);
}


TEST(PropertySetTest, ApplyInsertsAndRemovesOnlyWhenNeededUpToDoingNothing)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3, 4, 5};
  propEdit.set(set);

  const std::set<int> newSet{1, 3, 6, 7};
  const std::set<int> inserted{*set.find(4)};
  const std::set<int> removed{*newSet.find(6)};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.applyDelta(inserted, removed);
}


TEST(PropertySetTest, ApplyRemovesConflictingChange)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3};
  propEdit.set(set);

  const std::set<int> inserted{*set.find(1)};
  const std::set<int> removed{*set.find(1)};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>& change)
  {
    EXPECT_EQ(set.size() - 1, change.value.size());
    EXPECT_EQ(0u, change.inserted.size());
    EXPECT_EQ(1u, change.removed.size());
  });
  propEdit.applyDelta(inserted, removed);
}


TEST(PropertySetTest, ApplyNoopConflictingChangeIfAbsent)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3};
  propEdit.set(set);

  const std::set<int> newSet{4};
  const std::set<int> inserted{*newSet.find(4)};
  const std::set<int> removed{*newSet.find(4)};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.applyDelta(inserted, removed);
}


TEST(PropertySetTest, ApplyNothingDoesNothing)
{
  StoredPropertySet<int> propEdit;
  const std::set<int> set{1, 2, 3};
  propEdit.set(set);

  const std::set<int> inserted{};
  const std::set<int> removed{};

  PropertySet<int>& prop = propEdit;
  prop.connect([&](const qi::semantic::agent::SetChange<int>&)
  {
    FAIL();
  });
  propEdit.applyDelta(inserted, removed);
}


TEST(PropertySetTest, ApplyIsFasterThanSet)
{
  const int nofIterations = 1000;

  const std::set<int> set{1, 2, 3};
  const std::set<int> newSet{1, 3, 4};
  StoredPropertySet<int> propEdit;
  propEdit.set(set);

  const auto startSet = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < nofIterations; ++i)
  {
    propEdit.set(newSet);
    propEdit.set(set);
  }
  const auto endSet = std::chrono::high_resolution_clock::now();
  const auto setDuration = endSet - startSet;

  const std::set<int> inserted{*newSet.find(4)};
  const std::set<int> removed{*set.find(2)};
  const std::set<int> insertedReturn{*set.find(2)};
  const std::set<int> removedReturn{*newSet.find(4)};

  const auto startApply = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < nofIterations; ++i)
  {
    propEdit.applyDelta(inserted, removed);
    propEdit.applyDelta(insertedReturn, removedReturn);
  }
  const auto endApply = std::chrono::high_resolution_clock::now();
  const auto applyDuration = endApply - startApply;

  EXPECT_LE(applyDuration, setDuration)
      << "Set: " << std::chrono::duration_cast<std::chrono::microseconds>(setDuration).count() << " µs" << std::endl
      << "Apply: " << std::chrono::duration_cast<std::chrono::microseconds>(applyDuration).count() << " µs";
}


template <typename C1, typename C2>
testing::AssertionResult match(
    const C1& lhs,
    const C2& rhs)
{
  auto itPair = std::mismatch(lhs.begin(), lhs.end(), rhs.begin());
  if (itPair.first == lhs.end())
  {
    return testing::AssertionSuccess();
  }
  else
  {
    return testing::AssertionFailure()
        << qi::encodeJSON(lhs) << std::endl
        << " vs. " << qi::encodeJSON(rhs);
  }
}


TEST(ChangeTracker, trackChanges)
{
  const std::vector<int> nothing{};
  const std::vector<int> before{1, 2, 3};
  const std::vector<int> after{4, 5, 6};
  ChangeTracker<int> changeTracker;

  auto change = changeTracker.check(before);
  EXPECT_TRUE(match(before, change.inserted));
  EXPECT_TRUE(match(nothing, change.removed));
  EXPECT_TRUE(match(before, change.value));

  change = changeTracker.check(after);
  EXPECT_TRUE(match(after, change.inserted));
  EXPECT_TRUE(match(before, change.removed));
  EXPECT_TRUE(match(after, change.value));

  change = changeTracker.check(nothing);
  EXPECT_TRUE(match(nothing, change.inserted));
  EXPECT_TRUE(match(after, change.removed));
  EXPECT_TRUE(match(nothing, change.value));
}
