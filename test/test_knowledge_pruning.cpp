#include <gmock/gmock.h>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <qi/anymodule.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include <qi/jsoncodec.hpp>
#include "../src/utility/knowledge.hpp"
#include "../src/knowledge_pruning.hpp"
#include "debug_log_enabler.hpp"
#include "test_agent.hpp"
#include "utility.hpp"

qiLogCategory("Test.KnowledgePruning");

using namespace qi::semantic::agent;
using namespace qi::knowledge;
using namespace qi::teaching;

class KnowledgePruningTest: public ::testing::Test
{
protected:
  KnowledgePruningTest()
    : _debugLogEnabler("SemanticAgent.KnowledgePruning")
    , _knowledgeDebugLogEnabler("knowledge")
    , _rdfDebugLogEnabler("RDFEngine")
    , _knowledge(qi::import("knowledge_module").call<EditableKnowledgeBasePtr>(
                   "EditableKnowledgeBase", qi::os::mktmpdir("KnowledgeTest"), true))
    , _beliefs(_knowledge->editableKnowledgeGraph(beliefsDomain()))
  {
    _beliefs->timestampEnabled.set(false).value();
  }

  DebugLogEnabler _debugLogEnabler;
  DebugLogEnabler _knowledgeDebugLogEnabler;
  DebugLogEnabler _rdfDebugLogEnabler;
  EditableKnowledgeBasePtr _knowledge;
  EditableKnowledgeGraphPtr _beliefs;
};


TEST_F(KnowledgePruningTest, pruneOldCommunicationActsFromActualData1)
{
  auto dumpPath = qi::path::findData("SemanticAgent", "test_data/knowledge_content_1.json");
  QI_ASSERT(!dumpPath.empty());

  loadKnowledgeFromFile(dumpPath, _beliefs).value();

  auto pruning = pruneOldCommunicationActs(_knowledge);
  auto status = pruning.waitFor(largeTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status)
      << futureToString(pruning) << std::endl
      << "Knowledge:" << std::endl
      << dumpKnowledgeToJSON(_knowledge->knowledgeBase()).value();

  auto db = dumpKnowledge(_knowledge->knowledgeBase()).value();
  EXPECT_TRUE(db.empty())
      << dumpKnowledgeToJSON(_knowledge->knowledgeBase()).value();
}


TEST_F(KnowledgePruningTest, pruneOldCommunicationActsFromActualData3)
{
  auto dumpPath = qi::path::findData("SemanticAgent", "test_data/knowledge_content_3.json");
  QI_ASSERT(!dumpPath.empty());

  loadKnowledgeFromFile(dumpPath, _beliefs).value();

  auto pruning = pruneOldCommunicationActs(_knowledge);
  auto status = pruning.waitFor(largeTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status)
      << futureToString(pruning) << std::endl
      << "Knowledge:" << std::endl
      << dumpKnowledgeToJSON(_knowledge->knowledgeBase()).value();

  auto partialTriple = makeTripleFromNodes(
        makeEmptyResourceNode(),
        occurredAtResource(),
        makeEmptyNode());

  auto triples = _knowledge->knowledgeBase()->queryTriples(partialTriple, "");
  EXPECT_TRUE(triples.empty()) << triples;
}


TEST_F(KnowledgePruningTest, doNotPruneFreshCommunicationActs1)
{
  auto dumpPath = qi::path::findData("SemanticAgent", "test_data/fresh_communication_acts_1.json");
  QI_ASSERT(!dumpPath.empty());

  loadKnowledgeFromFile(dumpPath, _beliefs).value();

  auto wildcardTriple = makeTripleFromNodes(
        makeEmptyResourceNode(),
        occurredAtResource(),
        makeEmptyNode());
  auto initialTriples = _knowledge->knowledgeBase()->queryTriples(wildcardTriple, "");

  boost::posix_time::ptime now =
      boost::posix_time::from_iso_extended_string("2019-09-27T07:30:00.000");
  auto pruning = pruneOldCommunicationActs(_knowledge, [&]{ return now; });
  auto status = pruning.waitFor(largeTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status)
      << futureToString(pruning) << std::endl
      << "Knowledge:" << std::endl
      << dumpKnowledgeToJSON(_knowledge->knowledgeBase()).value();


  auto finalTriples = _knowledge->knowledgeBase()->queryTriples(wildcardTriple, "");
  EXPECT_EQ(initialTriples, finalTriples) << finalTriples;
}
