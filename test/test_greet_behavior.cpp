#include <gtest/gtest.h>
#include <future>

#include <qi/path.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qi/teaching/ontology.hpp>

#include <qi/actuation/gmock/actuation.hpp>
#include <qi/actuation/gmock/frame.hpp>
#include <qi/human/gmock/human.hpp>
#include <qi/humanawareness/gmock/humanawareness.hpp>

#include <qisem/linguisticanalyzer/linguisticanalyzer.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/semanticmodel/utility/noresult.hpp>
#include <qisem/semanticmodel/enum/semanticsourceenum.hpp>

#include "../src/behaviors/greet_behavior.hpp"
#include "../src/utility/knowledge.hpp"

#include "test_semantic_behaviors.hpp"
#include "utility.hpp"

qiLogCategory("SemanticAgent.Test.GreetBehavior");


using namespace qisem;
using namespace qi::human;
using namespace qi::knowledge;
using namespace qi::semantic::agent;
using testing::_;
using testing::Return;


/// Make a dummy human.
HumanPtr makeDummyHuman()
{
  return qi::AnyObject(boost::make_shared<qi::human::gmock::HumanGMock>());
}


class GreetBehaviorTest : public SemanticBehaviorsTest
{
protected:
  void SetUp()
  {
    _humanManager = std::make_shared<HumanManagerGMock>();
    _actuation = boost::make_shared<qi::actuation::gmock::ActuationGMock>();
    _server->registerService("Actuation", qi::AnyObject(_actuation)).value();
  }

  void TearDown()
  {
    _humanAwareness.reset();
  }


  /// Makes a new (dummy) human, declares it engaged, and returns it.
  HumanPtr engageDummyHuman()
  {
    auto human = makeDummyHuman();
    _humanAwareness->engagedHuman.set(human).value();
    return human;
  }

  std::string getGreetFeedbackResponse(const PhraseAndLocale& input)
  {
    auto semExp = phraseToSemanticExpression(
          *_semanticMemoryContext, input, SemanticDirection::EngagedUserToRobot);

    // get response from greetFeedback()
    auto semResponse =
        greetFeedback(
          input, _client, _contextObject, _semanticMemoryContext,
          semExp, _humanManager, _knowledge, _beliefs).value();

    if (semResponse && !semResponse->isEmpty())
    {
      auto phraseResponse =
          phraseFromSemanticExpression(
            *_semanticMemoryContext, *semResponse, input.locale,
            SemanticDirection::RobotToEngagedUser);
      return phraseResponse.text;
    }

    return constant::noResult;
  }

  std::shared_ptr<HumanManagerGMock> _humanManager;
  boost::shared_ptr<qi::actuation::gmock::ActuationGMock> _actuation;
};


TEST_F(GreetBehaviorTest, RespondToHelloPepper)
{
  auto socialAgentNode = qi::teaching::makeUniqueResource(qi::teaching::socialAgentPrefix());
  EXPECT_CALL(*_humanManager, getResNode(_))
      .WillRepeatedly(testing::Return(socialAgentNode));

  EXPECT_CALL(*_humanManager, getName(_))
      .WillRepeatedly(testing::Return(std::string()));

  auto response = getGreetFeedbackResponse(englishPhrase("Hello"));
  qiLogInfo() << response;
  ASSERT_NE(std::string(), response);
}


TEST_F(GreetBehaviorTest, NoReplyToUnexpectedPhrase)
{
  auto graph = _knowledge->editableKnowledgeGraph("human_graph");
  auto behavior = makeGreetBehavior(
        _client, _contextObject, _speechSourceRemote, _speechEngineObj,
        _semanticMemoryContext, _knowledge, graph, "", _humanManager).value();

  engageDummyHuman();
  expectNoResponseForUtterances(
        behavior,
        std::vector<std::string>{"plop"},
        qi::locale::Language::English);
}


TEST_F(GreetBehaviorTest, RespondToHello)
{
  auto graph = _knowledge->editableKnowledgeGraph("human_graph");
  auto behavior = makeGreetBehavior(
        _client, _contextObject, _speechSourceRemote, _speechEngineObj,
        _semanticMemoryContext, _knowledge, graph, "", _humanManager).value();

  engageDummyHuman();
  expectResponseForEachUtterance(
        behavior,
        std::vector<std::string>{"hello"},
        qi::locale::Language::English);
  // TODO: check the content of the response?
}


TEST_F(GreetBehaviorTest, AskNameNormalResponse)
{
  auto graph = _knowledge->editableKnowledgeGraph("human_graph");
  auto behavior = makeGreetBehavior(
        _client,
        _contextObject,
        _speechSourceRemote,
        _speechEngineObj,
        _semanticMemoryContext,
        _knowledge,
        graph,
        "",
        _humanManager).value();

  // Somebody with no name faces the robot
  auto human = engageDummyHuman();
  auto humanFrame = boost::make_shared<qi::actuation::gmock::FrameGMock>();
  human->headFrame.set(qi::AnyObject(humanFrame)).value();
  auto socialAgentNode = qi::teaching::makeUniqueResource(qi::teaching::socialAgentPrefix());
  EXPECT_CALL(*_humanManager, getResNode(_))
      .WillRepeatedly(testing::Return(socialAgentNode));
  EXPECT_CALL(*_humanManager, getName(_))
      .WillRepeatedly(testing::Return(std::string()));

  // "Pierre" says hello
  auto response = getGreetFeedbackResponse(englishPhrase("Hello"));
  EXPECT_EQ("Hello, what is your name?", response);

  response = getGreetFeedbackResponse(englishPhrase("My name is Pierre"));
  EXPECT_EQ("Nice to meet you Pierre", response);
  // TODO: check the human is named in the knowledge, and that the name can be retrieved
}


TEST_F(GreetBehaviorTest, AskNameStrangeResponse)
{
  // Somebody faces the robot
  engageDummyHuman();
  auto socialAgentNode = qi::teaching::makeUniqueResource(qi::teaching::socialAgentPrefix());
  EXPECT_CALL(*_humanManager, getResNode(_))
      .WillRepeatedly(testing::Return(socialAgentNode));

  // "Pierre" says hello
  auto response = getGreetFeedbackResponse(englishPhrase("Hello"));
  EXPECT_EQ("Hello, what is your name?", response);

  // "Pierre" replies something unexpected.
  response = getGreetFeedbackResponse(englishPhrase("Where is the kitchen"));
  EXPECT_EQ(constant::noResult, response);
}


TEST_F(GreetBehaviorTest, GreetNewHuman)
{
  auto graph = _knowledge->editableKnowledgeGraph("human_graph");
  auto behavior = makeGreetBehavior(
        _client,
        _contextObject,
        _speechSourceRemote,
        _speechEngineObj,
        _semanticMemoryContext,
        _knowledge,
        graph,
        "",
        _humanManager).value();

  // check the behavior suggests an action
  auto waitingForSuggestion = waitForSignal(behavior->actionSuggestions);

  // associate a human node
  auto socialAgentNode = qi::teaching::makeUniqueResource(qi::teaching::socialAgentPrefix());
  EXPECT_CALL(*_humanManager, getResNode(_))
      .WillRepeatedly(testing::Return(socialAgentNode));

  // human was last seen yesterday
  auto timestamp = makeNow() - boost::posix_time::hours(24l);
  EXPECT_CALL(*_humanManager, getTimestamp(_, _))
      .WillRepeatedly(testing::Return(timestamp));

  // here is a human
  auto human = makeDummyHuman();

  // we do not know human's name
  EXPECT_CALL(*_humanManager, getName(_))
      .WillRepeatedly(testing::Return(std::string()));

  // define a distance to this human
  auto humanFrame = boost::make_shared<qi::actuation::gmock::FrameGMock>();
  human->headFrame.set(qi::AnyObject(humanFrame)).value();
  auto transformToHuman = qi::geometry::TransformTime{{{}, {1, 0, 0}}, qi::Clock::now()};
  EXPECT_CALL(*humanFrame, computeTransform(_))
      .WillRepeatedly(Return(qi::Future<qi::geometry::TransformTime>(transformToHuman)));
  auto robotFrame = boost::make_shared<qi::actuation::gmock::FrameGMock>();
  EXPECT_CALL(*_actuation, robotFrame())
      .WillRepeatedly(Return(qi::Future<qi::actuation::FramePtr>(qi::AnyObject(robotFrame))));

  // the human engages
  _humanAwareness->engagedHuman.set(human).value();

  // the robot pro-actively suggests a greeting if the human is close enough
  auto status = waitingForSuggestion.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  auto suggestions = std::get<0>(waitingForSuggestion.value());
  ASSERT_FALSE(suggestions.empty());

  // run the action, and check it said something, and stops by itself
  waitingForSuggestion = waitForSignal(behavior->actionSuggestions);
  suggestions[0]->run();

  // then, check there is no suggestion left
  status = waitingForSuggestion.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  suggestions = std::get<0>(waitingForSuggestion.value());
  ASSERT_TRUE(suggestions.empty());
}


TEST_F(GreetBehaviorTest, HumanGivesHisName)
{
  auto graph = _knowledge->editableKnowledgeGraph("human_graph");
  auto behavior = makeGreetBehavior(
        _client,
        _contextObject,
        _speechSourceRemote,
        _speechEngineObj,
        _semanticMemoryContext,
        _knowledge,
        graph,
        "",
        _humanManager).value();

  // set a new engaged human
  engageDummyHuman();
}


// + test human is already known, greeting is different
//   - (tip: use a mock-up okao to simulate face reco results, using EXPECT_CALL(...).WillOnce(...))
//   - human corrects the robot "I'm not machin"
//   - human seems ok
//   - human talks about something else, do not assume anything?
// + test human was already seen today
// + test human may be known, but it is uncertain

// a human comes, a greeting suggestion happens, but it is not run
// then the human says hello, the robot greets,
// the previous greeting suggestion is dropped.
// => greeting is goal-oriented

// Files should be saved in qi::path::userWritableDataPath("SemanticAgent", "faces.whatever")

