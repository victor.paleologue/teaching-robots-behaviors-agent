#include <gtest/gtest.h>
#include <future>
#include <boost/filesystem.hpp>
#include <qi/anymodule.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "../src/knowledge_semantic.hpp"
#include "../src/utility/knowledge.hpp"
#include "test_semantic_behaviors.hpp"
#include "utility.hpp"


qiLogCategory("Test.KnowledgeSemantic");


using namespace qi::semantic::agent;
using namespace qi::knowledge;
using namespace qi::teaching;


class KnowledgeSemanticTest: public SemanticBehaviorsTest
{
protected:
  KnowledgeSemanticTest()
    : SemanticBehaviorsTest()
    , _debugLogEnabler("SemanticAgent.KnowledgeSemantic")
    , _knowledgeSemanticBridge(_knowledge->knowledgeBase(), _semanticMemoryContext)
  {}

  DebugLogEnabler _debugLogEnabler;
  KnowledgeSemanticBridge _knowledgeSemanticBridge;
};


TEST_F(KnowledgeSemanticTest, RunCancelRerun)
{
  auto running = _knowledgeSemanticBridge.run();
  ASSERT_EQ(qi::FutureState_Running, running.waitFor(usualTimeout));
  running.cancel();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, running.waitFor(usualTimeout));

  running = _knowledgeSemanticBridge.run();
  ASSERT_EQ(qi::FutureState_Running, running.waitFor(usualTimeout));
  running.cancel();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, running.waitFor(usualTimeout));
}


TEST_F(KnowledgeSemanticTest, FindExistingObjectBySemanticLabel)
{
  auto objectResource = makeUniqueResource(objectPrefix());
  auto objectLabel = makeLocalizedStringNode("une boulette", frenchLocale);
  std::vector<Triple> triples = {
    makeTripleFromNodes(objectResource, rdfTypeResource(), objectTypeNode()),
    makeTripleFromNodes(objectResource, rdfsLabelResource(), objectLabel)};

  addTriples(_beliefs, triples);
  auto updating = waitForSignal(_knowledgeSemanticBridge.updated);
  auto running = _knowledgeSemanticBridge.run();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updating.waitFor(usualTimeout));
  ASSERT_EQ(qi::FutureState_Running, running.wait(0));

  auto semanticOf = [&](const std::string& phrase)
  {
    return phraseToSemanticExpression(*_semanticMemoryContext, {phrase, frenchLocale});
  };

  std::vector<std::shared_ptr<qisem::SemanticExpression>> objectExpressions = {
    semanticOf("la boulette"), semanticOf("une boulette"),
    semanticOf("d'une boulette"), semanticOf("sur la boulette"),
    semanticOf("dans la boulette")};

  for (auto objectExpression: objectExpressions)
  {
    EXPECT_EQ(
          objectResource,
          _knowledgeSemanticBridge.findResourceFromSemanticLabel(
            objectExpression, frenchLocale).value());
  }

  updating = waitForSignal(_knowledgeSemanticBridge.updated);
  removeTriples(_beliefs, triples);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updating.waitFor(usualTimeout));
}
