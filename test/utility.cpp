#include "utility.hpp"
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/signals2/signal.hpp>
#include <qi/assert.hpp>
#include <qi/log.hpp>


qiLogCategory("SemanticAgent.Test.Utility");


std::string generateUuid()
{
  static boost::uuids::random_generator genUuid;
  return boost::uuids::to_string(genUuid());
}


qi::Future<void> waitForSignal(boost::signals2::signal<void()>& sig)
{
  auto data = std::make_shared<SignalWaitData<void>>();
  data->connection =
      sig.connect([data]()
  {
    data->connection.disconnect();
    data->promise.setValue(nullptr);
  });

  return data->promise.future();
}


qi::Future<void> waitForSignal(qi::SignalF<void()>& signal)
{
  auto link = std::make_shared<qi::SignalLink>(qi::SignalBase::invalidSignalLink);
  std::function<void()> disconnect = [&, link] {
    if (*link != qi::SignalBase::invalidSignalLink)
      signal.disconnect(*link);
  };

  qi::Promise<void> promise(
        [&, link](qi::Promise<void> p) {
    disconnect();
    p.setCanceled();
  });

  *link = signal.connect([=]() mutable {
    disconnect();
    promise.setValue(nullptr);
  });

  return promise.future();
}


//=============================================================================
// Copy Tracker
//=============================================================================
struct CopyTracker::Private
{
  std::promise<void> instanceDestroyed;
  std::shared_future<void> instanceDestroyedFuture = instanceDestroyed.get_future();
  std::string instanceId;

  std::shared_ptr<std::promise<void>> chainDestroyed;
  std::shared_ptr<std::shared_future<void>> chainDestroyedFuture;
  std::shared_ptr<std::string> chainId;

  void computeInstanceId()
  {
    std::stringstream ss;
    ss << *chainId << " (" << this << ")";
    instanceId = ss.str();
  }

  void setInstanceDestroyed()
  {
    if (!chainDestroyed) // happens if the instance was moved from
      return;
    instanceDestroyed.set_value();
    qiLogDebug() << instanceId << " destroyed";
  }
};


CopyTracker::CopyTracker()
  : _p(std::make_unique<Private>())
{
  _p->chainDestroyed = std::shared_ptr<std::promise<void>>(
      new std::promise<void>(),
      [](std::promise<void>* p)
  {
    p->set_value();
    qiLogDebug() << static_cast<void*>(p) << " destroyed";
    delete p;
  });

  _p->chainDestroyedFuture =
      std::make_shared<std::shared_future<void>>(_p->chainDestroyed->get_future());

  {
    std::stringstream ss;
    ss << static_cast<void*>(_p->chainDestroyed.get());
    _p->chainId = std::make_shared<std::string>(ss.str());
  }

  _p->computeInstanceId();

  qiLogDebug() << *_p->chainId << " created";
  qiLogDebug() << _p->instanceId << " created";
}


CopyTracker::~CopyTracker()
{
  _p->setInstanceDestroyed();
}


CopyTracker::CopyTracker(const CopyTracker& other)
  : _p(std::make_unique<Private>())
{
  _p->chainDestroyed = other._p->chainDestroyed;
  _p->chainDestroyedFuture = other._p->chainDestroyedFuture;
  _p->chainId = other._p->chainId;
  _p->computeInstanceId();
  qiLogDebug() << _p->instanceId << " copied from " << other._p->instanceId;
}


CopyTracker& CopyTracker::operator=(const CopyTracker& other)
{
  _p->setInstanceDestroyed();
  _p->chainDestroyed = other._p->chainDestroyed;
  _p->chainDestroyedFuture = other._p->chainDestroyedFuture;
  _p->chainId = other._p->chainId;
  _p->computeInstanceId();
  qiLogDebug() << _p->instanceId << " copy-assigned from " << other._p->instanceId;
  return *this;
}


CopyTracker::CopyTracker(CopyTracker&& other)
  : _p(std::make_unique<Private>())
{
  _p->chainDestroyed = std::move(other._p->chainDestroyed);
  _p->chainDestroyedFuture = std::move(other._p->chainDestroyedFuture);
  _p->chainId = std::move(other._p->chainId);
  _p->computeInstanceId();
  qiLogDebug() << _p->instanceId << " moved from " << other._p->instanceId;
}


CopyTracker& CopyTracker::operator=(CopyTracker&& other)
{
  _p->setInstanceDestroyed();
  _p->chainDestroyed = std::move(other._p->chainDestroyed);
  _p->chainDestroyedFuture = std::move(other._p->chainDestroyedFuture);
  _p->chainId = std::move(other._p->chainId);
  _p->computeInstanceId();
  qiLogDebug() << _p->instanceId << " move-assigned from " << other._p->instanceId;
  return *this;
}


std::uint64_t CopyTracker::count()
{
  return static_cast<std::uint64_t>(_p->chainDestroyed.use_count());
}


std::shared_future<void> CopyTracker::destroyed()
{
  return _p->instanceDestroyedFuture;
}


std::shared_future<void> CopyTracker::chainDestroyed()
{
  return *_p->chainDestroyedFuture;
}

//=============================================================================

qi::AnyValue makeASREvent(const std::string& phrase)
{
  std::vector<qi::AnyValue> event;
  event.push_back(qi::AnyValue{phrase});
  event.push_back(qi::AnyValue{1.f});
  return qi::AnyValue{event};
}


qi::AnyValue makeTTSEvent(const std::string& phrase)
{
  return qi::AnyValue{phrase};
}


boost::shared_ptr<qi::conversation::gmock::SpeechEngineGMock> makeMockSpeechEngine(
    boost::shared_ptr<MemorySubscriberGMock> ttsSubscriber)
{
  auto speechEngine = boost::make_shared<qi::conversation::gmock::SpeechEngineNiceGMock>();
  auto weakSpeechEngine = boost::weak_ptr<qi::conversation::gmock::SpeechEngineNiceGMock>(speechEngine);

  auto makeSay1 = [=](const qi::conversation::Phrase& phrase)
  {
    auto say = boost::make_shared<qi::conversation::gmock::SayNiceGMock>();
    ON_CALL(*say, run())
        .WillByDefault(testing::Invoke([=]()
    {
      QI_EMIT ttsSubscriber->signal(makeTTSEvent(phrase.text));
      if (auto speechEngine = weakSpeechEngine.lock())
        QI_EMIT speechEngine->saying(phrase);
      return qi::Future<void>{nullptr};
    }));
    return qi::Future<qi::conversation::SayPtr>{qi::AnyObject{say}};
  };

  ON_CALL(*speechEngine, makeSay(testing::_))
      .WillByDefault(testing::Invoke(makeSay1));

  auto makeSay3 = [=](const qi::conversation::Phrase& phrase, qi::conversation::BodyLanguageOption, qi::locale::Locale)
  {
    return makeSay1(phrase);
  };

  ON_CALL(*speechEngine, makeSay(testing::_, testing::_, testing::_))
      .WillByDefault(testing::Invoke(makeSay3));

  return std::move(speechEngine);
}


qi::Future<void> runFunctionAndWaitForStarted(
    qi::Signal<>& started,
    std::function<qi::Future<void>()>&& function)
{
  auto startedSpy = waitForSignal(started);
  auto running = function();

  if (qi::FutureState_Running == startedSpy.waitFor(qiFallbackAwareTimeout))
  {
    std::stringstream ss;
    ss << "task took too long to start, task's state: " << futureToString(running);
    running.cancel();
    throw std::runtime_error(ss.str());
  }

  auto state = running.waitFor(qi::MilliSeconds{0});
  switch (state)
  {
  case qi::FutureState_None:
    QI_ASSERT(false && "task's future state can never be none");
    throw std::runtime_error{"task future state is none"};
  case qi::FutureState_Canceled:
    throw std::runtime_error{"task was canceled"};
  case qi::FutureState_FinishedWithValue:
    throw std::runtime_error{"task has already finished"};
  case qi::FutureState_FinishedWithError:
    throw std::runtime_error{std::string{"task failed: "} + running.error()};
  case qi::FutureState_Running:
    break; // good
  }

  return running;
}


boost::shared_ptr<qi::semantic::agent::gmock::SpeechExtractorNiceGMock> makeMockSpeechExtractor()
{
  auto uuid = generateUuid();
  auto speechExtractor = boost::make_shared<qi::semantic::agent::gmock::SpeechExtractorNiceGMock>();
  boost::weak_ptr<qi::semantic::agent::gmock::SpeechExtractorNiceGMock> weakSpeechExtractor = speechExtractor;
  ON_CALL(*speechExtractor, run())
      .WillByDefault(
        testing::Invoke([weakSpeechExtractor]()
  {
    auto speechExtractor = weakSpeechExtractor.lock();
    if (!speechExtractor)
      throw std::runtime_error("Dead speech extractor");

    QI_EMIT speechExtractor->started();

    qi::Promise<void> p{[weakSpeechExtractor](qi::Promise<void> p)
    {
      auto speechExtractor = weakSpeechExtractor.lock();
      if (!speechExtractor)
        return;
      QI_EMIT speechExtractor->stopped();
      p.setCanceled();
    }};

    auto f = p.future();
    ON_CALL(*speechExtractor, stop())
        .WillByDefault(testing::Invoke([f]() mutable { f.cancel(); return f; }));
    f.andThen([p](void*){});
    return f;
  }));

  ON_CALL(*speechExtractor, start())
      .WillByDefault(
        testing::Invoke([weakSpeechExtractor]
  {
    auto speechExtractor = weakSpeechExtractor.lock();
    if (!speechExtractor)
      throw std::runtime_error("Dead speech extractor");
    return speechExtractor->run();
  }));

  ON_CALL(*speechExtractor, _id())
      .WillByDefault(testing::Return(qi::Future<std::string>(uuid)));
  return speechExtractor;
}


const char* IntendedException::what() const noexcept
{
  return "intended exception";
}

const char* NotHeardException::what() const noexcept
{
  return "robot did not get the user input";
}


static std::string _computeDistortedHearingWhat(const char* expected, const char* observed)
{
  std::stringstream ss;
  ss << "user input was distorted (\"" << expected << "\" vs. \"" << observed << "\")";
  return ss.str();
}


DistortedHearingException::DistortedHearingException(const char* expected, const char* observed)
  : _what(_computeDistortedHearingWhat(expected, observed)) {}

const char* DistortedHearingException::what() const noexcept
{
  return _what.c_str();
}


const char* NoSpokenResponseException::what() const noexcept
{
  return "robot did not respond to user input";
}


FocusSetup::FocusSetup()
  : _strand(std::make_shared<qi::Strand>())
  , _server(qi::makeSession())
  , _client(qi::makeSession())
{
  _server->listenStandalone("tcp://127.0.0.1:0").value();
  _client->connect(_server->endpoints()[0]).value();

  // prepare a mock of the focus management in the context
  _focusService = boost::make_shared<qi::focus::gmock::FocusNiceGMock>();

  ON_CALL(*_focusService, check(testing::_))
      .WillByDefault(testing::Return(qi::Future<bool>(true)));

  ON_CALL(*_focusService, take())
      .WillByDefault(testing::Invoke(_strand->schedulerFor([this]
  {
    _focusOwner = boost::make_shared<qi::focus::gmock::FocusOwnerNiceGMock>();
    boost::weak_ptr<qi::focus::gmock::FocusOwnerGMock> weakFocusOwner = _focusOwner;
    qi::Promise<void> waitForReleased;
    ON_CALL(*_focusOwner, release())
        .WillByDefault(testing::Invoke(_strand->schedulerFor([weakFocusOwner, waitForReleased]() mutable
    {
      const auto focusOwner = weakFocusOwner.lock();
      if (!focusOwner)
        throw std::runtime_error("focus owner was destroyed");
      QI_EMIT focusOwner->released();
      try { waitForReleased.setValue(nullptr); } catch (...) {}
    })));

    ON_CALL(*_focusOwner, waitForReleased())
        .WillByDefault(testing::Return(waitForReleased.future()));

    return qi::focus::FocusOwnerPtr(qi::AnyObject(_focusOwner));
  })));

  _server->registerService("Focus", _focusService);

  _context = boost::make_shared<qi::context::gmock::ContextNiceGMock>();
  _context->focus.set(_focusService->take().value()).value();
  _contextObject = qi::AnyObject(_context);
}


FocusSetup::~FocusSetup()
{
  _context.reset();
  _focusService.reset();
  _focusOwner.reset();

  _strand->join();
  _client->close().value();
  _client.reset();
  _server->close().value();
  _server.reset();
}


CommonSetup::CommonSetup()
  : _speaking(qi::Future<void>{nullptr})
{
  // BasicAwareness is required by dependencies.
  _basicAwareness = boost::make_shared<BasicAwarenessGMock>();
  _server->registerService("BasicAwareness", _basicAwareness);

  // HumanAwareness is required by dependencies.
  _humanAwareness = boost::make_shared<qi::humanawareness::gmock::HumanAwarenessNiceGMock>();
  _server->registerService("HumanAwareness", _humanAwareness);

  // ALMemory is still useful to track events, like ASR of the humans around.
  // TODO: replace with newer APIs.
  _memory = boost::make_shared<MemoryGMock>();
  ON_CALL(*_memory, subscriber(testing::_)).WillByDefault(testing::Return(boost::make_shared<MemorySubscriberGMock>()));
  _server->registerService("ALMemory", _memory);

  _asrSubscriber = boost::make_shared<MemorySubscriberGMock>();
  ON_CALL(*_memory, subscriber("WordRecognized"))
      .WillByDefault(testing::Return(_asrSubscriber));

  _asrEndSubscriber = boost::make_shared<MemorySubscriberGMock>();
  ON_CALL(*_memory, subscriber("ALSpeechRecognition/Status"))
      .WillByDefault(testing::Return(_asrEndSubscriber));

  _ttsSubscriber = boost::make_shared<MemorySubscriberGMock>();
  ON_CALL(*_memory, subscriber("ALTextToSpeech/CurrentSentence")).WillByDefault(testing::Return(_ttsSubscriber));

  _ttsLanguage = "English";
  _tts = boost::make_shared<TextToSpeechGMock>();
  ON_CALL(*_tts, getLanguage()).WillByDefault(testing::Return(_ttsLanguage));
  _server->registerService("ALTextToSpeech", _tts);

  _speech = boost::make_shared<AnimatedSpeechGMock>();
  _speaking = qi::Future<void>{nullptr};
  auto ttsSubscriber = _ttsSubscriber; // prefer holding that instance to relying on a mortal this
  ON_CALL(*_speech, say(testing::_))
      .WillByDefault(testing::Invoke(_strand->unwrappedSchedulerFor([this, ttsSubscriber](const std::string& text)
  {
    _speaking.cancel();
    _speaking = _speaking.then(_strand->unwrappedSchedulerFor([ttsSubscriber, text](qi::Future<void> f)
    {
      if (f.isCanceled())
        qiLogInfo() << "Previous utterance aborted";
      return qi::asyncDelay([=]
      {
        qiLogInfo() << "Actually uttering \"" << text << "\"";
        QI_EMIT ttsSubscriber->signal(qi::AnyValue{text});
      }, qi::MilliSeconds{100});
    })).unwrap();
    return _speaking;
  })));
  _server->registerService("ALAnimatedSpeech", _speech);

  _dialog = boost::make_shared<DialogGMock>();
  ON_CALL(*_dialog, getLanguage()).WillByDefault(testing::Return("English"));
  _server->registerService("ALDialog", _dialog);

  _speechEngine = makeMockSpeechEngine(_ttsSubscriber);
  _speechEngineObj = qi::AnyObject{_speechEngine};

  _conversation = boost::make_shared<qi::conversation::gmock::ConversationGMock>();
  ON_CALL(*_conversation, makeSpeechEngine(testing::_))
      .WillByDefault(testing::Return(qi::Future<qi::conversation::SpeechEnginePtr>{_speechEngineObj}));
  _server->registerService("Conversation", _conversation);
}


CommonSetup::~CommonSetup()
{
  _dialog.reset();
  _speech.reset();
  _ttsSubscriber.reset();
  _asrSubscriber.reset();
  _memory.reset();
  _basicAwareness.reset();
  destroy();
}


void CommonSetup::hear(
    const std::string& toHear,
    std::function<void(const std::string&)> inputSpeech)
{
  if (inputSpeech)
  {
    inputSpeech(toHear);
  }
  else
  { // KLUDGE: order of signals is not guaranteed.
    // TODO: let mock-ups accept strands, so that signals and properties are stranded.
    _asrSubscriber->signal(makeASREvent(toHear));
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    _asrEndSubscriber->signal(qi::AnyValue("EndOfProcess"));
  }
}


void CommonSetup::setLanguage(const std::string& languageName)
{
  _ttsLanguage = languageName;
  QI_EMIT _tts->languageTTS(_ttsLanguage);
}
