#include "action_factory.hpp"
#include <map>
#include <vector>
#include <boost/make_shared.hpp>
#include <qisem/semanticmodel/enum/grammaticaltype.hpp>
#include <qi/teaching/ontology.hpp>
#include "usual_locales.hpp"

using namespace qi::semantic::agent;

LookAtAction::LookAtAction()
{
  static bool _typeRegistered_ = []
  {
    // We register the type (adapted from mock-up registration).
    qi::ObjectTypeBuilder<LookAtAction> typeBuilder;
    typeBuilder.advertiseProperty("name", static_cast<::qi::Property<std::string> LookAtAction::*>(&LookAtAction::name));
    typeBuilder.advertiseProperty("pddl", static_cast<::qi::Property<std::string> LookAtAction::*>(&LookAtAction::pddl));
    typeBuilder.advertiseProperty("context", static_cast<::qi::Property<::qi::context::ContextPtr> LookAtAction::*>(&LookAtAction::context));
    typeBuilder.advertiseProperty("object_to_look_at", static_cast<::qi::Property<::qi::knowledge::ResourceNode> LookAtAction::*>(&LookAtAction::object_to_look_at));
    typeBuilder.advertiseMethod("run", static_cast<::qi::Future<void>(LookAtAction::*)()>(&LookAtAction::run));
    typeBuilder.advertiseSignal("started", static_cast<::qi::Signal<> LookAtAction::*>(&LookAtAction::started));
    typeBuilder.advertiseMethod("waitForStarted", static_cast<::qi::Future<void>(LookAtAction::*)()>(&LookAtAction::waitForStarted));
    typeBuilder.registerType();
    return true;
  }();
  boost::ignore_unused(_typeRegistered_);
}


PointAtAction::PointAtAction()
{
  static bool _typeRegistered_ = []
  {
    // We register the type (adapted from mock-up registration).
    qi::ObjectTypeBuilder<PointAtAction> typeBuilder;
    typeBuilder.advertiseProperty("name", static_cast<::qi::Property<std::string> PointAtAction::*>(&PointAtAction::name));
    typeBuilder.advertiseProperty("pddl", static_cast<::qi::Property<std::string> PointAtAction::*>(&PointAtAction::pddl));
    typeBuilder.advertiseProperty("context", static_cast<::qi::Property<::qi::context::ContextPtr> PointAtAction::*>(&PointAtAction::context));
    typeBuilder.advertiseProperty("object_to_point_at", static_cast<::qi::Property<::qi::knowledge::ResourceNode> PointAtAction::*>(&PointAtAction::object_to_point_at));
    typeBuilder.advertiseMethod("run", static_cast<::qi::Future<void>(PointAtAction::*)()>(&PointAtAction::run));
    typeBuilder.advertiseSignal("started", static_cast<::qi::Signal<> PointAtAction::*>(&PointAtAction::started));
    typeBuilder.advertiseMethod("waitForStarted", static_cast<::qi::Future<void>(PointAtAction::*)()>(&PointAtAction::waitForStarted));
    typeBuilder.registerType();
    return true;
  }();
  boost::ignore_unused(_typeRegistered_);
}


ActionFactoryMocks<LookAtAction> makeLookAtActionFactoryBehavior()
{
  ActionFactoryMocks<LookAtAction> result;

  result.action = boost::make_shared<LookAtAction>();
  auto& lookAtAction = result.action;

  lookAtAction->name.set("Look at object").value();

  // Look at action is exposed by a factory,
  // exposing the template "to look at <object:objectToLookAt>",
  // in English and in French.
  // Its effect is self-fulfilling.
  result.factory =
      boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock>();
  auto& actionFactory = result.factory;

  actionFactory->name = "Look at object";

  actionFactory->pddl =
      "(define (domain exploration)\n"
      "  (:requirements :strips :typing)\n"
      "  (:types qiknowledge_object_object)\n"
      "  (:constants look_at_event - event)\n"
      "  (:predicates (occurred ?e - event))\n"
      "  (:action look_at\n"
      "    :parameters (?object_to_look_at - qiknowledge_object_object)\n"
      "    :precondition ()\n"
      "    :effect (occurred look_at_event)\n"
      "  )\n"
      ")\n";

  std::map<std::string, qi::knowledge::ResourceNode> lookAtParameters;
  lookAtParameters["object_to_look_at"] = qi::teaching::objectTypeResource();
  actionFactory->parameters = lookAtParameters;

  const auto gramTypeObj = qisem::grammaticalType_toStr(qisem::GrammaticalType::OBJECT);

  SemanticTemplate lookAtEnglishSemanticTemplate;
  lookAtEnglishSemanticTemplate.lemma = "look~at";
  lookAtEnglishSemanticTemplate.children[gramTypeObj] = "object_to_look_at";

  SemanticTemplate lookAtFrenchSemanticTemplate;
  lookAtFrenchSemanticTemplate.lemma = "regarder";
  lookAtFrenchSemanticTemplate.children[gramTypeObj] = "object_to_look_at";

  std::map<std::string, SemanticTemplate> lookAtSemanticTemplates;
  lookAtSemanticTemplates[englishLocaleCode] = lookAtEnglishSemanticTemplate;
  lookAtSemanticTemplates[frenchLocaleCode] = lookAtFrenchSemanticTemplate;
  actionFactory->semanticTemplates = lookAtSemanticTemplates;

  // The factory is carried by a behavior,
  // that we register to the semantic agent.
  result.behavior =
      boost::make_shared<gmock::PDDLAwareQiBehaviorGMock>();
  auto& behavior = result.behavior;

  behavior->name.set("Explore and look at");
  behavior->actionFactories.set(
        std::vector<PDDLAwareQiActionFactoryPtr>{qi::AnyObject(actionFactory)}).value();

  return result;
}


ActionFactoryMocks<PointAtAction> makePointAtActionFactoryBehavior()
{
  ActionFactoryMocks<PointAtAction> result;

  result.action = boost::make_shared<PointAtAction>();
  auto& lookAtAction = result.action;

  lookAtAction->name.set("Pointer un objet").value();

  // Look at action is exposed by a factory,
  // exposing the template "to look at <object:objectToLookAt>",
  // in English and in French.
  // Its effect is self-fulfilling.
  result.factory =
      boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock>();
  auto& actionFactory = result.factory;

  actionFactory->name = "Pointer un objet";

  actionFactory->pddl =
      "(define (domain exploration)\n"
      "  (:requirements :strips :typing)\n"
      "  (:types qiknowledge_object_object)\n"
      "  (:constants point_at_event - event)\n"
      "  (:predicates (occurred ?e - event))\n"
      "  (:action point_at\n"
      "    :parameters (?object_to_point_at - qiknowledge_object_object)\n"
      "    :precondition ()\n"
      "    :effect (occurred point_at_event)\n"
      "  )\n"
      ")\n";

  std::map<std::string, qi::knowledge::ResourceNode> lookAtParameters;
  lookAtParameters["object_to_point_at"] = qi::teaching::objectTypeResource();
  actionFactory->parameters = lookAtParameters;

  const auto gramTypeObj = qisem::grammaticalType_toStr(qisem::GrammaticalType::OBJECT);
  const auto gramTypeLoc = qisem::grammaticalType_toStr(qisem::GrammaticalType::LOCATION);

  SemanticTemplate lookAtEnglishSemanticTemplate;
  lookAtEnglishSemanticTemplate.lemma = "point";
  lookAtEnglishSemanticTemplate.children[gramTypeLoc] = "object_to_point_at";

  SemanticTemplate lookAtFrenchSemanticTemplate;
  lookAtFrenchSemanticTemplate.lemma = "pointer";
  lookAtFrenchSemanticTemplate.children[gramTypeObj] = "object_to_point_at";

  std::map<std::string, SemanticTemplate> lookAtSemanticTemplates;
  lookAtSemanticTemplates[englishLocaleCode] = lookAtEnglishSemanticTemplate;
  lookAtSemanticTemplates[frenchLocaleCode] = lookAtFrenchSemanticTemplate;
  actionFactory->semanticTemplates = lookAtSemanticTemplates;

  // The factory is carried by a behavior,
  // that we register to the semantic agent.
  result.behavior =
      boost::make_shared<gmock::PDDLAwareQiBehaviorGMock>();
  auto& behavior = result.behavior;

  behavior->name.set("Explorer et pointer");
  behavior->actionFactories.set(
        std::vector<PDDLAwareQiActionFactoryPtr>{qi::AnyObject(actionFactory)}).value();

  return result;
}
