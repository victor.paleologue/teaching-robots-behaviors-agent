#include <gtest/gtest.h>
#include <future>
#include <boost/filesystem.hpp>
#include <qi/anymodule.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "../src/utility/knowledge.hpp"
#include "utility.hpp"
#include "debug_log_enabler.hpp"

qiLogCategory("SemanticAgent.Test.SemanticBehavior");

using namespace qi::semantic::agent;
using namespace qi::knowledge;
using namespace qi::teaching;

namespace
{
const qi::locale::Locale french{qi::locale::Language::French, qi::locale::Region::France};

} // anonymous namespace

class KnowledgeTest: public ::testing::Test
{
protected:
  KnowledgeTest()
    : _debugLogEnabler("SemanticAgent.Knowledge")
    , _knowledgeDir(qi::os::mktmpdir("KnowledgeTest"))
    , _knowledge(qi::import("knowledge_module").call<EditableKnowledgeBasePtr>(
                   "EditableKnowledgeBase", _knowledgeDir.str(), true))
    , _beliefs(_knowledge->editableKnowledgeGraph(beliefsDomain()))
  {
    _beliefs->timestampEnabled.set(false).value();
  }

  ~KnowledgeTest()
  {
    _knowledge.reset();
    boost::filesystem::remove_all(_knowledgeDir.bfsPath());
  }

  DebugLogEnabler _debugLogEnabler;
  qi::Path _knowledgeDir;
  EditableKnowledgeBasePtr _knowledge;
  EditableKnowledgeGraphPtr _beliefs;
};


TEST_F(KnowledgeTest, QuerySPARQLBindings)
{
  auto input = makeUniqueResource(eventPrefix());
  auto output = makeUniqueResource(eventPrefix());
  auto triple = makeRobotResponseToCommunicationAct(input, output);
  addTriples(_knowledge->editableKnowledgeGraph(""), triple).value();

  std::stringstream query;
  query << "select ?act ?time ?response where {"
           "  ?act <" << rdfType() << "> <" << communicationActType() << "> ."
           "  ?act <" << occurredAt() << "> ?time ."
           "  optional {?response <" << respondsTo() << "> ?act} ."
           "}"; // TODO: is it possible to do the time filter in SPARQL?

  auto results = _knowledge.call<std::vector<std::map<std::string, Node>>>(
        "querySPARQLBindings", query.str());
}
