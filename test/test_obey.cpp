#include <gmock/gmock.h>
#include <qi/teaching/ontology.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include <semanticnaoqilinker/semanticmemorylinker.hpp>
#include "test_semantic_behaviors.hpp"
#include "../src/behaviormodel/action_pool.hpp"
#include "../src/behaviors/obey.hpp"
#include "../src/behaviors/semantic_behaviors.hpp"
#include "../src/utility/semantic_helpers.hpp"
#include "../src/knowledge_semantic.hpp"
#include "test_pddl.hpp"
#include "debug_log_enabler.hpp"

qiLogCategory("Test.Obey");

using namespace qi::knowledge;
using namespace qi::semantic::agent;
using namespace qi::teaching;
using namespace qisem;

/// In this fixture, we have a minimal setup for semantic reasoning.
/// The memory will have basic primitive actions already available.
class ObeyTest: public SemanticBehaviorsTest
{
protected:
  void TearDown() override
  {
    _learnedActions.clear();
  }

  std::unique_ptr<SemanticExpression> toSemantic(const std::string& userInput)
  {
    TextProcessingContext textContext{
          SemanticAgentGrounding::currentUser,
          SemanticAgentGrounding::me,
          SemanticLanguageEnum::ENGLISH}; // TODO: async this

    auto expression = converter::textToSemExp(userInput, textContext, *_linguisticDatabase);
    memoryOperation::mergeWithContext(expression, *_semanticMemory, *_linguisticDatabase); // improve text with robot's knowledge
    return expression->clone();
  }

  std::string makeText(const SemanticExpression& expression)
  {
    TextProcessingContext textContext{
      SemanticAgentGrounding::me,
      SemanticAgentGrounding::currentUser,
      SemanticLanguageEnum::ENGLISH};

    std::string text;
    converter::semExpToText(
          text, expression.clone(), textContext, false, *_semanticMemory, *_linguisticDatabase, nullptr);
    return text;
  }

  std::list<SemanticExpressionAssociation> _learnedActions;
  DebugLogEnabler _obeyLogs{"SemanticAgent.Obey"};
};


TEST_F(ObeyTest, ObeyPerformsPrimitiveActions)
{
  // It should react when we ask it to perform it
  auto reaction = qi::semantic::agent::obey(
        *toSemantic("say hello"), englishLocale, _learnedActions, *_semanticMemoryContext);
  ASSERT_TRUE(reaction != nullptr);
}


TEST_F(ObeyTest, ObeyResolvesActionAliases)
{ // TODO: maybe not using semantic expressions for the action association, that's overly complicated
  auto action = statementExpressionFromEnglishVerbLemma("sleep"); // need some known action
  auto alias = statementExpressionFromEnglishVerbLemma("die"); // "smack" is misrecognized
  _learnedActions.push_back({std::move(alias), std::move(action)});

  auto reaction = qi::semantic::agent::obey(
        *toSemantic("die"), englishLocale, _learnedActions, *_semanticMemoryContext);
  ASSERT_TRUE(reaction != nullptr);
}


TEST_F(ObeyTest, ObeyResolvesActionSequences)
{
  auto actionExpression = toSemantic("sleep, wake up and say hello");
  auto alias = statementExpressionFromEnglishVerbLemma("resurrect");
  _learnedActions.push_back({std::move(alias), actionExpression->clone()});

  auto reaction = qi::semantic::agent::obey(
        *toSemantic("resurrect"), englishLocale, _learnedActions, *_semanticMemoryContext);
  ASSERT_TRUE(reaction != nullptr);
}


TEST_F(ObeyTest, ObeyResolvesThenActionSequences)
{
  auto actionExpression = toSemantic("sleep, then wake up and then say hello");
  auto alias = statementExpressionFromEnglishVerbLemma("resurrect");
  _learnedActions.push_back({std::move(alias), actionExpression->clone()});

  auto reaction = qi::semantic::agent::obey(
        *toSemantic("resurrect"), englishLocale, _learnedActions, *_semanticMemoryContext);
  ASSERT_TRUE(reaction != nullptr);
}


TEST_F(ObeyTest, ObeyResolvesComposition)
{
  auto actionExpression = toSemantic("turn away and move forward");
  auto alias = statementExpressionFromEnglishVerbLemma("leave");
  _learnedActions.push_back({std::move(alias), actionExpression->clone()});

  actionExpression = toSemantic("say bye and leave");
  alias = statementExpressionFromEnglishVerbLemma("flee");
  _learnedActions.push_back({std::move(alias), actionExpression->clone()});

  auto reaction = qi::semantic::agent::obey(
        *toSemantic("flee"), englishLocale, _learnedActions,  *_semanticMemoryContext);
  ASSERT_TRUE(reaction != nullptr);
}


TEST_F(ObeyTest, ObeyAcceptsOverridenActions)
{
  auto actionExpression = toSemantic("move forward and say hello");
  auto alias = toSemantic("look up");
  _learnedActions.push_back({std::move(alias), actionExpression->clone()});

  auto reaction = qi::semantic::agent::obey(
        *toSemantic("look  up"), englishLocale, _learnedActions,  *_semanticMemoryContext);
  ASSERT_TRUE(reaction != nullptr);
}


TEST_F(ObeyTest, ObeySurvivesRecursion)
{
  auto actionExpression = toSemantic("look up and sing");
  auto alias = toSemantic("look up");
  _learnedActions.push_back({std::move(alias), actionExpression->clone()});

  auto reaction = qi::semantic::agent::obey(
        *toSemantic("look up"), englishLocale, _learnedActions,  *_semanticMemoryContext);
  ASSERT_NE(nullptr, reaction);
}


TEST_F(ObeyTest, ObeyDoesNotReactToQuestions)
{
  auto reaction = qi::semantic::agent::obey(
        *toSemantic("what is to dance?"), englishLocale, _learnedActions,  *_semanticMemoryContext);
  EXPECT_TRUE(reaction == nullptr) << "Got reaction: " << makeText(*reaction);
}


TEST_F(ObeyTest, ObeyBehaviorCanRunAndBeInterrupted)
{
  auto taughtBehaviors = std::make_shared<std::list<SemanticExpressionAssociation>>();
  auto obey = makeObey(
        _speechSourceRemote, _speechEngineObj, _knowledge, _client,
        _contextObject, _semanticMemoryContext, taughtBehaviors)
      .value();
}


TEST_F(ObeyTest, ObeyBehaviorDoNotLeakSpeechExtractor)
{
  auto speechExtractor = makeMockSpeechExtractor();
  boost::weak_ptr<qi::semantic::agent::gmock::SpeechExtractorNiceGMock> weakSpeechExtractor = speechExtractor;
  {
    auto taughtBehaviors = std::make_shared<std::list<SemanticExpressionAssociation>>();
    auto obey = makeObey(
          qi::AnyObject{speechExtractor}, _speechEngineObj, _knowledge, _client, _contextObject, _semanticMemoryContext, taughtBehaviors).value();
    speechExtractor.reset();
  }
  EXPECT_FALSE(weakSpeechExtractor.lock());
}


TEST_F(ObeyTest, ObeyBehaviorReactsToPrimitiveOrders)
{
  auto taughtBehaviors = std::make_shared<std::list<SemanticExpressionAssociation>>();
  auto obey = makeObey(
        _speechSourceRemote, _speechEngineObj, _knowledge, _client,
        _contextObject, _semanticMemoryContext, taughtBehaviors)
      .value();

  std::vector<std::string> inputs{"say hello"};
  expectResponseForEachUtterance(obey, inputs);
}


struct MockActionWithParameter: qi::semantic::agent::gmock::PDDLAwareQiActionGMock
{
  qi::Property<ResourceNode> objectToLocate;
};


TEST_F(ObeyTest, ObeyBehaviorFindsActionFromPool)
{
  // An action that "locates" objects.
  auto mockAction = boost::make_shared<MockActionWithParameter>();
  EXPECT_CALL(*mockAction, run())
      .WillOnce(testing::Invoke([=]
  {
    const auto& objectIRI = mockAction->objectToLocate.get().value().url;
    EXPECT_FALSE(objectIRI.empty());
    std::stringstream reply;
    reply << "here is " << objectIRI;
    return _speechEngine->makeSay({reply.str()}).value()->async().run();
  }));

  // A factory to expose it.
  auto mockFactory = boost::make_shared<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock>();
  mockFactory->pddl.set(actionPddl4).value();
  std::map<std::string, SemanticTemplate> semanticTemplates;
  semanticTemplates["en"] = semanticTemplate4;
  mockFactory->semanticTemplates.set(semanticTemplates);
  EXPECT_CALL(*mockFactory, makeAction(testing::_))
      .WillOnce(testing::Invoke([=](const qi::context::ContextPtr&)
  {
    return qi::Future<qi::AnyObject>(mockAction);
  }));

  // Register the factory in an action pool.
  auto actionPool = std::make_shared<ActionPool>();
  auto updatingPDDL = waitForSignal(actionPool->pddl);
  auto factoryOwner = actionPool->addFactory(mockFactory).value();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updatingPDDL.waitFor(usualTimeout))
      << futureToString(updatingPDDL);

  // Put an object in the knowledge with a label.
  auto objectResource = makeUniqueResource(objectPrefix());
  auto objectLabel = makeLocalizedStringNode("the banana", englishLocale);
  std::vector<Triple> triples = {
    makeTripleFromNodes(objectResource, rdfTypeResource(), objectTypeNode()),
    makeTripleFromNodes(objectResource, rdfsLabelResource(), objectLabel)};
  addTriples(_beliefs, triples);

  // Start a knowledge-semantic bridge, to find knowledge by its label.
  auto knowledgeSemanticBridge =
      std::make_shared<KnowledgeSemanticBridge>(
        _knowledge->knowledgeBase(), _semanticMemoryContext);
  auto updating = waitForSignal(knowledgeSemanticBridge->updated);
  auto running = knowledgeSemanticBridge->run();
  ASSERT_EQ(qi::FutureState_FinishedWithValue, updating.waitFor(usualTimeout));
  ASSERT_EQ(qi::FutureState_Running, running.wait(0));

  // Prepare the obey behavior.
  auto taughtBehaviors = std::make_shared<std::list<SemanticExpressionAssociation>>();
  auto obey = makeObey(
        _speechSourceRemote, _speechEngineObj, _knowledge, _client,
        _contextObject, _semanticMemoryContext, taughtBehaviors,
        actionPool, knowledgeSemanticBridge)
      .value();

  std::vector<std::string> inputs{"locate the banana"};
  expectResponseForEachUtterance(obey, inputs);
}
