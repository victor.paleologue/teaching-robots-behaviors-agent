#ifndef SEMANTICAGENT_TEST_SEMANTIC_HELPERS_HPP
#define SEMANTICAGENT_TEST_SEMANTIC_HELPERS_HPP

#include <memory>
#include <gtest/gtest.h>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>

/// A fixture for tests performing semantic manipulations.
class SemanticTest: public testing::Test
{
protected:
  SemanticTest();

  std::shared_ptr<qisem::linguistics::LinguisticDatabase> _linguisticDatabase;
  std::shared_ptr<qisem::SemanticMemory> _memory;

  static qisem::TextProcessingContext userToMe;

  static qisem::TextProcessingContext meToUser;

  /// Extract the semantic expression of a phrase.
  std::unique_ptr<qisem::SemanticExpression> phraseToSemantic(
      const std::string& phrase,
      qisem::TextProcessingContext context);

  /// Produce a phrase from a semantic expression.
  std::string semanticToPhrase(
      const qisem::SemanticExpression& expression,
      qisem::TextProcessingContext context);
};

#endif // SEMANTICAGENT_TEST_SEMANTIC_HELPERS_HPP
