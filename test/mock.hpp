#ifndef SEMANTICAGENT_MOCK_HPP
#define SEMANTICAGENT_MOCK_HPP

#include <gmock/gmock.h>
#include <string>
#include <qi/anyobject.hpp>
#include <qi/context/gmock/context.hpp>
#include <qi/human/gmock/human.hpp>
#include <qi/human/human.hpp>
#include <qi/knowledge/knowledge.hpp>
#include "../src/humanmanager.hpp"
#include "../src/humanrecognizer_fake.hpp"

#include <boost/date_time/posix_time/posix_time.hpp>
using qi::context::gmock::ContextGMock;

struct MemoryDummy {};
struct MemoryGMock: public testing::NiceMock<MemoryDummy>
{
  MOCK_METHOD1(subscriber, qi::AnyObject(const std::string&));
  MOCK_METHOD1(getData, qi::AnyValue(const std::string&));
};

QI_REGISTER_OBJECT(MemoryGMock, subscriber, getData)

struct MemorySubscriberGMock
{
  qi::Signal<qi::AnyValue> signal;
};

QI_REGISTER_OBJECT(MemorySubscriberGMock, signal)

struct TextToSpeechDummy{};
struct TextToSpeechGMock: testing::NiceMock<TextToSpeechDummy>
{
  MOCK_METHOD0(getLanguage, std::string());
  qi::Signal<std::string> languageTTS;
};

QI_REGISTER_OBJECT(TextToSpeechGMock, getLanguage, languageTTS)

struct AnimatedSpeechDummy {};
struct AnimatedSpeechGMock: public testing::NiceMock<AnimatedSpeechDummy>
{
  MOCK_METHOD1(say, qi::Future<void>(const std::string&));
};

QI_REGISTER_OBJECT(AnimatedSpeechGMock, say)

struct SpeechRecognitionDummy {};
struct SpeechRecognitionGMock: public testing::NiceMock<SpeechRecognitionDummy>
{
  MOCK_METHOD1(pause, void(bool));
  MOCK_METHOD1(subscribe, std::string(const std::string&));
  MOCK_METHOD1(unsubscribe, void(const std::string&));
  MOCK_METHOD0(enableFreeSpeechToText, void());
};

QI_REGISTER_OBJECT(SpeechRecognitionGMock, pause, subscribe, unsubscribe, enableFreeSpeechToText)

struct DialogDummy {};
struct DialogGMock: public testing::NiceMock<DialogDummy>
{
  MOCK_METHOD0(getLanguage, std::string());
  qi::Property<bool> listening;
};

QI_REGISTER_OBJECT(DialogGMock, getLanguage, listening)

struct BasicAwarenessDummy {};
struct BasicAwarenessGMock: public testing::NiceMock<BasicAwarenessDummy>
{
  qi::Property<qi::AnyObject> trackedHuman;
};

QI_REGISTER_OBJECT(BasicAwarenessGMock, trackedHuman)


struct ActionPlanningGMock: testing::Mock
{
  MOCK_METHOD2(planFromPDDLContents, std::string(const std::string&, const std::string&));
};

QI_REGISTER_OBJECT(ActionPlanningGMock, planFromPDDLContents);

struct HumanManagerGMock: public qi::semantic::agent::HumanManager
{
  // KLUDGE: mock methods returning void crash when compiled in release, because of our current version of GMock.
  void removeLostHumans(){}
  void addName(const qi::knowledge::ResourceNode&, const std::string&, const qi::locale::Locale&){}
  void colocalize(const qi::knowledge::ResourceNode&, const qi::knowledge::ResourceNode&){}
  void setTimestamp(const qi::knowledge::ResourceNode&, qi::knowledge::EditableKnowledgeGraphPtr, const std::string&){}

  MOCK_METHOD1(getResNode, qi::knowledge::ResourceNode(const qi::human::HumanPtr&));
  MOCK_METHOD1(getHumanPtr, qi::human::HumanPtr(const qi::knowledge::ResourceNode&));
  MOCK_METHOD1(resourceFromIRI, qi::knowledge::ResourceNode(const std::string&));
  MOCK_METHOD1(getResNodeFromName, qi::knowledge::ResourceNode(const std::string&));
  MOCK_METHOD1(getName, std::string(const qi::knowledge::ResourceNode&));

  MOCK_METHOD2(changeResNode, bool(const qi::human::HumanPtr&, const qi::knowledge::ResourceNode&));

  MOCK_METHOD1(engage, qi::Future<void>(const qi::human::HumanPtr&));
  MOCK_METHOD2(getTimestamp, boost::posix_time::ptime(const qi::knowledge::ResourceNode&, const qi::knowledge::EditableKnowledgeGraphPtr&));
};

/// A mock-up human recognizer.
struct HumanRecognizerGMock: public qi::semantic::agent::HumanRecognizer
{
  HumanRecognizerGMock(){}
  MOCK_METHOD0(isInitialized, qi::Future<bool>());
  MOCK_METHOD1(identify, qi::Future<std::string>(qi::human::HumanPtr ));
  MOCK_METHOD2(learn, qi::Future<bool>(qi::human::HumanPtr, std::string));
};


#endif // SEMANTICAGENT_MOCK_HPP
