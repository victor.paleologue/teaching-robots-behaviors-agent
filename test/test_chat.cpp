#include <random>
#include <thread>
#include <gmock/gmock.h>
#include <boost/algorithm/string.hpp>
#include <qi/signalspy.hpp>
#include <qi/conversation/gmock/conversation.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/semantic/agent/gmock/agent.hpp>
#include <qi/teaching/ontology.hpp>
#include "../src/agent.hpp"
#include "../src/chat.hpp"
#include "../src/extractor.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "action_factory.hpp"
#include "debug_log_enabler.hpp"
#include "humanrecognition_mock.hpp"
#include "test_chatbot_behavior.hpp"
#include "test_agent.hpp"
#include "usual_knowledge.hpp"
#include "usual_locales.hpp"

qiLogCategory("SemanticAgent.ChatTest");

using namespace qi::knowledge;
using namespace qi::semantic::agent;
using namespace qi::teaching;

static const std::string notUnderstoodUtterance{"Sorry, I don't understand."};

class ChatTest: public AgentTest
{
protected:
  void SetUp() override
  {
    AgentTest::SetUp();
    _planner = boost::make_shared<ActionPlanningGMock>();
    ON_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
        .WillByDefault(testing::Invoke([](const std::string& domainPddl, const std::string& problemPddl)
    {
      auto description = qi::semantic::agent::parsePddlDomainAndProblem(domainPddl + problemPddl);
      const auto& domain = description->domain;
      if (!domain)
        throw std::runtime_error("no domain found");
      const auto& actions = domain->actions;
      if (actions.empty())
        throw std::runtime_error("no action in domain");
      auto randomIndex = static_cast<size_t>(std::rand()) % actions.size();
      return std::string{"("} + actions[randomIndex]->name + ")";
    }));
    _server->registerService("ActionPlanning", qi::AnyObject(_planner));

    // Human Recognition service (Bayesian stuff)
    _humanRecognition = boost::make_shared<HumanRecognitionNiceMock>();
    setCurrentIdentificationResult(_humanRecognition, "dummy");
    _server->registerService("HumanRecognition", qi::AnyObject(_humanRecognition));
  }


  /// Makes a dummy speech source from any speech extractor.
  /// The speech source simply forwards the phrases,
  /// and invents a knowledge node that does not really exist.
  qi::semantic::agent::SpeechSourcePtr dummySpeechSourceFromSpeechExtractor(
      qi::semantic::agent::SpeechExtractorPtr speechExtractor)
  {
    auto speechSource = boost::make_shared<qi::semantic::agent::gmock::SpeechSourceGMock>();
    boost::weak_ptr<qi::semantic::agent::gmock::SpeechSourceGMock> weakSpeechSource = speechSource;

    speechExtractor->heard.connect(
          [weakSpeechSource](const std::vector<qi::semantic::agent::PhraseAndLocale>& phrases)
    {
      if (auto speechSource = weakSpeechSource.lock())
      {
        QI_EMIT speechSource->heard(
              qi::semantic::agent::Utterance{
                qi::teaching::makeUniqueResource(qi::teaching::eventPrefix()), phrases});
      }
    });

    return qi::AnyObject(speechSource);
  }

  /// Make a chat, run it, and wait for it to be fully ready.
  void runUsualChat(qi::semantic::agent::SpeechExtractorPtr speechExtractor = {})
  {
    if (!speechExtractor)
      speechExtractor = _agentFromRemote->makeFakeSpeechExtractor();
    _speechExtractor = speechExtractor;
    _speechSource = dummySpeechSourceFromSpeechExtractor(speechExtractor);
    _chat = _agentFromRemote->makeChat(qi::AnyObject{_context}, speechExtractor);
    _running = runActionAndWaitForStarted(_chat);
  }


  void defaultInputSpeech(
      const std::vector<qi::semantic::agent::PhraseAndLocale>& phrases,
      const qi::semantic::agent::SpeechExtractorPtr& speechExtractor)
  { // KLUDGE: as long as signals can be emitted from the outside...
    QI_EMIT speechExtractor->heard(phrases);
  }


  void defaultInputSpeech(
      const std::string& phrase,
      const qi::semantic::agent::SpeechExtractorPtr& speechExtractor,
      qi::locale::Locale locale = englishLocale)
  {
    defaultInputSpeech(
          std::vector<qi::semantic::agent::PhraseAndLocale>{
              qi::semantic::agent::PhraseAndLocale{
                  std::move(phrase),
                  locale}},
          speechExtractor);
  }


  std::string respond(
      const std::string& userSay,
      std::function<void(const std::string&)> inputSpeech = {})
  {
    return respond(userSay, englishLocale, std::move(inputSpeech));
  }


  std::string respond(
      const std::string& userSay,
      qi::locale::Locale locale,
      std::function<void(const std::string&)> inputSpeech = {})
  {
    if (!inputSpeech)
    {
      if (!_speechSource)
      {
        _speechExtractor = _agentFromRemote->makeFakeSpeechExtractor();
        _speechSource = dummySpeechSourceFromSpeechExtractor(_speechExtractor);
      }

      inputSpeech = [this, locale](const std::string& phrase)
      {
        defaultInputSpeech(phrase, _speechExtractor, locale);
      };
    }

    return AgentTest::respond(_chat, userSay, inputSpeech);
  }


  qi::Object<qi::conversation::Chat> _chat;
  qi::semantic::agent::SpeechExtractorPtr _speechExtractor;
  qi::semantic::agent::SpeechSourcePtr _speechSource;
  qi::Future<void> _running;
  boost::shared_ptr<ActionPlanningGMock> _planner;
  boost::shared_ptr<HumanRecognitionMock> _humanRecognition;
  DebugLogEnabler _debugLogs{
    std::vector<std::string>{
      "SemanticAgent.Chat", "SemanticAgent.Transcription",
      "SemanticAgent.ActionPool", "SemanticAgent.AutonomousPlanning",
      "SemanticAgent.SemanticHelpers", "SemanticAgent.SemanticBehavior",
      "SemanticAgent.Chatbot", "Test.ChatbotBehavior",
      "SemanticAgent.NotUnderstood"}};
};


TEST_F(ChatTest, cancel)
{
  runUsualChat();
  _running.cancel();
  auto state = _running.waitFor(largeTimeout);
  ASSERT_EQ(qi::FutureState_Canceled, state);
}


TEST_F(ChatTest, behaviors)
{
  runUsualChat();
  qi::Object<qi::semantic::agent::Chat> agentChat{_chat};
  ASSERT_FALSE(agentChat->behaviorControls.get().value().empty());
}


TEST_F(ChatTest, destroyingSetsError)
{
  runUsualChat();
  _chat.reset();
  auto state = _running.waitFor(largeTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithError, state);
}


TEST_F(ChatTest, cannotMakeActionWithNullContext)
{
  ASSERT_ANY_THROW(_agentFromRemote->makeChat(qi::AnyObject{}, _agentFromRemote->makeFakeSpeechExtractor()));
}


TEST_F(ChatTest, cannotRunWithInvalidFocus)
{
  EXPECT_CALL(*_focusService, check(testing::_)).WillOnce(testing::Return(qi::Future<bool>{false}));
  auto chat = _agentFromRemote->makeChat(qi::AnyObject{_context}, _agentFromRemote->makeFakeSpeechExtractor());
  ASSERT_ANY_THROW(runActionAndWaitForStarted(chat));
}


TEST_F(ChatTest, cannotRunWithNullFocus)
{
  _context->focus.set(qi::AnyObject{});
  auto chat = _agentFromRemote->makeChat(qi::AnyObject{_context}, _agentFromRemote->makeFakeSpeechExtractor());
  ASSERT_ANY_THROW(runActionAndWaitForStarted(chat));
}


TEST_F(ChatTest, focusLossProducesError)
{
  runUsualChat();
  _context->focus.get().value()->release();
  auto state = _running.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithError, state);
}


TEST_F(ChatTest, canRunAnotherChatAfterFocusLoss)
{
  runUsualChat();
  _context->focus.get().value()->release();
  auto state = _running.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithError, state);

  auto previousChat = _chat; // keep the previous chat alive
  _context->focus.set(_focusService->take().value()); // renew the focus
  QI_UNUSED(previousChat);
  runUsualChat();
}


TEST_F(ChatTest, cannotChatTwiceAtTheSameTime)
{
  runUsualChat();
  // make a chat, run it, and wait for it to be fully ready
  auto secondChat = _agentFromRemote->makeChat(qi::AnyObject{_context}, _agentFromRemote->makeFakeSpeechExtractor());
  auto secondRunning = secondChat->async().run();
  auto state = secondRunning.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithError, state)
      << "Concurrent chat did not fail as expected";
}


TEST_F(ChatTest, canChatTwiceInARow)
{
  runUsualChat();
  _running.cancel();
  auto state = _running.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_Canceled, state);
  runUsualChat();
}


TEST_F(ChatTest, chattingTwiceInARowDoesNotMakeRobotReplyTwiceInARow)
{
  runUsualChat();
  respond("Say hello");
  _running.cancel();
  runUsualChat();
  qi::SignalSpy saidSpy{_chat->saying};
  respond("Say hello");
  ASSERT_FALSE(saidSpy.waitUntil(2, usualTimeout).value());
  ASSERT_EQ(1u, saidSpy.recordCount());
}


TEST_F(ChatTest, destroyingChatDoesNotKeepSpeechExtractorRunning)
{
  auto speechExtractor = makeMockSpeechExtractor();
  boost::weak_ptr<qi::semantic::agent::gmock::SpeechExtractorNiceGMock> weakSpeechExtractor = speechExtractor;
  runUsualChat(qi::AnyObject{speechExtractor});
  speechExtractor.reset();
  _speechExtractor.reset();
  _speechSource.reset();
  _chat.reset();
  auto state = _running.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithError, state);
  std::this_thread::sleep_for(stdUsualTimeout);
  EXPECT_FALSE(weakSpeechExtractor.lock());
}


// TODO: let the chat understand this
TEST_F(ChatTest, acknowledgeASR)
{
  runUsualChat();
  auto response = respond("Zut");
  ASSERT_EQ(notUnderstoodUtterance, response);
}


TEST_F(ChatTest, doSomething)
{
  runUsualChat();
  EXPECT_THROW(qiLogError("SemanticAgent.ChatTest")
               << "Responded " << respond("Look up"),
               NoSpokenResponseException);
}


TEST_F(ChatTest, saySomething)
{
  runUsualChat();
  EXPECT_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
      .Times(::testing::AtLeast(1))
      .WillRepeatedly(
        testing::Invoke([=](const std::string& domain, const std::string& problem)
  {
    auto description = qi::semantic::agent::parsePddlDomainAndProblem(domain + problem);
    return description->domain->actions[0]->name;
  }));
  auto response = respond("Say hello");
  ASSERT_EQ("Hello", response);
}


TEST_F(ChatTest, composedOrder)
{
  runUsualChat();
  auto response = respond("Raise the right arm and say hello");
  ASSERT_EQ("Hello", response);
}


TEST_F(ChatTest, storeAndRetrieveDoesNotWorkWithoutSemanticEngine)
{
  runUsualChat();
  auto response = respond("The carrots are cooked");
  ASSERT_EQ(notUnderstoodUtterance, response);
  response = respond("What are cooked?");
  ASSERT_EQ(notUnderstoodUtterance, response);
}


TEST_F(ChatTest, DISABLED_storeAndRetrieveWorksWithSemanticEngine)
{
  runUsualChat();
  auto response = respond("The carrots are cooked");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("What are cooked?");
  ASSERT_NE(notUnderstoodUtterance, response);
}


TEST_F(ChatTest, learnBehaviorSequenceOfThreeUsingEqualityThenDoIt)
{
  runUsualChat();
  auto waitingForLearned = waitForSignal(_agentFromRemote->behaviorLearned);

  auto response = respond("To greet is to raise the right arm, to say hello, and to lower the right arm");
  EXPECT_EQ("Is that all?", response);

  response = respond("Yes");
  EXPECT_NE("(acknowledged)", response);
  EXPECT_NE(notUnderstoodUtterance, response);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, waitingForLearned.wait(usualTimeoutMs));

  response = respond("Greet");
  ASSERT_NE("(acknowledged)", response);
  EXPECT_NE(notUnderstoodUtterance, response);
  ASSERT_NE("I can't greet.", response);
}


TEST_F(ChatTest, learnBehaviorSequenceOfThreeUsingEqualityThenRecallIt)
{
  runUsualChat();
  auto response = respond("To greet is to raise the right arm, to say hello, and to lower the right arm");
  ASSERT_NE("(acknowledged)", response);
  response = respond("Yes");
  ASSERT_NE("(acknowledged)", response);
  response = respond("What is to greet");
  ASSERT_NE("(acknowledged)", response);
  ASSERT_NE("I can't greet.", response);
}


TEST_F(ChatTest, learnBehaviorCompositeSequenceThenDoIt)
{
  runUsualChat();
  auto response = respond("To greet is to raise the right arm and to say hello");
  ASSERT_NE("(acknowledged)", response);

  response = respond("To welcome is to move forward and to greet");
  ASSERT_NE("(acknowledged)", response);

  response = respond("Welcome");
  ASSERT_NE("(acknowledged)", response);
  ASSERT_FALSE(boost::algorithm::contains(response, "can't"));
}


TEST_F(ChatTest, learnBehaviorAssociationInSeveralSentences)
{
  runUsualChat();
  auto response = respond("To greet");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("is to raise the right arm");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("and to say hello");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("Greet");
  ASSERT_NE("(acknowledged)", response);
  ASSERT_NE("I can't greet.", response);
  ASSERT_NE("Yes", response);
  ASSERT_NE(notUnderstoodUtterance, response);
}


TEST_F(ChatTest, learnSimpleAssociationToSayHello)
{
  DebugLogEnabler learnBehaviorDebug("SemanticAgent.LearnBehavior");
  DebugLogEnabler obeyDebug("SemanticAgent.Obey");
  runUsualChat();
  auto response = respond("To greet");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("is to say hello");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("yes");
  ASSERT_NE(notUnderstoodUtterance, response);
  response = respond("Greet");
  ASSERT_EQ("Hello", response);
}


TEST_F(ChatTest, testResponseGreetBehavior)
{
  runUsualChat();
  auto response = respond("Hello");
  EXPECT_NE(notUnderstoodUtterance, response);
  EXPECT_EQ("Hello, what is your name?", response);
}


TEST_F(ChatTest, DISABLED_semanticEngineTakesOverWhenUnusualStuffIsSaid)
{
  runUsualChat();
}


TEST_F(ChatTest, sayFromMockupConversation)
{
  const auto phrase = "Hello";
  auto conversation = _client->service("Conversation").value();
  auto speechEngine = conversation.call<qi::conversation::SpeechEnginePtr>("makeSpeechEngine", _contextObject);
  auto say = speechEngine->makeSay(
      {phrase}, qi::conversation::BodyLanguageOption::Neutral, englishLocale);
  say->run();
}


TEST_F(ChatTest, sayFromMockupConversationWithPhraseOnly)
{
  const auto phrase = "Hello";
  auto conversation = _client->service("Conversation").value();
  auto speechEngine = conversation.call<qi::conversation::SpeechEnginePtr>("makeSpeechEngine", _contextObject);
  auto say = speechEngine->makeSay({phrase});
  say->run();
}


TEST_F(ChatTest, chatbotBehaviorTakesOverNotUnderstoodBehavior)
{
  auto chatbotAndReply = makeMockChatbot();
  auto speechExtractor = _agentFromRemote->makeFakeSpeechExtractor();
  auto inputSpeech = [=](const std::string& phrase)
  {
    defaultInputSpeech(phrase, speechExtractor);
  };

  _chat = _agentFromRemote->makeChatWithoutDefaultContent(
        qi::AnyObject{_context},
        speechExtractor,
        {qi::AnyObject{chatbotAndReply.chatbot}},
        englishLocale);
  _running = runActionAndWaitForStarted(_chat);

  EXPECT_CALL(*(chatbotAndReply.chatbot), replyTo(testing::_, testing::_))
      .Times(1);
  EXPECT_CALL(*(chatbotAndReply.reaction), runWith(testing::_))
      .Times(1);

  const auto phrase = "Hello";
  EXPECT_NE(notUnderstoodUtterance, respond(phrase, inputSpeech));

  // And then let's check the agent does not reply something more.
  auto hearingMore = waitForSignal(_chat->heard);
  auto hearingStatus = hearingMore.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_Running, hearingStatus)
      << futureToString(hearingMore);
}


TEST_F(ChatTest, chatbotBehaviorFallsBackOnNotUnderstoodBehavior)
{
  auto chatbotAndReply = makeMockChatbot();
  auto speechExtractor = _agentFromRemote->makeFakeSpeechExtractor();
  auto inputSpeech = [=](const std::string& phrase)
  {
    defaultInputSpeech(phrase, speechExtractor);
  };

  _chat = _agentFromRemote->makeChatWithoutDefaultContent(
        qi::AnyObject{_context},
        speechExtractor,
        {qi::AnyObject{chatbotAndReply.chatbot}},
        englishLocale);
  _running = runActionAndWaitForStarted(_chat);

  EXPECT_CALL(*(chatbotAndReply.chatbot), replyTo(testing::_, testing::_))
      .Times(1)
      .WillOnce(testing::Throw(std::runtime_error("no reply")));
  EXPECT_CALL(*(chatbotAndReply.reaction), runWith(testing::_))
      .Times(0);

  const auto phrase = "Whatever";
  EXPECT_EQ(notUnderstoodUtterance, respond(phrase, inputSpeech));
}


TEST_F(ChatTest, executeActionFromFactory)
{
  // We know about an object: the box.
  auto knowledge = _agentFromRemote->editableKnowledge.get().value();
  auto knowledgeGraph = knowledge->editableKnowledgeGraph("Explore");

  // There is some box in the world.
  auto objectResource = createBoxObject(knowledgeGraph).value();

  // The action takes an object to look at.
  // When it is run, we expect a valid context,
  // and the box object to be the target.
  auto lookAtMocks = makeLookAtActionFactoryBehavior();
  auto& lookAtAction = lookAtMocks.action;
  EXPECT_CALL(*lookAtAction, run())
      .Times(1)
      .WillOnce(testing::Invoke([&]
  {
    EXPECT_TRUE(lookAtAction->context.get().value());
    EXPECT_EQ(objectResource, lookAtAction->object_to_look_at.get().value());
    return qi::Future<void>(nullptr);
  }));

  auto& actionFactory = lookAtMocks.factory;
  EXPECT_CALL(*actionFactory, makeAction(testing::_))
      .Times(1)
      .WillOnce(testing::Invoke([&](qi::context::ContextPtr context)
  {
    lookAtAction->context.set(context).value();
    return qi::Future<qi::AnyObject>(qi::AnyObject(lookAtAction));
  }));

  auto& behavior = lookAtMocks.behavior;
  auto behaviorRegistration =
      _agentFromRemote->registerBehavior(qi::AnyObject(behavior));

  // When the planner is called, let us suppose it is going to execute
  // obey's response, which name starts with "action_obey_".
  EXPECT_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
      .Times(testing::AtLeast(1))
      .WillRepeatedly(testing::Invoke([](const std::string& domainPddl, const std::string& problemPddl)
  {
    auto description = qi::semantic::agent::parsePddlDomainAndProblem(domainPddl + problemPddl);
    const auto& domain = description->domain;
    if (!domain)
      throw std::runtime_error("no domain found");
    const auto& actions = domain->actions;
    if (actions.empty())
      throw std::runtime_error("no action in domain");
    // prefer the obey response
    for (const auto& action: actions)
    {
      if (action->name.substr(0, 12) == "action_obey_")
        return std::string("(") + action->name + ")";
    }
    // random result otherwise
    auto randomIndex = static_cast<size_t>(std::rand()) % actions.size();
    return std::string{"("} + actions[randomIndex]->name + ")";
  }));

  // Now when we make the usual chat,
  // the "obey" behavior must be able to suggest the action,
  // and the action must be run as a response.
  runUsualChat();
  ASSERT_THROW(respond("look at the box"), NoSpokenResponseException);
}


TEST_F(ChatTest, teachAndExecuteBehaviorWithActionFactory)
{
  // We know about an object: the box.
  auto knowledge = _agentFromRemote->editableKnowledge.get().value();
  auto knowledgeGraph = knowledge->editableKnowledgeGraph("Explore");

  // There is some box in the world.
  auto objectResource = createBoxObject(knowledgeGraph).value();

  // The action takes an object to look at.
  // When it is run, we expect a valid context,
  // and the box object to be the target.
  auto lookAtMocks = makeLookAtActionFactoryBehavior();
  auto& lookAtAction = lookAtMocks.action;
  EXPECT_CALL(*lookAtAction, run())
      .Times(1)
      .WillOnce(testing::Invoke([&]
  {
    EXPECT_TRUE(lookAtAction->context.get().value());
    EXPECT_EQ(objectResource, lookAtAction->object_to_look_at.get().value());
    return qi::Future<void>(nullptr);
  }));

  auto& actionFactory = lookAtMocks.factory;
  EXPECT_CALL(*actionFactory, makeAction(testing::_))
      .Times(1)
      .WillOnce(testing::Invoke([&](qi::context::ContextPtr context)
  {
    lookAtAction->context.set(context).value();
    return qi::Future<qi::AnyObject>(qi::AnyObject(lookAtAction));
  }));

  auto& behavior = lookAtMocks.behavior;
  auto behaviorRegistration =
      _agentFromRemote->registerBehavior(qi::AnyObject(behavior));

  std::string actionToSelect;
  // When the planner is called, let us suppose it is going to execute
  // obey's response, which name starts with "action_obey_".
  EXPECT_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
      .Times(testing::AtLeast(1))
      .WillRepeatedly(testing::Invoke([&](const std::string& domainPddl, const std::string& problemPddl)
  {
    auto description = qi::semantic::agent::parsePddlDomainAndProblem(domainPddl + problemPddl);
    const auto& domain = description->domain;
    if (!domain)
      throw std::runtime_error("no domain found");
    const auto& actions = domain->actions;
    if (actions.empty())
      throw std::runtime_error("no action in domain");
    // prefer the obey response
    for (const auto& action: actions)
    {
      if (action->name == actionToSelect)
        return std::string("(") + action->name + ")";
    }
    // random result otherwise
    auto randomIndex = static_cast<size_t>(std::rand()) % actions.size();
    return std::string{"("} + actions[randomIndex]->name + ")";
  }));

  // Now when we make the usual chat,
  // the "obey" behavior must be able to suggest the action,
  // and the action must be run as a response.
  runUsualChat();

  actionToSelect = "action_is_that_all";
  EXPECT_NE(notUnderstoodUtterance, respond("to joke is to look at the box"));

  actionToSelect = "action_is_that_all";
  EXPECT_NE(notUnderstoodUtterance, respond("and to say ah que coucou"));

  actionToSelect = "action_ok_to_joke_is_to_look_at_the_box_and_then_to_say_ah_coucou";
  EXPECT_NE(notUnderstoodUtterance, respond("yes"));

  actionToSelect = "action_obey_to_joke";
  EXPECT_NE(notUnderstoodUtterance, respond("joke"));
}


TEST_F(ChatTest, teachAndExecuteBehaviorWithActionFactory2)
{
  // We know about an object: the box.
  auto knowledge = _agentFromRemote->editableKnowledge.get().value();
  auto knowledgeGraph = knowledge->editableKnowledgeGraph("Explore");

  // There is some box in the world.
  auto objectResource = createChocolateObject(knowledgeGraph).value();

  // The action takes an object to look at.
  // When it is run, we expect a valid context,
  // and the box object to be the target.
  auto pointAtMocks = makePointAtActionFactoryBehavior();
  auto& pointAtAction = pointAtMocks.action;
  EXPECT_CALL(*pointAtAction, run())
      .Times(1)
      .WillOnce(testing::Invoke([&]
  {
    EXPECT_TRUE(pointAtAction->context.get().value());
    EXPECT_EQ(objectResource, pointAtAction->object_to_point_at.get().value());
    return qi::Future<void>(nullptr);
  }));

  auto& actionFactory = pointAtMocks.factory;
  EXPECT_CALL(*actionFactory, makeAction(testing::_))
      .Times(1)
      .WillOnce(testing::Invoke([&](qi::context::ContextPtr context)
  {
    pointAtAction->context.set(context).value();
    return qi::Future<qi::AnyObject>(qi::AnyObject(pointAtAction));
  }));

  auto& behavior = pointAtMocks.behavior;
  auto behaviorRegistration =
      _agentFromRemote->registerBehavior(qi::AnyObject(behavior));

  std::string actionToSelect;
  // When the planner is called, let us suppose it is going to execute
  // obey's response, which name starts with "action_obey_".
  EXPECT_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
      .Times(testing::AtLeast(1))
      .WillRepeatedly(testing::Invoke([&](const std::string& domainPddl, const std::string& problemPddl)
  {
    auto description = qi::semantic::agent::parsePddlDomainAndProblem(domainPddl + problemPddl);
    const auto& domain = description->domain;
    if (!domain)
      throw std::runtime_error("no domain found");
    const auto& actions = domain->actions;
    if (actions.empty())
      throw std::runtime_error("no action in domain");
    // prefer the obey response
    for (const auto& action: actions)
    {
      if (action->name == actionToSelect)
        return std::string("(") + action->name + ")";
    }
    // random result otherwise
    auto randomIndex = static_cast<size_t>(std::rand()) % actions.size();
    return std::string{"("} + actions[randomIndex]->name + ")";
  }));

  // Now when we make the usual chat,
  // the "obey" behavior must be able to suggest the action,
  // and the action must be run as a response.
  ON_CALL(*_dialog, getLanguage()).WillByDefault(testing::Return("French"));
  runUsualChat();

  actionToSelect = "action_d_accord";
  EXPECT_NE(notUnderstoodUtterance, respond("offrir du chocolat", frenchLocale));

  actionToSelect = "action_c_est_tout";
  EXPECT_NE(notUnderstoodUtterance, respond("c'est pointer le chocolat", frenchLocale));

  actionToSelect = "action_c_est_tout";
  EXPECT_NE(notUnderstoodUtterance, respond("et dire sers-toi", frenchLocale));

  actionToSelect = "action_d_accord_offrir_le_chocolat_c_est_pointer_le_chocolat_et_puis_dire_se_sert";
  EXPECT_NE(notUnderstoodUtterance, respond("oui", frenchLocale));

  actionToSelect = "action_obey_to_offre_du_chocolat";
  EXPECT_NE(notUnderstoodUtterance, respond("offre du chocolat", frenchLocale));
}


TEST_F(ChatTest, teachParametricBehaviorAndExecuteIt)
{
  // We know about an object: the box.
  auto knowledge = _agentFromRemote->editableKnowledge.get().value();
  auto knowledgeGraph = knowledge->editableKnowledgeGraph("Explore");

  // There is some box in the world.
  auto objectResource = createBoxObject(knowledgeGraph).value();

  // The action takes an object to look at.
  // When it is run, we expect a valid context,
  // and the box object to be the target.
  auto lookAtMocks = makeLookAtActionFactoryBehavior();
  auto& lookAtAction = lookAtMocks.action;
  EXPECT_CALL(*lookAtAction, run())
      .Times(1)
      .WillOnce(testing::Invoke([&]
  {
    EXPECT_TRUE(lookAtAction->context.get().value());
    EXPECT_EQ(objectResource, lookAtAction->object_to_look_at.get().value());
    return qi::Future<void>(nullptr);
  }));

  auto& actionFactory = lookAtMocks.factory;
  EXPECT_CALL(*actionFactory, makeAction(testing::_))
      .Times(1)
      .WillOnce(testing::Invoke([&](qi::context::ContextPtr context)
  {
    lookAtAction->context.set(context).value();
    return qi::Future<qi::AnyObject>(qi::AnyObject(lookAtAction));
  }));

  auto& behavior = lookAtMocks.behavior;
  auto behaviorRegistration =
      _agentFromRemote->registerBehavior(qi::AnyObject(behavior));

  std::string actionToSelect;
  // When the planner is called, let us suppose it is going to execute
  // obey's response, which name starts with "action_obey_".
  EXPECT_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
      .Times(testing::AtLeast(1))
      .WillRepeatedly(testing::Invoke([&](const std::string& domainPddl, const std::string& problemPddl)
  {
    auto description = qi::semantic::agent::parsePddlDomainAndProblem(domainPddl + problemPddl);
    const auto& domain = description->domain;
    if (!domain)
      throw std::runtime_error("no domain found");
    const auto& actions = domain->actions;
    if (actions.empty())
      throw std::runtime_error("no action in domain");
    // prefer the obey response
    for (const auto& action: actions)
    {
      if (action->name == actionToSelect)
        return std::string("(") + action->name + ")";
    }
    // random result otherwise
    auto randomIndex = static_cast<size_t>(std::rand()) % actions.size();
    return std::string{"("} + actions[randomIndex]->name + ")";
  }));

  // Now when we make the usual chat,
  // the "obey" behavior must be able to suggest the action,
  // and the action must be run as a response.
  runUsualChat();

  actionToSelect = "action_ok";
  EXPECT_NE(notUnderstoodUtterance, respond("to inspect something"));

  actionToSelect = "action_is_that_all";
  EXPECT_NE(notUnderstoodUtterance, respond("is to look at it"));

  actionToSelect = "action_is_that_all";
  EXPECT_NE(notUnderstoodUtterance, respond("and to say what is this"));

  actionToSelect = "action_ok_to_inspect_something_is_to_look_at_something_and_then_to_say_what_it_s";
  EXPECT_NE(notUnderstoodUtterance, respond("yes"));

  actionToSelect = "action_obey_to_inspect_the_box";
  EXPECT_NE(notUnderstoodUtterance, respond("inspect the box"));
}
