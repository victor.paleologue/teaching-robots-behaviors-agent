#ifndef SEMANTICAGENT_TEST_SEMANTIC_BEHAVIORS_HPP
#define SEMANTICAGENT_TEST_SEMANTIC_BEHAVIORS_HPP

#include <qi/knowledge/knowledge.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qi/semantic/agent/gmock/agent.hpp>
#include "test_agent.hpp"
#include "../src/behaviormodel/action.hpp"
#include "../src/behaviormodel/behavior.hpp"
#include "../src/utility/propertyset.hpp"
#include "../src/utility/semantic_helpers.hpp"
#include "debug_log_enabler.hpp"


class SemanticBehaviorsTest: public CommonSetup
{
protected:
  SemanticBehaviorsTest();
  virtual ~SemanticBehaviorsTest();

  qi::knowledge::ResourceNode informUserSaid(
      const std::string& phrase,
      qi::locale::Language language = qi::locale::Language::English);

  qi::knowledge::ResourceNode informRobotRespondedTo(
      const qi::knowledge::ResourceNode& inputEvent);

  testing::AssertionResult forEachUtteranceResponse(
      qi::semantic::agent::PDDLAwareQiBehaviorPtr behavior,
      const std::vector<std::string>& inputs,
      qi::locale::Language language,
      std::function<testing::AssertionResult(const boost::optional<qi::semantic::agent::SetChange<qi::semantic::agent::ActionObj>>&)> predicate);


  void runResponseForEachUtterance(
      qi::semantic::agent::PDDLAwareQiBehaviorPtr behavior,
      const std::vector<std::string>& inputs,
      qi::locale::Language language);


  testing::AssertionResult expectResponseForEachUtterance(
      qi::semantic::agent::PDDLAwareQiBehaviorPtr behavior,
      const std::vector<std::string>& inputs,
      qi::locale::Language language = qi::locale::Language::English);

  testing::AssertionResult expectNoResponseForUtterances(
      qi::semantic::agent::PDDLAwareQiBehaviorPtr behavior,
      const std::vector<std::string>& inputs,
      qi::locale::Language language = qi::locale::Language::English);

  std::shared_ptr<qisem::linguistics::LinguisticDatabase> _linguisticDatabase;
  std::shared_ptr<qisem::SemanticMemory> _semanticMemory;
  std::shared_ptr<qi::semantic::agent::SemanticMemoryContext> _semanticMemoryContext;
  qi::Path _tmpDir;
  qi::knowledge::EditableKnowledgeBasePtr _knowledge;
  qi::knowledge::EditableKnowledgeGraphPtr _beliefs;
  boost::shared_ptr<qi::semantic::agent::gmock::SpeechSourceNiceGMock> _speechSource;
  std::string _speechSourceId;
  qi::semantic::agent::SpeechSourcePtr _speechSourceRemote;
  DebugLogEnabler _semanticBehaviorsDebug;
  DebugLogEnabler _semanticBehaviorsTestDebug;
};

#endif // SEMANTICAGENT_TEST_SEMANTIC_BEHAVIORS_HPP
