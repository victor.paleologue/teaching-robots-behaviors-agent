#ifndef SEMANTICAGENT_TEST_PATH_TO_TEST_DATA_HPP
#define SEMANTICAGENT_TEST_PATH_TO_TEST_DATA_HPP

#include <string>
#include <qi/path.hpp>

/// Get the full path to some file in test data.
/// The provided file path can be only a file name,
/// or may contain slashes.
/// It must be encoded using UTF-8.
/// It is considered as a relative path to the file.
/// The file is meant to be a regular file.
/// Throws if no regular file was found at that path.
qi::Path pathToTestFile(const std::string& relativeFilePath);

/// Get the text data of any file.
std::string readTextFile(const qi::Path& path);

/// Get the data from a test file.
/// @see pathToTestFile
std::string readTextTestFile(const std::string& relativeFilePath);

#endif // SEMANTICAGENT_TEST_PATH_TO_TEST_DATA_HPP
