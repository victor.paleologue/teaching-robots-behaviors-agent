#include <gmock/gmock.h>
#include <boost/filesystem.hpp>
#include <qi/anymodule.hpp>
#include <qi/log.hpp>
#include <qi/knowledge/gmock/knowledge.hpp>
#include "../src/autonomous_planning.hpp"
#include "../src/behaviormodel/action_pool.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "access_test_pddl.hpp"
#include "debug_log_enabler.hpp"
#include "utility.hpp"

qiLogCategory("Test.AutonomousPlanning");

using namespace qi::semantic::agent;
using namespace pddl::ast;

class AutonomousPlanningTest: public CommonSetup
{
protected:
  AutonomousPlanningTest()
    : _debugLogEnabler("SemanticAgent.Planning"),
      _editableKnowledge(),
      _actionPool(std::make_shared<ActionPool>()),
      _customProblem(std::make_shared<StoredProp<std::shared_ptr<const Description>>>())
  {
    // Prepare a knowledge base (a real one!)
    auto knowledgeModule = qi::import("knowledge_module");
    qi::Path knowledgePath{qi::path::userWritableDataPath("SemanticAgent", "knowledge")};
    boost::filesystem::create_directory(knowledgePath.bfsPath());
    _editableKnowledge = knowledgeModule.call<qi::AnyObject>("EditableKnowledgeBase", knowledgePath.str());
    _editableKnowledge->editableKnowledgeGraph("beliefs")->timestampEnabled.set(false).value();

    // Prepare a customizable problem.
    auto problemOwner = _actionPool->addProblemSource(
          std::static_pointer_cast<ProblemSource>(_customProblem)).value();
    _disposables.add(makeDisposable(DisposalFunction([=]
    {
      return problemOwner->async().release();
    })));

    // Prepare a mock-up planner.
    _planner = boost::make_shared<ActionPlanningGMock>();
    ON_CALL(*_planner, planFromPDDLContents(testing::_, testing::_))
        .WillByDefault(testing::Invoke([](const std::string& domainPddl, const std::string& problemPddl)
    {
      auto description = parsePddlDomainAndProblem(domainPddl + problemPddl);
      const auto& domain = description->domain;
      if (!domain)
        throw std::runtime_error("no domain found");
      const auto& actions = domain->actions;
      if (actions.empty())
        throw std::runtime_error("no action in domain");
      auto randomIndex = static_cast<size_t>(std::rand()) % actions.size();
      return std::string{"("} + actions[randomIndex]->name + ")";
    }));
    _server->registerService("ActionPlanning", qi::AnyObject(_planner));
  }

  ~AutonomousPlanningTest()
  {
    _disposables.dispose().value();
  }

  DebugLogEnabler _debugLogEnabler;
  qi::knowledge::EditableKnowledgeBasePtr _editableKnowledge;
  boost::shared_ptr<ActionPlanningGMock> _planner;
  std::shared_ptr<ActionPool> _actionPool;
  std::shared_ptr<StoredProp<std::shared_ptr<const Description>>> _customProblem;
  Disposables _disposables;
};


TEST_F(AutonomousPlanningTest, enumerationOfTinyPrioritizedSubsets)
{
  auto disposable =
      runAutonomousPlanning(_strand, _client, _contextObject, _editableKnowledge, _actionPool);
  disposable->dispose().value();

  auto description = std::make_shared<pddl::ast::Description>();
  _customProblem->set(readDomainAndProblem("communication_failure_1"));
}
