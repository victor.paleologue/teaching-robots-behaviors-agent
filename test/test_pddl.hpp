#ifndef SEMANTICAGENT_TEST_PDDL_HPP
#define SEMANTICAGENT_TEST_PDDL_HPP

#include <string>
#include <qi/semantic/agent/agent.hpp>
#include "access_test_data.hpp"
#include "../src/pddl/pddl_read.hpp"


static const std::string actionPddl1 =
    "(define (domain enter)\n"
    "  (:requirements :strips :typing :negative-preconditions)\n"
    "  (:types\n"
    "    hallway room - location\n"
    "  )\n"
    "  (:predicates\n"
    "    (at ?l - location)\n"
    "    (connected ?l1 - location ?l2 - location)\n"
    "  )"
    "  (:action enter\n"
    "    :parameters (?h - hallway ?r - room)\n"
    "    :precondition (and (connected ?h ?r) (connected ?r ?h) (at ?h)\n"
    "                (not (at ?r)))\n"
    "    :effect (and (at ?r) (not (at ?h)))\n"
    "  )\n"
    ")";

static const std::string actionPddl2 =
    "(define (domain exit)\n"
    "  (:requirements :strips :typing :negative-preconditions)\n"
    "  (:types\n"
    "    hallway room - location\n"
    "  )\n"
    "  (:predicates\n"
    "    (at ?l - location)\n"
    "    (connected ?l1 - location ?l2 - location)\n"
    "  )"
    "  (:action exit\n"
    "    :parameters (?r - room ?h - hallway)\n"
    "    :precondition (and (connected ?r ?h) (connected ?h ?r) (at ?r)\n"
    "                   (not (at ?h)))\n"
    "    :effect (and (at ?h) (not (at ?r)))\n"
    "  )\n"
    ")";

static const std::string actionPddl3 =
    "(define (domain non_parametric)\n"
    "  (:requirements :strips :typing :negative-preconditions)\n"
    "  (:types\n"
    "    act\n"
    "  )\n"
    "  (:predicates\n"
    "    (performed ?a - act)\n"
    "  )\n"
    "  (:constants act_42 - act)\n"
    "  (:action action_42\n"
    "    :parameters ()\n"
    "    :precondition (not (performed act_42))\n"
    "    :effect (performed act_42)\n"
    "  )\n"
    ")";

static const std::string domainTypeError1 =
    "(define (domain exit)\n"
    "  (:requirements :strips :typing :negative-preconditions)\n"
    "  (:types\n"
    "    location\n" // this makes a recursive type declaration
    "    hallway room - location\n"
    "  )\n"
    "  (:predicates\n"
    "    (at ?l - location)\n"
    "    (connected ?l1 - location ?l2 - location)\n"
    "  )"
    "  (:action exit\n"
    "    :parameters (?r - room ?h - hallway)\n"
    "    :precondition (and (connected ?r ?h) (connected ?h ?r) (at ?r)\n"
    "                   (not (at ?h)))\n"
    "    :effect (and (at ?h) (not (at ?r)))\n"
    "  )\n"
    ")";

// Note well the conventions used in this result:
// * using tabs are used for indention
// * making a new line for each statement
// * no shortcut in type declaration
static const std::string mergedActions12 =
    "(define (domain merged_domain)\n"
    "\t(:requirements\n"
    "\t\t:strips\n"
    "\t\t:typing\n"
    "\t\t:negative-preconditions)\n"
    "\t(:types\n"
    "\t\thallway - location\n"
    "\t\troom - location\n"
    "\t\tlocation)\n"
    "\t(:predicates\n"
    "\t\t(at ?l - location)\n"
    "\t\t(connected ?l1 - location ?l2 - location))\n"
    "\t(:action enter\n"
    "\t\t:parameters\n"
    "\t\t\t(?h - hallway\n"
    "\t\t\t?r - room)\n"
    "\t\t:precondition\n"
    "\t\t\t(and\n"
    "\t\t\t\t(connected ?h ?r)\n"
    "\t\t\t\t(connected ?r ?h)\n"
    "\t\t\t\t(at ?h)\n"
    "\t\t\t\t(not (at ?r)))\n"
    "\t\t:effect\n"
    "\t\t\t(and\n"
    "\t\t\t\t(at ?r)\n"
    "\t\t\t\t(not (at ?h))))\n"
    "\t(:action exit\n"
    "\t\t:parameters\n"
    "\t\t\t(?r - room\n"
    "\t\t\t?h - hallway)\n"
    "\t\t:precondition\n"
    "\t\t\t(and\n"
    "\t\t\t\t(connected ?r ?h)\n"
    "\t\t\t\t(connected ?h ?r)\n"
    "\t\t\t\t(at ?r)\n"
    "\t\t\t\t(not (at ?h)))\n"
    "\t\t:effect\n"
    "\t\t\t(and\n"
    "\t\t\t\t(at ?h)\n"
    "\t\t\t\t(not (at ?r)))))";

// A simple problem.
static const std::string problem1 =
    "(define (domain foo)\n"
    "\t(:requirements\n"
    "\t\t:strips\n"
    "\t\t:typing\n"
    "\t\t:negative-preconditions)\n"
    "\t(:types\n"
    "\t\tperson)\n"
    ")\n"
    "(define (problem bar)\n"
    "\t(:domain foo)\n"
    "\t(:requirements\n"
    "\t\t:strips\n"
    "\t\t:typing\n"
    "\t\t:negative-preconditions)\n"
    "\t(:objects person_42 - person)\n"
    "\t(:init)\n"
    "\t(:goal)\n"
    ")";

// Some PDDL that matches a semantic template
static const std::string actionPddl4 =
    "(define (domain exploration)\n"
    "\t(:requirements :strips :typing)\n"
    "\t(:types physobj area social_agent event - object)\n"
    "\t(:constants self - social_agent\n"
    "\t\tobject_to_discover - physobj\n"
    "\t\tlook_at_event point_at_event say_event locate_event - event)\n"
    "\t(:predicates (social_located_at ?s - social_agent)\n"
    "\t\t(knows ?s - social_agent ?o - physobj)\n"
    "\t\t(occurred ?e - event)\n"
    "\t\t(looking_at ?s - social_agent ?o - physobj)\n"
    "\t)\n"
    "\t(:action locate\n"
    "\t\t:parameters (?objectToLocate - physobj)\n"
    "\t\t:precondition (knows self ?objectToLocate)\n"
    "\t\t:effect (occurred locate_event)\n"
    "\t)\n"
    ")\n";

// A semantic template matching some PDDL.
static const qi::semantic::agent::SemanticTemplate semanticTemplate4 =
    { "locate", {{"object", "objectToLocate"}}};

#endif // SEMANTICAGENT_TEST_PDDL_HPP
