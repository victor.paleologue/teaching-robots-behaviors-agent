#ifndef SEMANTICAGENT_HUMANRECOGNITION_MOCK_HPP
#define SEMANTICAGENT_HUMANRECOGNITION_MOCK_HPP

#include <gmock/gmock.h>
#include <boost/shared_ptr.hpp>
#include "../src/humanrecognition_helpers.hpp"

/// This is a Qi Object implementation,
/// it can be converted from a boost::shared_pointer to a qi::AnyObject.
struct HumanRecognitionMock
{
  HumanRecognitionMock();

  // KLUDGE: mock methods returning void crash when compiled in release, because of our current version of GMock.
  void initSystem(){}
  void cleanDB(){}

  MOCK_METHOD0(isInitializing, bool());

  MOCK_METHOD8(
      get_identity,
      std::map<std::string, float>(
        const std::string&, // OKAO's main name estimation
        const std::vector<std::string>&, // All name estimations
        const std::vector<float>&, // Confidences for name estimations
        float, // An estimated accuracy score
        const qi::semantic::agent::AgeEstimation&,
        const qi::semantic::agent::GenderEstimation&,
        const qi::semantic::agent::HeightEstimation&,
        const std::vector<std::string>&)); // Date in format [%T, %u, %d, %B, %Y]";

  MOCK_METHOD9(
      learn,
      std::string(
        const std::string&, // Name given to OKAO for this face
        const std::vector<std::string>&, // All name estimations
        const std::vector<float>&, // Confidences for name estimations
        float, // An estimated accuracy score
        const qi::semantic::agent::AgeEstimation&,
        const qi::semantic::agent::GenderEstimation&,
        const qi::semantic::agent::HeightEstimation&,
        const std::vector<std::string>&, // Date in format [%T, %u, %d, %B, %Y]"
        bool)); // Whether it is a new face (??? TODO: understand. Did they mean a new identity?)
};

using HumanRecognitionNiceMock = testing::NiceMock<HumanRecognitionMock>;

/// Bayesian Network setting to recognize someone in particular.
void setCurrentIdentificationResult(
    boost::shared_ptr<HumanRecognitionMock> humanRecognition,
    const std::string& id);

#endif // SEMANTICAGENT_HUMANRECOGNITION_MOCK_HPP
