#ifndef SEMANTICAGENT_TEST_AGENT_HPP
#define SEMANTICAGENT_TEST_AGENT_HPP

#include <boost/shared_ptr.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "utility.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
class AgentImpl;
}
}
}

qi::Path testDataPath();

/// Manages an agent on the common setup.
class AgentTest: public CommonSetup
{
protected:
  AgentTest();
  ~AgentTest() override;
  boost::shared_ptr<qi::semantic::agent::AgentImpl> _agent;
  qi::Object<qi::semantic::agent::Agent> _agentFromRemote;
};

#endif // SEMANTICAGENT_TEST_AGENT_HPP
