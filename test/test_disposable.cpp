#include <gmock/gmock.h>
#include <qi/future.hpp>
#include "../src/utility/disposable.hpp"
#include "utility.hpp"
#include "debug_log_enabler.hpp"

using namespace qi::semantic::agent;

class DisposableTest: public testing::Test
{
protected:
  DebugLogEnabler _debug{"SemanticAgent.Disposable"};
};


TEST_F(DisposableTest, EmptyDisposablesIsSafeToDestroy)
{
  Disposables disposables;
}


TEST_F(DisposableTest, DisposablesDisposesConsecutivelyInReverseOrder)
{
  const std::vector<int> expectedOrder{9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
  std::vector<int> order;
  Disposables disposables;
  for (int i = 0; i < 10; ++i)
  {
    disposables.add(makeDisposable(DisposalFunctionSync([&order, i]{ order.push_back(i); })));
  }
  disposables.dispose().value();
  EXPECT_EQ(expectedOrder, order);
}
