#include "humanrecognition_mock.hpp"
#include <boost/core/ignore_unused.hpp>
#include <qi/type/objecttypebuilder.hpp>

using testing::_;

template <typename T>
static void registerHumanRecognitionInTypeBuilder(qi::ObjectTypeBuilder<T> typeBuilder)
{
  typeBuilder.advertiseMethod("initSystem", static_cast<void(T::*)()>(&T::initSystem));
  typeBuilder.advertiseMethod("isInitializing", static_cast<bool(T::*)()>(&T::isInitializing));
  typeBuilder.advertiseMethod("cleanDB", static_cast<void(T::*)()>(&T::cleanDB));

  typeBuilder.advertiseMethod(
              "get_identity",
              static_cast<std::map<std::string, float>(T::*)(
                  const std::string&,
                  const std::vector<std::string>&,
                  const std::vector<float>&,
                  float,
                  const qi::semantic::agent::AgeEstimation&,
                  const qi::semantic::agent::GenderEstimation&,
                  const qi::semantic::agent::HeightEstimation&,
                  const std::vector<std::string>&)>(&T::get_identity));

  typeBuilder.advertiseMethod(
              "learn",
              static_cast<std::string(T::*)(
                  const std::string&,
                  const std::vector<std::string>&,
                  const std::vector<float>&,
                  float,
                  const qi::semantic::agent::AgeEstimation&,
                  const qi::semantic::agent::GenderEstimation&,
                  const qi::semantic::agent::HeightEstimation&,
                  const std::vector<std::string>&,
                  bool)>(&T::learn));

  typeBuilder.registerType();
}


HumanRecognitionMock::HumanRecognitionMock()
{
  static const bool typeWasRegistered = []
  {
    registerHumanRecognitionInTypeBuilder(
                qi::ObjectTypeBuilder<HumanRecognitionMock>());
    registerHumanRecognitionInTypeBuilder(
                qi::ObjectTypeBuilder<HumanRecognitionNiceMock>());
    return true;
  }();
  boost::ignore_unused(typeWasRegistered);
}


void setCurrentIdentificationResult(
    boost::shared_ptr<HumanRecognitionMock> humanRecognition,
    const std::string& id)
{
  ON_CALL(*humanRecognition, learn(_,_,_,_,_,_,_,_,_))
      .WillByDefault(testing::Return(id));

  ON_CALL(*humanRecognition, get_identity(_,_,_,_,_,_,_,_))
      .WillByDefault(testing::Return(std::map<std::string, float>{{id, 1.0f}}));
}
