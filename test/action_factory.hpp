#ifndef SEMANTICAGENT_TEST_ACTION_FACTORY_HPP
#define SEMANTICAGENT_TEST_ACTION_FACTORY_HPP

#include <qi/semantic/agent/gmock/agent.hpp>


struct LookAtAction: qi::semantic::agent::gmock::PDDLAwareQiActionGMock
{
  LookAtAction();
  qi::Property<qi::knowledge::ResourceNode> object_to_look_at;
};


struct PointAtAction: qi::semantic::agent::gmock::PDDLAwareQiActionGMock
{
  PointAtAction();
  qi::Property<qi::knowledge::ResourceNode> object_to_point_at;
};


template <typename T>
struct ActionFactoryMocks
{
  boost::shared_ptr<qi::semantic::agent::gmock::PDDLAwareQiBehaviorGMock> behavior;
  boost::shared_ptr<qi::semantic::agent::gmock::PDDLAwareQiActionFactoryGMock> factory;
  boost::shared_ptr<T> action;
};

ActionFactoryMocks<LookAtAction> makeLookAtActionFactoryBehavior();
ActionFactoryMocks<PointAtAction> makePointAtActionFactoryBehavior();

#endif // SEMANTICAGENT_TEST_ACTION_FACTORY_HPP
