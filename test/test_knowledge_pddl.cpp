#include <gmock/gmock.h>
#include <boost/filesystem.hpp>
#include <qi/anymodule.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "../src/utility/knowledge.hpp"
#include "../src/knowledge_pddl.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "utility.hpp"
#include "debug_log_enabler.hpp"

qiLogCategory("Test.KnowledgePDDL");

using namespace qi::semantic::agent;
using namespace qi::knowledge;
using namespace qi::teaching;

class KnowledgePDDLTest: public ::testing::Test
{
protected:
  KnowledgePDDLTest()
    : _debugLogEnabler("SemanticAgent.KnowledgePDDL")
    , _knowledge(qi::import("knowledge_module").call<EditableKnowledgeBasePtr>(
                   "EditableKnowledgeBase", qi::os::mktmpdir("KnowledgeTest"), true))
    , _beliefs(_knowledge->editableKnowledgeGraph(beliefsDomain()))
    , _problemExtractor(_knowledge->knowledgeBase())
  {
    _beliefs->timestampEnabled.set(false).value();
  }

  DebugLogEnabler _debugLogEnabler;
  EditableKnowledgeBasePtr _knowledge;
  EditableKnowledgeGraphPtr _beliefs;
  KnowledgeProblemExtractor _problemExtractor;
};


qi::knowledge::Triple generateAgentTriple()
{
  return makeTripleFromNodes(
        makeUniqueResource(socialAgentPrefix()),
        rdfTypeResource(),
        socialAgentTypeNode());
};

TEST_F(KnowledgePDDLTest, SafeDestructionWhileRunning)
{
  _problemExtractor.run().then([&](qi::Future<void> f)
  {
    ASSERT_TRUE(f.isCanceled()) << futureToString(f);
  });
}

TEST_F(KnowledgePDDLTest, ProduceProblemsWithCurrentObjectsOnlyWhenRunning)
{
  // Put something in the knowledge.
  const auto person1 = generateAgentTriple();
  const auto person2 = generateAgentTriple();
  _beliefs->add(person1);
  _beliefs->add(person2);

  // Start extracting the problem from the knowledge.
  auto problemSource = _problemExtractor.worldState();
  auto problem = problemSource->get();
  EXPECT_EQ(nullptr, problem);
  auto waitingForProblem = waitForSignal(*problemSource);
  auto extracting = _problemExtractor.run();

  // At some point the problem should be updated and include the data from the knowledge.
  ASSERT_EQ(qi::FutureState_FinishedWithValue, waitingForProblem.waitFor(usualTimeout));
  problem = waitingForProblem.value();
  EXPECT_NE(nullptr, problem);
  ASSERT_NO_THROW(_problemExtractor.findPDDLNameFromKnowledge(person1.subject));
  ASSERT_NO_THROW(_problemExtractor.findPDDLNameFromKnowledge(person2.subject));

  // Then we can stop the extraction. The problem is reset.
  extracting.cancel();
  ASSERT_EQ(qi::FutureState_Canceled, extracting.waitFor(usualTimeout))
      << futureToString(extracting);
  problem = problemSource->get();
  EXPECT_EQ(nullptr, problem)
      << serializePddl(*problem);
}

TEST_F(KnowledgePDDLTest, LiveUpdateOfProblem)
{
  // What we will be looking for in the knowledge.
  const auto person1 = generateAgentTriple();
  const auto person2 = generateAgentTriple();

  // Start extracting the problem from the knowledge, but it is still empty.
  auto extracting = _problemExtractor.run();
  auto problemSource = _problemExtractor.worldState();
  auto problem = problemSource->get();
  EXPECT_EQ(nullptr, problem);

  // Now something appears in the knowledge.
  auto waitingForProblem = waitForSignal(*problemSource);
  _beliefs->add(person1);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, waitingForProblem.waitFor(usualTimeout));
  problem = waitingForProblem.value();
  EXPECT_NE(nullptr, problem);
  ASSERT_NO_THROW(_problemExtractor.findPDDLNameFromKnowledge(person1.subject).value());
  ASSERT_ANY_THROW(_problemExtractor.findPDDLNameFromKnowledge(person2.subject).value());

  // Something else appears in the knowledge
  waitingForProblem = waitForSignal(*problemSource);
  _beliefs->add(person2);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, waitingForProblem.waitFor(usualTimeout));
  problem = waitingForProblem.value();
  EXPECT_NE(nullptr, problem);
  ASSERT_NO_THROW(_problemExtractor.findPDDLNameFromKnowledge(person1.subject).value());
  ASSERT_NO_THROW(_problemExtractor.findPDDLNameFromKnowledge(person2.subject).value());

  // Something disappears from the knowledge.
  waitingForProblem = waitForSignal(*problemSource);
  _beliefs->remove(person1);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, waitingForProblem.waitFor(usualTimeout));
  problem = waitingForProblem.value();
  EXPECT_NE(nullptr, problem);
  ASSERT_ANY_THROW(_problemExtractor.findPDDLNameFromKnowledge(person1.subject).value());
  ASSERT_NO_THROW(_problemExtractor.findPDDLNameFromKnowledge(person2.subject).value());

  // Everything disappears from the knowledge.
  waitingForProblem = waitForSignal(*problemSource);
  _beliefs->remove(person2);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, waitingForProblem.waitFor(usualTimeout));
  problem = waitingForProblem.value();
  EXPECT_EQ(nullptr, problem);
  ASSERT_ANY_THROW(_problemExtractor.findPDDLNameFromKnowledge(person1.subject).value());
  ASSERT_ANY_THROW(_problemExtractor.findPDDLNameFromKnowledge(person2.subject).value());
}

TEST_F(KnowledgePDDLTest, CommunicationActResponses)
{
  auto eventResource = makeUniqueResource(eventPrefix());
  auto eventTriples = makeCommunicationActAddressedToSelf(eventResource);
  addTriples(_beliefs, eventTriples).value();

  const auto responseResource = makeUniqueResource(eventPrefix());
  auto responseTriples = makeRobotResponseToCommunicationAct(eventResource, responseResource);
  addTriples(_beliefs, responseTriples).value();

  // Start extracting the problem from the knowledge, after some time the problem is up-to-date.
  auto extracting = _problemExtractor.run();
  std::this_thread::sleep_for(stdUsualTimeout);
  auto description = _problemExtractor.worldState()->get();
  ASSERT_NE(nullptr, description);
  qiLogInfo() << "PDDL: " << serializePddl(*description);

  ASSERT_TRUE(description->problem);
  const auto& problem = description->problem->get();
  const auto& facts = problem->initialState.facts;
  EXPECT_EQ(3u, facts.size());
}


TEST_F(KnowledgePDDLTest, CommunicationActAddressees)
{
  auto eventResource = makeUniqueResource(eventPrefix());
  auto eventTriples = makeCommunicationActAddressedToSelf(eventResource);
  addTriples(_beliefs, eventTriples).value();

  const auto responseResource = makeUniqueResource(eventPrefix());
  auto responseTriples = makeRobotResponseToCommunicationAct(eventResource, responseResource);
  addTriples(_beliefs, responseTriples).value();

  // Start extracting the problem from the knowledge, after some time the problem is up-to-date.
  auto extracting = _problemExtractor.run();
  std::this_thread::sleep_for(stdUsualTimeout);
  auto description = _problemExtractor.respondToUnderstoodCommunicationActsGoal()->get();
  ASSERT_NE(nullptr, description);
  qiLogInfo() << "PDDL: " << serializePddl(*description);

  ASSERT_TRUE(description->problem);
  const auto& problem = description->problem->get();
  ASSERT_TRUE(problem->goal);
}
