#ifndef SEMANTICAGENT_TEST_OKAO_HPP
#define SEMANTICAGENT_TEST_OKAO_HPP

#include <gtest/gtest.h>
#include <okaowrapper/okaowrapper.hpp>
#include <qi/human/human.hpp>

class OkaoTest
{
public:

  OkaoTest();
  void learn(qi::human::HumanPtr human, std::string id);

  /**
   * \brief Recognizes a human from its HumanPtr using the mmhumanrecognition service.
   * \param human qi::HumanPtr of the human
   * \return identifier of type std::string as given in learnPerson() member function
   */
  std::string recognize(qi::human::HumanPtr human);

private:
  std::unique_ptr<AL::FaceAnalysis::OKAOWrapper> okaoWrapper; // okao FR

};

#endif // SEMANTICAGENT_TEST_OKAO_HPP
