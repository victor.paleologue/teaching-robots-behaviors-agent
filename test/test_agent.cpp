#include "test_agent.hpp"
#include <gmock/gmock.h>
#include <qi/application.hpp>
#include <qi/log.hpp>
#include <qi/path.hpp>
#include "../src/agent.hpp"
#include "test_chatbot_behavior.hpp"


qiLogCategory("SemanticAgent.Test");


int main(int argc, char** argv)
{
  qi::startEventLoop(12);
  qi::Application app{argc, argv};

  qi::path::ScopedDir tmpDir;
  qi::path::detail::setWritablePath(tmpDir.path().str());
  qiLogDebug() << "Directory used as the writable path: " << tmpDir.path().str();

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


AgentTest::AgentTest()
  : CommonSetup()
{
  _agent = boost::make_shared<qi::semantic::agent::AgentImpl>(_server);
  std::string serviceName = "SemanticAgent";
  _server->registerService(serviceName, _agent);
  _agentFromRemote = _client->service(serviceName);
}

AgentTest::~AgentTest()
{
  _agentFromRemote.reset();
  _agent.reset();
}
