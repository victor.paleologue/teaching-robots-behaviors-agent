#include "test_okao.hpp"
#include <okaowrapper/okaofacerecognition.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <okaowrapper/face.hpp>

OkaoTest::OkaoTest():
  okaoWrapper(AL::FaceAnalysis::OKAOWrapper::makeOKAOWrapperReal())
{
}


void OkaoTest::learn(qi::human::HumanPtr human, std::string id)
{
  // convert HumanPtr face picture to OpenCV image format
  auto image = cv::imdecode(human->_facePicture().value().wait(), cv::IMREAD_COLOR);

  // Learn face with Okao
  std::vector<AL::FaceAnalysis::Face> faces;
  if(okaoWrapper->detectFaces(image, &faces))
  {
    if (not okaoWrapper->learnFace(image, faces.front(), id))
    { qiLogError("Okao face learning failed."); }
  }
}



std::string OkaoTest::recognize(qi::human::HumanPtr human)
{
  auto image = cv::imdecode(human->_facePicture().value().wait(), cv::IMREAD_COLOR);
    // Recognize face with Okao
  std::vector<AL::FaceAnalysis::Face> faces;
  if(okaoWrapper->detectFaces(image, &faces))
  {
    if (not okaoWrapper->identifyFace(image, faces.front()))
      qiLogError("Okao face recognition failed.");
  }
  return faces.front().nameEstimation.name;
}
