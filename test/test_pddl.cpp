#include <future>
#include <qi/application.hpp>
#include <qi/log.hpp>
#include <pddl/AST.h>
#include <pddl/detail/ASTCopy.h>
#include "../src/pddl/pddl_edit.hpp"
#include "../src/pddl/pddl_goal.hpp"
#include "../src/pddl/pddl_read.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "access_test_pddl.hpp"
#include "debug_log_enabler.hpp"
#include "utility.hpp"
#include "test_pddl.hpp"

qiLogCategory("SemanticAgent.Test.PDDL");

using namespace qi::semantic::agent;


int main(int argc, char** argv)
{
  qi::Application app{argc, argv};
  qi::log::addFilter("SemanticAgent.*", qi::LogLevel_Debug);

  qi::path::ScopedDir tmpDir;
  qi::path::detail::setWritablePath(tmpDir.path().str());
  qiLogDebug() << "Directory used as the writable path: " << tmpDir.path().str();

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


class PddlTest: public testing::Test
{
protected:
  DebugLogEnabler _actionDebug{"SemanticAgent.PDDL"};
};


const pddl::ast::VariableDeclaration& findVariableDeclaration(
    const pddl::ast::VariableDeclarations& list,
    const std::string& name)
{
  const auto it = std::find_if(
        list.begin(), list.end(),
        [&](const pddl::ast::VariableDeclarationPointer& var) { return var->name == name; });

  if (list.end() == it)
  {
    std::stringstream message;
    message << "no variable declaration named \"" << name;
    message << "\" in " << static_cast<const void*>(&list);
    throw std::runtime_error(message.str());
  }
  else
  {
    return **it;
  }
}


TEST_F(PddlTest, ParsePDDLDomain)
{
  const auto domain = parsePddlDomain(actionPddl1);
  const auto& action = *domain->actions[0];
  EXPECT_EQ("enter", action.name);
  const auto& params = action.parameters;

  const auto& h = findVariableDeclaration(params, "h");
  EXPECT_EQ("hallway", h.type.value().get<std::unique_ptr<pddl::ast::PrimitiveType>>()->declaration->name);

  const auto& r = findVariableDeclaration(params, "r");
  EXPECT_EQ("room", r.type.value().get<std::unique_ptr<pddl::ast::PrimitiveType>>()->declaration->name);
}


TEST_F(PddlTest, ParsePDDLDomainWithRecursiveTypeDeclarationThrows)
{
  ASSERT_ANY_THROW(parsePddlDomain(domainTypeError1));
}


TEST_F(PddlTest, BuildDomainFromActions)
{
  const auto domains = [&]
  { // In C++17, can be factorized using a fold expression.
    std::vector<pddl::ast::DomainPointer> domains;
    domains.push_back(parsePddlDomain(actionPddl1));
    domains.push_back(parsePddlDomain(actionPddl2));
    return domains;
  }();

  auto domain = mergeDomains(domains.begin(), domains.end());
  EXPECT_EQ(mergedActions12, serializePddl(*domain));
}


TEST_F(PddlTest, splitAndReMergeGoalPreservesIt)
{
  auto description = readDomainAndProblem("communication_failure_1");
  auto goals = splitGoal(description->problem.value()->goal);
  ASSERT_EQ(2u, goals.size());
  auto goal = mergeGoals(goals, *description->domain);
  ASSERT_TRUE(isEquivalent(description->problem.value()->goal, goal));
}


TEST_F(PddlTest, badPDDLdoesNotCauseCrash1)
{
  ASSERT_ANY_THROW(readDomainAndProblem("parsing_failure_1"));
}
