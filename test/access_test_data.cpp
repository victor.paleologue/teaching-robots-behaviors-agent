#include "access_test_data.hpp"
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>


qi::Path pathToTestFile(const std::string& relativeFilePath)
{
  const qi::Path testDataPath = qi::path::findData("SemanticAgent", "test_data");
  if (!testDataPath.isDir())
    throw std::runtime_error("no test_data directory in SemanticAgent shared data");

  auto filePath = testDataPath / relativeFilePath;
  if (!filePath.isRegularFile())
    throw std::runtime_error(
        std::string("file \"") + relativeFilePath + "\" was not found in test data");

  return filePath;
}


std::string readTextFile(const qi::Path& path)
{
  boost::filesystem::fstream file;
  file.open(path.bfsPath(), std::ios::in);
  if (!file.is_open())
    throw std::runtime_error(
        std::string("failed to open file \"") + path.str() + "\" for reading");
  return std::string(
        std::istreambuf_iterator<char>(file),
        std::istreambuf_iterator<char>());
}


std::string readTextTestFile(const std::string& relativeFilePath)
{
  return readTextFile(pathToTestFile(relativeFilePath));
}
