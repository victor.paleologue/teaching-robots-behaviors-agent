#ifndef SEMANTICAGENT_TEST_USUAL_LOCALES_HPP
#define SEMANTICAGENT_TEST_USUAL_LOCALES_HPP

#include <qi/locale/locale.hpp>

static const qi::locale::Locale englishLocale{
  qi::locale::Language::English, qi::locale::Region::Unknown};
static const std::string englishLocaleCode = "en";

static const qi::locale::Locale frenchLocale{
  qi::locale::Language::French, qi::locale::Region::Unknown};
static const std::string frenchLocaleCode = "fr";

#endif // SEMANTICAGENT_TEST_USUAL_LOCALES_HPP
