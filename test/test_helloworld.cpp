#include <gtest/gtest.h>
#include "test/test_semantic_behaviors.hpp"
#include "../src/behaviors/helloworld.hpp"
#include <future>


qiLogCategory("SemanticAgent.Test.HelloWorldBehavior");


using HelloWorldTest = SemanticBehaviorsTest;


TEST_F(HelloWorldTest, TestHelloWorld)
{
  auto behavior = qi::semantic::agent::makeHelloworld(
        _speechSourceRemote, _speechEngineObj, _client, _contextObject, _semanticMemoryContext).value();

  runResponseForEachUtterance(
              behavior,
              std::vector<std::string>{"plop"},
              qi::locale::Language::English);
}

