#include "access_test_pddl.hpp"
#include "access_test_data.hpp"
#include "../src/pddl/pddl_io.hpp"

pddl::ast::DescriptionPointer readDomainAndProblem(const std::string& baseName)
{
  return qi::semantic::agent::parsePddlDomainAndProblem(
        readTextTestFile(baseName + "_domain.pddl") +
        readTextTestFile(baseName + "_problem.pddl"));
}
