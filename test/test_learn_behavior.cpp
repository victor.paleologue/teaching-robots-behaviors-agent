#include <gtest/gtest.h>
#include "test_semantic_behaviors.hpp"
#include "../src/behaviors/learn_behavior.hpp"


qiLogCategory("SemanticAgent.Test.LearnBehavior");


class LearnBehaviorTest: public SemanticBehaviorsTest
{
protected:
  LearnBehaviorTest()
    : _learnBehaviorLogs("SemanticAgent.LearnBehavior") {

  }

  void SetUp() override {
    _taughtBehaviors = std::make_shared<std::list<qi::semantic::agent::SemanticExpressionAssociation>>();
    _behavior = qi::semantic::agent::makeLearnBehavior(
          _speechSourceRemote, _speechEngineObj, _knowledge, _client, _contextObject, _semanticMemoryContext, _taughtBehaviors).value();
  }

  void TearDown() override {
    _taughtBehaviors->clear();
    _taughtBehaviors.reset();
  }

  DebugLogEnabler _learnBehaviorLogs;
  std::shared_ptr<std::list<qi::semantic::agent::SemanticExpressionAssociation>> _taughtBehaviors;
  qi::semantic::agent::PDDLAwareQiBehaviorPtr _behavior;
};


TEST_F(LearnBehaviorTest, LabelAssociationToSimpleRecipe)
{
  std::vector<std::string> inputs{
    "To deliver is to raise the right arm",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
  EXPECT_EQ(1u, _taughtBehaviors->size());
}

TEST_F(LearnBehaviorTest, LabelThenSimpleRecipeAssociation)
{
  std::vector<std::string> inputs{
    "To deliver",
    "is to raise the right arm",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, LabelThenSimpleRecipeAssociationBis)
{
  std::vector<std::string> inputs{
    "To welcome",
    "is to raise the right arm",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, LabelAssociationThenSimpleRecipe)
{
  std::vector<std::string> inputs{
    "To deliver is",
    "to raise the right arm",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, LabelAssociationThenDoubleRecipe)
{
  std::vector<std::string> inputs{
    "To deliver is",
    "to raise the right arm and to say hello",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, LabelAssociationToSimpleRecipeThenDoubleRecipe)
{
  std::vector<std::string> inputs{
    "To drink is to raise the right arm",
    "and to say cheers",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
  EXPECT_EQ(1u, _taughtBehaviors->size());
}

TEST_F(LearnBehaviorTest, LabelAssociationThenSimpleRecipeThenFinalRecipe)
{
  std::vector<std::string> inputs{
    "To deliver is",
    "to raise the right arm",
    "and to say hello",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, LabelAssociationThenSimpleRecipeAndThenFinalRecipe)
{
  std::vector<std::string> inputs{
    "To deliver is",
    "to raise the right arm and",
    "to say hello",
    "Yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, SimpleRecipeThenFinalRecipeThenLabelAssociation)
{
  std::vector<std::string> inputs{
    "To raise the right arm",
    "and to say hello",
    "is to greet"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, SimpleRecipeAndThenFinalRecipeThenLabelAssociation)
{
  std::vector<std::string> inputs{
    "To raise the right arm and",
    "to say hello",
    "is to greet"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, SimpleRecipeAndThenFinalRecipeAssociationThenLabel)
{
  std::vector<std::string> inputs{
    "To raise the right arm and",
    "to say hello is",
    "to greet"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, DISABLED_SimpleRecipeThenFinalRecipeAssociationThenLabel)
{
  std::vector<std::string> inputs{
    "To raise the right arm",
    "and to say hello is",
    "to greet"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, RecipeThenIsThenLabel)
{
  std::vector<std::string> inputs{
    "To raise the right arm",
    "and to say hello",
    "is",
    "to greet"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, LabelThenIsThenRecipe)
{
  std::vector<std::string> inputs{
    "To greet",
    "is",
    "to raise the right arm",
    "and to say hello",
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, RecipeThenIsThenWelcome)
{
  std::vector<std::string> inputs{
    "To raise the right arm",
    "and to say hello",
    "is"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));

  // Too bad, the behavior label was poorly utterred.
  inputs = std::vector<std::string>{
    "Welcome"
  };

  EXPECT_TRUE(expectNoResponseForUtterances(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, Rollback)
{
  std::vector<std::string> inputs{
    "To raise the right arm and",
    "to say goodbye",
    "no",
    "to say hello",
    "is to greet"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, ForgetIt)
{
  std::vector<std::string> inputs{
    "To raise the right arm and",
    "to say hello is",
    "forget it"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}

TEST_F(LearnBehaviorTest, DISABLED_StopILikeBananasIsNotATeaching)
{
  std::vector<std::string> inputs{
    "Stop",
    "I like bananas"
  };

  EXPECT_TRUE(expectNoResponseForUtterances(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, TryingToStopDoesNotCrash)
{
  std::vector<std::string> inputs{
    "To surprise",
    "Both arms",
    "And to say boot",
    "Stop this",
    "To surprise"
  };

  // We don't care about the result, all we want is this not to crash.
  expectNoResponseForUtterances(_behavior, inputs);
}

TEST_F(LearnBehaviorTest, NiceToMeetYouDoesNotCrash)
{
  std::vector<std::string> inputs{
    "Turn left",
    "Say hello",
    "Hello I'm Victor",
    "Nice to meet you"
  };

  EXPECT_TRUE(expectNoResponseForUtterances(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, JustNoDoesNotCrash)
{
  std::vector<std::string> inputs{
    "No"
  };

  runResponseForEachUtterance(_behavior, inputs, qi::locale::Language::English);
}


TEST_F(LearnBehaviorTest, GreatNoDoesNotCrash)
{
  std::vector<std::string> inputs{
    "Great",
    "No"
  };

  runResponseForEachUtterance(_behavior, inputs, qi::locale::Language::English);
}


TEST_F(LearnBehaviorTest, SomeTeachingDoesNotCrash)
{
  std::vector<std::string> inputs{
    "To reach is to say",
    "No",
    "Rich is to say",
    "No",
    "To reach",
    "Is to say",
    "Used to say",
    "Is to raise your left arm",
    "Is to raise your left arm",
    "To search",
    "And look ahead",
    "Search",
    "To help",
    "And move forward"
  };

  runResponseForEachUtterance(_behavior, inputs, qi::locale::Language::English);
}


TEST_F(LearnBehaviorTest, YetAnotherCuriousTeachingDoesNotCrash)
{
  std::vector<std::string> inputs{
    "Hello",
    "My name is Victor",
    "Move backwards",
    "To assist",
    "Is to turn around",
    "You can help",
    "No",
    "And to ask how you can head",
    "Yes",
    "No"
  };

  runResponseForEachUtterance(_behavior, inputs, qi::locale::Language::English);
}


TEST_F(LearnBehaviorTest, StuffInFrenchMustNotCrash)
{
  setLanguage("French");

  std::vector<std::string> inputs{
    "Accueillir",
    "C'est lever les bras",
    "Et dire bonjour",
    "Oui",
    "Bonjour",
    "Baisse la tête",
    "Lève la tête",
    "Baisser la tête",
    "Et dire que je suis désolé",
    "Say s'excuser",
    "S'excuser"
  };

  // We don't care about the result, all we want is this not to crash.
  expectNoResponseForUtterances(_behavior, inputs, qi::locale::Language::French);
}

// TODO: use parameters to factorize this test code
TEST_F(LearnBehaviorTest, OtherStuffInFrenchMustNotCrash)
{
  setLanguage("French");

  std::vector<std::string> inputs{
    "Lever les bras",
    "Lever le bras gauche",
    "Lever le bras droit"
  };

  // We don't care about the result, all we want is this not to crash.
  expectResponseForEachUtterance(_behavior, inputs, qi::locale::Language::French);
}


TEST_F(LearnBehaviorTest, TeachingInFrench)
{
  setLanguage("French");

  std::vector<std::string> inputs{
    "S'excuser",
    "C'est baisser la tête",
    "Et dire pardon",
    "Oui"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs, qi::locale::Language::French));
}


TEST_F(LearnBehaviorTest, RecompositionLowercasesFirstLetter)
{
  std::vector<std::string> inputs{
    "To say hello"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));

  // This awkward example happened in practice.
  // With a capital letter, "Meant" was seen as a proper noun, making it a complement of
  // previous proposition. "To greet" is then seen as a piece of recipe, whereas altogether
  // it should not have been recognized as a teaching.
  inputs = std::vector<std::string>{
    "Meant to greet"
  };

  EXPECT_TRUE(expectNoResponseForUtterances(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, ComposeParametrized1)
{
  std::vector<std::string> inputs{
    "To invite",
    "is to go to the seat",
    "and to say please sit here",
    "yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, ComposeParametrized2)
{
  std::vector<std::string> inputs{
    "To joke is to look at the box",
    "and then to say hi",
    "yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}


TEST_F(LearnBehaviorTest, ComposeParametrable)
{
  std::vector<std::string> inputs{
    "To show something",
    "is to go to it",
    "and then to point at it",
    "yes"
  };

  EXPECT_TRUE(expectResponseForEachUtterance(_behavior, inputs));
}
