#ifndef SEMANTICAGENT_TEST_USUAL_KNOWLEDGE_HPP
#define SEMANTICAGENT_TEST_USUAL_KNOWLEDGE_HPP

#include <qi/knowledge/knowledge.hpp>

// Creates and adds an object to the graph, with the desired label.
qi::Future<qi::knowledge::ResourceNode> createObject(
    qi::knowledge::EditableKnowledgeGraphPtr graph,
    std::vector<qi::knowledge::LocalizedString> labels);

// Creates and adds a default object: "the box"@en, "la boîte"@fr.
qi::Future<qi::knowledge::ResourceNode> createBoxObject(
    qi::knowledge::EditableKnowledgeGraphPtr graph);

// Creates and adds a default object: "chocolate"@en, "chocolat"@fr.
qi::Future<qi::knowledge::ResourceNode> createChocolateObject(
    qi::knowledge::EditableKnowledgeGraphPtr graph);

#endif // SEMANTICAGENT_TEST_USUAL_KNOWLEDGE_HPP
