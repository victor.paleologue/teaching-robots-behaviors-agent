#include <gmock/gmock.h>
#include <qi/jsoncodec.hpp>
#include "../src/pddl/pddl_goal.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "../src/rules.hpp"
#include "access_test_pddl.hpp"
#include "debug_log_enabler.hpp"

qiLogCategory("Test.Rules");

using namespace qi::semantic::agent;


class RulesTest: public ::testing::Test
{
protected:
  RulesTest()
    : _debugLogEnabler("SemanticAgent.Rules")
  {}

  DebugLogEnabler _debugLogEnabler;
};


TEST_F(RulesTest, enumerationOfTinyPrioritizedSubsets)
{
  std::set<int> set{1, 2};
  std::vector<int> tryToKeep{2};
  std::vector<std::set<int>> expected{{1, 2}, {2}, {1}};
  auto enumerated = qi::semantic::agent::enumeratePrioritizedSubSets(set, tryToKeep);
  EXPECT_EQ(expected, enumerated);
}


TEST_F(RulesTest, enumerationOfSmallPrioritizedSubsets)
{
  std::set<int> set{1, 2, 3};
  std::vector<int> tryToKeep{3, 2};
  std::vector<std::set<int>> expected{{1, 2, 3}, {2, 3}, {1, 3}, {3}, {1, 2}, {2}, {1}};
  auto enumerated = qi::semantic::agent::enumeratePrioritizedSubSets(set, tryToKeep);
  EXPECT_EQ(expected, enumerated);
}


TEST_F(RulesTest, enumerationOfPrioritizedSubsets)
{
  std::set<int> set{1, 2, 3, 4, 5};
  std::vector<int> tryToKeep{3, 1};
  std::set<int> expectKept(tryToKeep.begin(), tryToKeep.end());

  auto subsets = qi::semantic::agent::enumeratePrioritizedSubSets(set, tryToKeep);

  // There should be 2ⁿ - 1 (proper) subsets
  EXPECT_EQ(std::pow(2, set.size()) - 1, subsets.size())
      << qi::encodeJSON(subsets);

  for (const auto& subset: subsets)
  {
    qiLogInfo() << "Current Subset: " << qi::encodeJSON(subset);
    // Subsets are subsets!
    ASSERT_TRUE(std::includes(set.begin(), set.end(), subset.begin(), subset.end()));
  }
}


TEST_F(RulesTest, preferNotMisunderstoodCommunicationActs)
{
  auto description = readDomainAndProblem("communication_failure_1");
  const auto& goal = description->problem.value()->goal;
  auto goals = splitGoal(goal);
  ASSERT_EQ(2u, goals.size())
      << "Missplitted goal: " << goalsToString(goals);
  const pddl::ast::Goal* notMisunderstoodGoal;
  notMisunderstoodGoal = *std::find_if(goals.begin(), goals.end(), [&](const pddl::ast::Goal* goalPtr)
  {
    return serializePddl(*goalPtr) == "(not (was_misunderstood qiknowledge_event_event_ddf860ba))";
  });

  // There is only one preferred goal: do not misunderstand.
  auto preferredGoals = preferNotMisunderstoodCommunicationActs(goals, *description);
  ASSERT_EQ(1u, preferredGoals.size())
      << "Wrong number of preferred goals: " << goalsToString(preferredGoals);

  auto alternativeGoals = qi::semantic::agent::enumeratePrioritizedSubSets(goals, preferredGoals);
  ASSERT_EQ(3u, alternativeGoals.size())
      << "Mis-enumerated goals: " << goalsToString(alternativeGoals);
  for (const auto& alternative: alternativeGoals)
    ASSERT_NO_THROW(mergeGoals(alternative, *description->domain));
  EXPECT_TRUE(alternativeGoals[0].count(notMisunderstoodGoal)) << goalsToString(alternativeGoals);
  EXPECT_TRUE(alternativeGoals[1].count(notMisunderstoodGoal)) << goalsToString(alternativeGoals);
  EXPECT_FALSE(alternativeGoals[2].count(notMisunderstoodGoal)) << goalsToString(alternativeGoals);
}
