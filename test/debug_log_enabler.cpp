#include "debug_log_enabler.hpp"
#include <qi/log.hpp>


DebugLogEnabler::DebugLogEnabler(std::string category)
  : DebugLogEnabler(std::vector<std::string>{category})
{}


DebugLogEnabler::DebugLogEnabler(std::vector<std::string> categories)
  : _categories(std::move(categories))
{
  for (const auto& category: _categories)
    qi::log::addFilter(category, qi::LogLevel_Debug);
}


DebugLogEnabler::~DebugLogEnabler()
{
  for (const auto& category: _categories)
    qi::log::addFilter(category, qi::LogLevel_Info);
}
