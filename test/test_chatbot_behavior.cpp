#include <future>
#include <gmock/gmock.h>
#include <qi/log.hpp>
#include <qi/context/gmock/context.hpp>
#include <qi/conversation/gmock/conversation.hpp>
#include <qi/focus/gmock/focus.hpp>
#include <qi/signalspy.hpp>
#include "../src/behaviors/chatbot.hpp"
#include "../src/utility/setchange.hpp"
#include "test_chatbot_behavior.hpp"
#include "test_semantic_behaviors.hpp"

qiLogCategory("SemanticAgent.Test.ChatbotBehavior");

using namespace qi::semantic::agent;
using testing::_;

ChatbotAndReply makeMockChatbot()
{
  auto chatbot = boost::make_shared<qi::conversation::gmock::ChatbotGMock>();
  auto weakChatbot = boost::weak_ptr<qi::conversation::gmock::ChatbotGMock>(chatbot);
  auto reply = boost::make_shared<qi::conversation::gmock::ReplyReactionGMock>();
  reply->replyPriority.set(qi::conversation::ReplyPriority::Normal).value(); // default priority
  auto weakReply = boost::weak_ptr<qi::conversation::gmock::ReplyReactionGMock>(reply);
  auto reaction = boost::make_shared<qi::conversation::gmock::ChatbotReactionGMock>();
  auto weakReaction = boost::weak_ptr<qi::conversation::gmock::ChatbotReactionGMock>(reaction);

  ON_CALL(*chatbot, replyTo(_, _))
      .WillByDefault(testing::Invoke([=](qi::conversation::Phrase phrase, qi::locale::Locale)
  {
    ON_CALL(*reaction, runWith(_))
        .WillByDefault(testing::Invoke([=](qi::conversation::SpeechEnginePtr speechEngine)
    {
      auto reaction = weakReaction.lock();
      if (!reaction)
        throw std::runtime_error("chatbot reaction was destroyed");

      qiLogDebug("ChatbotBehavior.Test") << "Chatbot reaction run: " << static_cast<void*>(reaction.get());
      auto say = speechEngine->makeSay(phrase);
      qiLogDebug("ChatbotBehavior.Test") << "Say action ready for reaction: " << static_cast<void*>(reaction.get());
      return say->async().run();
    }));

    auto reaction = weakReaction.lock();
    if (!reaction)
      throw std::runtime_error("chatbot reaction was destroyed");

    auto reply = weakReply.lock();
    if (!reply)
      throw std::runtime_error("chatbot reply was destroyed");

    reply->chatbotReaction.set(qi::AnyObject{reaction}).value();
    return qi::Future<qi::conversation::ReplyReactionPtr>{qi::AnyObject{reply}};
  }));

  ON_CALL(*chatbot, acknowledgeHeard(_, _))
      .WillByDefault(testing::Return(qi::Future<void>(nullptr)));

  return {chatbot, reply, reaction};
};


class ChatbotBehaviorTest: public SemanticBehaviorsTest
{
protected:
  ChatbotBehaviorTest()
    : SemanticBehaviorsTest()
  {
    _speechEngine = makeMockSpeechEngine(_ttsSubscriber);
    _speechEngineObj = qi::AnyObject{_speechEngine};
  }

  ~ChatbotBehaviorTest()
  {
    _context.reset();
    _focusOwner.reset();
    _focusService.reset();
    _speechEngine.reset();
  }

  boost::shared_ptr<qi::conversation::gmock::SpeechEngineGMock> _speechEngine;
  qi::conversation::SpeechEnginePtr _speechEngineObj;
  DebugLogEnabler _debugLogs = {"SemanticAgent.Chatbot"};
};


TEST_F(ChatbotBehaviorTest, SingleChatbot)
{
  auto chatbotAndReply = makeMockChatbot();
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj,
        qi::AnyObject{chatbotAndReply.chatbot}).value();

  EXPECT_CALL(*chatbotAndReply.chatbot, replyTo(_, _)).Times(1);

  auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);
  auto phrase = "Cool story bro";
  informUserSaid(phrase);

  auto status = nextSuggestions.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  auto suggestions = std::get<0>(nextSuggestions.value());
  ASSERT_EQ(1u, suggestions.size());

  // Make sure the underlying reply is called.
  EXPECT_CALL(*chatbotAndReply.reaction, runWith(_)).Times(1);
  qi::SignalSpy saidSpy{_ttsSubscriber->signal};
  auto reply = *suggestions.begin();
  reply->context.set(_contextObject).value();
  reply->run();
  ASSERT_TRUE(saidSpy.waitUntil(1, usualTimeout).value());
  EXPECT_FALSE(saidSpy.waitUntil(2, usualTimeout).value());
  EXPECT_EQ(phrase, saidSpy.record(0).arg<std::string>(0));
}


TEST_F(ChatbotBehaviorTest, ChatbotReplyIsDroppedAfterReplyRepeatedly)
{
  auto chatbotAndReply = makeMockChatbot();
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj,
        qi::AnyObject{chatbotAndReply.chatbot}).value();

  ChangeTracker<ActionObj> suggestionChanges(
        chatbotBehavior->actionSuggestions.get().value());

  for (size_t i = 0; i < 3; ++i)
  {
    EXPECT_CALL(*chatbotAndReply.chatbot, replyTo(_, _)).Times(1);
    auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);

    auto phrase = "Cool story bro";
    informUserSaid(phrase);

    auto status = nextSuggestions.wait(usualTimeoutMs);
    ASSERT_EQ(qi::FutureState_FinishedWithValue, status)
        << futureToString(nextSuggestions);
    auto suggestions = std::get<0>(nextSuggestions.value());
    ASSERT_EQ(1u, suggestions.size());
    auto change = suggestionChanges.check(suggestions);
    EXPECT_EQ(1u, change.value.size());
    EXPECT_EQ(1u, change.inserted.size());
    EXPECT_EQ(0u, change.removed.size());

    nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);
    EXPECT_CALL(*chatbotAndReply.reaction, runWith(_)).Times(1);
    qi::SignalSpy saidSpy{_ttsSubscriber->signal};
    auto reply = *suggestions.begin();
    reply->context.set(_contextObject).value();
    reply->run();
    ASSERT_TRUE(saidSpy.waitUntil(1, usualTimeout).value());
    EXPECT_FALSE(saidSpy.waitUntil(2, usualTimeout).value());
    EXPECT_EQ(phrase, saidSpy.record(0).arg<std::string>(0));

    status = nextSuggestions.wait(usualTimeoutMs);
    ASSERT_EQ(qi::FutureState_FinishedWithValue, status)
        << futureToString(nextSuggestions);
    suggestions = std::get<0>(nextSuggestions.value());
    change = suggestionChanges.check(suggestions);
    EXPECT_EQ(0u, change.value.size());
    EXPECT_EQ(0u, change.inserted.size());
    EXPECT_EQ(1u, change.removed.size());
  }
}


TEST_F(ChatbotBehaviorTest, ChatbotReplyStaysUntilNextInputRepeatedly)
{
  auto chatbotAndReply = makeMockChatbot();
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj,
        qi::AnyObject{chatbotAndReply.chatbot}).value();

  ChangeTracker<ActionObj> suggestionChanges(
        chatbotBehavior->actionSuggestions.get().value());

  bool first = true;
  for (size_t i = 0; i < 3; ++i)
  {
    EXPECT_CALL(*chatbotAndReply.chatbot, replyTo(_, _)).Times(1);
    auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);

    auto phrase = "Cool story bro";
    informUserSaid(phrase);

    auto status = nextSuggestions.wait(usualTimeoutMs);
    ASSERT_EQ(qi::FutureState_FinishedWithValue, status)
        << futureToString(nextSuggestions);
    auto suggestions = std::get<0>(nextSuggestions.value());
    ASSERT_EQ(1u, suggestions.size());
    auto change = suggestionChanges.check(suggestions);
    EXPECT_EQ(1u, change.value.size());
    EXPECT_EQ(1u, change.inserted.size());
    if (first)
    {
      EXPECT_EQ(0u, change.removed.size());
      first = false;
    }
    else
    {
      EXPECT_EQ(1u, change.removed.size());
    }
  }
}


TEST_F(ChatbotBehaviorTest, FirstChatbotNormalReplyWins)
{
  auto chatbotAndReply1 = makeMockChatbot();
  auto chatbotAndReply2 = makeMockChatbot();
  auto chatbots = std::vector<qi::conversation::ChatbotPtr>{
      qi::AnyObject{chatbotAndReply1.chatbot},
      qi::AnyObject{chatbotAndReply2.chatbot}};
  auto chatbot = combineChatbots(chatbots);
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj, chatbot).value();

  // Check that only the first chatbot is requested a reply.
  EXPECT_CALL(*chatbotAndReply1.chatbot, replyTo(_, _)).Times(1);
  EXPECT_CALL(*chatbotAndReply2.chatbot, replyTo(_, _)).Times(0);

  auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);
  auto phrase = "Cool story bro";
  informUserSaid(phrase);

  auto status = nextSuggestions.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  auto suggestions = std::get<0>(nextSuggestions.value());
  ASSERT_EQ(1u, suggestions.size());
}


TEST_F(ChatbotBehaviorTest, SecondChatbotWinsIfFirstDontReply)
{
  auto chatbotAndReply1 = makeMockChatbot();
  auto chatbotAndReply2 = makeMockChatbot();
  auto chatbots = std::vector<qi::conversation::ChatbotPtr>{
      qi::AnyObject{chatbotAndReply1.chatbot},
      qi::AnyObject{chatbotAndReply2.chatbot}};
  auto chatbot = combineChatbots(chatbots);
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj, chatbot).value();

  // Check that both chatbots are requested a reply.
  EXPECT_CALL(*chatbotAndReply1.chatbot, replyTo(_, _)).Times(1)
      .WillOnce(testing::Throw(std::runtime_error("no reply")));
  EXPECT_CALL(*chatbotAndReply2.chatbot, replyTo(_, _)).Times(1);

  auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);
  auto phrase = "Cool story bro";
  informUserSaid(phrase);

  auto status = nextSuggestions.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  auto suggestions = std::get<0>(nextSuggestions.value());
  ASSERT_EQ(1u, suggestions.size());

  // Check that the second chatbot's reply was selected.
  EXPECT_CALL(*chatbotAndReply1.reaction, runWith(_)).Times(0);
  EXPECT_CALL(*chatbotAndReply2.reaction, runWith(_)).Times(1);
  EXPECT_CALL(*chatbotAndReply1.chatbot, acknowledgeHeard(_, _)).Times(1);
  EXPECT_CALL(*chatbotAndReply2.chatbot, acknowledgeHeard(_, _)).Times(0);

  auto reply = *suggestions.begin();
  reply->context.set(_contextObject).value();
  auto replying = reply->async().run();
  auto reactionStatus = replying.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, reactionStatus)
      << futureToString(replying);
}


TEST_F(ChatbotBehaviorTest, NormalReplyWinsOverFallback)
{
  auto chatbotAndReply1 = makeMockChatbot();
  chatbotAndReply1.reply->replyPriority.set(
        qi::conversation::ReplyPriority::Fallback).value();

  auto chatbotAndReply2 = makeMockChatbot();
  chatbotAndReply2.reply->replyPriority.set(
        qi::conversation::ReplyPriority::Normal).value();

  auto chatbots = std::vector<qi::conversation::ChatbotPtr>{
      qi::AnyObject{chatbotAndReply1.chatbot},
      qi::AnyObject{chatbotAndReply2.chatbot}};
  auto chatbot = combineChatbots(chatbots);
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj, chatbot).value();

  auto reaction1 = chatbotAndReply1.chatbot->replyTo({}, {}).value();
  ASSERT_EQ(qi::conversation::ReplyPriority::Fallback,
            reaction1->replyPriority.get().value());

  // Check that both chatbots are requested a reply.
  EXPECT_CALL(*chatbotAndReply1.chatbot, replyTo(_, _)).Times(1);
  EXPECT_CALL(*chatbotAndReply2.chatbot, replyTo(_, _)).Times(1);

  auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);
  auto phrase = "Cool story bro";
  informUserSaid(phrase);

  auto status = nextSuggestions.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  auto suggestions = std::get<0>(nextSuggestions.value());
  ASSERT_EQ(1u, suggestions.size());

  // Check that the second chatbot's reply was selected.
  EXPECT_CALL(*chatbotAndReply1.reaction, runWith(_)).Times(0);
  EXPECT_CALL(*chatbotAndReply2.reaction, runWith(_)).Times(1);
  EXPECT_CALL(*chatbotAndReply1.chatbot, acknowledgeHeard(_, _)).Times(1);
  EXPECT_CALL(*chatbotAndReply2.chatbot, acknowledgeHeard(_, _)).Times(0);
  auto reply = *suggestions.begin();
  reply->context.set(_contextObject).value();
  auto replying = reply->async().run();
  auto reactionStatus = replying.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, reactionStatus)
      << futureToString(replying);
}


TEST_F(ChatbotBehaviorTest, FirstFallbackWinIfNoNormal)
{
  auto chatbotAndReply1 = makeMockChatbot();
  chatbotAndReply1.reply->replyPriority.set(
        qi::conversation::ReplyPriority::Fallback).value();

  auto chatbotAndReply2 = makeMockChatbot();
  chatbotAndReply2.reply->replyPriority.set(
        qi::conversation::ReplyPriority::Fallback).value();

  auto chatbots = std::vector<qi::conversation::ChatbotPtr>{
      qi::AnyObject{chatbotAndReply1.chatbot},
      qi::AnyObject{chatbotAndReply2.chatbot}};
  auto chatbot = combineChatbots(chatbots);
  auto chatbotBehavior = makeChatbotBehavior(
        _client, _speechSourceRemote, _speechEngineObj, chatbot).value();

  // Check that both chatbots are requested a reply.
  EXPECT_CALL(*chatbotAndReply1.chatbot, replyTo(_, _)).Times(1);
  EXPECT_CALL(*chatbotAndReply2.chatbot, replyTo(_, _)).Times(1);

  auto nextSuggestions = waitForSignal(chatbotBehavior->actionSuggestions);
  auto phrase = "Cool story bro";
  informUserSaid(phrase);

  auto status = nextSuggestions.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, status);
  auto suggestions = std::get<0>(nextSuggestions.value());
  ASSERT_EQ(1u, suggestions.size());

  // Check that the first chatbot's reply was selected.
  EXPECT_CALL(*chatbotAndReply1.reaction, runWith(_)).Times(1);
  EXPECT_CALL(*chatbotAndReply2.reaction, runWith(_)).Times(0);
  EXPECT_CALL(*chatbotAndReply1.chatbot, acknowledgeHeard(_, _)).Times(0);
  EXPECT_CALL(*chatbotAndReply2.chatbot, acknowledgeHeard(_, _)).Times(1);
  auto reply = *suggestions.begin();
  reply->context.set(_contextObject).value();
  auto replying = reply->async().run();
  auto reactionStatus = replying.waitFor(usualTimeout);
  ASSERT_EQ(qi::FutureState_FinishedWithValue, reactionStatus)
      << futureToString(replying);
}
