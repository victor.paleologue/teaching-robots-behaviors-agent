#ifndef SEMANTICAGENT_TEST_DEBUG_LOG_ENABLER_HPP
#define SEMANTICAGENT_TEST_DEBUG_LOG_ENABLER_HPP

#include <string>
#include <vector>

/// An RAII-style object to enable debug logs of a category locally.
struct DebugLogEnabler
{
  /// Enables the debug logs for the given category.
  DebugLogEnabler(std::string category);

  /// Enables the debug logs for the given categories.
  DebugLogEnabler(std::vector<std::string> categories);

  /// Disables the logs the object enabled.
  ~DebugLogEnabler();

private:
  const std::vector<std::string> _categories;
};

#endif // SEMANTICAGENT_TEST_DEBUG_LOG_ENABLER_HPP
