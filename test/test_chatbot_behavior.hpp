#ifndef SEMANTICAGENT_TEST_CHATBOT_BEHAVIOR_HPP
#define SEMANTICAGENT_TEST_CHATBOT_BEHAVIOR_HPP

#include <gmock/gmock.h>
#include <boost/shared_ptr.hpp>
#include <qi/conversation/gmock/conversation.hpp>
#include "utility.hpp"

struct ChatbotAndReply
{
  boost::shared_ptr<qi::conversation::gmock::ChatbotGMock> chatbot;
  boost::shared_ptr<qi::conversation::gmock::ReplyReactionGMock> reply;
  boost::shared_ptr<qi::conversation::gmock::ChatbotReactionGMock> reaction;
};

/// Makes a chatbot that repeats what was said.
/// @return A structure aggregating every mock-up involved.
ChatbotAndReply makeMockChatbot();

#endif // SEMANTICAGENT_TEST_CHATBOT_BEHAVIOR_HPP
