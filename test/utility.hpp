#ifndef SEMANTICAGENT_TEST_UTILITY_HPP
#define SEMANTICAGENT_TEST_UTILITY_HPP

#include <chrono>
#include <future>
#include <sstream>
#include <stdexcept>
#include <string>

#include <qi/clock.hpp>
#include <qi/future.hpp>
#include <qi/session.hpp>

#include <qi/context/gmock/context.hpp>
#include <qi/conversation/gmock/conversation.hpp>
#include <qi/focus/gmock/focus.hpp>
#include <qi/humanawareness/gmock/humanawareness.hpp>
#include <qi/semantic/agent/gmock/agent.hpp>

#include "../src/behaviors/not_understood_fallback.hpp"
#include "mock.hpp"
#include "future_io.hpp"
#include "usual_locales.hpp"

/// A timeout factor shared by all tests, that is larger in debug than in release.
#ifdef NDEBUG
constexpr int timeoutFactor = 1;
#else
constexpr int timeoutFactor = 3;
#endif

/// A default timeout value.
constexpr unsigned usualTimeoutMs = 200 * timeoutFactor;
constexpr qi::MilliSeconds usualTimeout{usualTimeoutMs};
constexpr std::chrono::milliseconds stdUsualTimeout{usualTimeout.count()};

/// A larger timeout value.
constexpr unsigned largeTimeoutMs = 3000 * timeoutFactor;
constexpr qi::MilliSeconds largeTimeout{largeTimeoutMs};

/// A timeout value designed to include the "not understood fallback" response time.
constexpr unsigned qiFallbackAwareTimeoutMs =
    qi::semantic::agent::notUnderstoodFallbackTimeoutMs * 3 * timeoutFactor;
constexpr qi::MilliSeconds qiFallbackAwareTimeout{qiFallbackAwareTimeoutMs};
constexpr std::chrono::milliseconds stdFallbackAwareTimeout{qiFallbackAwareTimeout.count()};

/// A helper function for mock-up actions.
static const auto dummyActionFunction = [](qi::context::ContextPtr) {
  return qi::Future<void>{nullptr};
};

std::string generateUuid();

// Data structure for waiting for signals.
template <typename T>
struct SignalWaitData
{
  qi::Promise<T> promise;
  boost::signals2::connection connection;

  SignalWaitData<T>()
    : promise([this](qi::Promise<T>){ cancel(); })
    , connection()
  {}

  void cancel()
  {
    connection.disconnect();
    promise.setCanceled();
  }
};


/// A helper to wait for a signal.
template<typename T>
qi::Future<T> waitForSignal(boost::signals2::signal<void(const T&)>& sig)
{
  auto data = std::make_shared<SignalWaitData<T>>();
  data->connection =
      sig.connect([data](const T& value)
  {
    data->connection.disconnect();
    data->promise.setValue(value);
  });
  return data->promise.future();
}


/// Wait for a Qi Signal, and returns the result.
template<typename... Args>
qi::Future<std::tuple<typename std::decay<Args>::type...>>
waitForSignal(qi::SignalF<void(Args...)>& signal)
{
  auto link = std::make_shared<qi::SignalLink>(qi::SignalBase::invalidSignalLink);
  std::function<void()> disconnect = [&, link] {
    if (*link != qi::SignalBase::invalidSignalLink)
      signal.disconnect(*link);
  };

  qi::Promise<std::tuple<typename std::decay<Args>::type...>> promise(
        [&, link](qi::Promise<std::tuple<typename std::decay<Args>::type...>> p) {
    disconnect();
    p.setCanceled();
  });

  *link = signal.connect([=](Args... args) mutable {
    disconnect();
    promise.setValue(std::make_tuple(std::move(args)...));
  });

  return promise.future();
}


/// Wait for a void Qi Signal
qi::Future<void> waitForSignal(qi::SignalF<void()>& signal);


/// Logs when it is constructed, copied, moved, or destroyed.
/// Keeps a count, like a shared pointer.
/// Provides a future to track the destruction of the last instance.
struct CopyTracker
{
  CopyTracker();
  ~CopyTracker();

  CopyTracker(const CopyTracker&);
  CopyTracker& operator=(const CopyTracker&);

  CopyTracker(CopyTracker&&);
  CopyTracker& operator=(CopyTracker&&);

  std::uint64_t count();
  std::shared_future<void> destroyed();
  std::shared_future<void> chainDestroyed();

private:
  struct Private;
  std::unique_ptr<Private> _p;
};


/// Produce an ASR event data structure out of the given phrase.
qi::AnyValue makeASREvent(const std::string& phrase);


/// Produce an TTS event data structure out of the given phrase.
qi::AnyValue makeTTSEvent(const std::string& phrase);


/// Makes a speech engine that raises an event on the given TTS subscriber.
boost::shared_ptr<qi::conversation::gmock::SpeechEngineGMock> makeMockSpeechEngine(
    boost::shared_ptr<MemorySubscriberGMock> ttsSubscriber);


/// Run a function associated to a signal that should be emitted soon after running it.
/// Expect that signal to raise, and return the future whenever it occurs.
qi::Future<void> runFunctionAndWaitForStarted(
    qi::Signal<>& started,
    std::function<qi::Future<void>()>&& function);


/// Run a Qi Action as provided by Qi Lang proxies, and wait for it to start.
template <typename QiActionObjType>
qi::Future<void> runActionAndWaitForStarted(QiActionObjType&& action)
{
  return runFunctionAndWaitForStarted(
        action->started, [&]{ return action->async().run(); });
}


/// Run a task as provided by the semantic agent, and wait for it to start.
template <typename TaskPtrType>
qi::Future<void> runTaskAndWaitForStarted(TaskPtrType&& task)
{
  return runFunctionAndWaitForStarted(
        task->started, [&]{ return task->run(); });
}


/// To make a dummy speech extractor.
boost::shared_ptr<qi::semantic::agent::gmock::SpeechExtractorNiceGMock> makeMockSpeechExtractor();


struct IntendedException: std::exception
{
  const char* what() const noexcept override;
};

struct NotHeardException: std::exception
{
  const char* what() const noexcept override;
};


struct DistortedHearingException: std::exception
{
  DistortedHearingException(const char* expected, const char* observed);
  const char* what() const noexcept override;

private:
  const std::string _what;
};


struct NoSpokenResponseException: std::exception
{
  const char* what() const noexcept override;
};

/// Factorizes the setup with sessions, a focus service and a context owning it.
class FocusSetup:
    public testing::Test
{
protected:
  FocusSetup();
  virtual ~FocusSetup();

  std::shared_ptr<qi::Strand> _strand;
  qi::SessionPtr _server;
  qi::SessionPtr _client;
  boost::shared_ptr<qi::focus::gmock::FocusGMock> _focusService;
  boost::shared_ptr<qi::focus::gmock::FocusOwnerGMock> _focusOwner;
  boost::shared_ptr<qi::context::gmock::ContextGMock> _context;
  qi::context::ContextPtr _contextObject;
};


/// Factorizes the setup required to run a semantic agent.
/// Includes mockups for ALMemory, ALAnimatedSpeech, ALDialog.
class CommonSetup:
    public FocusSetup,
    public qi::Trackable<CommonSetup>
{
protected:
  CommonSetup();
  virtual ~CommonSetup();

  void hear(
      const std::string& toHear,
      std::function<void(const std::string&)> inputSpeech = {});

  // Simulates a user spoken input, retrieves the response of the robot.
  // Throws if a response was not received in time.
  template <typename DiscussionBasedActionType>
  std::string respond(
      DiscussionBasedActionType&& action,
      const std::string& userSay,
      std::function<void(const std::string&)> inputSpeech = {})
  {
    auto heardSpy = waitForSignal(action->heard);
    auto sayingSpy = waitForSignal(action->saying);
    qiLogInfo("SemanticAgent.Test") << "Input: " << userSay;

    hear(userSay, inputSpeech);

    if (qi::FutureState_Running == heardSpy.waitFor(usualTimeout))
      throw NotHeardException{};

    auto heard = std::get<0>(heardSpy.value()).text;
    if (userSay != heard)
      throw DistortedHearingException{userSay.c_str(), heard.c_str()};

    if (qi::FutureState_Running == sayingSpy.waitFor(qiFallbackAwareTimeout))
      throw NoSpokenResponseException{};

    auto response = std::get<0>(sayingSpy.value()).text;
    qiLogInfo("SemanticAgent.Test") << "Response: " << response;
    return response;
  }

  void setLanguage(const std::string& languageName);

  boost::shared_ptr<BasicAwarenessGMock> _basicAwareness;
  boost::shared_ptr<qi::humanawareness::gmock::HumanAwarenessNiceGMock> _humanAwareness;

  boost::shared_ptr<MemoryGMock> _memory;
  boost::shared_ptr<AnimatedSpeechGMock> _speech;
  qi::Future<void> _speaking;
  boost::shared_ptr<DialogGMock> _dialog;
  std::string _ttsLanguage;
  boost::shared_ptr<TextToSpeechGMock> _tts;
  boost::shared_ptr<MemorySubscriberGMock> _asrSubscriber;
  boost::shared_ptr<MemorySubscriberGMock> _asrEndSubscriber;
  boost::shared_ptr<MemorySubscriberGMock> _ttsSubscriber;

  boost::shared_ptr<qi::conversation::gmock::SpeechEngineGMock> _speechEngine;
  qi::conversation::SpeechEnginePtr _speechEngineObj;

  boost::shared_ptr<qi::conversation::gmock::ConversationGMock> _conversation;

  // A helper to wrap callbacks in the execution context of this behavior.
  template <typename Function>
  auto wrap(Function f) -> decltype(_strand->unwrappedSchedulerFor(track(f, this)))
  { return _strand->unwrappedSchedulerFor(track(f, this)); }
};

#endif // SEMANTICAGENT_TEST_UTILITY_HPP
