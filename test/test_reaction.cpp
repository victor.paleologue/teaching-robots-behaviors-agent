#include <gmock/gmock.h>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include <semanticnaoqilinker/semanticmemorylinker.hpp>
#include "test_semantic_behaviors.hpp"
#include "../src/behaviors/learn_behavior.hpp"
#include "../src/behaviors/reaction.hpp"
#include "../src/behaviors/semantic_behaviors.hpp"
#include "../src/utility/semantic_helpers.hpp"

qiLogCategory("SemanticAgent.Test.Reaction");

using namespace qisem;
using namespace qi::semantic::agent;

/// In this fixture, we have a minimal setup for semantic reasoning.
/// The memory will have basic primitive actions already available.
class ReactionTest: public testing::Test
{
protected:
  ReactionTest()
    : _linguisticDatabase(semanticNaoqiLinker::makeLinguisticDatabase())
    , _memory(qi::semantic::agent::makeMemoryKnowingCommands(*_linguisticDatabase))
  {}

  ~ReactionTest()
  {
    _learnedActions.clear();
  }

  std::unique_ptr<SemanticExpression> toSemantic(const std::string& userInput)
  {
    TextProcessingContext textContext{
          SemanticAgentGrounding::currentUser,
          SemanticAgentGrounding::me,
          SemanticLanguageEnum::ENGLISH}; // TODO: async this

    auto expression = converter::textToSemExp(userInput, textContext, *_linguisticDatabase);
    memoryOperation::mergeWithContext(expression, *_memory, *_linguisticDatabase); // improve text with robot's knowledge
    return expression->clone();
  }

  std::string makeText(const SemanticExpression& expression)
  {
    TextProcessingContext textContext{
      SemanticAgentGrounding::me,
      SemanticAgentGrounding::currentUser,
      SemanticLanguageEnum::ENGLISH};

    std::string text;
    converter::semExpToText(
          text, expression.clone(), textContext, false, *_memory, *_linguisticDatabase, nullptr);
    return text;
  }

  std::unique_ptr<SemanticExpression> learnAction(const std::string& userInput)
  {
    return qi::semantic::agent::learnAction(
          *toSemantic(userInput), _learnedActions, *_linguisticDatabase, *_memory);
  }

  std::shared_ptr<linguistics::LinguisticDatabase> _linguisticDatabase;
  std::shared_ptr<SemanticMemory> _memory;
  std::list<SemanticExpressionAssociation> _learnedActions;
};


TEST_F(ReactionTest, verbalGroupMatchesItself)
{
  auto verbalGroup = statementExpressionFromEnglishVerbLemma("change");
  ASSERT_TRUE(qi::semantic::agent::matchVerbalGroup(
                *verbalGroup, *verbalGroup, *_memory, *_linguisticDatabase));
}

TEST_F(ReactionTest, verbalGroupDoesNotMatchIfVerbDiffers)
{
  auto verbalGroup = statementExpressionFromEnglishVerbLemma("change");
  auto statementExpression = statementExpressionFromEnglishVerbLemma("smack");
  ASSERT_FALSE(qi::semantic::agent::matchVerbalGroup(
                 *statementExpression, *verbalGroup, *_memory, *_linguisticDatabase));
}

TEST_F(ReactionTest, verbalGroupMatchesEvenWithAnExtraComplement)
{
  auto verbalGroup = statementExpressionFromEnglishVerbLemma("change");
  auto statementExpression = verbalGroup->clone();
  auto objectGrounding = mystd::make_unique<SemanticTextGrounding>("the pitch");
  auto object = mystd::make_unique<GroundedExpression>(std::move(objectGrounding));
  statementExpression->children.emplace(GrammaticalType::OBJECT, std::move(object));
  ASSERT_TRUE(qi::semantic::agent::matchVerbalGroup(
                *statementExpression, *verbalGroup, *_memory, *_linguisticDatabase));
}

TEST_F(ReactionTest, applyVerbalGroupWithAnExtraComplement)
{
  auto verbalGroup = statementExpressionFromEnglishVerbLemma("smack");

  auto statementExpression = statementExpressionFromEnglishVerbLemma("change");
  auto objectGrounding = mystd::make_unique<SemanticTextGrounding>("the pitch");
  auto object = mystd::make_unique<GroundedExpression>(std::move(objectGrounding));
  statementExpression->children.emplace(GrammaticalType::OBJECT, std::move(object));

  ASSERT_FALSE(qi::semantic::agent::matchVerbalGroup
               (*statementExpression, *verbalGroup, *_memory, *_linguisticDatabase));

  qi::semantic::agent::applyVerbalGroup(*statementExpression, *verbalGroup);

  ASSERT_TRUE(qi::semantic::agent::matchVerbalGroup(
                *statementExpression, *verbalGroup, *_memory, *_linguisticDatabase));
}

TEST_F(ReactionTest, learnActionAliasUsingEquality)
{
  auto reaction = learnAction("To greet is to raise the right arm");
  ASSERT_TRUE(reaction != nullptr);
  ASSERT_FALSE(_learnedActions.empty());
}

TEST_F(ReactionTest, learnActionSequenceOfTwoUsingEquality)
{
  auto reaction = learnAction("To greet is to raise the right arm and to say hello");
  ASSERT_TRUE(reaction != nullptr);
  ASSERT_FALSE(_learnedActions.empty());

  // This check is not ideal because it is too fragile.
  // But it is better to be warned when this changes than to get surprised.
  auto textReaction = makeText(*reaction);
  ASSERT_EQ("Ok, to greet is to raise the right arm and to say hello.", textReaction);
}

TEST_F(ReactionTest, learnActionSequenceOfThree)
{
  auto reaction = learnAction("To greet is to raise the right arm, to say hello, and to lower the right arm");
  ASSERT_TRUE(reaction != nullptr);
  ASSERT_FALSE(_learnedActions.empty());
}

TEST_F(ReactionTest, learnComposition)
{
  auto reaction = learnAction("To greet is to raise the right arm and to say hello");
  ASSERT_TRUE(reaction != nullptr);
  ASSERT_EQ(1u, _learnedActions.size());
  reaction = learnAction("To welcome is to move forward and to greet");
  ASSERT_TRUE(reaction != nullptr);
  ASSERT_EQ(2u, _learnedActions.size());
}


TEST_F(ReactionTest, introspectPrimitiveAction)
{
  auto introspectionQuestion = toSemantic("what is to move forward");
  auto reaction = qi::semantic::agent::introspectAction(
        *introspectionQuestion, _learnedActions, *_linguisticDatabase, *_memory);
  ASSERT_EQ(nullptr, reaction); // TODO: have the robot demonstrate it
}

TEST_F(ReactionTest, introspectAlias)
{
  auto action = statementExpressionFromEnglishVerbLemma("sleep");
  auto alias = statementExpressionFromEnglishVerbLemma("die");
  _learnedActions.push_back({std::move(alias), std::move(action)});

  auto introspectionQuestion = toSemantic("what is to die");
  auto reaction = qi::semantic::agent::introspectAction(
        *introspectionQuestion, _learnedActions, *_linguisticDatabase, *_memory);
  ASSERT_TRUE(reaction != nullptr);
}

TEST_F(ReactionTest, introspectComposition)
{
  auto actionKnowledge = toSemantic("turn away and move forward");
  auto alias = statementExpressionFromEnglishVerbLemma("leave");
  _learnedActions.push_back({std::move(alias), actionKnowledge->clone()});

  actionKnowledge = toSemantic("say bye and leave");
  alias = statementExpressionFromEnglishVerbLemma("flee");
  _learnedActions.push_back({std::move(alias), actionKnowledge->clone()});

  auto introspectionQuestion = toSemantic("what is to flee");
  auto reaction = qi::semantic::agent::introspectAction(
        *introspectionQuestion, _learnedActions, *_linguisticDatabase, *_memory);
  ASSERT_TRUE(reaction != nullptr);
}
