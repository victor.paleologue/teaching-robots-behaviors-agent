#include "test_agent.hpp"
#include <gmock/gmock.h>
#include <qi/application.hpp>
#include <qi/log.hpp>
#include <qi/path.hpp>

qiLogCategory("SemanticAgent.Test");

int main(int argc, char** argv)
{
  qi::Application app{argc, argv};
  qi::log::addFilter("SemanticAgent.*", qi::LogLevel_Debug);

  qi::path::ScopedDir tmpDir;
  qi::path::detail::setWritablePath(tmpDir.path().str());
  qiLogDebug() << "Directory used as the writable path: " << tmpDir.path().str();

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
