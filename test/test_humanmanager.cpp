#include <gtest/gtest.h>

#include <qi/anymodule.hpp>
#include <qi/os.hpp>
#include <qi/humanawareness/gmock/humanawareness.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>

#include "../src/humanrecognizer.hpp"
#include "utility.hpp"

qiLogCategory("Test.SemanticAgent.HumanRecognizer");


using testing::_;
using namespace qi::human;
using namespace qi::knowledge;
using namespace  qi::teaching;

class HumanManagerTest : public CommonSetup
{   
protected:
  void SetUp()
  {
    _headFrame = qi::actuation::FramePtr();
    _human = boost::make_shared<qi::human::gmock::HumanNiceGMock>();
    _humanObj = qi::AnyObject(_human);
    _knowledge = [&]
    {
      auto knowledgeModule = qi::import("knowledge_module");
      auto knowledgePath = qi::os::mktmpdir("HumanManagerTest");
      return knowledgeModule.call<qi::knowledge::EditableKnowledgeBasePtr>(
            "EditableKnowledgeBase", knowledgePath, true);
    }();
    _recognizerGMock = std::make_shared<HumanRecognizerGMock>();
    _recognizer = _recognizerGMock;

    _humanManager =
        makeHumanManager(
          _strand, _contextObject, _client,
          qi::AnyObject(_humanAwareness), _knowledge, _recognizer);
  }

  void TearDown() {}

  qi::actuation::FramePtr _headFrame;
  boost::shared_ptr<qi::human::gmock::HumanGMock> _human;
  qi::Object<qi::human::Human> _humanObj;
  qi::knowledge::EditableKnowledgeBasePtr _knowledge;
  std::shared_ptr<HumanRecognizerGMock> _recognizerGMock;
  std::shared_ptr<qi::semantic::agent::HumanRecognizer> _recognizer;
  boost::shared_ptr<qi::semantic::agent::HumanManager> _humanManager;
};


TEST_F(HumanManagerTest, gettingResourceOfEmptyHumanThrows)
{
  ASSERT_ANY_THROW(_humanManager->getResNode(qi::human::HumanPtr()));
}


TEST_F(HumanManagerTest, addHumanToKnowledgeAndReturnsResNode)
{
  // The human is given a name.
  const auto humanObj = qi::Object<Human>(qi::AnyObject(boost::make_shared<gmock::HumanGMock>()));
  const auto humanName = "someone";

  EXPECT_CALL(*_recognizerGMock, learn(_, _)).
      Times(testing::AtLeast(1)).
      WillRepeatedly(testing::Return(qi::Future<bool>(true)));

  EXPECT_CALL(*_recognizerGMock, identify(_)).
      Times(testing::AtLeast(1)).
      WillRepeatedly(testing::Return(qi::Future<std::string>(humanName)));

  auto humanNode = _humanManager->getResNode(humanObj);
  ASSERT_TRUE(isNodeValid(humanNode) && !isNodeEmpty(humanNode));

  // check the knowledge for the node
  ASSERT_TRUE(
        _knowledge->knowledgeBase()->contains(
          makeTripleFromResources(
            humanNode, rdfTypeResource(), socialAgentTypeResource()), ""));
}


TEST_F(HumanManagerTest, getHumanPtrTest)
{
  const auto humanName = "someone";
  _humanAwareness->humansAround.set({_humanObj}).value();

  EXPECT_CALL(*_recognizerGMock, learn(_, _)).
      Times(testing::AtLeast(1)).
      WillRepeatedly(testing::Return(qi::Future<bool>(true)));

  EXPECT_CALL(*_recognizerGMock, identify(_)).
      Times(testing::AtLeast(1)).
      WillRepeatedly(testing::Return(qi::Future<std::string>(humanName)));

  auto resNode = _humanManager->getResNode(_humanObj);
  auto foundHuman = _humanManager->getHumanPtr(resNode);
  ASSERT_TRUE(foundHuman);
  EXPECT_EQ(_humanObj, foundHuman);
}


TEST_F(HumanManagerTest, sameHumanIsAssociatedToSameNode)
{
  const auto humanName = "someone";
  _humanAwareness->humansAround.set({_humanObj}).value();

  EXPECT_CALL(*_recognizerGMock, learn(_, _)).
      Times(testing::AtLeast(1)).
      WillRepeatedly(testing::Return(qi::Future<bool>(true)));

  EXPECT_CALL(*_recognizerGMock, identify(_)).
      Times(testing::AtLeast(1)).
      WillRepeatedly(testing::Return(qi::Future<std::string>(humanName)));

  auto resnode1 = _humanManager->getResNode(_humanObj);
  auto resnode2 = _humanManager->getResNode(_humanObj);
  EXPECT_EQ(resnode1.url, resnode2.url);
}


TEST_F(HumanManagerTest, humanLearnedAndLostAndFoundAndRecognized)
{
  // human is not added to HumansAround so that it will not stay in HumanManager memory
  EXPECT_CALL(*_recognizerGMock, identify(_)).Times(2).WillRepeatedly(
        testing::Return(qi::Future<std::string>("1")));
  EXPECT_CALL(*_recognizerGMock, learn(_, _)).Times(2).WillRepeatedly(
        testing::Return(qi::Future<bool>(true)));
  auto resnode1 = _humanManager->getResNode(_humanObj); // create resnode
  // creates the node again, because the human was not kept in memory
  auto resnode2 = _humanManager->getResNode(_humanObj);
  EXPECT_NE(resnode1.url, resnode2.url);
}
