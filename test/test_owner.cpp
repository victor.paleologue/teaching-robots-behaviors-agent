#include <gmock/gmock.h>
#include <qi/future.hpp>
#include "../src/utility/owner.hpp"
#include "utility.hpp"
#include "debug_log_enabler.hpp"

using qi::semantic::agent::CustomOwner;

class OwnerTest: public testing::Test
{
protected:
  DebugLogEnabler _debug{"SemanticAgent.Owner"};
};


TEST_F(OwnerTest, OwnerRelease)
{
  bool calledMe = false;
  CustomOwner::ReleaseFunction callMe = [&]{ calledMe = true; return qi::Future<void>{nullptr}; };
  CustomOwner owner(std::move(callMe));
  EXPECT_FALSE(calledMe);
  auto waitingForReleased = owner.waitForReleased();
  auto releasing = owner.release();
  EXPECT_EQ(qi::FutureState_FinishedWithValue, releasing.waitFor(usualTimeout))
      << futureToString(releasing);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, waitingForReleased.waitFor(usualTimeout))
      << futureToString(waitingForReleased);
  EXPECT_TRUE(calledMe);
}


TEST_F(OwnerTest, OwnerDestruction)
{
  bool calledMe = false;
  CustomOwner::ReleaseFunction callMe = [&]{ calledMe = true; return qi::Future<void>{nullptr}; };
  auto owner = std::make_unique<CustomOwner>(std::move(callMe));
  EXPECT_FALSE(calledMe);
  auto waitingForReleased = owner->waitForReleased();
  owner.reset();
  EXPECT_EQ(qi::FutureState_FinishedWithValue, waitingForReleased.waitFor(usualTimeout))
      << futureToString(waitingForReleased);
  EXPECT_TRUE(calledMe);
}
