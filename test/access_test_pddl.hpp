#ifndef SEMANTICAGENT_TEST_PATH_TO_TEST_PDDL_HPP
#define SEMANTICAGENT_TEST_PATH_TO_TEST_PDDL_HPP

#include <string>
#include <pddl/AST.h>

/// Will read <baseName>_domain.pddl and <baseName>_problem.pddl,
/// parse them, and return a PDDL description.
pddl::ast::DescriptionPointer readDomainAndProblem(const std::string& baseName);

#endif // SEMANTICAGENT_TEST_PATH_TO_TEST_PDDL_HPP
