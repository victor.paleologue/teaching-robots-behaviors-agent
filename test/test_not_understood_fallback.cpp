#include <future>
#include <gtest/gtest.h>
#include <ka/scoped.hpp>
#include "../src/behaviormodel/action.hpp"
#include "../src/behaviormodel/behavior.hpp"
#include "../src/behaviors/not_understood_fallback.hpp"
#include "../src/utility/disposable.hpp"
#include "test_agent.hpp"
#include "test_semantic_behaviors.hpp"

qiLogCategory("SemanticAgent.Test.NotUnderstood");

using namespace qi::semantic::agent;

class NotUnderstoodFallbackTest: public SemanticBehaviorsTest
{
protected:
  NotUnderstoodFallbackTest()
    : SemanticBehaviorsTest()
    , _notUnderstoodDebug("SemanticAgent.NotUnderstood")
    , _knowledgeDebug("SemanticAgent.Knowledge")
    , _notUnderstoodBehavior()
  {}

  void SetUp() override
  {
    _notUnderstoodBehavior =
        makeNotUnderstoodFallbackBehavior(
            _speechSourceRemote, _speechEngineObj, _client, _contextObject, _semanticMemoryContext,
          _knowledge->knowledgeBase(), _beliefs)
        .value();
  }

  void TearDown() override
  {
  }

  using CommonSetup::wrap;

  DebugLogEnabler _notUnderstoodDebug;
  DebugLogEnabler _knowledgeDebug;
  qi::Future<void> _behaviorRunning;
  PDDLAwareQiBehaviorPtr _notUnderstoodBehavior;
};


TEST_F(NotUnderstoodFallbackTest, DoNotSuggestAnyThingIfNothingHappens)
{
  // No callback should occur.
  auto suggestionLink =
      _notUnderstoodBehavior->actionSuggestions.connect([](const ActionObjs&)
  {
    FAIL() << "Suggestions unexpectedly changed";
  });

  auto scoped = ka::scoped([&]
  {
    _notUnderstoodBehavior->actionSuggestions.disconnect(suggestionLink);
  });
  // It should run quietly because nothing happens.
}


TEST_F(NotUnderstoodFallbackTest, SuggestFallbackWhenSomethingIsSaid)
{
  // Track the callback.
  std::promise<void> p;
  auto suggestionConnection =
      _notUnderstoodBehavior->actionSuggestions.connect(wrap([&](const ActionObjs& suggestions)
  {
    EXPECT_EQ(1u, suggestions.size()); // it should be our "not understood"
    p.set_value();
  }));

  auto scoped = ka::scoped([=]
  {
    _notUnderstoodBehavior->actionSuggestions.disconnect(suggestionConnection);
  });

  // When something is said, expect a call.
  informUserSaid("life is life");
  auto status = p.get_future().wait_for(stdFallbackAwareTimeout);
  EXPECT_EQ(status, std::future_status::ready);
}


TEST_F(NotUnderstoodFallbackTest, FallbackSuggestionIsClearedAfterBeingRun)
{
  ActionObj suggestedAction;

  // Track the callback.
  {
    std::promise<void> p;
    auto suggestionConnection =
        _notUnderstoodBehavior->actionSuggestions.connect(wrap([&](const ActionObjs& suggestions)
    {
      ASSERT_EQ(1u, suggestions.size()); // it should be our "not understood"
      suggestedAction = *suggestions.begin();
      p.set_value();
    }));

    auto scoped = ka::scoped([=]
    {
      _notUnderstoodBehavior->actionSuggestions.disconnect(suggestionConnection);
    });

    // When something is said, expect a call.
    informUserSaid("life is life");
    auto status = p.get_future().wait_for(stdFallbackAwareTimeout);
    ASSERT_EQ(status, std::future_status::ready);
  }

  {
    // Run the action, and check the suggestions are empty again.
    std::promise<void> p;
    auto suggestionConnection =
        _notUnderstoodBehavior->actionSuggestions.connect(wrap([&](const ActionObjs& suggestions)
    {
      EXPECT_EQ(0u, suggestions.size()); // it should be our "not understood"
      EXPECT_NO_THROW(p.set_value()); // checks that it is called only once
    }));

    auto scoped = ka::scoped([=]
    {
      _notUnderstoodBehavior->actionSuggestions.disconnect(suggestionConnection);
    });

    suggestedAction->run();
    auto status = p.get_future().wait_for(stdUsualTimeout);
    EXPECT_EQ(status, std::future_status::ready);
  }
}


TEST_F(NotUnderstoodFallbackTest, StopSuggestingFallbackWhenInputWasRespondedTo)
{
  auto inputEvent = [&]
  {
    // Track callback to prepare the next step.
    std::promise<void> p;
    auto suggestionConnection =
        _notUnderstoodBehavior->actionSuggestions.connect([&](const ActionObjs&)
    {
      p.set_value();
    });

    Disposables disposables;
    disposables.add(makeDisposable([=]
    {
      _notUnderstoodBehavior->actionSuggestions.disconnect(suggestionConnection);
    }));

    // Something is said.
    auto inputEvent = informUserSaid("life is life");

    auto status = p.get_future().wait_for(stdFallbackAwareTimeout);
    EXPECT_EQ(status, std::future_status::ready);
    disposables.dispose().value();

    return inputEvent;
  }();

  {
    // Track the callback for real.
    auto p = std::promise<void>{};
    auto suggestionConnection =
        _notUnderstoodBehavior->actionSuggestions.connect(wrap([&](const ActionObjs& suggestions)
    {
      EXPECT_TRUE(suggestions.empty()); // No suggestion should be remaining
      p.set_value();
    }));

    Disposables disposables;
    disposables.add(makeDisposable([=]
    {
      _notUnderstoodBehavior->actionSuggestions.disconnect(suggestionConnection);
    }));

    // A response is produced by the robot.
    informRobotRespondedTo(std::move(inputEvent));

    // Suggestions are removed.
    auto status = p.get_future().wait_for(stdFallbackAwareTimeout);
    EXPECT_EQ(status, std::future_status::ready);
  }
}
