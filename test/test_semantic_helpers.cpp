#include "test_semantic_helpers.hpp"
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include "../src/utility/semantic_helpers.hpp"


using namespace qisem;


SemanticTest::SemanticTest()
  : _linguisticDatabase(semanticNaoqiLinker::makeLinguisticDatabase())
  , _memory(new SemanticMemory{})
{}

qisem::TextProcessingContext SemanticTest::userToMe{
      qisem::SemanticAgentGrounding::currentUser,
      qisem::SemanticAgentGrounding::me,
      qisem::SemanticLanguageEnum::ENGLISH};

qisem::TextProcessingContext SemanticTest::meToUser{
      qisem::SemanticAgentGrounding::me,
      qisem::SemanticAgentGrounding::currentUser,
      qisem::SemanticLanguageEnum::ENGLISH};

std::unique_ptr<SemanticExpression> SemanticTest::phraseToSemantic(
    const std::string& phrase,
    TextProcessingContext context)
{
  return converter::textToSemExp(phrase, context, *_linguisticDatabase)->clone();
}

std::string SemanticTest::semanticToPhrase(
    const SemanticExpression& expression,
    TextProcessingContext context)
{
  std::string text;
  converter::semExpToText(
        text, expression.clone(), context, false, *_memory, *_linguisticDatabase, nullptr);
  return text;
}


using SemanticHelpersTest = SemanticTest;


TEST_F(SemanticHelpersTest, actionDeclarationIsActionDeclaration)
{
  std::string phrase{"to crumble"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionStatementGroundingPtr = qi::semantic::agent::asActionDeclaration(*semantic);
  ASSERT_TRUE(actionStatementGroundingPtr != nullptr);
  ASSERT_EQ(actionStatementGroundingPtr->word.lemma, "crumble");
}


TEST_F(SemanticHelpersTest, actionDeclarationWithComplementIsActionDeclaration)
{
  std::string phrase{"to crumble to the sea"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionStatementGroundingPtr = qi::semantic::agent::asActionDeclaration(*semantic);
  ASSERT_TRUE(actionStatementGroundingPtr != nullptr);
  ASSERT_EQ(actionStatementGroundingPtr->word.lemma, "crumble");
}


TEST_F(SemanticHelpersTest, listOfActionsIsNotActionDeclaration)
{
  std::string phrase{"to crumble and to sing"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionStatementGroundingPtr = qi::semantic::agent::asActionDeclaration(*semantic);
  ASSERT_FALSE(actionStatementGroundingPtr != nullptr);
}


TEST_F(SemanticHelpersTest, listOfActionsIsListOfActions)
{
  std::string phrase{"to crumble and to sing"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionStatementGroundingPtr = qi::semantic::agent::asActionDeclarationList(*semantic);
  ASSERT_TRUE(actionStatementGroundingPtr != nullptr);
}


TEST_F(SemanticHelpersTest, actionDeclarationIsNotListOfActions)
{
  std::string phrase{"to crumble"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionListExpression = qi::semantic::agent::asActionDeclarationList(*semantic);
  ASSERT_EQ(nullptr, actionListExpression);
}


TEST_F(SemanticHelpersTest, unfinishedListOfActionsIsListOfActions)
{
  std::string phrase{"to crumble and"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionListExpression = qi::semantic::agent::asActionDeclarationList(*semantic);
  ASSERT_TRUE(actionListExpression != nullptr);
}


TEST_F(SemanticHelpersTest, unstartedListOfActionsIsListOfActions)
{
  std::string phrase{"and to sing"};
  auto semantic = phraseToSemantic(phrase, userToMe);
  auto* actionListExpression = qi::semantic::agent::asActionDeclarationList(*semantic);
  ASSERT_TRUE(actionListExpression != nullptr);
}

TEST_F(SemanticHelpersTest, DISABLED_notListsOfActions)
{
  std::vector<std::string> phrases;
  phrases.emplace_back("And to say hello No No No No Stop No No No No Stop No No Stop No Stop I like bananas No No No No Stop No No No No Stop No No Stop No Stop I like bananas No No No No Stop No No Stop No Stop I like bananas No No Stop No No Stop No Stop I like bananas No No Stop No Stop I like bananas No Stop No Stop I like bananas No Stop I like bananas Stop I like bananas What do I like");
  for (const auto& phrase: phrases)
  {
    auto semantic = phraseToSemantic(phrase, userToMe);
    auto* actionListExpression = qi::semantic::agent::asActionDeclarationList(*semantic);
    EXPECT_FALSE(actionListExpression) << "\"" << phrase << "\" erroneously considered an action declaration list";
  }
}
