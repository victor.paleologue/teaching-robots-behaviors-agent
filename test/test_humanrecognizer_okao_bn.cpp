#include <gmock/gmock.h>

#include <boost/filesystem/path.hpp>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include <okaowrapper/face.hpp>
#include <okaowrapper/okaoface.hpp>
#include <okaowrapper/okaofacedetection.hpp>
#include <okaowrapper/okaowrapper.hpp>

#include <qi/anyobject.hpp>
#include <qi/application.hpp>
#include <qi/log.hpp>
#include <qi/path.hpp>
#include <qi/periodictask.hpp>

#include <qi/actuation/gmock/actuation.hpp>
#include <qi/actuation/gmock/frame.hpp>
#include <qi/human/gmock/human.hpp>

#include "access_test_data.hpp"
#include "debug_log_enabler.hpp"
#include "humanrecognition_mock.hpp"
#include "../src/humanrecognition_helpers.hpp"
#include "../src/humanrecognizer_okao_bn.hpp"

qiLogCategory("Test.SemanticAgent.HumanRecognizer");

using namespace qi::semantic::agent;
using testing::_;

namespace
{
/// Gets the raw image data from a file name relative to the test data path.
cv::Mat imageFromTestData(const std::string& imageFileName)
{
  const auto imageFilePath = pathToTestFile(imageFileName);
  auto imageRaw = cv::imread(imageFilePath.bfsPath().native());
  qiLogInfo() << "Read face image " << imageFileName
              << " (size " << imageRaw.cols << "x" << imageRaw.rows << ")";
  return imageRaw;
}


/// Gets the PNG data from a file name relative to the test data path.
std::vector<qi::uint8_t> pngDataFromTestData(const std::string& imageFileName)
{
  // KLUDGE: I failed to transmit the .png data directly,
  // so we re-encode it after decoding the image.
  auto imageRaw = imageFromTestData(imageFileName);
  std::vector<qi::uint8_t> imagePng;
  const std::vector<int> encodingParams{cv::IMWRITE_PNG_COMPRESSION, 0};
  cv::imencode(".png", imageRaw, imagePng, encodingParams);
  return imagePng;
}


/// Helper to replace the face of the human with the given PNG data.
void setFacePicture(
    boost::shared_ptr<qi::human::gmock::HumanNiceGMock> human,
    const std::vector<qi::uint8_t> facePng)
{
  // The data will read and be wrapped in a Qi structure.
  qi::image::TimestampedImage faceTimestampedImage;
  faceTimestampedImage.image.data = qi::AnyValue(qi::typeOf<qi::Buffer>());

  // KLUDGE: libQi does not allow to read directly to the Qi structure, this copy is inevitable
  faceTimestampedImage.image.data.setRaw(
        reinterpret_cast<const char*>(facePng.data()),
        facePng.size());
  faceTimestampedImage.time = qi::Clock::now();

  human->facePicture.set(std::move(faceTimestampedImage)).value();
  qiLogInfo() << "Updated face picture";
}


/// Helper to replace the face of the human with an "empty" picture.
void setNoFacePicture(
    boost::shared_ptr<qi::human::gmock::HumanNiceGMock> human)
{
  std::vector<qi::uint8_t> facePng;
  const std::vector<int> params_encoding{cv::IMWRITE_PNG_COMPRESSION, 0};
  cv::imencode(".png",
               cv::Mat::zeros(cv::Size(1, 1), CV_8U), // 1x1 Matrix of zero
               facePng,
               params_encoding);

  setFacePicture(human, facePng);
  qiLogInfo() << "Mock human has no face";
}


/// Helper to replace the face of the human with the content of a test file data.
void setFacePictureFromTestData(
    boost::shared_ptr<qi::human::gmock::HumanNiceGMock> human,
    const std::string& faceFileName)
{
  setFacePicture(human, pngDataFromTestData(faceFileName));
  qiLogInfo() << "Mock human has face " << faceFileName;
}


/// Make a dummy human, 1 meter away, with no face.
boost::shared_ptr<qi::human::gmock::HumanNiceGMock> makeMockHuman()
{
  auto headFrame = boost::make_shared<qi::actuation::gmock::FrameNiceGMock>();
  qi::Object<qi::actuation::Frame> headFrameObj = qi::AnyObject(headFrame);

  qi::geometry::TransformTime transformToHuman{{{}, {1, 0, 0}}, qi::Clock::now()};

  ON_CALL(*headFrame, computeTransform(_))
      .WillByDefault(testing::Return(qi::Future<qi::geometry::TransformTime>(transformToHuman)));

  auto human = boost::make_shared<qi::human::gmock::HumanNiceGMock>();
  human->headFrame.set(headFrameObj).value();
  setNoFacePicture(human);
  return human;
}


/// Alternate a list of face pictures in the background.
qi::Future<void> swapFacePicturesRegularly(
    std::shared_ptr<qi::Strand> strand,
    boost::shared_ptr<qi::human::gmock::HumanNiceGMock> human,
    const std::vector<std::string>& faceFiles)
{
  auto facePngsPtr = std::make_shared<std::vector<std::vector<qi::uint8_t>>>();
  std::transform(
        faceFiles.begin(), faceFiles.end(),
        std::back_inserter(*facePngsPtr),
        pngDataFromTestData);

  auto task = std::make_shared<qi::PeriodicTask>();
  task->setName("face swap");
  task->setPeriod(qi::Seconds(1));

  size_t index = 0u;
  task->setCallback([=]() mutable
  {
    index = (index + 1) % facePngsPtr->size();
    qiLogInfo() << "Setting face #" << index;
    setFacePicture(human, facePngsPtr->at(index));
  });

  task->start();
  qi::Promise<void> p([=](qi::Promise<void>& p)
  {
    task->stop();
    p.setCanceled();
  });

  p.future().then([p](qi::Future<void>){});
  return p.future();
}

} // end of anonymous namespace

class HumanRecognitionOkaoBnTest: public testing::Test
{
protected:
  HumanRecognitionOkaoBnTest()
    : _strand(std::make_shared<qi::Strand>()),
      _server(qi::makeSession()),
      _client(qi::makeSession()),
      _semanticAgentLogs("SemanticAgent.*"),
      _okaoLogs("OKAO*")
  {
    qi::path::detail::setWritablePath(_testDir.path().str());
  }

  static void SetUpTestCase()
  {
    qi::getEventLoop()->setMinThreads(8);
  }

  void SetUp()
  {
    _server->listenStandalone("tcp://127.0.0.1:0").value();
    _client->connect(_server->endpoints()[0]).value();

    // Actuation
    _actuation = boost::make_shared<qi::actuation::gmock::ActuationNiceGMock>();

    auto robotFrame = boost::make_shared<qi::actuation::gmock::FrameNiceGMock>();
    ON_CALL(*_actuation, robotFrame())
        .WillByDefault(
          testing::Return(qi::Future<qi::actuation::FramePtr>(qi::AnyObject(robotFrame))));

    _server->registerService("Actuation", qi::AnyObject(_actuation));

    // Human Recognition service (Bayesian stuff)
    _humanRecognition = boost::make_shared<HumanRecognitionNiceMock>();
    setCurrentIdentificationResult(_humanRecognition, "dummy");
    _server->registerService("HumanRecognition", qi::AnyObject(_humanRecognition));

    // The human
    _human = makeMockHuman();
    _humanObj = qi::AnyObject(_human);

    // What we test
    _recognizer = std::make_unique<HumanRecognizerOkaoBN>(_strand, _client);
  }

  qi::path::ScopedDir _testDir;
  std::shared_ptr<qi::Strand> _strand;
  qi::SessionPtr _server;
  qi::SessionPtr _client;

  DebugLogEnabler _semanticAgentLogs;
  DebugLogEnabler _okaoLogs;

  boost::shared_ptr<qi::actuation::gmock::FrameNiceGMock> _headFrame;
  qi::Object<qi::actuation::Frame> _headFrameObj;
  boost::shared_ptr<qi::human::gmock::HumanNiceGMock> _human;
  qi::Object<qi::human::Human> _humanObj;
  boost::shared_ptr<qi::actuation::gmock::ActuationNiceGMock> _actuation;
  boost::shared_ptr<HumanRecognitionNiceMock> _humanRecognition;

  std::unique_ptr<HumanRecognizer> _recognizer;
};


TEST_F(HumanRecognitionOkaoBnTest, okaoSanityCheck)
{
  // Get some face picture from the data.
  auto image = imageFromTestData("face1_clean1.png");

  // OKAO works on 8-bit grayscale images
  cv::Mat gray(image.rows, image.cols, CV_8UC1);
  cv::cvtColor(image, gray, cv::COLOR_RGB2GRAY);
  image = gray;

  // For face detection there is a minimum size of picture.
  AL::FaceAnalysis::scaleFacePictureUpIfTooSmallInPlace(image);

  // Let's do face detection.
  auto okao = AL::FaceAnalysis::OKAOWrapper::makeOKAOWrapperReal();
  std::vector<AL::FaceAnalysis::Face> faces;
  ASSERT_TRUE(okao->detectFaces(image, &faces));
  ASSERT_TRUE(okao->detectFacialParts(image, &faces));
  ASSERT_EQ(1u, faces.size());
  AL::FaceAnalysis::Face face = faces[0];

  cv::Point2i zero(0, 0);
  for(int i = 0; i < AL::FaceAnalysis::Face::POINT_KIND_MAX; ++i)
  {
    EXPECT_NE(zero, face.facialParts.points[i]);
  }
}


TEST_F(HumanRecognitionOkaoBnTest, noFaceNoIdentification)
{
  ASSERT_THROW(_recognizer->identify(_humanObj).value(), qi::semantic::agent::NoFacePictureException);
}


TEST_F(HumanRecognitionOkaoBnTest, newRecognizerDoesNotRecognize)
{
  setFacePictureFromTestData(_human, "face1_clean1.png");
  ASSERT_EQ(false, _recognizer->isInitialized().value());
  ASSERT_EQ("", _recognizer->identify(_humanObj).value());
}


TEST_F(HumanRecognitionOkaoBnTest, learningAndRecognizing)
{
  auto swappingFaces =
      swapFacePicturesRegularly(
        _strand, _human,
        {"face2_blur1.png", "face2_blur2.png", "face2_blur3.png", "face2_clean1.png"});

  ka::scoped([=]() mutable
  {
    swappingFaces.cancel();
    swappingFaces.wait(1000);
  });

  setCurrentIdentificationResult(_humanRecognition, "frank-f24eb");

  qiLogInfo() << "Frank is swapping faces..."; // I know, that reads awkward.

  // First learning initializes the recognition.
  ASSERT_EQ(false, _recognizer->isInitialized().value());
  ASSERT_TRUE(_recognizer->learn(_humanObj, "Frank").value());
  ASSERT_EQ(true, _recognizer->isInitialized().value());

  // The recognizer now knows Frank.
  EXPECT_EQ("Frank", _recognizer->identify(_humanObj).value());

  // Then George comes around.
  auto george = makeMockHuman();
  setFacePictureFromTestData(george, "face1_clean1.png");
  qi::Object<qi::human::Human> georgeObj = qi::AnyObject(george);

  // The recognizer does not know George.
  EXPECT_NE("George", _recognizer->identify(georgeObj).value());

  // Let's learn George once.
  setCurrentIdentificationResult(_humanRecognition, "george-a02bc");
  EXPECT_TRUE(_recognizer->learn(georgeObj, "George").value());

  // Now we know him.
  setCurrentIdentificationResult(_humanRecognition, "george-a02bc");
  EXPECT_EQ("George", _recognizer->identify(georgeObj).value());

  // We may recognize him in other conditions (or not).
  setFacePictureFromTestData(george, "face1_obstructed1.png");
  qiLogInfo() << "On obstructed image, George was seen as "
              << _recognizer->identify(georgeObj).value();

  // We may still be able to recognize Frank.
  setCurrentIdentificationResult(_humanRecognition, "frank-f24eb");
  EXPECT_EQ("Frank", _recognizer->identify(_humanObj).value());

  // Now we drop everything we're doing.
  _recognizer.reset();

  // We reload everything.
  _recognizer = std::make_unique<qi::semantic::agent::HumanRecognizerOkaoBN>(_strand, _client);

  // We still know Frank.
  setCurrentIdentificationResult(_humanRecognition, "frank-f24eb");
  EXPECT_EQ("Frank", _recognizer->identify(_humanObj).value());

  // We still know George.
  setCurrentIdentificationResult(_humanRecognition, "george-a02bc");
  setFacePictureFromTestData(george, "face1_clean1.png");
  EXPECT_EQ("George", _recognizer->identify(georgeObj).value());
}

// TODO:
// - test with around ~20 different persons
