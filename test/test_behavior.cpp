#include <functional>
#include <future>
#include <gmock/gmock.h>
#include <qi/application.hpp>
#include <qi/log.hpp>
#include <qi/path.hpp>
#include <qi/signalspy.hpp>
#include "../src/behaviormodel/behavior.hpp"
#include "debug_log_enabler.hpp"
#include "utility.hpp"

qiLogCategory("SemanticAgent.Test.Behavior");


int main(int argc, char** argv)
{
  qi::Application app{argc, argv};
  qi::log::addFilter("SemanticAgent.*", qi::LogLevel_Debug);

  qi::path::ScopedDir tmpDir;
  qi::path::detail::setWritablePath(tmpDir.path().str());
  qiLogDebug() << "Directory used as the writable path: " << tmpDir.path().str();

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


using qi::semantic::agent::PropertyPtr;
using qi::semantic::agent::ActionObj;
using qi::semantic::agent::ActionObjs;


class BehaviorTest: public testing::Test
{
protected:
  DebugLogEnabler _actionDebug{"SemanticAgent.Action"};
  DebugLogEnabler _behaviorDebug{"SemanticAgent.Behavior"};
};


// TODO: factorize those tests as "task tests", that should work on actions and behaviors.
// Maybe one day, give these tools for the Qi SDK developers?
TEST_F(BehaviorTest, BehaviorMadeFromFunctionRunsAndFinishesProperly)
{
  std::promise<void> p;
  auto task = qi::semantic::agent::makeBehaviorFromFunction(
        "To do nothing",
        [&p](PropertyPtr<ActionObjs>)
  {
    p.set_value();
    return qi::Future<void>{nullptr};
  }, "");

  qi::SignalSpy taskSpy{task->started};
  auto running = task->run();
  EXPECT_TRUE(taskSpy.waitUntil(1, usualTimeout).value());

  auto status = p.get_future().wait_for(stdUsualTimeout);
  EXPECT_EQ(std::future_status::ready, status);

  auto runningStatus = running.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, runningStatus);
}


TEST_F(BehaviorTest, BehaviorMadeFromCancellableFunctionIsCancellable)
{
  std::promise<void> cancelCallbackPromise;
  auto task = qi::semantic::agent::makeBehaviorFromFunction(
        "To do nothing actively",
        [&cancelCallbackPromise](PropertyPtr<ActionObjs>)
  {
    qi::Promise<void> actionPromise;
    actionPromise.setOnCancel([&cancelCallbackPromise, actionPromise](qi::Promise<void> p)
    {
      cancelCallbackPromise.set_value();
      p.setCanceled();
    });
    return actionPromise.future();
  }, "");

  qi::SignalSpy taskSpy{task->started};
  auto running = task->run();
  ASSERT_TRUE(taskSpy.waitUntil(1, usualTimeout).value());
  running.cancel();

  auto status = cancelCallbackPromise.get_future().wait_for(stdUsualTimeout);
  EXPECT_EQ(std::future_status::ready, status);

  auto runningStatus = running.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_Canceled, runningStatus);
}

// TODO: make a test that shows that the scheduling may also be cancelled.
// It requires a GMock of strands, to let the test specify how to react on each calls to the strand.

TEST_F(BehaviorTest, MakeBehaviorFromStrandedFunction)
{
  qi::Strand strand;
  auto function = [](PropertyPtr<ActionObjs>) {
    return qi::Future<void>{nullptr};
  };
  auto behavior = qi::semantic::agent::makeBehaviorFromFunction(
        "Stranded", strand.unwrappedSchedulerFor(std::move(function)), "");
  auto running = behavior->run();
  auto runningStatus = running.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_FinishedWithValue, runningStatus);
}


TEST_F(BehaviorTest, BehaviorFunctionDestroyedAfterBehaviorDestroyed)
{
  // We will track every copy of the function, and its final destruction.
  DebugLogEnabler debugCopies("SemanticAgent.Test");
  std::shared_future<void> functionDestroyed;

  {
    CopyTracker functionTracker;
    functionDestroyed = functionTracker.chainDestroyed();

    // Our function sets a promise when run, and finishes immediately.
    auto functionCalledPromise = std::make_shared<std::promise<void>>();
    auto function = [functionCalledPromise, functionTracker=std::move(functionTracker)](PropertyPtr<ActionObjs>)
    {
      functionCalledPromise->set_value();
      return qi::Future<void>{nullptr};
    };

    // Making the behavior and running it, until its (immediate) end.
    auto behavior = qi::semantic::agent::makeBehaviorFromFunction(
          "Straight", std::move(function), "");
    auto running = behavior->run();
    auto runningStatus = running.waitFor(usualTimeout);
    EXPECT_EQ(qi::FutureState_FinishedWithValue, runningStatus);

    // Our function should have been called.
    auto future = functionCalledPromise->get_future();
    auto status = future.wait_for(stdUsualTimeout);
    ASSERT_EQ(std::future_status::ready, status);
  }

  // After all this, the function should have been destroyed.
  auto status = functionDestroyed.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
}


TEST_F(BehaviorTest, SuggestionsFoundRunAndClearedWhenBehaviorStopped)
{
  std::atomic<bool> actionWasRun{false};
  auto behavior = qi::semantic::agent::makeBehaviorFromFunction(
        "To do nothing",
        [&](PropertyPtr<ActionObjs> suggestions)
  {
    suggestions->set(ActionObjs{
        qi::semantic::agent::makeQiActionNoFocusCheck("Nothing to say", [&]
    {
      actionWasRun = true;
      // The suggestions are not cleared here to check behavior clean
      // them up automatically.
      return qi::Future<void>{nullptr};
    })});

    qi::Promise<void> behaviorPromise;
    behaviorPromise.setOnCancel([behaviorPromise](qi::Promise<void> p)
    {
      p.setCanceled();
    });

    return behaviorPromise.future();
  }, "");

  auto waitingForSuggestions = waitForSignal(behavior->actionSuggestions);
  auto runningBehavior = behavior->run();
  auto signalTuple = waitingForSuggestions.value(usualTimeout);
  const auto& suggestions = std::get<0>(signalTuple);
  ASSERT_EQ(1u, suggestions.size());

  auto action = *suggestions.begin();
  action->async().run().value(usualTimeout);
  EXPECT_TRUE(actionWasRun);
  auto behaviorStatus = runningBehavior.waitFor(usualTimeout);
  EXPECT_EQ(qi::FutureState_Running, behaviorStatus)
        << "Behavior stopped: " << futureToString(runningBehavior);

  waitingForSuggestions = waitForSignal(behavior->actionSuggestions);
  runningBehavior.cancel();
  signalTuple = waitingForSuggestions.value(usualTimeout);
  EXPECT_TRUE(std::get<0>(signalTuple).empty());
}
