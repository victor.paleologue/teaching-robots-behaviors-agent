#include "test_semantic_behaviors.hpp"


qiLogCategory("SemanticAgent.Test.SemanticEngine");


using namespace qisem;


using SemanticEngineTest = SemanticBehaviorsTest;


TEST_F(SemanticEngineTest, DISABLED_SayHello)
{
  std::vector<std::string> inputs{
    "Say hello"
  };

//  auto behavior = qi::semantic::agent::makeSemanticEngineBehavior(
//        _knowledge, _client, _linguisticDatabase, _memory);
//  ASSERT_TRUE(expectResponseForEachUtterance(*behavior, inputs));
}


TEST_F(SemanticEngineTest, DISABLED_StoreAndRetrieve)
{
  std::vector<std::string> inputs{
    "The carrots are cooked",
    "What is cooked?"
  };

//  auto behavior = qi::semantic::agent::makeSemanticEngineBehavior(
//        _knowledge, _client, _linguisticDatabase, _memory);
//  ASSERT_TRUE(expectResponseForEachUtterance(*behavior, inputs));
}
