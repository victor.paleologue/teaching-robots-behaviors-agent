#include <gmock/gmock.h>
#include <future>
#include <boost/make_shared.hpp>
#include <boost/scope_exit.hpp>
#include <qi/knowledge/gmock/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include <qi/teaching/ontology.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qi/semantic/agent/gmock/agent.hpp>
#include "test_agent.hpp"
#include "utility.hpp"
#include "debug_log_enabler.hpp"
#include "../src/utility/knowledge.hpp"
#include "../src/extractor.hpp"
#include "../src/speechextractors/fake_speechextractor.hpp"

using testing::_;
using testing::Return;
using namespace qi::knowledge;
using namespace qi::knowledge::gmock;
using namespace qi::semantic::agent;
using namespace qi::semantic::agent::gmock;
using namespace qi::locale;
using namespace qi::teaching;

class ExtractorTest: public CommonSetup
{
protected:
  ExtractorTest()
    : CommonSetup()
    , _beliefs(boost::make_shared<EditableKnowledgeGraphNiceGMock>())
    , _knowledge([&]
      {
        auto knowledgeBase = boost::make_shared<EditableKnowledgeBaseNiceGMock>();
        ON_CALL(*knowledgeBase, editableKnowledgeGraph(beliefsDomain()))
            .WillByDefault(Return(qi::Future<EditableKnowledgeGraphPtr>{qi::AnyObject{_beliefs}}));
        return knowledgeBase;
      }())
    , _speechExtractor(
        qi::AnyObject{boost::make_shared<FakeSpeechExtractor>(_strand, _server)})
    , _speechSource()
    , _extractor(new Extractor{
                   _strand, _server, qi::AnyObject{_knowledge},
                   _speechExtractor})
    , _extractingSpeech()
    , _debugSpeechExtractors("SemanticAgent.SpeechExtractor")
    , _debugFakeSpeechExtractor("SemanticAgent.FakeSpeechExtractor")
  {
  }

  void SetUp() override
  { // assertions seemingly cannot happen in constructor
    auto startingForSpeechExtractor = _extractor->waitForNextStartOfSpeechExtraction();
    _extractingSpeech = _extractor->extractSpeech();

    ASSERT_EQ(qi::FutureState_FinishedWithValue, startingForSpeechExtractor.waitFor(usualTimeout))
        << "Speech extractor did not start normally ("
        << futureToString(startingForSpeechExtractor) << ")";

    ASSERT_EQ(qi::FutureState_Running, _extractingSpeech.wait(0))
        << futureToString(_extractingSpeech);
  }

  ~ExtractorTest() override
  {
    _extractor.reset();
  }

  std::shared_ptr<qisem::linguistics::LinguisticDatabase> _linguisticDatabase;
  boost::shared_ptr<EditableKnowledgeGraphNiceGMock> _beliefs;
  boost::shared_ptr<EditableKnowledgeBaseNiceGMock> _knowledge;
  SpeechExtractorPtr _speechExtractor;
  SpeechSourcePtr _speechSource;
  std::unique_ptr<Extractor> _extractor;
  qi::Future<void> _extractingSpeech;
  DebugLogEnabler _debugSpeechExtractors;
  DebugLogEnabler _debugFakeSpeechExtractor;
};


TEST_F(ExtractorTest, fakeSpeechExtractor)
{
  std::string toHear = "Une phrase choc";
  std::promise<std::string> p;
  auto subscriber =
      _speechExtractor->heard.connect([&](const std::vector<PhraseAndLocale>& phrases)
  {
    ASSERT_FALSE(phrases.empty());
    ASSERT_NO_THROW(p.set_value(phrases[0].text));
  });
  BOOST_SCOPE_EXIT_ALL(&) { _speechExtractor->heard.disconnect(subscriber); };

  hear(toHear);
  auto f = p.get_future();
  auto status = f.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
  EXPECT_EQ(toHear, f.get());
}


TEST_F(ExtractorTest, speechSource)
{
  std::string toHear = "C'est un chien qui traverse la rue, et puis paf, le chien.";
  auto speechSource = _extractor->speechSource().value();

  std::promise<std::string> p;
  speechSource->heard.connect([&](const Utterance& utterance)
  {
    ASSERT_NO_THROW(p.set_value(utterance.possiblePhrases[0].text));
  });
  QI_EMIT _speechExtractor->heard({{toHear, frenchLocale}});
  auto f = p.get_future();
  auto status = f.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
  EXPECT_EQ(toHear, f.get());
}


TEST_F(ExtractorTest, fakeSpeechSource)
{
  std::string toHear = "Y'a du monde sur la corde à linge";
  auto speechSource = _extractor->speechSource().value();

  std::promise<std::string> p;
  speechSource->heard.connect([&](const Utterance& utterance)
  {
    ASSERT_NO_THROW(p.set_value(utterance.possiblePhrases[0].text));
  });

  hear(toHear);
  auto f = p.get_future();
  auto status = f.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
  EXPECT_EQ(toHear, f.get());
}


TEST_F(ExtractorTest, knowledgeFromASREvent)
{

  auto memory = _client->service("ALMemory").value();
  auto endSubscriber = memory.call<qi::AnyObject>("subscriber", "ALSpeechRecognition/Status");
  endSubscriber.connect(
        "signal", boost::function<void(qi::AnyValue)>([endSubscriber](qi::AnyValue v)
  {
    try
    {
      auto status = v.to<std::string>();
      qiLogInfo("pouet") << "Speech reco status: " << status;
    }
    catch (const std::exception& e)
    {
      qiLogError("pouet") << "Error processing ASR status: " << e.what();
    }
  })).value();

  qiLogVerbose("pouet") << "Subscribed to ALSpeechRecognition/Status";


  const std::string toHear = "Moving things with words is magic";
  std::promise<void> p;
  int nofAddedTriples = 0;
  EXPECT_CALL(*_beliefs, add(_))
      .Times(4)
      .WillRepeatedly(testing::Invoke([&](const Triple& triple)
  {
    if (++nofAddedTriples == 4)
      p.set_value();
    return qi::Future<void>{nullptr};
  }));

  hear(toHear);
  auto f = p.get_future();
  auto status = f.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status)
      << futureStatusToString(status);
}


TEST_F(ExtractorTest, twoExtractorsInSameProcess)
{
  Extractor extractorBis{
    _strand, _server, qi::AnyObject{_knowledge},
    qi::AnyObject{boost::make_shared<FakeSpeechExtractor>(_strand, _server)}};
  auto extracting = extractorBis.extractSpeech();
  ASSERT_EQ(qi::FutureState_Running, extracting.waitFor(usualTimeout));
}
