#include "usual_knowledge.hpp"
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "usual_locales.hpp"

using namespace qi::knowledge;
using namespace qi::teaching;

qi::Future<ResourceNode> createObject(
    EditableKnowledgeGraphPtr graph,
    std::vector<LocalizedString> labels)
{
  std::vector<Triple> triples;

  // Here is a new object.
  auto objectResource = makeUniqueResource(objectPrefix());

  // Object is of type object.
  triples.push_back(
      makeTripleFromNodes(
        objectResource, rdfTypeResource(), objectTypeNode()));

  for (const auto& label: labels)
  {
    Node labelNode;
    labelNode.value = qi::AnyValue::from(label);
    triples.push_back(
          makeTripleFromNodes(objectResource, rdfsLabelResource(), std::move(labelNode)));
  }

  return addTriples(graph, triples).andThen([=](void*){ return objectResource; });
}


qi::Future<ResourceNode> createBoxObject(
    EditableKnowledgeGraphPtr graph)
{
  return createObject(graph, {{"the box", englishLocale}, {"la boîte", frenchLocale}});
}


qi::Future<ResourceNode> createChocolateObject(
    EditableKnowledgeGraphPtr graph)
{
  return createObject(graph, {{"chocolate", englishLocale}, {"chocolat", frenchLocale}});
}
