#include "test_semantic_behaviors.hpp"
#include <future>
#include <gtest/gtest.h>
#include <boost/filesystem.hpp>
#include <boost/scope_exit.hpp>
#include <audiolanguage/languagecodes.hpp>
#include <qi/anymodule.hpp>
#include <qi/path.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>
#include <qi/semantic/agent/gmock/agent.hpp>
#include <qi/teaching/ontology.hpp>
#include "../src/utility/knowledge.hpp"
#include "../src/pddl/pddl_io.hpp"
#include "../src/behaviors/reaction.hpp"
#include "../src/behaviors/semantic_behaviors.hpp"
#include "../src/utility/propertyset.hpp"
#include "utility.hpp"

qiLogCategory("SemanticAgent.Test.SemanticBehavior");

using namespace qi::semantic::agent;
using namespace qi::teaching;
using namespace qi::knowledge;
using std::vector;

namespace
{
ResourceNode informSaid(
    boost::shared_ptr<qi::semantic::agent::gmock::SpeechSourceGMock> speechSource,
    qi::knowledge::EditableKnowledgeGraphPtr knowledgeGraph,
    const ResourceNode& speakerNode,
    const std::string& phrase,
    qi::locale::Language language)
{
  qi::locale::Locale locale{language, qi::locale::Region::Unknown};

  auto eventResource = makeUniqueResource(eventPrefix());

  auto eventOccurredNow =
      makeTripleFromNodes(
        eventResource, occurredAtResource(), makeNowNode());
  knowledgeGraph->add(std::move(eventOccurredNow));

  auto eventIsAnEvent =
      makeTripleFromNodes(
        eventResource, rdfTypeResource(), eventTypeNode());
  knowledgeGraph->add(std::move(eventIsAnEvent));

  auto eventIsACommunicationAct =
      makeTripleFromNodes(
        eventResource, rdfTypeResource(), communicationActTypeNode());
  knowledgeGraph->add(std::move(eventIsACommunicationAct));

  auto communicationActIsAddressedToSelf =
      ::makeTripleFromNodes(
        eventResource, communicationAddresseeResource(), agentSelfNode());
  knowledgeGraph->add(std::move(communicationActIsAddressedToSelf));
  // TODO: remove leftover events, to prevent memory saturation.

  if (speakerNode.url != agentSelf())
    QI_EMIT speechSource->heard({eventResource, {{phrase, locale}}});

  return eventResource;
}

} // anonymous


SemanticBehaviorsTest::SemanticBehaviorsTest()
  : CommonSetup()
  , _linguisticDatabase(qisem::semanticNaoqiLinker::makeLinguisticDatabase())
  , _semanticMemory(makeMemoryKnowingCommandsAndInfos(*_linguisticDatabase))
  , _semanticMemoryContext(makeSemanticMemoryContext(_strand, _linguisticDatabase, _semanticMemory))
  , _tmpDir(qi::os::mktmpdir("TestAgent"))
  , _knowledge([&]
  {
    auto knowledgeModule = qi::import("knowledge_module");
    auto knowledgePath = qi::os::mktmpdir("SemanticBehaviorsTest");
    return knowledgeModule.call<qi::knowledge::EditableKnowledgeBasePtr>(
          "EditableKnowledgeBase", knowledgePath, true);
  }())
  , _beliefs(_knowledge->editableKnowledgeGraph(beliefsDomain()))
  , _speechSource(boost::make_shared<qi::semantic::agent::gmock::SpeechSourceNiceGMock>())
  , _speechSourceId(generateUuid())
  , _speechSourceRemote(qi::AnyObject{_speechSource})
  , _semanticBehaviorsDebug("SemanticAgent.SemanticBehavior")
  , _semanticBehaviorsTestDebug("Test.SemanticBehaviors")
{
  ON_CALL(*_speechSource, _id())
      .WillByDefault(testing::Return(qi::Future<std::string>{_speechSourceId}));
  _ttsSubscriber->signal.connect(wrap([this](const qi::AnyValue& value)
  {
    auto language = qi::locale::languageFromStr(AL::languageFromLegacyName(_dialog->getLanguage()));
    informSaid(_speechSource, _beliefs, qi::teaching::agentSelfResource(), value.to<std::string>(), language);
  }));
}


SemanticBehaviorsTest::~SemanticBehaviorsTest()
{
  _knowledge.reset();
  boost::filesystem::remove_all(_tmpDir.bfsPath());
  _semanticMemory.reset();
  _linguisticDatabase.reset();
}


ResourceNode SemanticBehaviorsTest::informUserSaid(
    const std::string& phrase,
    qi::locale::Language language)
{
  return informSaid(
        _speechSource, _beliefs,
        makeResourceNodeFromUrl(socialAgentPrefix() + "42"),
        phrase, language);
}


ResourceNode SemanticBehaviorsTest::informRobotRespondedTo(
    const ResourceNode& inputEvent)
{
  auto responseEvent = makeUniqueResource(eventPrefix());
  auto triples = makeRobotResponseToCommunicationAct(inputEvent, responseEvent);
  addTriples(_beliefs, std::move(triples)).value();
  return responseEvent;
}


testing::AssertionResult SemanticBehaviorsTest::forEachUtteranceResponse(
    PDDLAwareQiBehaviorPtr behavior,
    const std::vector<std::string> &inputs,
    qi::locale::Language language,
    std::function<testing::AssertionResult(const boost::optional<SetChange<ActionObj>>&)> predicate)
{
  ChangeTracker<ActionObj> changeTracker;
  for (const auto& input: inputs)
  {
    // Each input utterance will trigger a change of suggestion.
    auto suggesting = waitForSignal(behavior->actionSuggestions);
    qiLogInfo() << "Simulating user input: \"" << input << "\"";
    informUserSaid(input, language);
    boost::optional<SetChange<ActionObj>> change;
    switch (suggesting.waitFor(usualTimeout))
    {
    case qi::FutureState_Running: // no change received
      break;

    case qi::FutureState_FinishedWithValue: // change received
      change = changeTracker.check(std::get<0>(suggesting.value()));
      break;

    case qi::FutureState_FinishedWithError:
      return testing::AssertionFailure() << "unexpected failure: " << suggesting.error();

    case qi::FutureState_Canceled:
      return testing::AssertionFailure() << "waiting for suggestion was cancelled";

    case qi::FutureState_None:
      QI_ASSERT(false && "impossible n'est pas Aldébaran");
    }

    auto predicateResult = predicate(change);
    if (!predicateResult)
      return predicateResult;
  }

  return testing::AssertionSuccess();
}


void SemanticBehaviorsTest::runResponseForEachUtterance(
    PDDLAwareQiBehaviorPtr behavior,
    const std::vector<std::string>& inputs,
    qi::locale::Language language)
{
  ChangeTracker<ActionObj> changeTracker;
  forEachUtteranceResponse(
        behavior, inputs, language,
        [changeTracker](const boost::optional<SetChange<ActionObj>>& change) mutable
  {
    if (change)
    {
      const auto added = change.get().inserted;
      if (!added.empty())
      {
        try
        {
          (*added.begin())->run();
        }
        catch (const std::exception& e)
        {
          qiLogVerbose() << "Response produced an error: " << e.what();
        }
      }
    }
    return testing::AssertionSuccess();
  });
}


testing::AssertionResult SemanticBehaviorsTest::expectResponseForEachUtterance(
    PDDLAwareQiBehaviorPtr behavior,
    const std::vector<std::string>& inputs,
    qi::locale::Language language)
{
  auto predicate = [&](const boost::optional<SetChange<ActionObj>>& suggestionChange)
  {
    if (!suggestionChange)
      return testing::AssertionFailure()
          << "A new suggestion was expected, but no suggestion change occurred";

    if (suggestionChange.get().inserted.empty())
      return testing::AssertionFailure()
          << "A new suggestion was expected, but no new suggestion occurred";

    // The first suggestion is an action that can be run and that will reply something.
    auto saying = waitForSignal(_speechEngine->saying);

    // After being run, the suggestion will be dropped.
    auto suggesting = waitForSignal(behavior->actionSuggestions);

    // Let us set the context.
    const auto& suggestion = (*suggestionChange.get().inserted.begin());
    suggestion->context.set(_contextObject).value();

    // Let us run the suggestion.
    suggestion->run();

    // Was the reply utterred?
    if (qi::FutureState_FinishedWithValue != saying.waitFor(usualTimeout))
      return testing::AssertionFailure() << "nothing said during action execution";
    qiLogInfo() << "Robot replies: \"" << std::get<0>(saying.value()).text << "\"";

    // Was the suggestion dropped?
    if (qi::FutureState_FinishedWithValue != suggesting.waitFor(usualTimeout))
      return testing::AssertionFailure() << "no suggestions update after action execution";
    auto actionSuggestions = behavior->actionSuggestions.get().value();

    // At this point there should be no remaining suggestion.
    if (!actionSuggestions.empty())
      return testing::AssertionFailure()
          << "It was expected that no suggestion was remaining";

    return testing::AssertionSuccess();
  };

  return forEachUtteranceResponse(behavior, inputs, language, std::move(predicate));
}


testing::AssertionResult SemanticBehaviorsTest::expectNoResponseForUtterances(
    PDDLAwareQiBehaviorPtr behavior,
    const std::vector<std::string>& inputs,
    qi::locale::Language language)
{

  auto predicate = [&](const boost::optional<SetChange<ActionObj>>& suggestionChange)
  {
    if (suggestionChange)
      return testing::AssertionFailure() << "suggestion change spotted";
    else
      return testing::AssertionSuccess();
  };

  return forEachUtteranceResponse(behavior, inputs, language, std::move(predicate));
}


TEST_F(SemanticBehaviorsTest, SemanticAction)
{
  static const qi::locale::Locale french{
    qi::locale::Language::French,
    qi::locale::Region::France};

  auto semanticExecuter = std::make_shared<SemanticExpressionExecuter>(
        _client, _contextObject, _semanticMemoryContext, _speechEngineObj);

  auto semanticAction = makeSemanticAction(
        _client, _contextObject, _semanticMemoryContext,
        semanticExecuter, notUnderstood(), french);

  auto running = semanticAction->async().run();
  auto status = running.waitFor(qi::Seconds{1}); // this timeout is specific to this action
  EXPECT_EQ(qi::FutureState_FinishedWithValue, status)
      << ((status == qi::FutureState_FinishedWithError) ? running.error() : "");
}

TEST_F(SemanticBehaviorsTest, SemanticBehavior)
{
  // To check that the provided semantic reaction function will be dropped.
  std::promise<void> functionDestroyedPromise;

  {
    // The promise is set only if the shared count drops to 0.
    auto functionCalledPromise = std::shared_ptr<std::promise<void>>(
        new std::promise<void>(),
        [&](std::promise<void>* ptr)
    {
      functionDestroyedPromise.set_value();
      delete ptr;
    });

    // The semantic reaction function simply repeats the input.
    auto semanticReaction = [=](const PhraseAndLocale&, const qisem::SemanticExpression& k)
    {
      functionCalledPromise->set_value();
      return k.clone();
    };

    // The behavior made from the function.
    auto semanticBehavior = makeSemanticBehavior(
          "To repeat",
          _speechSourceRemote, _speechEngineObj, _client, _contextObject, _semanticMemoryContext, semanticReaction,
          "").value();

    // We provide an input, check that the semantic function is called,
    // and that a suggestion is provided in response.
    auto nextSuggestions = waitForSignal(semanticBehavior->actionSuggestions);
    informUserSaid("Zut");
    auto future = functionCalledPromise->get_future();
    auto status = future.wait_for(stdUsualTimeout);
    ASSERT_EQ(std::future_status::ready, status);
    ASSERT_EQ(qi::FutureState_FinishedWithValue, nextSuggestions.waitFor(usualTimeout));
    auto suggestions = std::get<0>(nextSuggestions.value());
    ASSERT_EQ(1u, suggestions.size());
    const auto& suggestion = suggestions[0];

    // The reaction should provide a valid PDDL domain.
    const auto pddl = suggestion->pddl.get().value();
    qiLogDebug() << "Action suggestion's PDDL: " << pddl;
    const auto domain = qi::semantic::agent::parsePddlDomain(pddl);
    EXPECT_EQ(1u, domain->actions.size());

    // We run the reaction, the suggestion should then be dropped.
    nextSuggestions = waitForSignal(semanticBehavior->actionSuggestions);
    suggestion->context.set(_contextObject).value();
    auto runningSuggestion = suggestion->async().run();
    ASSERT_EQ(qi::FutureState_FinishedWithValue, runningSuggestion.waitFor(usualTimeout));
    ASSERT_EQ(qi::FutureState_FinishedWithValue, nextSuggestions.waitFor(usualTimeout));
    suggestions = std::get<0>(nextSuggestions.value());
    ASSERT_EQ(0u, suggestions.size());
  }

  auto future = functionDestroyedPromise.get_future();
  auto status = future.wait_for(stdUsualTimeout);
  ASSERT_EQ(std::future_status::ready, status);
}
