#include "future_io.hpp"


std::string futureStatusToString(const std::future_status& status)
{
  switch (status)
  {
  case std::future_status::ready:
    return "ready";
  case std::future_status::timeout:
    return "timeout";
  case std::future_status::deferred:
    return "deferred";
  }
  QI_ASSERT(false && "unexpected future state");
  return "unknown"; // srsly?
}
