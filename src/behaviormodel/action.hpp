#pragma once
#ifndef SEMANTICAGENT_ACTION_HPP
#define SEMANTICAGENT_ACTION_HPP

#include <memory>
#include <qi/future.hpp>
#include <qi/session.hpp>
#include <qi/trackable.hpp>
#include <qi/context/context.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <src/agent/agent_p.hpp>

namespace qi
{
class Strand;

namespace semantic
{
namespace agent
{
/// An action is an asynchronous function which calls can be cancelled.
/// Anything matching this concept can be considered an action too.
/// Using an object allows to systematically cancel the run future when
/// the action is destroyed.
struct Action
{
  Action();

  // This has entity semantics, and is therefore not copyable.
  Action(const Action&) = delete;
  Action& operator=(const Action&) = delete;

  virtual ~Action();

  /// Runs the action asynchronously.
  /// @return a Future that can be cancelled to interrupt the action.
  virtual Future<void> run() = 0;

  /// To be emitted when the action has actually started.
  Signal<> started;

  /// To be emitted when the action has finished successfully.
  Signal<> completed;

  /// Wait for the next started signal.
  /// TODO: mix action into a class that resembles the Future, but includes the started there!
  virtual Future<void> waitForStarted() = 0;
};


/// Interface for named objects.
struct Named
{
  /// Name is constant and read-only.
  Property<std::string> name;
};


/// Helper to make a PhraseAndLocale from an English string.
PhraseAndLocale englishPhrase(const std::string& name);


/// Interface for objects that have an associated PDDL description.
struct PDDLAware
{
  /// PDDL is constant and read-only.
  Property<std::string> pddl;
};

/// Interface for objects requiring and providing a session.
struct Connected
{
protected:
  /// The session stored by the object.
  virtual boost::shared_ptr<Session> session() = 0;
};


/// Interface for objects that can be put in Qi Contexts.
class QiContextAware: Connected
{
  struct Private;
  std::shared_ptr<Private> _qiContextAwarePrivate;

public:
  QiContextAware(boost::shared_ptr<Session> session);

  /// Qi Context for running the action.
  Property<context::ContextPtr> context;

protected:
  /// A local access to the current focus state.
  /// If the focus is owned by the context,
  /// the function returns a valid owner **of the callback registration**.
  /// Whenever the focus is lost, the callback is called.
  /// If the owner is released the callback is dropped.
  /// If the context switches, the focus is lost and the callback is called.
  Future<Object<Owner>> checkFocusAndTrack(std::function<Future<void>()> callback);
};

using ActionFunction = std::function<Future<void>()>;

/// An action simply made of a function.
/// The function signature must be compatible with the method run(),
/// and the returned future must be cancelable.
struct FunctionAction: Action, Named
{
  /// The action uses a strand to be thread-safe.
  FunctionAction(
      std::string name,
      ActionFunction function,
      std::shared_ptr<Strand> strand = std::make_shared<Strand>());

  virtual ~FunctionAction();

  /// TODO: add the support of the focus to make something reusable in the Qi SDK.
  Future<void> run() override;

  Future<void> waitForStarted() override;

private:
  struct Private;
  std::unique_ptr<Private> _functionAction;
};


using QiContextAwareActionFunction = std::function<Future<void>(context::ContextPtr)>;

// Ultimate action object is aware of the context and has PDDL information.
struct QiContextAndPDDLAwareAction: QiContextAware, PDDLAware, Action, Named
{
  QiContextAndPDDLAwareAction(
      boost::shared_ptr<Session> session,
      std::string name,
      QiContextAwareActionFunction function,
      std::shared_ptr<Strand> strand,
      std::string pddl);

  ~QiContextAndPDDLAwareAction();

  Future<void> run();
  Future<void> waitForStarted();
  boost::shared_ptr<Session> session();

private:
  struct Private;
  std::unique_ptr<Private> _p;
};


using ActionObj = Object<PDDLAwareQiAction>;
using ActionObjFunction = QiContextAwareActionFunction;

/// The recommended way to make a Qi Action, that will check the context is ok.
ActionObj makeQiAction(
    boost::shared_ptr<Session> session,
    std::string name,
    ActionObjFunction function,
    std::shared_ptr<Strand> strand = std::make_shared<Strand>(),
    const std::string& pddl = "");

/// Alternate way to make a Qi Action that does not check the context is ok.
/// Note that the returned object does not have context property, nor a PDDL one.
ActionObj makeQiActionNoFocusCheck(
    std::string name,
    ActionFunction function,
    std::shared_ptr<Strand> strand = std::make_shared<Strand>());


/// A simple continuation function.
using Continuation = std::function<Future<void>(Future<void>)>;


/// Continue an action with a function, and wrap them in a composed action.
/// If name or pddl are not provided, they will be inherited from the action.
ActionObj then(
    boost::shared_ptr<Session> session,
    ActionObj action,
    Continuation continuation,
    std::shared_ptr<Strand> strand = std::make_shared<Strand>(),
    std::string name = "",
    const std::string& pddl = "");

} // agent
} // semantic
} // qi

QI_REGISTER_IMPLEMENTATION_H(
    ::qi::semantic::agent::PDDLAwareQiAction,
    ::qi::semantic::agent::QiContextAndPDDLAwareAction);

#endif // SEMANTICAGENT_ACTION_HPP
