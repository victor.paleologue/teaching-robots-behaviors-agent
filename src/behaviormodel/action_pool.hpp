#ifndef SEMANTICAGENT_ACTION_POOL_HPP
#define SEMANTICAGENT_ACTION_POOL_HPP

#include <memory>
#include <string>
#include <utility>
#include <qi/anyobject.hpp>
#include <qi/future.hpp>
#include <qi/property.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "../utility/property.hpp"
#include "../utility/semantic_helpers.hpp"

namespace pddl
{
namespace ast
{
class Description;
}
}

namespace qisem
{
struct SemanticExpression;
}

namespace qi
{
namespace semantic
{
namespace agent
{
class Owner;

struct PDDLDomainAndProblem
{
  std::string domain;
  std::string problem;
};

using ProblemSource = Prop<std::shared_ptr<const pddl::ast::Description>>;


/// Associates a behavior to a suggestion.
struct BehaviorAndSuggestion
{
  AnyObject behavior;
  AnyObject suggestion;
};


/// A structure representing a parameter.
struct ActionParameter
{
  std::string name;
  // TODO: add the type specification, for PDDL and knowledge.
};

bool operator==(const ActionParameter& lhs, const ActionParameter& rhs);
bool operator<(const ActionParameter& lhs, const ActionParameter& rhs);


/// A factory and its parameters.
struct ActionFactoryWithParameters
{
  AnyObject actionFactory;
  std::vector<ActionParameter> parameters;
};


/// A factory with its parameters assigned with semantic expressions.
struct ActionFactoryWithSemanticSlots
{
  AnyObject actionFactory;
  std::map<ActionParameter, std::shared_ptr<qisem::SemanticExpression>> semanticSlots;
};


class ActionPool
{
  struct Private;
  std::unique_ptr<Private> _p;

public:
  ActionPool(std::shared_ptr<SemanticMemoryContext> semanticMemoryContext = makeSemanticMemoryContext());
  virtual ~ActionPool();

  /// Returns the PDDL domain corresponding to the factories known to the pool.
  Property<PDDLDomainAndProblem> pddl;

  /// Adds the factory to the pool.
  /// @return An owner which removes the factory when released.
  Future<Object<Owner>> addFactory(AnyObject factory);

  /// Adds the behavior to the pool.
  /// The behavior's factories will be added to the pool,
  /// and when an action suggested, it appears in the possible actions of the pool.
  /// Also, the goals of the problems are updated according to the behavior's
  /// goal or action suggestions.
  /// @return An owner which removes the behavior when released.
  Future<Object<Owner>> addBehavior(AnyObject behavior);

  /// Adds a problem provider.
  /// The provided problem will be added to the overall problem.
  Future<Object<Owner>> addProblemSource(std::shared_ptr<ProblemSource> problemProvider);

  /// The total list of actions suggested by behaviors.
  Property<std::vector<AnyObject>> actionSuggestions;

  /// Find an action suggested by a behavior matching the given PDDL action name.
  /// If the object cannot be found, future is set to error.
  Future<AnyObject> findActionSuggestion(const std::string& actionName) const;

  /// Find an action suggested by a behavior matching the given PDDL action name.
  /// If the object cannot be found, future is set to error.
  Future<BehaviorAndSuggestion> findActionSuggestionAndBehavior(const std::string& actionName) const;

  /// Find a factory providing actions matching the given PDDL action name.
  /// If the object cannot be found, future is set to error.
  Future<ActionFactoryWithParameters> findActionFactory(
          const std::string& actionName) const;

  /// Find semantic templates matching the provided statement,
  /// with slots filled with the provided statement.
  Future<std::vector<ActionFactoryWithSemanticSlots>> findActionFactory(
      const std::shared_ptr<qisem::SemanticExpression>& statement,
      const locale::Locale& locale) const;
};
} // agent
} // semantic
} // qi

QI_TYPE_STRUCT(qi::semantic::agent::PDDLDomainAndProblem, domain, problem);

#endif // SEMANTICAGENT_ACTION_POOL_HPP
