#include "behavior.hpp"
#include <qi/log.hpp>

qiLogCategory("SemanticAgent.Behavior");

namespace qi
{
namespace semantic
{
namespace agent
{
Behavior::Behavior(
    const std::string& name,
    FunctionType function,
    const std::string& pddl)
  : Behavior(
      name,
      [function = std::move(function)](
          PropertyPtr<ActionObjs> suggestions,
          PropertyPtr<Goals>,
          PropertyPtr<ActionFactories>) {
        return function(suggestions);
      },
      pddl)
{}


template <typename T>
std::shared_ptr<Property<T>> makeProperty()
{
  return std::make_shared<Property<T>>(
      typename Property<T>::Getter(),
      util::SetAndNotifyIfChanged());
}


Behavior::Behavior(
    const std::string& name,
    FullFunctionType function,
    const std::string& pddl)
  : name(
      name,
      Property<std::string>::Getter(),
      Property<std::string>::Setter())
  , pddl(pddl, Property<std::string>::Getter(), Property<std::string>::Setter())
  , _actionSuggestions(makeProperty<ActionObjs>())
  , _goalSuggestions(makeProperty<Goals>())
  , _actionFactories(makeProperty<ActionFactories>())
  , actionSuggestions(*_actionSuggestions)
  , goalSuggestions(*_goalSuggestions)
  , actionFactories(*_actionFactories)
  , _action()
{
  std::weak_ptr<Property<ActionObjs>> weakSuggestions = _actionSuggestions;
  std::weak_ptr<Property<Goals>> weakGoals = _goalSuggestions;
  std::weak_ptr<Property<ActionFactories>> weakFactories = _actionFactories;
  auto asActionFunction = [
      weakSuggestions, weakGoals, weakFactories,
      function = std::move(function)]
  {
    auto suggestions = weakSuggestions.lock();
    auto goals = weakGoals.lock();
    auto factories = weakFactories.lock();
    if (suggestions && goals && factories)
    {
      auto behaviorRunning = function(suggestions, goals, factories);
      behaviorRunning.then([=](Future<void>)
      {
        if (auto suggestions = weakSuggestions.lock())
          suggestions->set(ActionObjs());
      });
      return behaviorRunning;
    }
    return Future<void>{nullptr};
  };
  _action = std::make_unique<FunctionAction>(name, std::move(asActionFunction));
  _action->started.connect(started);
}

Behavior::~Behavior()
{
  _action.reset();
}

Future<void> Behavior::run()
{
  return _action->run();
}

Future<void> Behavior::waitForStarted()
{
  return _action->waitForStarted();
}

std::string actionsToString(const ActionObjs& actions)
{
  std::stringstream ss;
  bool first = true;
  for (const auto& action: actions)
  {
    if (!first)
      ss << ", ";
    first = false;

    ss << "\"" << action->name << "\"";
  }
  return ss.str();
}

std::unique_ptr<Behavior> makeBehaviorFromFunction(
    const std::string& name,
    Behavior::FunctionType function,
    const std::string& pddl)
{
  // TODO: replace with std::make_unique when not targetting VS2013 or lower anymore.
  return std::unique_ptr<Behavior>{new Behavior{name, std::move(function), pddl}};
}

Future<PDDLAwareQiBehaviorPtr> makeQiBehavior(
    const std::string& name,
    Behavior::FullFunctionType function,
    const std::string& pddl)
{
  auto behavior = boost::make_shared<Behavior>(name, std::move(function), pddl);
  behavior->run();
  return behavior->waitForStarted().andThen([=](void*)
  {
    return PDDLAwareQiBehaviorPtr{behavior};
  });
}

} // agent
} // semantic
} // qi

REGISTER_PDDLAWAREQIBEHAVIOR(qi::semantic::agent::Behavior);
