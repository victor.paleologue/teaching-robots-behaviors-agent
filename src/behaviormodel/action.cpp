#include "action.hpp"
#include <boost/core/ignore_unused.hpp>
#include <qi/context/commonerrormessages.hpp>
#include <qi/log.hpp>
#include <qi/signalspy.hpp>
#include <qi/context/utils.hpp>
#include "../utility/incontext.hpp"
#include "../utility/owner.hpp"

qiLogCategory("SemanticAgent.Action");

namespace qi
{
namespace semantic
{
namespace agent
{
Action::Action() = default;
Action::~Action() = default;

PhraseAndLocale englishPhrase(const std::string& phrase)
{
  return PhraseAndLocale{phrase, qi::locale::Locale{qi::locale::Language::English}};
}


struct QiContextAware::Private: SafeContext
{
  Private(boost::shared_ptr<Session> session);

  /// To connect to check the focus.
  boost::shared_ptr<Session> session;

  /// The focus tracker object does the real job.
  std::shared_ptr<context::FocusTracker> focusTracker;

  /// The callbacks we have
  std::set<Object<Owner>> callbackOwners;
};

QiContextAware::Private::Private(boost::shared_ptr<Session> pSession)
  : session(std::move(pSession))
{}

QiContextAware::QiContextAware(boost::shared_ptr<Session> pSession)
  : _qiContextAwarePrivate(std::make_shared<Private>(pSession))
  , context(
      context::ContextPtr(),
      Property<context::ContextPtr>::Getter(),
      _qiContextAwarePrivate->inContext(
        [&](context::ContextPtr& storage, const context::ContextPtr& value)
      {
        storage = value;
        for (const auto& owner: _qiContextAwarePrivate->callbackOwners)
        {
          try
          {
            owner->release();
          }
          catch (const std::exception& e)
          {
            qiLogDebug() << "Error releasing owner when switching context: " << e.what();
          }
        }

        _qiContextAwarePrivate->focusTracker =
            std::make_unique<context::FocusTracker>(_qiContextAwarePrivate->session, storage);
        return true;
      }))
{}

Future<Object<Owner>> QiContextAware::checkFocusAndTrack(std::function<Future<void>()> callback)
{
  return _qiContextAwarePrivate->inContext([&, callback = std::move(callback)]
  {
    // No focus tracker => no valid context => no focus
    if (!_qiContextAwarePrivate->focusTracker)
      return Future<Object<Owner>>(Object<Owner>());

    auto focusTracker = _qiContextAwarePrivate->focusTracker;
    auto checkingFocus = focusTracker->trackFocus(std::move(callback));

    // We transform the result into an owner that disables the callback when released.
    return checkingFocus.then(_qiContextAwarePrivate->inContext([=](Future<std::string> track)
    {
      if (track.hasError())
        qiLogError() << "Error checking focus: " << track.error();

      std::string trackId;
      if (track.hasValue())
        trackId = track.value();

      if (trackId.empty())
      {
        qiLogDebug() << "Focus is not owned";
        return Object<Owner>();
      }

      qiLogDebug() << "Focus is tracked";
      Object<Owner> owner(boost::make_shared<CustomOwner>([=]
      {
        focusTracker->eraseCallback(trackId);
      }));

      _qiContextAwarePrivate->callbackOwners.insert(owner);
      return owner;
    })).unwrap();
  })();
}

struct FunctionAction::Private: SafeContext
{
  Private(std::shared_ptr<Strand> strand);

  /// The name of the action.
  std::string name;

  /// The ongoing function call.
  Future<void> functionFuture;

  /// The state of the action.
  Promise<void> actionPromise;

  /// The start state.
  Promise<void> startingPromise;

  /// The function to call.
  ActionFunction function;

};

FunctionAction::Private::Private(std::shared_ptr<Strand> strand)
  : SafeContext(strand)
{}

FunctionAction::FunctionAction(
    std::string pName,
    ActionFunction function,
    std::shared_ptr<Strand> strand)
  : Action()
  , Named()
  , _functionAction(std::make_unique<Private>(strand))
{
  _functionAction->name = pName;
  name.set(pName).value();

  _functionAction->functionFuture = Future<void>(nullptr);
  _functionAction->function = std::move(function);
  _functionAction->actionPromise.setValue(nullptr);

  // Every time the signal started is emitted, we set the starting promise.
  started.connect(_functionAction->inContext([=]
  {
    _functionAction->startingPromise.setValue(nullptr);
  }));
}


FunctionAction::~FunctionAction()
{
  // Ensure all callbacks are over forever.
  _functionAction->destroy();

  // Interrupt function because it is destroyed.
  auto name = _functionAction->name;
  qiLogDebug() << "Destroying action \"" << name << "\"";
  auto actionPromise = _functionAction->actionPromise;

  try
  { // KLUDGE: cancel may throw, remove when it is guaranteed not to.
    _functionAction->functionFuture.cancel();
  }
  catch (const std::exception& e)
  {
    qiLogVerbose() << "Cancelling function threw: " << e.what();
  }

  _functionAction->functionFuture.then(
        [name, actionPromise](Future<void> result) mutable
  {
    auto logOnLeave = ka::scoped([&]{ qiLogDebug() << "Destroyed action \"" << name << "\""; });
    if (!actionPromise.future().isRunning())
      return; // Action state is already set.
    actionPromise.setError(error::actionDestroyed);
  });

  // Ensure there is no one connected to the signal anymore.
  try
  {
    started.disconnectAll();
  }
  catch (const std::exception& e)
  {
    qiLogError() << "Error disconnecting callbacks from an action's start signal: " << e.what();
  }

  // As long as the callbacks setting the promises are wrapped, it is
  // safe to check their state to know what to do with them.
  if (_functionAction->startingPromise.future().isRunning())
    _functionAction->startingPromise.setError(error::actionDestroyed);
}

/// TODO: add the support of the focus to make something reusable in the Qi SDK.
Future<void> FunctionAction::run()
{
  return _functionAction->inContext([&]
  {
    if (_functionAction->actionPromise.future().isRunning())
      throw std::runtime_error(error::alreadyRunning);

    qiLogVerbose() << "Starting action \"" << _functionAction->name << "\"...";
    _functionAction->actionPromise = Promise<void>();

    // Cancelling the action consists in cancelling the underlying future.
    auto onCancel = _functionAction->inContext([=](Promise<void> runPromise) mutable
    {
      qiLogVerbose() << "Stopping action \"" << _functionAction->name << "\"...";
      _functionAction->functionFuture.cancel();
      _functionAction->functionFuture.then([=](Future<void> result) mutable
      { // Forward errors...
        if (result.hasError())
          runPromise.setError(result.error());
        // ... but otherwise consider it is canceled. Ignore irrelevant result.
        else
          runPromise.setCanceled();
      });
    });

    // The function is allowed to throw, the error will be naturally forwarded by libqi.
    auto functionRunning = _functionAction->function();

    // But reporting the end of the underlying future depends on the state of the action.
    // If the action is being destroyed, this continuation should not be called anymore.
    // For this reason the destructor must set the final state of the promise.
    _functionAction->functionFuture = functionRunning.then(
          _functionAction->inContext([&](Future<void> f)
    {
      QI_ASSERT(!f.isRunning());
      if (f.hasValue())
      {
        qiLogInfo() << "Action \"" << _functionAction->name << "\" has finished successfully.";
        _functionAction->actionPromise.setValue(nullptr);
        QI_EMIT completed();
      }
      else if (f.isCanceled())
      {
        qiLogInfo() << "Action \"" << _functionAction->name << "\" was canceled unexpectedly.";
        _functionAction->actionPromise.setCanceled();
      }
      else if (f.hasError())
      {
        qiLogInfo() << "Action \"" << _functionAction->name << "\" failed with error: " << f.error();
        _functionAction->actionPromise.setError(f.error());
      }
    })).unwrap();

    _functionAction->actionPromise.setOnCancel(std::move(onCancel));
    _functionAction->actionPromise.future().then(_functionAction->inContext([=](Future<void>)
    {
      // Renew the starting promise for the next run.
      _functionAction->startingPromise = Promise<void>();
    }));

    qiLogInfo() << "Action \"" << _functionAction->name << "\" has started.";
    QI_EMIT started();

    return _functionAction->actionPromise.future();
  })();
}


Future<void> FunctionAction::waitForStarted()
{
  return _functionAction->inContext(
        [=]{ return _functionAction->startingPromise.future(); })();
}


struct QiContextAndPDDLAwareAction::Private: SafeContext
{
  Private(std::shared_ptr<Strand> strand);
  boost::shared_ptr<Session> session;
  std::unique_ptr<Action> functionAction;
  std::shared_ptr<Future<void>> functionRunning;
  Promise<void> runPromise;
};

QiContextAndPDDLAwareAction::Private::Private(std::shared_ptr<Strand> strand)
  : SafeContext(strand)
{}


// KLUDGE: trick to register type automatically, even in static:
// this macro defines "initTypePddlAwareQiAction()",
// that can be called when the first instance of QiContextAndPDDLAwareAction is constructed.
REGISTER_PDDLAWAREQIACTION(qi::semantic::agent::QiContextAndPDDLAwareAction);


QiContextAndPDDLAwareAction::QiContextAndPDDLAwareAction(
    boost::shared_ptr<Session> session,
    std::string pName,
    QiContextAwareActionFunction function,
    std::shared_ptr<Strand> strand,
    std::string pPddl)
  : QiContextAware(session),
    PDDLAware(),
    Action(),
    Named(),
    _p(std::make_unique<Private>(strand))
{
  static auto unused = initTypePDDLAwareQiAction();
  boost::ignore_unused(unused);

  name.set(pName).value();
  pddl.set(std::move(pPddl)).value();

  _p->session = session;
  _p->functionRunning = std::make_shared<Future<void>>(nullptr);
  _p->runPromise.setValue(nullptr);

  ActionFunction runFunction = _p->inContext([=, function = std::move(function)]()
  {
    // Throw if it the underlying task is already running.
    // Note that the run promise may still be running,
    // but it is being stopped, so we can drop it.
    // Every time the action must stop, this future is set to finished.
    if (_p->functionRunning->isRunning())
      throw std::runtime_error(error::alreadyRunning);

    // We wrap the result with our own future, that can be cancelled.
    auto functionRunning = std::make_shared<Future<void>>();
    _p->runPromise = Promise<void>([=](Promise<void> runPromise)
    {
      functionRunning->cancel();
      functionRunning->then([runPromise, pName](Future<void> result) mutable
      { // Forward errors...
        qiLogDebug() << "Context-aware action \"" << pName << "\" was canceled";
        if (result.hasError())
          runPromise.setError(result.error());
        // ... but otherwise consider it is canceled. Ignore irrelevant result.
        else
          runPromise.setCanceled();
      });
      *functionRunning = Future<void>(nullptr);
    });

    // Keep it accessible from other contexts,
    // because setting the promise must be possible even after action's death.
    auto runPromise = _p->runPromise;

    // Check the focus, and report an error for when it is lost.
    auto checkingFocus =  QiContextAware::checkFocusAndTrack([=]() mutable
    {
      functionRunning->cancel();
      functionRunning->then([runPromise, pName](Future<void> result) mutable
      { // Forward errors...
        qiLogDebug() << "Focus was lost for action \"" << pName << "\"";
        if (result.hasError())
          runPromise.setError(result.error());
        // ... otherwise publicize a focus lost error. Ignore irrelevant result.
        else
          runPromise.setError(error::focusLost);
      });
      *functionRunning = Future<void>(nullptr);
      return Future<void>(nullptr);
    });

    // Run the function only if we have the focus.
    auto runningFunctionAfterCheckingFocus = checkingFocus.andThen(
          _p->inContext([=, function = std::move(function)](Object<Owner> focusTracking)
    {
      if (!focusTracking)
      {
        qiLogDebug() << "Focus was not owned to start action \"" << pName << "\"";
        return makeFutureError<void>(error::focusNotOwned);
      }
      auto running = function(context.get().value());
      QI_EMIT started();
      return running;
    })).unwrap();

    // Adapt the result to our promise.
    *functionRunning = runningFunctionAfterCheckingFocus
        .then([=](Future<void> f) mutable
    {
      if (f.hasError())
      {
        qiLogDebug() << "Context-aware action \"" << pName << "\" failed: " << f.error();
        runPromise.setError(f.error());
      }
      else if (f.hasValue())
      {
        qiLogDebug() << "Context-aware action \"" << pName << "\" finished successfully";
        runPromise.setValue(nullptr);
      }
      else
      {
        qiLogError() << "Action function was canceled but was not requested to do so";
      }
    });

    // Emit completed when successful.
    auto running = runPromise.future();
    running.andThen(_p->inContext([=](void*)
    {
      QI_EMIT completed();
    }));

    return running;
  });

  _p->functionAction =
      std::make_unique<FunctionAction>(std::move(pName), std::move(runFunction), strand);

  qiLogDebug() << "PDDL-aware action \"" << pName << "\" was created";
}

QiContextAndPDDLAwareAction::~QiContextAndPDDLAwareAction()
{
  _p->destroy();
  auto runPromise = _p->runPromise;

  auto nameText = name.get().value();
  _p->functionRunning->cancel();
  _p->functionRunning->then([nameText, runPromise](Future<void> result) mutable
  { // Forward errors...
    qiLogDebug() << "Destroying action \"" << nameText << "\"";
    if (!result.hasError())
      runPromise.setError(result.error());
    // ... but publicize an action destroyed. Ignore irrelevant result.
    runPromise.setError(error::actionDestroyed);
  });

  *_p->functionRunning = Future<void>(nullptr);
}

Future<void> QiContextAndPDDLAwareAction::run()
{
  return _p->functionAction->run();
}

Future<void> QiContextAndPDDLAwareAction::waitForStarted()
{
  return _p->functionAction->waitForStarted();
}

boost::shared_ptr<Session> QiContextAndPDDLAwareAction::session()
{
  return _p->session;
}

ActionObj makeQiAction(
    boost::shared_ptr<Session> session,
    std::string name,
    ActionObjFunction function,
    std::shared_ptr<Strand> strand,
    const std::string& pddl)
{
  return ActionObj(
        boost::make_shared<QiContextAndPDDLAwareAction>(
          std::move(session), std::move(name), std::move(function), std::move(strand), pddl));
}

ActionObj makeQiActionNoFocusCheck(
    std::string name,
    ActionFunction function,
    std::shared_ptr<Strand> strand)
{
  return ActionObj(
        AnyObject(
          boost::make_shared<FunctionAction>(
            std::move(name), std::move(function), std::move(strand))));
}


ActionObj then(
    boost::shared_ptr<Session> session,
    ActionObj action,
    Continuation continuation,
    std::shared_ptr<Strand> strand,
    std::string name,
    const std::string& pddl)
{
  ActionObjFunction function = [=](context::ContextPtr context)
  {
    // Try setting the context.
    // It is expected to fail for actions that are not aware of the context.
    auto settingContext = action->context.set(context).async();

    auto runningAction = settingContext.then([=](Future<void>)
    {
      return action->async().run();
    }).unwrap();

    auto runningContinuation = runningAction.then(continuation).unwrap();

    return runningContinuation;
  };

  return makeQiAction(
        session,
        name.empty() ? action->name.get().value() : name,
        std::move(function), strand,
        pddl.empty() ? action->pddl.get().value() : pddl);
}

} // agent
} // semantic
} // qi

QI_REGISTER_OBJECT(
    qi::semantic::agent::FunctionAction,
    name, run, started, completed, waitForStarted);
