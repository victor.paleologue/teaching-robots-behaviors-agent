#ifndef SEMANTICAGENT_BEHAVIOR_HPP
#define SEMANTICAGENT_BEHAVIOR_HPP

#include <memory>
#include <string>
#include <qi/semantic/agent/agent.hpp>
#include <src/agent/agent_p.hpp>
#include "action.hpp"
#include "../utility/propertyset.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
template<typename T>
using PropertyPtr = std::shared_ptr<Property<T>>;
using ActionObjs = std::vector<ActionObj>;
using Goal = std::string;
using Goals = std::vector<std::string>;
using ActionFactory = PDDLAwareQiActionFactoryPtr;
using ActionFactories = std::vector<ActionFactory>;

/// A Behavior is an arbitrary planner that can suggest actions to perform.
/// It reacts on knowledge changes or when suggested actions are performed.
/// Like actions, it has preconditions, parameters and effects.
/// Unlike actions, they are not meant to do anything else than suggesting actions.
/// Another component should be responsible to choosing what to do the action suggestions.
/// TODO: study if it is bad if behaviors can update knowledge themselves.
/// TODO: letting a behavior do anything may bypass the task selection, that's no good.
class Behavior
{
public:
  using FunctionType = std::function<Future<void>(PropertyPtr<ActionObjs>)>;
  using FullFunctionType =
      std::function<Future<void>(
          PropertyPtr<ActionObjs>,
          PropertyPtr<Goals>,
          PropertyPtr<ActionFactories>)>;

  /// The constructor takes the backend function and the metadata.
  /// The backend function of a behavior differs in the fact that it should take
  /// a reference to the property to update in time.
  /// The function takes a reference to the property holding the lists of suggestions.
  Behavior(
      const std::string& name,
      FunctionType function,
      const std::string& pddl);

  Behavior(
      const std::string& name,
      FullFunctionType function,
      const std::string& pddl);

  virtual ~Behavior();

  /// Name is constant and read-only.
  Property<std::string> name;

  /// PDDL is constant and read-only.
  Property<std::string> pddl;

  /// Like actions, the behavior can be run.
  Future<void> run();
  Future<void> waitForStarted();

  Signal<> started;

private:
  std::shared_ptr<Property<ActionObjs>> _actionSuggestions;
  std::shared_ptr<Property<Goals>> _goalSuggestions;
  std::shared_ptr<Property<ActionFactories>> _actionFactories;

public:
  /// Currently suggested actions.
  /// TODO: only show the read-only facet here.
  Property<ActionObjs>& actionSuggestions;

  /// Goals expected by the behavior.
  /// TODO: is it really the right way to express them?
  Property<Goals>& goalSuggestions;

  /// Action factories provided by the behavior.
  Property<ActionFactories>& actionFactories;

  /// Speech source to use.
  /// @todo actually use it.
  Property<SpeechSourcePtr> speechSource;

  /// Speech engine to use.
  /// @todo actually use it.
  Property<conversation::SpeechEnginePtr> speechEngine;

private:
  // Mechanics
  std::unique_ptr<Action> _action;
};

std::unique_ptr<Behavior> makeBehaviorFromFunction(
    const std::string& name,
    Behavior::FunctionType function,
    const std::string& pddl);

Future<PDDLAwareQiBehaviorPtr> makeQiBehavior(
    const std::string& name,
    Behavior::FullFunctionType function,
    const std::string& pddl);

std::string actionsToString(const ActionObjs& actions);
} // agent
} // semantic
} // qi

QI_REGISTER_IMPLEMENTATION_H(
    ::qi::semantic::agent::PDDLAwareQiBehavior,
    ::qi::semantic::agent::Behavior);

#endif // SEMANTICAGENT_BEHAVIOR_HPP
