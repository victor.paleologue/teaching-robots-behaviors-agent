#include "action_pool.hpp"
#include <unordered_map>
#include <map>
#include <set>
#include <utility>
#include <boost/algorithm/string.hpp>
#include <boost/range/join.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <pddl/Normalize.h>
#include <qi/jsoncodec.hpp>
#include <qi/locale/util.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/groundedexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticmetagrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticstatementgrounding.hpp>
#include "behavior.hpp"
#include "../pddl/pddl_edit.hpp"
#include "../utility/incontext.hpp"
#include "../utility/owner.hpp"
#include "../utility/semantic_helpers.hpp"
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qisem/semanticcontroller/tool/semexpcomparator.hpp>


qiLogCategory("SemanticAgent.ActionPool");


using namespace pddl::ast;


namespace qi
{
namespace locale
{
bool operator==(const qi::locale::Locale& lhs, const qi::locale::Locale& rhs)
{
  if (lhs.language != rhs.language)
    return false;
  return lhs.region == rhs.region;
}


bool operator<(const qi::locale::Locale& lhs, const qi::locale::Locale& rhs)
{
  if (lhs.language < rhs.language)
    return true;
  return lhs.region < rhs.region;
}
}

namespace semantic
{
namespace agent
{
bool operator==(const ActionParameter& lhs, const ActionParameter& rhs)
{
  return std::tie(lhs.name) == std::tie(rhs.name);
}


bool operator<(const ActionParameter& lhs, const ActionParameter& rhs)
{
  return std::tie(lhs.name) < std::tie(rhs.name);
}


/// Takes the second member of a pair.
template<typename T1, typename T2>
const T2& selectSecond(const std::pair<T1, T2>& pair)
{
  return pair.second;
}


/// Private implementation.
struct ActionPool::Private: SafeContext
{
  /// Context of the semantic memory.
  std::shared_ptr<SemanticMemoryContext> semanticMemoryContext;

  /// Stores all the registered factories, and their PDDL description.
  std::unordered_map<AnyObject, std::shared_ptr<const Description>> factoriesAndPddl;

  /// Index of action factories by PDDL name.
  std::unordered_map<std::string, ActionFactoryWithParameters> factoriesByName;

  /// Stores all the action suggestions, and their PDDL description.
  std::unordered_map<AnyObject, std::shared_ptr<const Description>> actionSuggestionsAndPddl;

  /// Index of action suggestions by PDDL name.
  std::unordered_map<std::string, AnyObject> suggestionsByName;

  /// Behavior for each suggestion.
  std::unordered_map<AnyObject, AnyObject> behaviorBySuggestion;

  /// Stores all the problem providers, and their PDDL description.
  std::unordered_map<std::shared_ptr<ProblemSource>, std::shared_ptr<const Description>> problemsAndPddl;

  /// All the semantic templates, associated with action factory descriptions.
  std::map<
      locale::Language,
      std::unordered_map<
          std::shared_ptr<qisem::SemanticExpression>,
          ActionFactoryWithParameters>> factoriesBySemExp;

  /// The PDDL version of the total problem and domain description.
  StoredProp<std::string> mergedPddl;

  /// Update the PDDL domain according to the pool.
  /// Must be called from within the strand.
  void updateMergedPddl(Property<PDDLDomainAndProblem>& pddlStorage)
  {
    qiLogDebug() << "Updating the merged PDDL world";

    // Wouldn't it be nice to use boost::range and transforms?
    // I tried and I gave up: it had crashy side effects I could not understand,
    // and it took so much more time to compile that it became unbearable.
    std::vector<std::shared_ptr<const Description>> allPddl;
    allPddl.reserve(
          actionSuggestionsAndPddl.size() +
          factoriesAndPddl.size() +
          problemsAndPddl.size());
    auto allPddlInserter = std::back_inserter(allPddl);

    // Gather domains from action suggestions.
    std::transform(
          actionSuggestionsAndPddl.begin(), actionSuggestionsAndPddl.end(),
          allPddlInserter,
          selectSecond<qi::AnyObject, std::shared_ptr<const Description>>);

    // Gather the domains from the factories.
    std::transform(
          factoriesAndPddl.begin(), factoriesAndPddl.end(),
          allPddlInserter,
          selectSecond<qi::AnyObject, std::shared_ptr<const Description>>);

    // Gather all provided problems.
    std::transform(
          problemsAndPddl.begin(), problemsAndPddl.end(),
          allPddlInserter,
          selectSecond<std::shared_ptr<ProblemSource>, std::shared_ptr<const Description>>);

    try
    {
      auto mergedDescription = mergeDescriptions(boost::begin(allPddl), boost::end(allPddl));

      // Serialize.
      PDDLDomainAndProblem pddlData;
      if (mergedDescription)
      {
        if (mergedDescription->domain && !mergedDescription->domain->actions.empty())
          pddlData.domain = serializePddl(*mergedDescription->domain);
        if (mergedDescription->problem)
          pddlData.problem = serializePddl(*mergedDescription->problem.value());
      }

      qiLogDebug()
          << "Merged PDDL is now:\n" << pddlData.domain << "\n" << pddlData.problem;
      pddlStorage.set(std::move(pddlData)).async();
    }
    catch (const std::exception& e)
    {
      qiLogError() << "Error while merging problem descriptions: " << e.what();
    }
  }
};

ActionPool::ActionPool(
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext)
  : _p(new Private())
  , pddl()
{
  _p->semanticMemoryContext = std::move(semanticMemoryContext);
}

ActionPool::~ActionPool()
{
  _p->strand->join();
  _p.reset();
}

/// Throws extract a list of signatures from a map of qi::Object memebers.
template <typename MetaMemberMap>
std::set<std::string> extractMemberSignature(const MetaMemberMap& members)
{
  std::set<std::string> signatures;
  auto resultInserter = std::inserter(signatures, signatures.begin());
  std::transform(
        members.begin(), members.end(), resultInserter,
        [](const typename MetaMemberMap::value_type& it)
  {
    return it.second.toString();
  });
  return signatures;
}

/// Throws if an expected element is missing from the actual set.
template <typename Set>
void checkNoMissing(
    const Set& expected, const Set& actual,
    const std::string& humanReadableElementType)
{
  Set missing;
  auto missingInserter = std::inserter(missing, missing.begin());
  std::set_difference(
        expected.begin(), expected.end(),
        actual.begin(), actual.end(),
        missingInserter);
  if (!missing.empty())
  {
    std::stringstream ss;
    ss << "expected members (" << humanReadableElementType << "):" << std::endl;
    for (const auto& elt: expected)
      ss << elt << std::endl;
    ss << "actual members (" << humanReadableElementType << "):" << std::endl;
    for (const auto& elt: actual)
      ss << elt << std::endl;
    ss << "missing members (" << humanReadableElementType << "):" << std::endl;
    for (const auto& elt: missing)
      ss << "\"" << elt << "\"" << std::endl;
    qiLogDebug() << ss.str();

    const auto firstMissing = *missing.begin();
    throw std::runtime_error(
          std::string{"missing "} + humanReadableElementType
          + std::string{" "} + firstMissing);
  }
}

/// Throws if the given qi::Object instance does not have the expected interface.
template <typename InterfaceType>
void checkInstanceHasInterface(
    qi::AnyObject obj,
    const std::string& humanReadableObjectType)
{
  // Extract statically the information related to the expected type.
  // It is possible because it is declared statically.
  // KLUDGE: QiLang specialized proxies are registered strangely.
  // Making Object<InterfaceType> works but the type interface is not
  // officially registered as an object one. Instead, it is unknown
  // and Object<InterfaceType>::interface() throws.
  // Also typeOf<InterfaceType> is not an instance of ObjectTypeInterface.
  // To workaround this, we look at the first object as if it was an
  // Object<InterfaceType>. Accessing the metaobject of default-constructed
  // Object<InterfaceType> would throw.
  static const auto expectedInterface = Object<InterfaceType>(obj).metaObject();

  try
  {
    const auto& actualInterface = obj.metaObject();

    // Check that methods are present.
    static const auto methods = expectedInterface.methodMap();
    static const auto methodSignatures = extractMemberSignature(methods);
    for (const auto& signature: methodSignatures)
    {
      const auto compatibleMethods =
          actualInterface.findCompatibleMethod(signature);
      if (compatibleMethods.empty())
        throw std::runtime_error(std::string{"missing method "} + signature);
    }

    // Check that expected properties are present.
    static const auto properties = expectedInterface.propertyMap();
    static const auto propertySignatures = extractMemberSignature(properties);
    const auto actualPropertySignatures = extractMemberSignature(actualInterface.propertyMap());
    checkNoMissing(propertySignatures, actualPropertySignatures, "property");

    // Check that expected signals are present.
    static const auto signals = expectedInterface.signalMap();
    static const auto signalSignatures = extractMemberSignature(signals);
    const auto actualSignalSignatures = extractMemberSignature(actualInterface.signalMap());
    checkNoMissing(signalSignatures, actualSignalSignatures, "signal");
  }
  catch (const std::exception& e)
  {
    auto message = [&]
    {
      std::stringstream ss;
      ss << "provided " << humanReadableObjectType
         << " does not respect the expected interface: " << e.what();
      return ss.str();
    }();

    qiLogDebug() << "Object checked wrong, " << message;
    throw std::runtime_error(std::move(message));
  }
}

/// Gets the PDDL of the given object.
/// If an error occurs, a nice error message is produced.
static Future<std::string> getPDDL(
    const AnyObject& obj,
    const std::string& humanReadableObjectType)
{
  return obj.property<std::string>("pddl").async().then(
        [=](const Future<std::string>& futurePddl)
  {
    // Report errors retrieving PDDL data.
    if (!futurePddl.hasValue())
    {
      std::string message = humanReadableObjectType + "'s PDDL could not be retrieved: ";
      if (futurePddl.hasError())
        message += futurePddl.error();
      else if (futurePddl.isCanceled())
        message += "call was canceled";
      else
      {
        message += "unexpected future state";
        QI_ASSERT(false && "unexpected future state");
      }
      throw std::runtime_error(message);
    }

    // else
    return futurePddl.value();
  });
}


/// Parses PDDL.
/// If an error occurs, a nice error message is produced.
static std::shared_ptr<Description> parsePDDL(
    const std::string& pddl,
    const std::string& humanReadableObjectType)
{
  // Parse the PDDL content (also checks its validity).
  try
  {
    return std::shared_ptr<Description>(parsePddlDomainAndProblem(pddl));
  }
  catch (const std::exception& e)
  {
    qiLogVerbose() << "Erroneous PDDL encountered: " << pddl;
    throw std::runtime_error(
          humanReadableObjectType + "'s PDDL could not be parsed: " + e.what());
  }
}


/// Gets the PDDL, and then parses it.
/// If an error occurs, a nice error message is produced.
static Future<std::shared_ptr<Description>> getAndParsePDDL(
    const AnyObject& obj,
    const std::string& humanReadableObjectType)
{
  return getPDDL(obj, humanReadableObjectType)
      .andThen([=](const std::string& pddl)
  {
    return parsePDDL(pddl, humanReadableObjectType);
  });
}


static void checkJustOneAction(const pddl::ast::Domain& domain)
{
  auto nofActions = domain.actions.size();
  if (nofActions != 1u)
  {
    auto errorMessage = [&]
    {
      if (nofActions == 0u)
      {
        return std::string("provided PDDL contains no action");
      }
      else
      {
        return std::string("provided PDDL contains ") + std::to_string(nofActions)
            + " actions but should have contained only one";
      }
    }();

    throw std::runtime_error(errorMessage);
  }
}


Future<Object<Owner>> ActionPool::addFactory(qi::AnyObject factory)
{
  // First check that the factory has the required interface.
//  checkInstanceHasInterface<PDDLAwareQiActionFactory>(factory, "factory");
  qiLogInfo()
      << "Adding factory " << factory.uid();

  // Semantic
  auto expressionsByLocale =
      std::make_shared<
          std::map<locale::Locale, std::shared_ptr<qisem::SemanticExpression>>>();

  // Getting the natural language templates of the factory.
  auto gettingSemantic =
      factory.property<std::map<std::string, SemanticTemplate>>("semanticTemplates").async();

  // Building semantic expressions from the language templates.
  auto gettingSemanticAndThenPDDL = gettingSemantic
      .andThen(_p->semanticMemoryContext->strand()->unwrappedSchedulerFor(
                 [=](const std::map<std::string, SemanticTemplate>& semanticTemplates) mutable
  {
    for (const auto& semanticTemplate: semanticTemplates)
    {
      qiLogInfo()
          << "Factory " << factory.uid()
          << " has semantic template: " << qi::encodeJSON(semanticTemplate);
      auto locale = locale::localeFromStr(semanticTemplate.first);

      auto statementGrounding = std::make_unique<qisem::SemanticStatementGrounding>();
      statementGrounding->word =
          qisem::SemanticWord(
            localeToSemanticEnum(locale),
            semanticTemplate.second.lemma,
            qisem::PartOfSpeech::VERB);

      auto groundedExpression =
          std::make_unique<qisem::GroundedExpression>(std::move(statementGrounding));

      for (const auto& slotSpec: semanticTemplate.second.children)
      {
        try
        {
          auto gramTypeStr = slotSpec.first;
          boost::algorithm::to_lower(gramTypeStr);
          auto grammaticalType = qisem::grammaticalType_fromStr(gramTypeStr);
          const auto& parameterName = slotSpec.second;
          auto metaGrounding =
              std::make_unique<qisem::SemanticMetaGrounding>(
                qisem::SemanticGroudingType::META, 0, parameterName);
          groundedExpression->children[std::move(grammaticalType)] =
              std::make_unique<qisem::GroundedExpression>(std::move(metaGrounding));
        }
        catch (const std::exception& e)
        {
          qiLogError() << "Error translating semantic slot: " << e.what();
        }
      }

      auto semanticExpression = qisem::UniqueSemanticExpression(std::move(groundedExpression));
      qiLogInfo()
          << "Registered semantic template: " << std::endl << *semanticExpression;
      expressionsByLocale->insert(
            std::make_pair(std::move(locale), semanticExpression.getSharedPtr()));
    }

    // Then, check the PDDL data.
    return getAndParsePDDL(factory, "factory");
  })).unwrap();

  // We have everything, let us add the factory.
  auto addingFactory = gettingSemanticAndThenPDDL.andThen(
       _p->inContext([=](const std::shared_ptr<Description>& actionDescription)
  {
    // Effectively put the factory in the list.
    // Note that it maintains a reference to the factory.
    auto result = _p->factoriesAndPddl.insert(std::make_pair(factory, actionDescription));
    if (!result.second)
    {
      throw std::runtime_error("provided factory was already registered");
    }

    const auto& pddlActions = actionDescription->domain->actions;
    if (pddlActions.size() != 1)
      throw std::runtime_error("action factory should have only one action");

    checkJustOneAction(*actionDescription->domain);
    const auto& action = pddlActions[0];
    qiLogInfo()
        << "Action factory registered:\n" << serializePddl(action);
    const auto& actionName = action->name;
    ActionFactoryWithParameters actionAndParamSpecs;
    actionAndParamSpecs.actionFactory = factory;
    actionAndParamSpecs.parameters.reserve(action->parameters.size());
    for (const auto& parameter: action->parameters)
      actionAndParamSpecs.parameters.push_back({parameter->name});
    _p->factoriesByName[action->name] = actionAndParamSpecs;

    // Update the domain with this new action.
    _p->updateMergedPddl(pddl);

    for (const auto& expression: *expressionsByLocale)
    {
      _p->factoriesBySemExp[expression.first.language][expression.second] = std::move(actionAndParamSpecs);
    }

    // Return an owner that removes the factory when released.
    // Note that it drops the reference to the factory?
    std::function<Future<void>()> eraseFactory = _p->inContext([=]
    {
      for (const auto& expression: *expressionsByLocale)
      {
        _p->factoriesBySemExp[expression.first.language].erase(expression.second);
      }

      _p->factoriesAndPddl.erase(factory);
      _p->factoriesByName.erase(actionName);

      // Update the domain without this action.
      _p->updateMergedPddl(pddl);
    });

    return Object<Owner>(boost::make_shared<CustomOwner>(std::move(eraseFactory)));
  })).unwrap();

  return addingFactory;
}


Future<Object<Owner>> ActionPool::addBehavior(AnyObject behavior)
{
  // First check that the factory has the required interface.
//  checkInstanceHasInterface<PDDLAwareQiBehavior>(behavior, "behavior");

  return behavior.property<std::string>("name").async()
      .andThen([=](const std::string& name)
  {
    qiLogInfo() << "Adding behavior \"" << name << "\"";

    // Tracking action suggestions.
    auto changeTracker = std::make_shared<ChangeTracker<AnyObject>>();

    // TODO: protect access to _p here.
    // boost::function is used here to please AnyObject.connect("...", callback).
    boost::function<Future<void>(const std::vector<AnyValue>&)> onSuggestionChanged =
        _p->inContext([=](const std::vector<AnyValue>& anySuggestions)
    {
      // KLUDGE: Behavior provided from Java cannot advertise lists of AnyObject,
      // instead it will resemble lists of AnyValue.
      std::vector<AnyObject> suggestions;
      suggestions.reserve(anySuggestions.size());
      std::transform(
            anySuggestions.begin(), anySuggestions.end(),
            std::back_inserter(suggestions),
            [](const AnyValue& value){ return value.to<AnyObject>(); });

      auto change = changeTracker->check(suggestions);
      bool mergedDomainUpdateIsRequired = false;

      // Get and keep the names of actions.
      for (const auto& action: change.inserted)
      {
        try
        {
//          checkInstanceHasInterface<PDDLAwareQiAction>(action, "action");
          auto pddlDescription = getPDDL(action, "action")
              .then([=](Future<std::string> futurePddl)
          {
            if (futurePddl.hasValue())
              return futurePddl.value();

            // Else, fall back to a default PDDL.
            // The logic of this fallback is tautological:
            // the effect of the action is to perform itself,
            // and the goal of the behavior is to perform the action.
            std::stringstream fallbackPddl;
            fallbackPddl
                << "(define (domain tmp)\n"
                << "  (:requirements :strips :typing :negative-preconditions)\n"
                << "  (:types\n"
                << "    act\n"
                << "  )\n"
                << "  (:predicates\n"
                << "    (performed ?a - act)\n"
                << "  )\n"
                << "  (:constants suggested_act_" << action.uid() << " - act)\n"
                << "  (:action suggested_action_" << action.uid() << "\n"
                << "    :parameters ()\n"
                << "    :effect (performed suggested_act_" << action.uid() << ")\n"
                << "  )\n"
                << ")"
                << "(define (problem merged_problem)\n"
                << "(:domain tmp)\n"
                << "(:requirements :strips :typing :negative-preconditions)\n"
                << "(:objects)\n"
                << "(:init)\n"
                << "(:goal (performed suggested_act_" << action.uid() << "))\n";

            qiLogDebug() << "Fallback PDDL generated:\n" << fallbackPddl.str();
            return fallbackPddl.str();
          })
              .andThen([=](const std::string& pddl)
          {
            qiLogDebug()
                << "Base PDDL for suggested action " << action.uid() << ": " << pddl;
            auto description = parsePDDL(pddl, "action");

            // Actions are suggested because their origin behavior
            // believes it is what the robot should do.
            // Whichever the effect the action is meant to have,
            // the behavior intends to produce it.
            // Therefore it implies an additional goal to the overall problem.
            // Here we warn if no effect or goal was stated.
            auto& effectOpt = description->domain->actions[0]->effect;
            if (!effectOpt)
            { // TODO: auto-generate an effect?
              qiLogWarning()
                  << "Suggested action " << action.uid()
                  << " has no effect and may never be selected";
            }
            else if (!name.empty())
            {
              // Every suggestion has for effect
              // to enter the context of its behavior.
              // This way, we can favor keeping in the same context.
              auto predicateDeclaration =
                  ensurePredicate(*description, "in_context", {{"cxt", "context"}});
              auto behaviorContext =
                  ensureConstant(*description, pddlNameFromPhrase(name), "context");
              auto arg1 = std::make_unique<Constant>(behaviorContext);
              auto args = Terms();
              args.push_back(std::move(arg1));
              auto newEffect =
                  std::make_unique<Predicate>(
                    std::move(args), predicateDeclaration);

              auto& effect = effectOpt.value();
              if (effect.is<AndPointer<Effect>>())
              { // If it is already a "and", append the new effect.
                auto& asAnd = effect.get_unchecked<AndPointer<Effect>>();
                asAnd->arguments.push_back(std::move(newEffect));
              }
              else
              { // Else, make an "and" to combine the effects.
                std::vector<Effect> effects;
                effects.push_back(std::move(effect));
                effects.push_back(std::move(newEffect));
                effectOpt = std::make_unique<And<Effect>>(std::move(effects));
              }
            }

            if (!description->problem || !description->problem.value()->goal)
            {
              qiLogWarning()
                  << "Suggested action " << action.uid()
                  << " has no goal and may never be selected";
            }

            return description;
          }).value();

          const auto& domain = *pddlDescription->domain;
          checkJustOneAction(domain);
          const auto& actionAST = domain.actions[0];
          qiLogInfo()
              << "Behavior \"" << name << "\" suggests action \""
              << actionAST->name << "\" (object " << action.uid() << ")";
          qiLogInfo()
              << "PDDL of action " << actionAST->name << ":" << std::endl
              << serializePddl(actionAST);

          _p->actionSuggestionsAndPddl[action] = pddlDescription;
          _p->behaviorBySuggestion[action] = behavior;
          _p->suggestionsByName[actionAST->name] = action;
          mergedDomainUpdateIsRequired = true;
        }
        catch (const std::exception& e)
        {
          qiLogError()
              << "Action suggested by behavior \"" << name
              << "\" is erroneous and will be ignored: " << e.what();
        }
      }

      for (const auto& action: change.removed)
      {
        auto found = _p->actionSuggestionsAndPddl.find(action);
        if (found == _p->actionSuggestionsAndPddl.end())
          continue;

        const auto& actionName = found->second->domain->actions[0]->name;
        _p->suggestionsByName.erase(actionName);
        _p->behaviorBySuggestion.erase(action);
        qiLogInfo()
            << "Behavior \"" << name << "\" dropped action \"" << actionName
            << "\" (object " << action.uid() << ")";
        _p->actionSuggestionsAndPddl.erase(found);

        mergedDomainUpdateIsRequired = true;
      }

      if (mergedDomainUpdateIsRequired)
        _p->updateMergedPddl(pddl);
    });

    return behavior.connect("actionSuggestions", onSuggestionChanged).async()
        .andThen([=](SignalLink link)
    {
      return behavior.property<std::vector<AnyValue>>("actionSuggestions").async()
          .andThen(onSuggestionChanged).unwrap()
          .andThen([=](void*)
      {
        std::function<Future<void>()> eraseBehavior = [=]()
        {
          return behavior.disconnect(link).async()
              .then([=](Future<void>)
          {
            return onSuggestionChanged(std::vector<AnyValue>());
          }).unwrap()
              .then([=](Future<void>)
          {
            qiLogDebug() << "Behavior \"" << name << "\" was unregistered";
          });
        };

        qiLogDebug() << "Behavior \"" << name << "\" was registered";
        return Object<Owner>(boost::make_shared<CustomOwner>(std::move(eraseBehavior)));
      });
    }).unwrap();
  }).unwrap();
}

/// Adds a problem provider.
/// The provided problem will be added to the overall problem.
Future<Object<Owner>> ActionPool::addProblemSource(
    std::shared_ptr<ProblemSource> problemProvider)
{
  return _p->inContext([=]
  {
    auto found = _p->problemsAndPddl.find(problemProvider);
    if (found != _p->problemsAndPddl.end())
      throw std::runtime_error("problem provider already in action pool");

    auto updateProblem = _p->inContext(
          [=](std::shared_ptr<const pddl::ast::Description> problemDescription)
    {
      _p->problemsAndPddl[problemProvider] = problemDescription;
      _p->updateMergedPddl(pddl);
    });
    auto connection = problemProvider->connect(updateProblem);
    updateProblem(problemProvider->get());

    return Object<Owner>(
          boost::make_shared<CustomOwner>(
            CustomOwner::ReleaseFunction(_p->inContext([=]
    {
      connection.disconnect();
      _p->problemsAndPddl.erase(problemProvider);
      _p->updateMergedPddl(pddl);
    }))));
  })();
}


Future<AnyObject> ActionPool::findActionSuggestion(const std::string& name) const
{
  return _p->inContext([=]
  {
    auto found = _p->suggestionsByName.find(name);
    if (found != _p->suggestionsByName.end())
      return found->second;
    throw std::runtime_error(
          std::string("no action suggestion named \"") + name + "\"");
  })();
}


Future<BehaviorAndSuggestion> ActionPool::findActionSuggestionAndBehavior(
        const std::string& name) const
{
  return _p->inContext([=]
  {
    auto suggestionIt = _p->suggestionsByName.find(name);
    if (suggestionIt == _p->suggestionsByName.end())
        throw std::runtime_error(
              std::string("no action suggestion named \"") + name + "\"");
    auto behaviorIt = _p->behaviorBySuggestion.find(suggestionIt->second);
    QI_ASSERT(behaviorIt != _p->behaviorBySuggestion.end());
    if (behaviorIt == _p->behaviorBySuggestion.end())
        throw std::runtime_error(
              std::string("no behavior associated to action suggestion \"") + name + "\"");
    return BehaviorAndSuggestion{behaviorIt->second, behaviorIt->first};
  })();
}


Future<ActionFactoryWithParameters>
ActionPool::findActionFactory(
    const std::string& name) const
{
  return _p->inContext([=]
  {
    auto found = _p->factoriesByName.find(name);
    if (found != _p->factoriesByName.end())
      return found->second;
    throw std::runtime_error(
          std::string("no action factory named \"") + name + "\"");
  })();
}


Future<std::vector<ActionFactoryWithSemanticSlots>>
ActionPool::findActionFactory(
    const std::shared_ptr<qisem::SemanticExpression>& statement,
    const locale::Locale& locale) const
{
  return _p->inContext([=]
  {
    std::vector<ActionFactoryWithSemanticSlots> actionsWithSlots;
    const auto& factoriesBySemanticTemplate = _p->factoriesBySemExp[locale.language];
    qiLogInfo()
        << "There are " << factoriesBySemanticTemplate.size()
        << " factory(ies) for current language " << locale::languageToStr(locale.language);

    for (const auto& factoryForTemplate: factoriesBySemanticTemplate)
    {
      const auto& semanticTemplate = factoryForTemplate.first;
      qisem::SemExpComparator::ComparisonExceptions ignoreOptions;
      ignoreOptions.request = true;
      ignoreOptions.verbTense = true;
      ignoreOptions.grammaticalTypes.insert(qisem::GrammaticalType::SUBJECT);

      qisem::ImbricationType imbrication =
          qisem::SemExpComparator::getSemExpsImbrications(
            *semanticTemplate, *statement,
            _p->semanticMemoryContext->semanticMemory()->memBloc,
            *_p->semanticMemoryContext->linguisticDatabase(),
            &ignoreOptions);

      static const std::set<qisem::ImbricationType> targetImbrications =
          {qisem::ImbricationType::CONTAINS,
           qisem::ImbricationType::ISCONTAINED,
           qisem::ImbricationType::EQUALS};

      qiLogInfo()
          << "Semantic expression:\n"
          << *statement << "\nAction template:\n"
          << *semanticTemplate << "\nImbrication: "
          << static_cast<int>(imbrication);

      if (targetImbrications.count(imbrication))
      {
        ActionFactoryWithSemanticSlots factoryWithSlots;
        const auto& factoryWithParamSpecs = factoryForTemplate.second;
        factoryWithSlots.actionFactory = factoryWithParamSpecs.actionFactory;
        const auto& groundedTemplate = semanticTemplate->getGrdExp();
        const auto groundedStatement = statement->getGrdExpPtr_SkipWrapperPtrs();
        QI_ASSERT(groundedStatement);

        for (const auto& child: groundedTemplate.children)
        {
          const auto& slotName =
              child.second->getGrdExp().grounding().getMetaGrounding().attibuteName;
          factoryWithSlots.semanticSlots[{slotName}] =
              groundedStatement->children.at(child.first)->clone();
        }
        actionsWithSlots.push_back(std::move(factoryWithSlots));
      }
    }
    return actionsWithSlots;
  })();
}
} // agent
} // semantic
} // qi

QI_REGISTER_OBJECT(qi::semantic::agent::ActionPool, pddl, addFactory, addBehavior)
