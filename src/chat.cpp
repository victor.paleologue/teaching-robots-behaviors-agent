#include "chat.hpp"
#include <functional>
#include <memory>
#include <unordered_map>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/trim_all.hpp>
#include <boost/core/ignore_unused.hpp>

#include <qi/log.hpp>
#include <qi/periodictask.hpp>
#include <qi/context/commonerrormessages.hpp>
#include <qi/context/utils.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include <pddl/AST.h>

#include "autonomous_planning.hpp"
#include "extractor.hpp"
#include "knowledge_pddl.hpp"
#include "knowledge_pruning.hpp"
#include "behaviormodel/action_pool.hpp"
#include "pddl/pddl_read.hpp"
#include "utility/disposable.hpp"
#include "utility/knowledge.hpp"
#include "utility/incontext.hpp"
#include "utility/owner.hpp"

qiLogCategory("SemanticAgent.Chat");

using namespace pddl::ast;

namespace qi
{
namespace semantic
{
namespace agent
{
struct BehaviorControlImpl
{
  BehaviorControlImpl(const std::string& pName);
  Property<std::string> name;
  Property<bool> enabled;
};
} // agent
} // semantic
} // qi

QI_REGISTER_IMPLEMENTATION_H(qi::semantic::agent::BehaviorControl, qi::semantic::agent::BehaviorControlImpl)
// KLUDGE: see Chat's trick below.
REGISTER_BEHAVIORCONTROL(qi::semantic::agent::BehaviorControlImpl)

// KLUDGE: trick to register type automatically, even in static:
// this macro defines "initTypeChat()", that can be called when the first
// instance of ChatImpl is constructed.
// TODO: either let QiLang generate a separate macro to separate the
// definition of the registration function from calling it, or let it be
// invoked from the constructor's body.
REGISTER_CHAT(qi::semantic::agent::Chat)

namespace qi
{
namespace semantic
{
namespace agent
{
BehaviorControlImpl::BehaviorControlImpl(const std::string& pName)
{
  static auto unused = initTypeBehaviorControl();
  boost::ignore_unused(unused);
  name.setValue(pName);
  enabled.setValue(true);
}

std::unique_ptr<Disposable> makeDisposable(OwnerPtr owner)
{
  return makeDisposable(DisposalFunction([=]
  {
    return owner->async().release();
  }));
}

struct ChatImpl::Private: public SafeContext
{
  Private(
        Promise<void>& pSharedChatting,
        std::shared_ptr<ActionPool> pActionPool)
    : sharedChatting(pSharedChatting),
      actionPool(pActionPool)
  {}

  ~Private() { destroy(); }

  /// The shared knowledge between behaviors.
  knowledge::EditableKnowledgeBasePtr knowledge;

  /// Session towards the private service directory.
  SessionPtr session;

  /// The qi context for actions to run properly.
  context::ContextPtr context;

  /// A utility to easily track the focus.
  std::unique_ptr<context::FocusTracker> focusTracker;

  /// Keeps alive the extractor producing knowledge events.
  std::shared_ptr<Extractor> extractor;

  /// Shared reference to any running chat task.
  Promise<void>& sharedChatting;

  /// Behaviors to run when chatting.
  std::vector<PDDLAwareQiBehaviorPtr> behaviors;

  /// Function to call to interrupt the action when the action is destroyed.
  std::function<void()> onDestroyed;

  /// Keeps everything there is to know about possible and suggested actions.
  std::shared_ptr<ActionPool> actionPool;

  /// Whatever should be cleaned up at destruction can be put here.
  Disposables disposables;
};

ChatImpl::ChatImpl(
      context::ContextPtr context,
      std::shared_ptr<Strand> strand,
      SessionPtr session,
      std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
      Object<knowledge::EditableKnowledgeBase> knowledge,
      std::shared_ptr<Extractor> extractor,
      SpeechSourcePtr speechSource,
      conversation::SpeechEnginePtr speechEngine,
      Promise<void>& chatting,
      std::vector<PDDLAwareQiBehaviorPtr> behaviors,
      std::unordered_set<AnyObject> actionFactories,
      std::shared_ptr<ActionPool> actionPool)
    : language(
        std::string{},
        [](const std::string&)->std::string{ throw std::runtime_error{"language support is not implemented"}; },
        [](std::string&, const std::string&)->bool{ throw std::runtime_error{"language support is not implemented"}; },
        SignalBase::OnSubscribers{}),
      confidenceThreshold(extractor->confidenceThreshold),
      _p(std::make_unique<Private>(chatting, actionPool))
{
  _p->knowledge = knowledge;
  _p->strand = std::move(strand);
  _p->session = session;
  _p->context = std::move(context);
  _p->focusTracker = std::make_unique<context::FocusTracker>(session, _p->context);
  _p->extractor = extractor;
  _p->sharedChatting = chatting;
  _p->behaviors = std::move(behaviors);
  _p->onDestroyed = []{};

  static const auto unused = initTypeChat();
  boost::ignore_unused(unused);

  // Signal when something is being said or heard.
  QI_EMIT listening.setValue(false);

  SignalLink sayingLink =
      speechEngine->saying.connect(_p->inContext([this](const conversation::Phrase& said)
  {
    qiLogVerbose("SemanticAgent.Transcription") << "R: " << said.text;
    QI_EMIT saying(said);
  }));

  _p->disposables.add(
        makeDisposable(
          DisposalFunction(
            _p->inContext([=]{ speechEngine->saying.disconnectAsync(sayingLink); }))));

  speechSource->heard.connect(
        _p->inContext([this](const Utterance& utterance)
  {
    const auto& phrases = utterance.possiblePhrases;
    if (phrases.empty())
      return;
    qiLogVerbose("SemanticAgent.Transcription") << "H: " << phrases[0].text;
    QI_EMIT heard(conversation::Phrase{phrases[0].text});
  }));

  // Put behaviors in the action pool, if and when they are enabled.
  // ==========================================================================
  std::vector<BehaviorControlPtr> lBehaviorControls;
  for (const auto& behavior: _p->behaviors)
  { // TODO: do this in a more asynchronous manner
    auto name = behavior->name.get().value();
    qiLogDebug() << "Making behavior control for \"" << name << "\"";
    auto behaviorControl = boost::make_shared<BehaviorControlImpl>(name);

    auto controlDisposables = std::make_shared<Disposables>();
    auto controlDisposed = std::make_shared<bool>(true);
    auto enableBehavior = _p->inContext([=](bool enable) mutable
    {
      if (enable && *controlDisposed)
      {
        *controlDisposed = false;
        auto owner = _p->actionPool->addBehavior(behavior).value();
        controlDisposables->add(makeDisposable(owner));
        auto factories = behavior->actionFactories.get().value();
        if (factories.empty())
          qiLogDebug()
              << "No factory provided by behavior \"" << name << "\"";
        else
          qiLogDebug()
              << "Behavior \"" << name << "\" provides factories";

        for (const auto& factory: factories)
        {
          try
          {
            owner = _p->actionPool->addFactory(factory).value();
            controlDisposables->add(makeDisposable(owner));
          }
          catch (const std::exception& e)
          {
            qiLogWarning()
                << "Failed to register a factory: " << e.what();
          }
        }

        try
        {
          behavior.setProperty("speechSource", speechSource).value();
          behavior.setProperty("speechEngine", speechEngine).value();
        }
        catch (const std::exception& e)
        {
          qiLogInfo() << "Behavior " << name << " did not accept speech source "
                      << "or speech engine: " << e.what();
        }
      }
      else if (!enable && !*controlDisposed)
      {
        qiLogInfo() << "Removing behavior " << name;
        *controlDisposed = true;
        controlDisposables->dispose();

        try
        {
          behavior.setProperty("speechSource", AnyObject());
          behavior.setProperty("speechEngine", AnyObject());
        }
        catch (const std::exception& e)
        {
          qiLogDebug() << "Behavior " << name
                       << " could not reset speech source or speech engine: "
                       << e.what();
        }
      }
    });

    auto link = behaviorControl->enabled.connect(enableBehavior);
    enableBehavior(behaviorControl->enabled.get().value());
    _p->disposables.add(makeDisposable([=]() mutable
    {
      behaviorControl->enabled.disconnect(link);
      enableBehavior(false);
    }));

    lBehaviorControls.emplace_back(std::move(behaviorControl));
  }
  behaviorControls.set(std::move(lBehaviorControls));

  // Put action factories in the action pool.
  // =========================================================================
  for (const auto& factory: actionFactories)
  {
    auto ownerPtr = _p->actionPool->addFactory(factory).value();
    _p->disposables.add(makeDisposable([=]{ ownerPtr->release(); }));
  }
}

ChatImpl::~ChatImpl()
{
  qiLogInfo() << "Chat destroyed";
  _p->onDestroyed();
  _p->disposables.dispose().then([](Future<void> f)
  {
    if (f.hasError())
      qiLogWarning()
          << "Errors in chat's clean-up: " << f.error();
  });
}

Future<void> ChatImpl::run()
{
  return _p->inContext([=]
  {
    auto safe = _p->inContextTransform();

    // Action state management
    //------------------------

    // If the action is already running, throw immediately.
    if (_p->sharedChatting.future().isRunning())
    {
      qiLogInfo() << "Chat " << this << " refused: already chatting";
      throw std::runtime_error(error::alreadyRunning);
    }

    // The action state relies on a future.
    auto chattingPromise = Promise<void>();
    _p->sharedChatting = chattingPromise;

    // Every clean-up operation should be put here.
    auto runDisposables = std::make_shared<Disposables>();

    // General clean-up operation, named "interrupt".
    auto disposing = std::make_shared<Future<void>>();
    auto interrupt = [runDisposables, disposing]() mutable
    {
      if (disposing->isValid())
        return *disposing;
      *disposing = runDisposables->dispose();
      return *disposing;
    };

    // When cancelling, interrupt and set promise to canceled.
    auto onCancelRequested = [=](Promise<void> chatting) mutable
    {
      qiLogInfo() << "Canceling chat...";
      qiLogInfo() << "Chat canceled";
      interrupt().then([=](Future<void>) mutable { chatting.setCanceled(); });
    };

    chattingPromise.setOnCancel(std::move(onCancelRequested));

    // When focus is lost, interrupt and set promise to "focus lost" error.
    auto onFocusLost = [=]() mutable
    {
      qiLogInfo() << "Focus lost, interrupting chat...";
      qiLogInfo() << "Chat interrupted";
      return interrupt().then([=](Future<void>) mutable {
        chattingPromise.setError(error::focusLost);
      });
    };

    auto startTrackingFocus = _p->focusTracker->trackFocus(std::move(onFocusLost));

    // When the focus is tracked, it is also checked, and the action can start.
    return startTrackingFocus.andThen(safe([=](const std::string& trackingId) mutable
    {
      if (trackingId.empty())
      {
        qiLogInfo() << "Chat " << this << " refused: no focus";
        return makeFutureError<void>(error::focusNotOwned);
      }

      // When we stop the action, we stop tracking the focus too.
      runDisposables->add(makeDisposable(DisposalFunction(safe([=]
      {
        return _p->focusTracker->eraseCallback(trackingId);
      }))));

      qiLogInfo() << "Starting chatting " << this;

      // The subtasks
      // ======================================================================
      // Start autonomous planning
      //--------------------------
      runDisposables->add(
            runAutonomousPlanning(
              _p->strand, _p->session, _p->context, _p->knowledge, _p->actionPool));

      // Starting speech recognition
      //----------------------------
      auto startingSpeechExtraction = _p->extractor->waitForNextStartOfSpeechExtraction();
      auto extractingSpeech = _p->extractor->extractSpeech();
      runDisposables->add(makeDisposable(DisposalFunction([=]() mutable
      {
        extractingSpeech.cancel();
        return extractingSpeech;
      })));

      startingSpeechExtraction.andThen(safe([=](void*) mutable
      {
        qiLogInfo() << "Chatting started " << this;
        QI_EMIT listening.setValue(true);
        QI_EMIT started();
      }));
      //=======================================================================

      // Reset relevant properties when finished.
      auto chattingFuture = chattingPromise.future();
      chattingFuture.then(safe([this](Future<void>)
      {
        QI_EMIT listening.setValue(false);

        // KLUDGE: stop recording just in case the Android client did not.
        try
        {
          _p->session->service("MultimodalRecorder").async().andThen([](AnyObject mtt)
          {
            mtt.async<void>("stopRecording");
          });

        }
        catch (const std::exception& e)
        {
          qiLogDebug() << "Error stopping to record: " << e.what();
        }
      }));

      // When action is destroyed interrupt and set proper error.
      _p->onDestroyed = [=]() mutable
      {
        try
        {
          if (chattingFuture.isRunning())
            chattingPromise.setError(error::actionDestroyed);
        }
        catch(const std::exception& e)
        {
          qiLogDebug() << "Failed to set promise at destruction: " << e.what();
        }
        interrupt();
        chattingFuture.wait();
      };

      return chattingFuture;
    })).unwrap();
  })();
}
} // agent
} // semantic
} // qi
