#include <qi/applicationsession.hpp>
#include "client.hpp"

qiLogCategory("SemanticAgent");

int main(int argc, char** argv)
{
  qi::ApplicationSession app{argc, argv};
  int retCode = EXIT_SUCCESS;

  auto session = app.session();
  session->setClientAuthenticatorFactory(
        boost::make_shared<qi::semantic::agent::UserPassAuthProviderFactory>());

  app.atRun([&]
  {
    try
    {
      qi::semantic::agent::AgentPtr agent = session->service("SemanticAgent").value();
      qiLogInfo() << "Found SemanticAgent.";
      auto chatting = qi::semantic::agent::chatPreemptFocus(session, agent).then([&](qi::Future<void> f)
      {
        if (f.hasError())
          qiLogError() << "Chat failed: " << f.error();
        if (!f.isCanceled())
          app.stop();
      });
      qiLogInfo() << "Chatting...";

      app.atStop([chatting]() mutable
      {
        chatting.cancel();
      });
    }
    catch (const std::exception& e)
    {
      qiLogError() << e.what();
      retCode = EXIT_FAILURE;
      app.stop();
    }
  });

  app.run();
  return retCode;
}
