#include "knowledge_semantic.hpp"
#include <map>
#include <qi/jsoncodec.hpp>
#include <qi/log.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticgenericgrouding.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qisem/semanticcontroller/tool/semexpcomparator.hpp>
#include "utility/knowledge.hpp"
#include "utility/disposable.hpp"
#include "utility/incontext.hpp"
#include "utility/semantic_helpers.hpp"

qiLogCategory("SemanticAgent.KnowledgeSemantic");

using namespace qisem;

namespace qi
{
namespace semantic
{
namespace agent
{

struct KnowledgeSemanticBridge::Private: SafeContext
{
  ~Private() { destroy(); }

  /// The knowledge base we track for labels.
  knowledge::KnowledgeBasePtr knowledgeBase;

  /// The semantic memory context.
  std::shared_ptr<SemanticMemoryContext> semanticMemoryContext;

  /// Maintaining the association of semantic expressions with nodes.
  // KLUDGE: the semantic memory could be used for this,
  // but we cannot yet because of this: https://gitlab.aldebaran.lan/naoqi/mind/issues/21
  std::map<
      knowledge::ResourceNode,
      std::map<
          locale::Language,
          std::shared_ptr<SemanticExpression>>> expressionByNode;

  /// Whether the extractor is running.
  Promise<void> promise;

  /// Everything that must be cleaned when stopped.
  std::shared_ptr<Disposables> disposables;
};


KnowledgeSemanticBridge::KnowledgeSemanticBridge(
    knowledge::KnowledgeBasePtr knowledgeBase,
    std::shared_ptr<SemanticMemoryContext> semanticContext)
  : _p(std::make_unique<Private>())
{
  _p->strand = semanticContext->strand();
  _p->knowledgeBase = knowledgeBase;
  _p->semanticMemoryContext = semanticContext;
  _p->promise.setValue(nullptr);
}


KnowledgeSemanticBridge::~KnowledgeSemanticBridge()
{
  if (_p->disposables)
    _p->disposables->dispose();
  _p.reset();
}


Future<void> KnowledgeSemanticBridge::run()
{
  static const std::set<knowledge::Node> supportedNodeTypes{
    teaching::objectTypeNode(),
    teaching::areaTypeNode()};

  return _p->inContext([=]
  {
    if (_p->promise.future().isRunning())
      throw std::runtime_error("already running");

    // Set cancellation function.
    auto disposables = std::make_shared<Disposables>();
    Promise<void> promise([=](Promise<void>) mutable
    {
      qiLogDebug() << "Stopping...";
      disposables->dispose();
    });

    // Setting the promise to finished is part of the clean-up.
    disposables->add(makeDisposable([=]() mutable
    {
      promise.setValue(nullptr);
      qiLogDebug() << "Stopped";
    }));

    // Reset the members.
    _p->promise = promise;
    _p->disposables = disposables;

    // On new labeling triple, extract the semantics of the label.
    auto onTripleAdded = [=](const knowledge::Triple& triple) mutable
    {
      qiLogDebug()
          << "Tracked triple added: " << qi::encodeJSON(triple);

      const auto types =
          _p->knowledgeBase->queryObjects(triple.subject, teaching::rdfTypeResource(), "");
      auto found = std::find_if(
            types.begin(), types.end(),
            [&](const knowledge::Node& typeNode)
      {
        return static_cast<bool>(supportedNodeTypes.count(typeNode));
      });

      if (found == types.end())
        return; // This node type is not supported, ignore it.

      try
      {
        auto label = triple.object.value.to<knowledge::LocalizedString>();
        auto labelExpression =
            phraseToSemanticExpression(*_p->semanticMemoryContext, {label.string, label.locale});
        labelExpression =
            labelExpression->getGrdExpPtr_SkipWrapperPtrs()->clone();
        _p->expressionByNode[triple.subject][label.locale.language] = std::move(labelExpression);
        QI_EMIT updated();
      }
      catch (const std::exception& e)
      {
        qiLogWarning() << "Error handling label in knowledge: " << e.what();
      }
    };

    // On loss of labeling triple, discard the semantics.
    auto onTripleRemoved = [=](const knowledge::Triple& triple) mutable
    {
      auto label = triple.object.value.to<knowledge::LocalizedString>();
      _p->expressionByNode[triple.subject].erase(label.locale.language);
      QI_EMIT updated();
    };

    // Track labels from the knowledge.
    const auto tripleQuery =
        knowledge::makeTripleFromNodes(
          knowledge::makeEmptyResourceNode(),
          teaching::rdfsLabelResource(),
          knowledge::makeEmptyNode());

    auto subscriber = _p->knowledgeBase->subscribe(tripleQuery, "");
    subscriber->tripleAdded.connect(_p->inContext(onTripleAdded));
    subscriber->tripleRemoved.connect(_p->inContext(onTripleRemoved));
    const auto triples = _p->knowledgeBase->queryTriples(tripleQuery, "");
    for (const auto& triple: triples)
      onTripleAdded(triple);

    // Untrack on clean-up.
    _p->disposables->add(makeDisposable(DisposalFunction(_p->inContext([=]
    {
      subscriber->tripleAdded.disconnectAll();
      subscriber->tripleRemoved.disconnectAll();
      _p->expressionByNode.clear();
    }))));

    qiLogDebug() << "Knowledge semantic bridge has started";
    return _p->promise.future();
  })();
}


Future<knowledge::ResourceNode>
KnowledgeSemanticBridge::findResourceFromSemanticLabel(
    const std::shared_ptr<SemanticExpression> semanticLabel,
    const locale::Locale& locale,
    const knowledge::ResourceNode& rdfType)
{
  return _p->inContext([=]() -> knowledge::ResourceNode
  {
    qiLogInfo()
        << "Looking for resource called \"" << *semanticLabel << "\"";
    for (const auto& nodeAndExpressionByLocale: _p->expressionByNode)
    {
      auto it = nodeAndExpressionByLocale.second.find(locale.language);
      if (it == nodeAndExpressionByLocale.second.end())
        continue;

      // KLUDGE: if it is a simple noun, just check the word.
      const auto& nodeAndExpression = *it;
      auto lookedUpLabelGrdExp = semanticLabel->getGrdExpPtr_SkipWrapperPtrs();
      auto knownLabelGrdExp = nodeAndExpression.second->getGrdExpPtr_SkipWrapperPtrs();
      if (lookedUpLabelGrdExp && knownLabelGrdExp)
      {
        auto lookedUpLabelGrounding = lookedUpLabelGrdExp->grounding().getGenericGroundingPtr();
        auto knownLabelGrounding = knownLabelGrdExp->grounding().getGenericGroundingPtr();
        if (lookedUpLabelGrounding && knownLabelGrounding)
        {
          if (lookedUpLabelGrounding->word.lemma == knownLabelGrounding->word.lemma)
          {
            return nodeAndExpressionByLocale.first;
          }
        }
      }

      // Otherwise getSemExpsImbrications could work.
      qisem::SemExpComparator::ComparisonExceptions ignoreOptions;
      ignoreOptions.request = true;
      ignoreOptions.verbTense = true;
      ignoreOptions.grammaticalTypes.insert(qisem::GrammaticalType::SUBJECT);

      qisem::ImbricationType imbrication =
          qisem::SemExpComparator::getSemExpsImbrications(
            *semanticLabel, *nodeAndExpression.second,
            _p->semanticMemoryContext->semanticMemory()->memBloc,
            *_p->semanticMemoryContext->linguisticDatabase(),
            &ignoreOptions);

      static const std::set<qisem::ImbricationType> targetImbrications =
          {qisem::ImbricationType::CONTAINS,
           qisem::ImbricationType::ISCONTAINED,
           qisem::ImbricationType::EQUALS};

      qiLogInfo()
          << "Looking for:\n" << *semanticLabel
          << "Comparing with:\n" << *nodeAndExpression.second
          << "\nImbrication: " << static_cast<int>(imbrication);

      if (targetImbrications.count(imbrication))
      {
        qiLogInfo()
            << "Found " << *nodeAndExpression.second;
        return nodeAndExpressionByLocale.first;
      }
    }

    std::stringstream errorMsg;
    errorMsg << "no node corresponding to label: " << std::endl << *semanticLabel;
    throw std::runtime_error(errorMsg.str());
  })();
}

} // agent
} // semantic
} // qi

