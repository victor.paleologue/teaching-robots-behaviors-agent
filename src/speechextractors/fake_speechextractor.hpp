#include <memory>
#include <qi/semantic/agent/agent.hpp>
#include <qi/session.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
class FakeSpeechExtractor: public qi::Trackable<FakeSpeechExtractor>
{
public:
  FakeSpeechExtractor(std::shared_ptr<Strand> strand, SessionPtr session);
  ~FakeSpeechExtractor();
  Future<void> run();
  Future<void> stop();
  Signal<void> started;
  Property<bool> hearing; // TODO: do something about it?
  Signal<std::vector<PhraseAndLocale>> heard;
  Property<float> confidenceThreshold;
  std::string _id();

private:
  std::shared_ptr<Strand> _strand;
  SessionPtr _session;
  Future<void> _listening;
  const std::string _uuid;
};
} // agent
} // semantic
} // qi
