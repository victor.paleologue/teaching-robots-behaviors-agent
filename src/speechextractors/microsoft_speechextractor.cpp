#include "microsoft_speechextractor.hpp"
#include <audiolanguage/languagecodes.hpp>
#include <qi/locale/util.hpp>
#include "../language.hpp"

qiLogCategory("SemanticAgent.SpeechExtractor");

namespace qi
{
namespace semantic
{
namespace agent
{
MicrosoftSpeechExtractor::MicrosoftSpeechExtractor(
    std::shared_ptr<Strand> strand,
    SessionPtr session)
  : _strand(std::move(strand))
  , _session(session)
  , _listening(nullptr)
{}


MicrosoftSpeechExtractor::~MicrosoftSpeechExtractor()
{
  destroy();
  _listening.cancel();
}


Future<void> MicrosoftSpeechExtractor::run()
{
  if (_listening.isRunning())
    throw std::runtime_error("already listening");

  // track execution
  auto finishedConnection = std::make_shared<SignalLink>(SignalBase::invalidSignalLink);
  auto session = _session; // keep an extra reference rather than relying on this and the strand

  // getting language
  auto localeProperty = std::make_shared<CurrentLocaleProperty>(session);

  // listening
  _listening = session->service("ALCognitiveSvc").async()

  .andThen(_strand->unwrappedSchedulerFor(trackSilent([=](AnyObject cognitiveService)
  {
    _cognitiveService = std::move(cognitiveService);
    return _cognitiveService.async<void>("msAsrLiveStop");
  }, this))).unwrap()

  .andThen(_strand->unwrappedSchedulerFor(trackSilent([=](void*)
  {
    return _cognitiveService.connect(
          "audioReco", boost::function<void(std::string)>(
            _strand->unwrappedSchedulerFor(trackSilent([=](std::string phrase) -> void
    {
      QI_EMIT heard({{phrase, localeProperty->locale.get().value()}});
    }, this)))).async()

    .andThen(_strand->unwrappedSchedulerFor(trackSilent([=](SignalLink connection)
    {
      _asrEventConnection = std::move(connection);

      // TODO: do this asynchronously too, don't be lazy
      auto languageCode = locale::localeToStr(localeProperty->value().value());
      *finishedConnection = _cognitiveService.connect("audioRecoFinished", [=]
      {
        if (*finishedConnection != SignalBase::invalidSignalLink)
          _cognitiveService.async<void>("msAsrLive", languageCode);
      }).value();

      qiLogInfo() << "Listening in " << languageCode << "...";
      auto f = _cognitiveService.async<void>("msAsrLive", languageCode);
      QI_EMIT started();
      return f;
    }, this))).unwrap();
  }, this))).unwrap();

  // logging
  _listening.then([=](Future<void> f) mutable
  {
    if (f.hasValue()) {
      qiLogDebug() << "Speech extraction finished by itself";
    }
    else if (f.isCanceled()) {
      qiLogDebug() << "Speech extraction was stopped";
    }
    else if (f.hasError()) {
      qiLogDebug() << "Speech extraction failed: " << f.error();
    }
  });

  return _listening;
}
} // agent
} // semantic
} // qi

QI_REGISTER_OBJECT(
    qi::semantic::agent::MicrosoftSpeechExtractor,
    run, stop, started, hearing, heard, confidenceThreshold)
