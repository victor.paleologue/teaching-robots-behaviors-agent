#include "fake_speechextractor.hpp"
#include <qi/session.hpp>
#include "aldialog_speechextractor.hpp"

qiLogCategory("SemanticAgent.FakeSpeechExtractor");

namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
const float defaultConfidenceThreshold = 0.62f;
} // anonymous

FakeSpeechExtractor::FakeSpeechExtractor(
    std::shared_ptr<Strand> strand,
    SessionPtr session)
  : _strand(std::move(strand))
  , _session(session)
  , _listening(nullptr)
  , _uuid(generateUuid())
{
  confidenceThreshold.set(defaultConfidenceThreshold);
}

FakeSpeechExtractor::~FakeSpeechExtractor()
{
  destroy();
  _listening.cancel();
}

Future<void> FakeSpeechExtractor::run()
{
  if (_listening.isRunning())
    throw std::runtime_error("already listening");

  qiLogInfo() << "Starting listening...";
  auto session = _session;
  auto wordRecognized = makeWordRecognizedSubscriber(session, confidenceThreshold);
  SignalLink link = wordRecognized->connect(heard);

  // Cancellation may happen when or after this object dies.
  // We keep it as independent as possible from the object.
  auto stopListening = [=]() mutable -> Future<void>
  {
    qiLogInfo() << "Stopping listening...";
    return wordRecognized->disconnectAsync(link).andThen([](bool){});
  };

  // This is the promise to return.
  auto starting = Future<void>{nullptr};
  auto listening = Promise<void>{[starting, stopListening](Promise<void> p) mutable
  {
    qiLogDebug() << "Client cancelled the listening";
    starting.cancel();
    starting.then([=](Future<void> f) mutable
    {
      if (f.isCanceled())
        return; // if starting is effectively canceled, it is dealt with elsewhere

      stopListening().then([p](Future<void>) mutable
      {
        qiLogInfo() << "Stopped listening";
        p.setCanceled();
      });
    });
  }};

  starting.then(_strand->unwrappedSchedulerFor(trackSilent([=](Future<void> f) mutable
  {
    switch (f.waitFor(qi::MilliSeconds{0}))
    {
    case FutureState_FinishedWithError:
      qiLogError() << "Failed to listen: " << f.error();
      stopListening().then([=](Future<void>) mutable
      {
        listening.setError(f.error());
      });
      break;

    case FutureState_Canceled:
      qiLogInfo() << "Stopped listening";
      stopListening().then([=](Future<void>) mutable
      {
        listening.setCanceled();
      });
      break;

    case FutureState_FinishedWithValue:
      qiLogInfo() << "Listening";
      QI_EMIT started();
      break; // alright

    case FutureState_None:
    case FutureState_Running:
      QI_ASSERT(false && "you gotta be kidding me");
      break;
    }
  }, this)));

  auto future = listening.future();
  future.then([listening](Future<void>){}); // keeps the promise alive
  _listening = future;
  return future;
}

Future<void> FakeSpeechExtractor::stop()
{
  return _strand->async(track([=]
  {
    _listening.cancel();
    return _listening;
  }, this)).unwrap();
}

std::string FakeSpeechExtractor::_id()
{
  return _uuid;
}
} // agent
} // semantic
} // qi

QI_REGISTER_OBJECT(
    qi::semantic::agent::FakeSpeechExtractor,
    run, stop, started, hearing, heard, confidenceThreshold, _id)
