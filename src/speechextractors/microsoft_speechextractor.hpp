#include <memory>
#include <qi/semantic/agent/agent.hpp>
#include <qi/session.hpp>
#include <qi/conversation/conversation.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
class MicrosoftSpeechExtractor: public Trackable<MicrosoftSpeechExtractor>
{
public:
  MicrosoftSpeechExtractor(std::shared_ptr<Strand> strand, SessionPtr session);
  ~MicrosoftSpeechExtractor();
  Future<void> run();
  Future<void> stop() { _listening.cancel(); return _listening; }
  Signal<void> started;
  Property<bool> hearing; // TODO: keep this up-to-date
  Signal<std::vector<PhraseAndLocale>> heard;
  Property<float> confidenceThreshold;

private:
  std::shared_ptr<Strand> _strand;
  SessionPtr _session;
  AnyObject _cognitiveService;
  SignalLink _asrEventConnection;
  Future<void> _listening;
};
} // agent
} // semantic
} // qi
