#include "aldialog_speechextractor.hpp"
#include <thread>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include "../language.hpp"

qiLogCategory("SemanticAgent.SpeechExtractor");

namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
const float defaultConfidenceThreshold = 0.5f;
const std::string asrSubscriberName = "SemanticAgent";
} // anonymous

std::string generateUuid()
{
  static boost::uuids::random_generator genUuid;
  return boost::uuids::to_string(genUuid());
}

std::shared_ptr<Signal<std::vector<PhraseAndLocale>>>  makeWordRecognizedSubscriber(
    const SessionPtr& session,
    Property<float>& confidenceThreshold)
{
  auto asrEventSubscriberPtr = std::make_shared<AnyObject>();
  auto asrEventConnectionPtr = std::make_shared<std::atomic<SignalLink>>();
  auto subscriber = std::make_shared<Signal<std::vector<PhraseAndLocale>>>();
  auto weakSignal = std::weak_ptr<Signal<std::vector<PhraseAndLocale>>>(subscriber);
  subscriber->setOnSubscribers([=, &confidenceThreshold](bool subscribe)
  {
    if (subscribe)
    {
      auto sharedSignal = weakSignal.lock();
      if (!sharedSignal)
        return Future<void>{nullptr};

      auto localeProperty = std::make_shared<CurrentLocaleProperty>(session);
      return session->service("ALMemory").async()

      .andThen([](AnyObject memory)
      {
        return memory.async<AnyObject>("subscriber", "WordRecognized");
      }).unwrap()

      .andThen([=, &confidenceThreshold](AnyObject subscriber) mutable
      {
        auto bestPhrases = std::make_shared<std::vector<PhraseAndLocale>>();
        auto strand = std::make_shared<Strand>();

        auto memory = session->service("ALMemory").value();
        auto endSubscriber = memory.call<AnyObject>("subscriber", "ALSpeechRecognition/Status");
        endSubscriber.connect(
              "signal", boost::function<void(AnyValue)>(
                strand->schedulerFor([strand, weakSignal, bestPhrases, endSubscriber](AnyValue v)
        {
          try
          {
            auto status = v.to<std::string>();
            qiLogDebug() << "Speech reco status: " << status;
            if (status == "EndOfProcess")
            {
              qiLogDebug() << "End of ASR process, emitting results";
              auto sharedSignal = weakSignal.lock();
              if (!sharedSignal)
                return;
              QI_EMIT (*sharedSignal)(*bestPhrases);
              bestPhrases->clear();
            }
          }
          catch (const std::exception& e)
          {
            qiLogError() << "Error processing ASR status: " << e.what();
          }
        }))).value();
        qiLogVerbose() << "Subscribed to ALSpeechRecognition/Status";

        *asrEventSubscriberPtr = subscriber;
        return asrEventSubscriberPtr->connect(
              "signal", boost::function<void(AnyValue)>(
                strand->schedulerFor(
                  [strand, bestPhrases, subscriber, localeProperty, &confidenceThreshold](AnyValue v)
        {
          // ASR event is structured like: [phrase, confidence, phrase, confidence, ...]
          // TODO: use the fact that if the first phrase is empty, it means that speech
          // was heard, but not textual representation has been identified.
          try
          {
            // TODO: use "as" instead of "to", after unwrapping the ALValue.
            auto resultsAsVector = v.to<std::vector<AnyValue>>();
            std::list<std::pair<std::string, float>> phrasesAndConfidences;
            // TODO: maintain a cache of the confidence threshold.
            float confidenceThresholdCache = confidenceThreshold.get().value();
            for (auto it = resultsAsVector.begin(); it != resultsAsVector.end(); ++it)
            {
              auto jt = it;
              ++it;
              if (it == resultsAsVector.end())
              {
                qiLogError() << "Invalid event format for word recognized";
                break;
              }

              // Confidence is checked first because it is more efficient.
              auto confidence = it->to<float>();
              if (confidence < confidenceThresholdCache)
                continue;

              auto phrase = jt->to<std::string>();
              if (phrase.empty())
                continue;

              qiLogVerbose() << "ASR recognizes \"" << phrase
                             << "\" with confidence " << confidence;

              auto kt = std::find_if( // start by the end, because we expect confidences to be descending
                  phrasesAndConfidences.rbegin(), phrasesAndConfidences.rend(),
                  [&](const std::pair<std::string, float>& phraseAndConfidence)
              {
                return confidence > phraseAndConfidence.second;
              });
              phrasesAndConfidences.insert(kt.base(), std::make_pair(std::move(phrase), std::move(confidence)));
            }

            if (phrasesAndConfidences.empty())
              return;

            std::stringstream ss;
            ss << "Extracted speech: ";
            auto locale = localeProperty->locale.get().value(); // don't worry, this is local and immediate
            size_t i = 0;
            for (const auto& phraseAndConfidence: phrasesAndConfidences)
            {
              if (i > 0)
                ss << ", ";
              bestPhrases->push_back({std::move(phraseAndConfidence.first), locale});
              ss << "\"" << bestPhrases->back().text << "\"";
              ++i;
            }
            qiLogDebug() << ss.str();
          }
          catch (const std::exception& e)
          {
            qiLogError() << "Failed to retrieve user input: " << e.what();
          }
        }))).async();
      }).unwrap()

      .andThen([=](SignalLink link)
      {
        qiLogVerbose() << "Subscribed to WordRecognized";
        *asrEventConnectionPtr = link;
      });
    }
    else
    {
      auto asrSignalLink = asrEventConnectionPtr->exchange(SignalBase::invalidSignalLink);
      if (*asrEventSubscriberPtr && asrSignalLink != SignalBase::invalidSignalLink)
        return asrEventSubscriberPtr->disconnect(asrSignalLink).async();
      return Future<void>(nullptr);
    }
  });

  return subscriber;
}

ALDialogSpeechExtractor::ALDialogSpeechExtractor(
    std::shared_ptr<Strand> strand,
    SessionPtr session)
  : _strand(std::move(strand))
  , _session(session)
  , _listening(nullptr)
  , _heard(makeWordRecognizedSubscriber(_session, confidenceThreshold))
  , _uuid(generateUuid())
{
  confidenceThreshold.set(defaultConfidenceThreshold);

  auto asr = _session->service("ALSpeechRecognition").value();
  static const auto nofTries = 1; // 3 would be good, but I'm afraid being considered a spammer
  asr.call<void>("pause", true);
  for (auto i = 0; i < nofTries; ++i)
  {
    try
    {
      asr.call<void>("enableFreeSpeechToText");
      break;
    }
    catch (const std::exception& e)
    {
      qiLogDebug() << "Enable free speech attempt #" << i << " failed:" << e.what();
      const auto iMax = nofTries - 1;
      if (i == iMax)
      {
        asr.call<void>("pause", false);
        throw std::runtime_error{"could not set up speech extraction: " + std::string{e.what()}};
      }
      std::this_thread::sleep_for(std::chrono::milliseconds{100});
    }
  }
  asr.call<void>("pause", false);
}


ALDialogSpeechExtractor::~ALDialogSpeechExtractor()
{
  destroy();
  _listening.cancel();
}

Future<void> ALDialogSpeechExtractor::run()
{
  if (_listening.isRunning())
    throw std::runtime_error("already listening");

  qiLogInfo() << "Starting listening...";
  SignalLink link = _heard->connect(heard);

  auto gettingASR = _session->service("ALSpeechRecognition").async();

  auto starting = gettingASR.andThen([=](AnyObject asr)
  {
    auto connecting = asr.async<void>("connectRemote");
    auto starting = connecting.then([=](Future<void> connected)
    {
      if (connected.hasError())
        qiLogWarning() << "error connecting to remote speech recognition: " << connected.error();
      return asr.async<void>("startDetection");
    }).unwrap();
    return starting;
  }).unwrap();

  auto session = _session;

  // Cancellation may happen when or after this object dies.
  // We keep it as independent as possible from the object.
  auto stopListening = [=]() mutable -> Future<void>
  {
    qiLogInfo() << "Stopping listening...";
    return session->service("ALSpeechRecognition").async()

    .andThen([](AnyObject asr)
    {
      auto stopping = asr.async<void>("stopDetection");

      auto disconnecting = stopping.then([=](Future<void> stopped)
      {
        if (stopped.hasError())
          qiLogWarning() << "error connecting to remote speech recognition: " << stopped.error();
        return asr.async<void>("disconnectRemote");
      }).unwrap();

      return disconnecting;
    }).unwrap()

    .then(track([=](Future<void>) mutable
    {
      try
      {
        session->service("ALSpeechRecognition").value().call<void>("disconnectRemote");
      }
      catch (const std::exception& e)
      {
        qiLogWarning() << "error disconnecting to remote speech recognition: " << e.what();
      }

      return _heard->disconnectAsync(link).andThen([](bool){});
    }, this)).unwrap();
  };

  // This is the promise to return the future of.
  auto listening = Promise<void>{[starting, stopListening](Promise<void> p) mutable
  {
    qiLogDebug() << "Client cancelled the listening";
    starting.cancel();
    starting.then([=](Future<void> f) mutable
    {
      if (f.isCanceled())
        return; // if starting is effectively canceled, it is dealt with elsewhere

      stopListening().then([p](Future<void>) mutable
      {
        qiLogInfo() << "Stopped listening";
        p.setCanceled();
      });
    });
  }};

  starting.then(_strand->unwrappedSchedulerFor(trackSilent([=](Future<void> f) mutable
  {
    switch (f.waitFor(qi::MilliSeconds{0}))
    {
    case FutureState_FinishedWithError:
      qiLogError() << "Failed to listen: " << f.error();
      stopListening().then([=](Future<void>) mutable
      {
        listening.setError(f.error());
      });
      break;

    case FutureState_Canceled:
      qiLogInfo() << "Stopped listening";
      stopListening().then([=](Future<void>) mutable
      {
        listening.setCanceled();
      });
      break;

    case FutureState_FinishedWithValue:
      qiLogInfo() << "Listening";
      QI_EMIT started();
      break; // alright

    case FutureState_None:
    case FutureState_Running:
      QI_ASSERT(false && "you gotta be kidding me");
      break;
    }
  }, this)));

  auto future = listening.future();
  future.then([listening](Future<void>){}); // keeps the promise alive
  _listening = future;
  return future;
}

Future<void> ALDialogSpeechExtractor::stop()
{
  return _strand->async(track([=]
  {
    _listening.cancel();
    return _listening;
  }, this)).unwrap();
}

std::string ALDialogSpeechExtractor::_id()
{
  return _uuid;
}

} // agent
} // semantic
} // qi

QI_REGISTER_OBJECT(
    qi::semantic::agent::ALDialogSpeechExtractor,
    run, stop, started, hearing, heard, confidenceThreshold, _id)
