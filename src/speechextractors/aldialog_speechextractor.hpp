#include <memory>
#include <boost/signals2.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qi/session.hpp>

namespace qi
{
class Strand;

namespace semantic
{
namespace agent
{
std::string generateUuid();

/// Common code to subscribe to the ALMemory event "WordRecognized",
/// filter the results given a confidence threshold, and order them.
/// Please note that the confidence threshold MUST outlive the signal.
std::shared_ptr<Signal<std::vector<PhraseAndLocale>>> makeWordRecognizedSubscriber(
    const SessionPtr& session,
    Property<float>& confidenceThreshold);

class ALDialogSpeechExtractor: public qi::Trackable<ALDialogSpeechExtractor>
{
public:
  ALDialogSpeechExtractor(std::shared_ptr<Strand> strand, SessionPtr session);
  ~ALDialogSpeechExtractor();
  Future<void> run();
  Future<void> stop();
  Signal<void> started;
  Property<bool> hearing; // TODO: emit
  Signal<std::vector<PhraseAndLocale>> heard;
  Property<float> confidenceThreshold;
  std::string _id();

private:
  std::shared_ptr<Strand> _strand;
  SessionPtr _session;
  Future<void> _listening;
  std::shared_ptr<Signal<std::vector<PhraseAndLocale>>> _heard;
  const std::string _uuid;
};
} // agent
} // semantic
} // qi
