#ifndef SEMANTICAGENT_HUMANRECOGNITION_FAKE_HPP
#define SEMANTICAGENT_HUMANRECOGNITION_FAKE_HPP

#include <memory>
#include <qi/human/human.hpp>
#include "humanrecognizer.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
/// \brief A fake implementation of human recognizer.
/// This implementation does remember Human objects,
/// and associate them to string identifiers.
/// However nothing is saved.
class HumanRecognizerFake: public HumanRecognizer
{
public:
  HumanRecognizerFake();
  ~HumanRecognizerFake();
  Future<bool> isInitialized() override;
  Future<std::string> identify(qi::human::HumanPtr humanPtr) override;
  Future<bool> learn(qi::human::HumanPtr humanPtr, std::string id) override;

private:
  struct Private;
  std::unique_ptr<Private> _p;
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_HUMANRECOGNITION_FAKE_HPP
