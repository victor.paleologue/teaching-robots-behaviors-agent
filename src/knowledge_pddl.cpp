#include "knowledge_pddl.hpp"
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/jsoncodec.hpp>
#include <qi/log.hpp>
#include <qi/teaching/ontology.hpp>
#include "pddl/pddl_edit.hpp"
#include "pddl/pddl_io.hpp"
#include "pddl/pddl_read.hpp"
#include "pddl/trivial_lisp_expression.hpp"
#include "utility/disposable.hpp"
#include "utility/incontext.hpp"

qiLogCategory("SemanticAgent.KnowledgePDDL");

using namespace pddl::ast;

namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
// Constants
//----------
// Types
static const auto communicationActTypeName = pddlNameFromIRI(teaching::communicationActType());
static const auto socialAgentTypeName = pddlNameFromIRI(teaching::socialAgentType());
// Predicates
static const auto respondsToPredicateName = pddlNameFromIRI(teaching::respondsTo());
static const auto addressedToPredicateName = pddlNameFromIRI(teaching::communicationAddressee());
static const std::string wasRespondedTo = "was_responded_to";
static const std::string wasMisunderstood = "was_misunderstood";
// Operators
static const std::string notOp = "not";


struct NodeInPDDL
{
  knowledge::ResourceNode node;
  std::string pddlName;
  std::string pddlType;
};

using LispExpression = TrivialLispExpression;
using LispExpressions = std::vector<LispExpression>;

std::ostream& operator<<(std::ostream& out, const LispExpressions& stmts)
{
  out << "[";
  bool first = true;
  for (const auto& stmt: stmts)
  {
    if (first)
      first = false;
    else
      out << ",";
    out << stmt;
  }
  out << "]";
  return out;
}

using Triples = std::vector<knowledge::Triple>;


/// Subscribe and get the given knowledge to a partial triple (aka triple query).
/// This function must be called from within the desired execution context.
/// @param knowledge Read-only knowledge base to look into.
/// @param tripleQuery A partial triple to match other triples.
/// @param onTriplesAdded Callback for when triples are found,
/// including when the triple are present at subscription time.
/// @param onTriplesRemoved Callback for when triples are lost,
/// including when the triple are present at unsubscription time.
/// @param execContextTransform a transform to defer callbacks
/// to the desired execution context.
/// @return A disposable to unsubscribe when desired.
template <typename ExecutionContextTransform>
std::unique_ptr<Disposable> subscribeToTriple(
    const knowledge::KnowledgeBasePtr& knowledge,
    const knowledge::Triple& tripleQuery,
    std::function<void(const Triples& triples)> onTriplesAdded,
    std::function<void(const Triples& triples)> onTriplesRemoved,
    ExecutionContextTransform&& execContextTransform)
{
  auto currentTriples = std::make_shared<Triples>();

  auto tripleAddedCallback = [=](const knowledge::Triple& triple)
  {
    onTriplesAdded({triple});
    currentTriples->push_back(triple);
  };

  auto tripleRemovedCallback = [=](const knowledge::Triple& triple)
  {
    auto found = std::find(currentTriples->begin(), currentTriples->end(), triple);
    if (found != currentTriples->end())
      currentTriples->erase(found);
    onTriplesRemoved({triple});
  };

  auto subscriber = knowledge->subscribe(tripleQuery, "");
  SignalLink addedLink =
      subscriber->tripleAdded.connect(execContextTransform(tripleAddedCallback));
  SignalLink removedLink =
      subscriber->tripleRemoved.connect(execContextTransform(tripleRemovedCallback));
  qiLogDebug() << "Subscribed to " << qi::encodeJSON(tripleQuery);

  *currentTriples = knowledge->queryTriples(tripleQuery, "");
  onTriplesAdded(*currentTriples);

  return makeDisposable(DisposalFunction(execContextTransform([=]
  {
    subscriber->tripleAdded.disconnectAsync(addedLink);
    subscriber->tripleAdded.disconnectAsync(removedLink);

    onTriplesRemoved(*currentTriples);
    currentTriples->clear();

    qiLogDebug() << "Unsubscribed from " << qi::encodeJSON(tripleQuery);
  })));
}

/// Appends facts.
void addFacts(LispExpressions& destination, LispExpressions facts)
{
  qiLogDebug() << "New facts: " << facts;
  std::move(facts.begin(), facts.end(), std::back_inserter(destination));
}


/// Removes facts.
/// Removes only the first occurrence for each fact found.
void removeFacts(LispExpressions& origin, const LispExpressions& facts)
{
  qiLogDebug() << "Dropping facts: " << facts;
  for (const auto& fact: facts)
  {
    auto found = std::find(origin.begin(), origin.end(), fact);
    if (found != origin.end())
      origin.erase(found);
  }
}

} // anonymous namespace

struct KnowledgeProblemExtractor::Private: SafeContext
{
  /// Trackables must call something at destruction.
  ~Private() { destroy(); }

  /// The knowledge base we bind to PDDL.
  knowledge::KnowledgeBasePtr knowledgeBase;

  /// To map PDDL names to knowledge nodes.
  std::unordered_map<std::string, NodeInPDDL> nodesInPDDLByName;

  /// Facts from knowledge.
  LispExpressions initFacts;

  /// Understand and respond goals.
  LispExpressions goalFacts;

  /// Whether the extractor is running.
  Promise<void> promise;

  /// Everything that must be cleaned when stopped.
  Disposables disposables;

  /// Set to true when PDDL should be updated.
  bool shouldUpdatePDDL = false;

  /// The communication acts to understand and respond to.
  // TODO: check that, maybe use it
  std::vector<knowledge::ResourceNode> actsToRespondTo;

  /// Stores the problem representing the world state.
  std::shared_ptr<StoredProp<std::shared_ptr<const Description>>> worldState =
      std::make_shared<StoredProp<std::shared_ptr<const Description>>>();

  /// Stores the problem representing what to respond to.
  std::shared_ptr<StoredProp<std::shared_ptr<const Description>>>
  respondToUnderstoodCommunicationActsProblem =
      std::make_shared<StoredProp<std::shared_ptr<const Description>>>();

  /// This function adds a node <-> PDDL binding.
  const NodeInPDDL& addNodeToPDDL(const knowledge::ResourceNode& node, const std::string& type)
  {
    auto pddlName = pddlNameFromIRI(node.url);
    auto& nodeInPDDL = nodesInPDDLByName[pddlName];
    nodeInPDDL = NodeInPDDL{node, std::move(pddlName), type};
    return nodeInPDDL;
  }


  /// This function removes a node <-> PDDL binding.
  void removeNodeFromPDDL(const std::string& name)
  {
    nodesInPDDLByName.erase(name);
  }


  // Updating the PDDL
  //------------------
  /// Actually update the PDDL according to the bound nodes.
  void updatePDDL()
  {
    shouldUpdatePDDL = false;
    if (nodesInPDDLByName.empty())
    {
      worldState->set(nullptr);
      qiLogDebug() << "Problem was updated (to none)";
      return;
    }

    {
      auto worldDescription = std::make_shared<Description>();
      ensureDomain(*worldDescription);
      auto& problem = ensureProblem(*worldDescription);

      // Gathering known objects. (:objects)
      for (const auto& nodeInPDDLByName: nodesInPDDLByName)
      {
        try
        {
          const auto& nodeInPDDL = nodeInPDDLByName.second;
          addObject(*worldDescription, nodeInPDDL.pddlName, nodeInPDDL.pddlType);
        }
        catch (const std::exception& e)
        {
          qiLogError() << "Failed to add node in PDDL: " << e.what();
        }
      }

      // Gathering known predicates. (:predicates)
      std::unordered_map<std::string, PredicateDeclaration*> predicates;

      predicates[respondsToPredicateName] =
          ensurePredicate(
            *worldDescription, respondsToPredicateName, {
              {"input", communicationActTypeName},
              {"response", communicationActTypeName}});

      predicates[wasRespondedTo] =
          ensurePredicate(
            *worldDescription, wasRespondedTo, {
              {"act", communicationActTypeName}});

      predicates[wasMisunderstood] =
          ensurePredicate(
            *worldDescription, wasMisunderstood, {
              {"act", communicationActTypeName}});

      predicates[addressedToPredicateName] =
          ensurePredicate(
            *worldDescription, addressedToPredicateName, {
              {"act", communicationActTypeName},
              {"addressee", socialAgentTypeName}});

      // Gathering known facts. (:init)
      auto& facts = problem.initialState.facts;
      for (const auto& fact: initFacts)
      {
        QI_ASSERT(!fact.empty());
        auto predicateStr = std::move(fact.literal);
        auto* predicate = predicates.at(predicateStr);
        QI_ASSERT(predicate->parameters.size() == fact.args.size());

        Predicate::Arguments args;
        for (size_t i = 0; i < fact.args.size(); ++i)
        {
          const auto& arg = fact.args[i];
          auto* object = [&]
          {
            const auto& param = predicate->parameters[i];
            if (param->type)
            {
              // By construction, our types are always primitive.
              auto typeDeclaration = param->type->get<PrimitiveTypePointer>()->declaration;
              return ensureObject(*worldDescription, arg.literal, typeDeclaration->name);
            }
            return ensureObject(*worldDescription, arg.literal);
          }();

          args.push_back(std::make_unique<Constant>(object));
        }

        facts.push_back(std::make_unique<Predicate>(std::move(args), predicate));
      }

      // Done.
      worldState->set(std::move(worldDescription));
      qiLogDebug() << "World problem was updated";
    }

    {
      auto responseDescription = std::make_shared<Description>();
      ensureDomain(*responseDescription);
      auto& problem = ensureProblem(*responseDescription);

      // Gathering known objects. (:objects)
      for (const auto& nodeInPDDLByName: nodesInPDDLByName)
      {
        if (nodeInPDDLByName.second.pddlType != communicationActTypeName)
          continue;

        try
        {
          const auto& nodeInPDDL = nodeInPDDLByName.second;
          addObject(*responseDescription, nodeInPDDL.pddlName, nodeInPDDL.pddlType);
        }
        catch (const std::exception& e)
        {
          qiLogError() << "Failed to add node in PDDL: " << e.what();
        }
      }

      // Gathering known predicates. (:predicates)
      std::unordered_map<std::string, PredicateDeclaration*> predicates;

      predicates[wasRespondedTo] =
          ensurePredicate(
            *responseDescription, wasRespondedTo, {
              {"act", communicationActTypeName}});

      predicates[wasMisunderstood] =
          ensurePredicate(
            *responseDescription, wasMisunderstood, {
              {"act", communicationActTypeName}});


      // Gathering known facts. (:goal)
      auto buildPrecondition = [&](const auto& statement)
      {
        auto predicateStr = std::move(statement.literal);
        auto* predicate = predicates.at(predicateStr);
        QI_ASSERT(predicate->parameters.size() == statement.args.size());

        Predicate::Arguments args;
        for (size_t i = 0; i < statement.args.size(); ++i)
        {
          const auto& arg = statement.args[i];
          auto* object = [&]
          {
            const auto& param = predicate->parameters[i];
            if (param->type)
            {
              // By construction, our types are always primitive.
              auto typeDeclaration = param->type->template get<PrimitiveTypePointer>()->declaration;
              return ensureObject(*responseDescription, arg.literal, typeDeclaration->name);
            }
            return ensureObject(*responseDescription, arg.literal);
          }();

          args.push_back(std::make_unique<Constant>(object));
        }

        return std::make_unique<Predicate>(std::move(args), predicate);
      };

      std::vector<Precondition> goals;
      for (const auto& goal: goalFacts)
      {
        QI_ASSERT(!goal.empty());

        if (goal.literal == notOp)
        { // Note that there is no recursivity.
          QI_ASSERT(goal.args.size() == 1u);
          goals.push_back({std::make_unique<Not<Precondition>>(buildPrecondition(goal.args[0]))});
        }
        else
        {
          goals.push_back(buildPrecondition(goal));
        }
      }

      problem.goal = std::make_unique<And<Precondition>>(std::move(goals));

      // Done.
      respondToUnderstoodCommunicationActsProblem->set(std::move(responseDescription));
      qiLogDebug() << "Response problem was updated";
    }
  }


  /// Schedule an update of PDDL, if not already scheduled.
  void requestPDDLUpdate()
  {
    if (shouldUpdatePDDL)
      return;
    shouldUpdatePDDL = true;
    qi::async(inContext([=]{ updatePDDL(); }));
  }


  // Bridging Knowledge to PDDL
  //---------------------------
  /// To synchronize the resources from the knowledge with the PDDL.
  /// When a new node is found, it is possible to provide extra facts,
  /// so that they get bound along with the node.
  /// @param typeNode The node type to track.
  /// @param extraTriple A function providing facts
  std::unique_ptr<Disposable> bindNodesToPDDLByType(
      const knowledge::ResourceNode& typeResource)
  {
    const auto pddlType = pddlNameFromIRI(typeResource.url);
    auto boundNodes = std::make_shared<std::unordered_map<std::string, NodeInPDDL>>();

    const auto typeTripleQuery =
        knowledge::makeTripleFromNodes(
            {}, teaching::rdfTypeResource(), knowledge::makeNodeFromResourceNode(typeResource));

    auto onTypeTriplesAdded = [=](const Triples& triples) mutable
    {
      for (const auto& triple: triples)
      {
        const auto& nodeInPDDL = addNodeToPDDL(triple.subject, pddlType);
        qiLogDebug() << "New " << pddlType << " " << nodeInPDDL.pddlName;
        (*boundNodes)[triple.subject.url] = nodeInPDDL;
      }
      requestPDDLUpdate();
    };

    auto onTypeTriplesRemoved = [=](const Triples& triples)
    {
      for (const auto& triple: triples)
      {
        // TODO: optimize
        const auto& nodeInPDDL = (*boundNodes)[triple.subject.url];
        removeNodeFromPDDL(nodeInPDDL.pddlName);
        qiLogDebug() << "Dropped " << pddlType << " " << nodeInPDDL.pddlName;
        boundNodes->erase(triple.subject.url);
      }
      requestPDDLUpdate();
    };

    return subscribeToTriple(
          knowledgeBase, typeTripleQuery,
          onTypeTriplesAdded, onTypeTriplesRemoved, inContextTransform());
  }


  /// Helper subscribing to a partial triple (a triple "query"),
  /// and process the result to produce facts, added to the PDDL.
  /// The facts are removed when the triple is removed.
  // TODO: switch to SPARQL subscription when possible.
  std::unique_ptr<Disposable> bindTripleQueryToPDDLFacts(
      const knowledge::Triple& tripleQuery,
      std::function<LispExpressions(const knowledge::Triple& triple)> computeFacts)
  {
    QI_ASSERT(strand->isInThisContext());
    auto onTriplesAdded = [=](const Triples& triples) mutable
    {
      for (const auto& triple: triples)
        addFacts(initFacts, computeFacts(triple));
      requestPDDLUpdate();
    };

    auto onTriplesRemoved = [=](const Triples& triples)
    {
      for (const auto& triple: triples)
        removeFacts(initFacts, computeFacts(triple));
      requestPDDLUpdate();
    };

    return subscribeToTriple(
          knowledgeBase, tripleQuery,
          onTriplesAdded, onTriplesRemoved, inContextTransform());
  }
};


KnowledgeProblemExtractor::KnowledgeProblemExtractor(
    knowledge::KnowledgeBasePtr knowledgeBase,
    std::shared_ptr<Strand> strand)
  : _p(std::make_unique<Private>())
{
  _p->strand = strand;
  _p->knowledgeBase = std::move(knowledgeBase);
  _p->promise.setValue(nullptr);
}

KnowledgeProblemExtractor::~KnowledgeProblemExtractor()
{
  _p->promise.future().cancel();
  _p->promise.future().wait();
}

Future<void> KnowledgeProblemExtractor::run()
{
  return _p->inContext([=]
  {
    if (_p->promise.future().isRunning())
      throw std::runtime_error("knowledge to PDDL extractor is already running");

    qiLogDebug() << "Starting the knowledge-to-PDDL extractor...";

    // Specifying the knowledge to bridge
    //-----------------------------------
    // Bind persons, objects, areas and communication acts.
    _p->disposables.add(_p->bindNodesToPDDLByType(teaching::socialAgentTypeResource()));
    _p->disposables.add(_p->bindNodesToPDDLByType(teaching::objectTypeResource()));
    _p->disposables.add(_p->bindNodesToPDDLByType(teaching::areaTypeResource()));
    _p->disposables.add(_p->bindNodesToPDDLByType(teaching::communicationActTypeResource()));

    // Bind responses to communication acts.
    auto responseQuery =
        knowledge::makeTripleFromNodes(
          knowledge::makeEmptyResourceNode(),
          teaching::respondsToResource(),
          knowledge::makeEmptyNode());

    auto responseToFacts = [=](const knowledge::Triple& triple)
    {
      qiLogInfo() << "Received response triple: " << encodeJSON(triple);
      auto respondedAct = triple.object.value.to<knowledge::ResourceNode>();
      auto respondedName = pddlNameFromIRI(respondedAct.url);
      auto responseName = pddlNameFromIRI(triple.subject.url);
      return LispExpressions{
          {respondsToPredicateName, responseName, respondedName},
          {wasRespondedTo, respondedName}};
    };

    _p->disposables.add(
          _p->bindTripleQueryToPDDLFacts(
            std::move(responseQuery),
            std::move(responseToFacts)));

    // Bind addressees of communication acts
    // D'une pierre, deux coups, we add goals for when addressee is self.
    auto addresseesQuery =
        knowledge::makeTripleFromNodes(
          knowledge::makeEmptyResourceNode(),
          teaching::communicationAddresseeResource(),
          knowledge::makeEmptyNode());

    auto addresseesToFacts = [=](const knowledge::Triple& triple)
    {
      const auto& inputCommunicationAct = triple.subject;
      auto inputCommunicationActPddl = pddlNameFromIRI(inputCommunicationAct.url);
      auto addresseePddl = pddlNameFromIRI(triple.object.value.to<knowledge::ResourceNode>().url);
      return LispExpressions{
          {addressedToPredicateName, inputCommunicationActPddl, addresseePddl}};
    };

    auto respondedAndNotMisunderstoodFacts = [=](const knowledge::ResourceNode& communicationAct)
    {
      auto inputCommunicationActPddl = pddlNameFromIRI(communicationAct.url);
      return LispExpressions{
          {wasRespondedTo, inputCommunicationActPddl},
          {notOp, {{wasMisunderstood, inputCommunicationActPddl}}}};
    };

    auto onAddresseeAdded = [=](const Triples& triples)
    {
      for (const auto& triple: triples)
      {
        addFacts(_p->initFacts, addresseesToFacts(triple));
        const auto& addressee = triple.object.value.to<knowledge::ResourceNode>();
        if (addressee == teaching::agentSelfResource())
        {
          const auto& communicationAct = triple.subject;
          addFacts(_p->goalFacts, respondedAndNotMisunderstoodFacts(communicationAct));
        }
      }
      // TODO add goal
      _p->requestPDDLUpdate();
    };

    auto onAddresseeRemoved = [=](const Triples& triples)
    {
      for (const auto& triple: triples)
      {
        removeFacts(_p->initFacts, addresseesToFacts(triple));
        const auto& addressee = triple.object.value.to<knowledge::ResourceNode>();
        if (addressee == teaching::agentSelfResource())
        {
          const auto& communicationAct = triple.subject;
          removeFacts(_p->goalFacts, respondedAndNotMisunderstoodFacts(communicationAct));
        }
      }
      // TODO remove goal
      _p->requestPDDLUpdate();
    };

    _p->disposables.add(
          subscribeToTriple(
            _p->knowledgeBase,
            std::move(addresseesQuery),
            onAddresseeAdded, onAddresseeRemoved, _p->inContextTransform()));

    // ... hard work is now done.
    qiLogDebug() << "Started the knowledge-to-PDDL extractor";

    auto onCancel = [=](Promise<void> p)
    {
      qiLogDebug() << "Stopping the knowledge-to-PDDL extractor...";
      _p->disposables.dispose()
          .then([problem = _p->worldState, p](Future<void> f) mutable
      {
        problem->set(nullptr);
        qiLogDebug() << "Stopped the knowledge-to-PDDL extractor";
        p.setCanceled();
      });
    };

    _p->promise = Promise<void>(_p->inContext(onCancel));
    return _p->promise.future();
  })();
}


std::shared_ptr<ProblemSource>
KnowledgeProblemExtractor::worldState()
{
  return std::static_pointer_cast<ProblemSource>(_p->worldState);
}


Future<knowledge::ResourceNode>
KnowledgeProblemExtractor::findKnowledgeFromPDDLName(
    const std::string& name)
{
  return _p->inContext([=]
  {
    auto it = _p->nodesInPDDLByName.find(name);
    if (it == _p->nodesInPDDLByName.end())
      throw std::runtime_error("no node associated to given pddl name");
    return it->second.node;
  })();
}


Future<std::string>
KnowledgeProblemExtractor::findPDDLNameFromKnowledge(
    const knowledge::ResourceNode& node)
{
  return _p->inContext([=]
  {
    auto it = std::find_if(
          _p->nodesInPDDLByName.begin(), _p->nodesInPDDLByName.end(),
          [&](const std::pair<std::string, NodeInPDDL>& keyValue)
    {
      return keyValue.second.node == node;
    });
    if (it == _p->nodesInPDDLByName.end())
      throw std::runtime_error("no pddl name associated to given node");
    qiLogDebug() << "Found PDDL name " << it->first << " for node " << node.url;
    return it->first;
  })();
}


std::shared_ptr<ProblemSource>
KnowledgeProblemExtractor::respondToUnderstoodCommunicationActsGoal()
{
  return _p->respondToUnderstoodCommunicationActsProblem;
}

} // agent
} // semantic
} // qi
