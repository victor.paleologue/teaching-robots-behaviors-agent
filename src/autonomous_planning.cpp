#include "autonomous_planning.hpp"

#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim_all.hpp>

#include <qi/log.hpp>
#include <qi/anyobject.hpp>
#include <qi/periodictask.hpp>
#include <qi/context/context.hpp>
#include <qi/knowledge/knowledge.hpp>

#include "behaviormodel/action_pool.hpp"
#include "knowledge_pddl.hpp"
#include "knowledge_pruning.hpp"
#include "pddl/pddl_edit.hpp"
#include "pddl/pddl_goal.hpp"
#include "pddl/pddl_io.hpp"
#include "pddl/pddl_read.hpp"
#include "rules.hpp"
#include "utility/disposable.hpp"
#include "utility/incontext.hpp"

using namespace pddl::ast;

qiLogCategory("SemanticAgent.AutonomousPlanning");

namespace qi
{
namespace semantic
{
namespace agent
{
Future<std::string> tryPlanWithAllGoals(
    AnyObject actionPlanning,
    std::shared_ptr<Description> baseDescription,
    std::shared_ptr<Description> adaptedDescription,
    std::shared_ptr<std::vector<std::set<const Goal*>>> goals,
    size_t goalIndex = 0u)
{
  const auto& currentGoal = (*goals)[goalIndex];
  auto& adaptedProblem = adaptedDescription->problem;
  QI_ASSERT(adaptedProblem);
  adaptedProblem.value()->goal =
      mergeGoals(currentGoal, *baseDescription->domain);

  if (goalIndex == 0u)
  {
    qiLogInfo() << "Problem situation is: " << serializePddl(adaptedProblem.value());
    qiLogInfo() << "Domain: " << serializePddl(*adaptedDescription->domain);
  }

  auto& adaptedGoal = adaptedProblem.value()->goal;
  if (adaptedGoal)
  {
    qiLogInfo()
        << "Performing planning for goals:" << std::endl
        << serializePddl(adaptedProblem.value()->goal.value());
  }
  else
  {
    qiLogWarning()
        << "No goal left after merging";
  }

  // TODO: note how this is expansive if no goal is compatible!
  // We should use formal logic to perform a quick sanity check first.
  auto planning = actionPlanning.async<std::string>(
        "planFromPDDLContents",
        serializePddl(adaptedDescription->domain),
        serializePddl(adaptedProblem.value()));
  ++goalIndex;

  auto maybeReplanning = planning.then([=](Future<std::string> planning)
  {
    // On success or cancellation, go on.
    if (!planning.hasError())
      return planning;
    // On error, retry with next alternative goals, if there are some left.
    qiLogInfo()
        << "Planner produced error: " << planning.error();
    if (goalIndex >= goals->size())
      return makeFutureError<std::string>("no solution found");
    return tryPlanWithAllGoals(actionPlanning, baseDescription, adaptedDescription, goals, goalIndex);
  }).unwrap();

  return maybeReplanning;
}


std::unique_ptr<Disposable> runAutonomousPlanning(
    std::shared_ptr<Strand> strand,
    SessionPtr session,
    context::ContextPtr context,
    knowledge::EditableKnowledgeBasePtr knowledge,
    std::shared_ptr<ActionPool> actionPool)
{
  auto safeContext = std::make_shared<SafeContext>(strand);
  auto safe = safeContext->inContextTransform();
  auto runDisposables = std::make_unique<Disposables>();

  // Clean the knowledge from time to time
  // --------------------------------------------------------------------------
  auto pruneOldCommunicationActsTask = std::make_shared<PeriodicTask>();
  pruneOldCommunicationActsTask->setStrand(strand.get());
  pruneOldCommunicationActsTask->setCallback(
        [=]{ pruneOldCommunicationActs(knowledge); });
  pruneOldCommunicationActsTask->setPeriod(Minutes(1));
  pruneOldCommunicationActsTask->start();
  runDisposables->add(makeDisposable([=]
  {
    pruneOldCommunicationActsTask->stop();
  }));

  // Feed the action pool with extra PDDL problems.
  // --------------------------------------------------------------------------
  // Translate objects from knowledge into an additional PDDL problem.
  auto knowledgeProblemExtractor =
      std::make_shared<KnowledgeProblemExtractor>(knowledge->knowledgeBase());
  {
    auto extractingProblemFromKnowledge = knowledgeProblemExtractor->run();
    auto problemOwner =
        actionPool->addProblemSource(knowledgeProblemExtractor->worldState()).value();
    runDisposables->add(makeDisposable(DisposalFunction([=]() mutable
    {
      extractingProblemFromKnowledge.cancel();
      return problemOwner->async().release();
    })));
  }
  qiLogDebug() << "Knowledge problem extractor is set up";

  // Maintain a notion of "context",
  // a goal to select actions from given behaviors,
  // and a rule to favor this goal.
  auto behaviorContext = std::make_shared<std::string>();
  auto contextProblem =
      std::make_shared<StoredProp<std::shared_ptr<const pddl::ast::Description>>>();

  auto setBehaviorContext = [=](const std::string& cxt) mutable
  {
    *behaviorContext = cxt;
    auto description = std::make_shared<pddl::ast::Description>();
    auto inContextDeclaration =
        ensurePredicate(*description, "in_context", {{"cxt", "context"}});
    auto contextObject =
        ensureObject(*description, cxt, "context");
    auto arg1 = std::make_unique<Constant>(contextObject);
    auto args = Terms();
    args.push_back(std::move(arg1));
    auto& problem = description->problem;
    QI_ASSERT(problem);
    problem.value()->goal =
        std::make_unique<Predicate>(
          std::move(args), inContextDeclaration);
    qiLogInfo()
        << "Current behavior context: " << cxt;
    contextProblem->set(description);
  };

  auto owner = actionPool->addProblemSource(contextProblem).value();
  runDisposables->add(makeDisposable(DisposalFunction([=]()
  {
    return owner->async().release();
  })));

  // Set up rules to priorize certain goals.
  // --------------------------------------------------------------------------
  auto priorizeGoals = [=](
      const std::set<const Goal*>& goals,
      Description& description)
  {
    std::vector<const Goal*> priorities;

    auto wasRespondedTo = preferRespondingToCommunicationActs(goals, description);
    std::move(wasRespondedTo.begin(), wasRespondedTo.end(), std::back_inserter(priorities));

    auto notMisunderstood = preferNotMisunderstoodCommunicationActs(goals, description);
    std::move(notMisunderstood.begin(), notMisunderstood.end(), std::back_inserter(priorities));

    auto inContext = preferRemainingInContext(goals, description);
    std::move(inContext.begin(), inContext.end(), std::back_inserter(priorities));

    return priorities;
  };


  // Track the action pool, select next action and run it
  // --------------------------------------------------------------------------

  // We track every action we will run.
  auto runningActions =
      std::make_shared<std::unordered_map<std::string, std::unique_ptr<Disposable>>>();

  // We stop them all when we clean up.
  runDisposables->add(makeDisposable(DisposalFunction(safe([runningActions]
  {
    std::vector<Future<void>> disposing;
    disposing.reserve(runningActions->size());
    for (const auto& runningAction: *runningActions)
    {
      disposing.push_back(runningAction.second->dispose());
    }
    return waitForAll(disposing).async().then([](Future<std::vector<Future<void>>>){});
  }))));

  // Common code to use to track one action, and stop it.
  auto trackRunningAction = [=](
      const std::string& behaviorPDDLName,
      const std::string& actionDescription,
      Future<void> running) mutable
  {
    runningActions->insert(
          std::make_pair(actionDescription, makeDisposable(
                           DisposalFunction([=]() mutable
    {
      qiLogInfo()
          << "Cancelling " << actionDescription << "...";
      running.cancel();
      return running;
    }))));

    running.then(safe([=](Future<void> f) mutable
    {
      if (f.hasError())
        qiLogError() << "Error in action \"" << actionDescription << "\": " << f.error();
      else if (f.hasValue())
      {
        qiLogInfo() << "Action \"" << actionDescription << "\" finished normally";
        setBehaviorContext(behaviorPDDLName);
      }
      else if (f.isCanceled())
        qiLogInfo() << "Action \"" << actionDescription << "\" was canceld";
      runningActions->erase(actionDescription);
    }));
  };

  auto applyPlan = [=](const std::string& plan) mutable
  {
    if (plan.empty())
      qiLogInfo() << "No plan to apply";
    else
      qiLogInfo() << "Applying action plan:\n" << plan;

    std::string actionDescription;
    if (!plan.empty())
    {
      // Read the result plan, check only the first result.
      // TODO: check also the other results, and support parallel actions.
      auto eolPos = plan.find_first_of('\n');
      actionDescription = plan.substr(0, eolPos);
      if (actionDescription[0] == '(')
        actionDescription = actionDescription.substr(1, actionDescription.size() - 2);
      boost::algorithm::trim_all(actionDescription);
      qiLogDebug()
          << "Action that should be running: " << actionDescription;

      if (runningActions->count(actionDescription) != 0)
      {
        qiLogInfo()
            << "Desired action is already running.";
        return; // the action is already running
      }
    }

    qiLogDebug() << "Stopping other actions...";
    runningActions->clear();

    if (actionDescription.empty())
      return;
    qiLogDebug() << "Now looking for action " << actionDescription;

    std::vector<std::string> splitted;
    boost::algorithm::split(
          splitted, actionDescription, boost::is_any_of("\t "), boost::token_compress_on);
    const auto actionName = boost::algorithm::trim_copy(splitted[0]);
    // TODO: check last part that says the order for running them

    std::string behaviorPDDLName;
    Future<void> running;
    if (splitted.size() == 1)
    { // this may be an action suggestion from a behavior, check this first
      auto findingActionSuggestion =
              actionPool->findActionSuggestionAndBehavior(actionName);
      if (findingActionSuggestion.hasValue())
      {
        const auto behaviorAndSuggestion = findingActionSuggestion.value();
        const auto& behavior = behaviorAndSuggestion.behavior;

        try
        {
          behaviorPDDLName =
              pddlNameFromPhrase(behavior.property<std::string>("name").value());
        }
        catch (const std::exception& e)
        {
          qiLogError()
              << "Failed to get the behavior context: " << e.what();
        }

        // TODO use that behavior info to build a context
        const auto& actionSuggestion = behaviorAndSuggestion.suggestion;
        qiLogInfo() << "Action " << actionDescription
                     << " was found as a suggestion and will be run";
        running = actionSuggestion.property<std::string>("name").async()
            .then([=](Future<std::string> name)
        {
          if (name.hasValue())
          {
            qiLogInfo()
                << "Action " << actionDescription
                << " is named \"" << name.value() << "\"";
          }
          return actionSuggestion.setProperty("context", context).async();
        }).unwrap().then([=](Future<void> f)
        {
          if (f.hasError())
            qiLogWarning()
                << "Suggested action did not accept the context, it may fail to run.";

          return actionSuggestion.async<void>("run");
        }).unwrap();
      }
      else if (findingActionSuggestion.hasError())
      {
        qiLogDebug()
            << "Action not found in the suggestions: " << findingActionSuggestion.error();
      }
    }

    if (!running.isValid())
    { // otherwise, it may be an action provided by a factory.
      auto findingActionFactory = actionPool->findActionFactory(actionName);
      if (findingActionFactory.hasValue())
      {
        auto actionFactoryWithParams = findingActionFactory.value();
        auto& parameters = actionFactoryWithParams.parameters;
        if (parameters.size() == splitted.size() - 1)
        {
          auto& actionFactory = actionFactoryWithParams.actionFactory;
          auto action = actionFactory.call<AnyObject>("makeAction", context);
          for (size_t i = 1u; i < splitted.size(); ++i)
          {
            const auto& arg = splitted[i];
            const auto& paramName = parameters[i-1].name;
            auto findingNode = knowledgeProblemExtractor->findKnowledgeFromPDDLName(arg);
            if (!findingNode.hasValue())
            { // this may happen if the state of the world changed during planning
              qiLogInfo()
                  << "No knowledge node associated to PDDL object \""
                  << arg << "\", aborting action \"" << actionDescription << "\"";
            }
            action.setProperty(paramName, findingNode.value());
          }
        }
        else
        {
          qiLogError()
              << "Action \"" << actionDescription
              << "\" does not match parameter specificiations.";
          return;
        }
      }
      else if (findingActionFactory.hasError())
      {
        qiLogDebug()
            << "Action not found in the factories: " << findingActionFactory.error();
      }
    }

    if (!running.isValid())
    {
      qiLogError()
          << "Planned to run action \"" << actionDescription
          << "\", but the action was not found";
    } // TODO: then, switch to next action
    else
    {
      trackRunningAction(behaviorPDDLName, actionDescription, running);
    }
  };

  // What happens every time the planning domain or problem is updated, we:
  // - parse the problem again
  // - split the goals, we prioritize them
  // - rebuild problems with prioritized goals
  // - run the planner until a solution is found
  // - apply the plan
  // This operation is costly and should be avoided.
  // Since the PDDL is usually updated in batches,
  // we'll try to wait a bit before performing the planning,
  // and thus reduce the number of planning attempts.
  auto planningScheduled = std::make_shared<std::atomic<bool>>(false);
  auto ongoingPlanning = std::make_shared<Future<void>>(nullptr);
  auto preparingPlanning = std::make_shared<Future<void>>(nullptr);
  auto nextPddl = std::make_shared<PDDLDomainAndProblem>();

  auto performPlanning = safe([=](const PDDLDomainAndProblem& pddl) mutable
  {
    *planningScheduled = false;
    if (pddl.problem.empty() || pddl.domain.empty())
    {
      qiLogVerbose() << "No problem or domain to plan for, no action to perform";
      applyPlan("");
      return;
    }

    // Else, perform planning normally
    QI_ASSERT(ongoingPlanning->isFinished());

    // Goals may be impossible to satisfy.
    // We generate partial goals to try to get a result anyway.
    // We use rules to prioritize some of the goals.
    std::shared_ptr<Description> currentDescription =
        parsePddlDomainAndProblem(pddl.domain + pddl.problem);
    auto& currentProblem = currentDescription->problem;
    QI_ASSERT(currentProblem);
    const auto goals = splitGoal(currentProblem.value()->goal);
    auto alternativeGoals = std::make_shared<std::vector<std::set<const Goal*>>>();
    *alternativeGoals =
        enumeratePrioritizedSubSets(goals, priorizeGoals(goals, *currentDescription));
    qiLogDebug()
        << "Identified " << alternativeGoals->size()
        << " candidate goal alternative(s)";

    qiLogVerbose() << "Getting ActionPlanning service...";
    *ongoingPlanning = session->service("ActionPlanning").async()

    .andThen([currentDescription, alternativeGoals, goals, pddl](AnyObject actionPlanning)
    {
      std::shared_ptr<Description> adaptedDescription =
          parsePddlDomainAndProblem(pddl.domain + pddl.problem);
      return tryPlanWithAllGoals(actionPlanning, currentDescription, adaptedDescription, alternativeGoals);
    }).unwrap()

    .andThen(safe(applyPlan)).unwrap()

    .then(safe([=](Future<void> f)
    {
      if (f.hasValue())
      {
        qiLogDebug() << "Planning iteration was successful";
      }
      else
      {
        if (f.hasError())
          qiLogError() << "Error performing planning: " << f.error();
        else if (f.isCanceled())
          qiLogDebug() << "Planning canceled";
        if (!runningActions->empty())
        {
          qiLogDebug() << "Stopping other actions...";
          runningActions->clear();
        }
      }
    })).unwrap();
  });

  auto requestPlanning = safe([=](const PDDLDomainAndProblem& pddl) mutable
  {
    qiLogDebug() << "Planning requested for problem: " << pddl.problem;
    *nextPddl = pddl;
    if (*planningScheduled)
    {
      qiLogDebug() << "Planning already scheduled, request was ignored";
      return;
    }
    *planningScheduled = true;
    // ongoingPlanning is updated when calling performPlanning.
    ongoingPlanning->then(safe([=](Future<void>) mutable
    {
      return performPlanning(*nextPddl);
    })).unwrap();
  });

  auto prepareForRequestingPlanning = safe([=](const PDDLDomainAndProblem& pddl) mutable
  {
    preparingPlanning->cancel();
    *preparingPlanning = asyncDelay([=]() mutable { requestPlanning(pddl); }, MilliSeconds(150));
  });

  auto link = actionPool->pddl.connect(prepareForRequestingPlanning);
  requestPlanning(actionPool->pddl.get().value());
  runDisposables->add(makeDisposable(DisposalFunction(safe([actionPool, safeContext, link]
  { // also keep alive the safe context
    actionPool->pddl.disconnect(link);
  }))));

  qiLogDebug() << "Action pool is tracked";

  // TODO: find desired action, run it
  // TODO: if it succeeds, try to go on the next action
  // TODO: reconnect with behavior controls
  // TODO: move as much of this as possible to the run function

  return runDisposables;
}

} // agent
} // semantic
} // qi
