#pragma once
#ifndef SEMANTICAGENT_RULES_HPP
#define SEMANTICAGENT_RULES_HPP

#include <set>
#include <vector>
#include <pddl/AST.h>
#include "behaviormodel/action_pool.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
/// Generate all possible sub-sequences of the given sequence,
/// putting in priority the sequences that keep the given elements.
/// The elements to keep are ordered by descending priority:
/// the first elements will be kept in priority.
template <typename T>
std::vector<std::set<T>> enumeratePrioritizedSubSets(
    const std::set<T>& set,
    const std::vector<T>& tryToKeep);

/// Goal stating that
/// if some communication act has to be produced,
/// and it would be the first one since the human was engaged,
/// it is preferred that some introductory act is performed first.
pddl::ast::Goal interactionIntroductionGoal();

/// Goal filter stating that it is preferred
/// to respond to the human,
/// rather than doing something else.
std::vector<const pddl::ast::Goal*> preferRespondingToCommunicationActs(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Description& description);

/// Goal filter stating that it is preferred
/// that incoming communication acts
/// are not misunderstood.
std::vector<const pddl::ast::Goal*> preferNotMisunderstoodCommunicationActs(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Description& description);

/// Goal filter stating that
/// we should remain in the same context,
/// so that to avoid switching if possible.
std::vector<const pddl::ast::Goal*> preferRemainingInContext(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Description& description);

// Definitions
//===========================================================================
/// A comparison operator,
/// that tells which set is matching the priorities the most.
template <typename T>
struct SetPriorityComparer
{
  std::vector<T> priorityList;
  bool operator()(const std::set<T>& lhs, const std::set<T>& rhs);
};


template <typename T>
bool SetPriorityComparer<T>::operator()(const std::set<T>& lhs, const std::set<T>& rhs)
{
  for (const auto& priority: priorityList)
  {
    if (lhs.count(priority) > rhs.count(priority))
      return true;
    if (lhs.count(priority) < rhs.count(priority))
      return false;
    QI_ASSERT(lhs.count(priority) == rhs.count(priority));
  }

  // Both sets respect as much the priorities.
  // Then size matters.
  if (lhs.size() > rhs.size())
    return true;
  if (lhs.size() < rhs.size())
    return false;

  // Both sets are of same size. Use lexicalographical comparison to finish.
  return lhs < rhs;
}


template <typename T>
std::vector<std::set<T>> enumeratePrioritizedSubSets(
    const std::set<T>& set,
    const std::vector<T>& tryToKeep)
{
  SetPriorityComparer<T> comparer{tryToKeep};
  std::set<std::set<T>, SetPriorityComparer<T>> result(comparer);

  std::deque<std::set<T>> toProcess;
  toProcess.push_back(set);
  while (!toProcess.empty())
  {
    auto& current = toProcess.front();
    if (current.size() > 1)
    {
      for (const auto& elt: current)
      {
        auto subset = current;
        subset.erase(elt);
        toProcess.push_back(std::move(subset));
      }
    }
    result.insert(std::move(current));
    toProcess.pop_front();
  }
  return std::vector<std::set<T>>(result.begin(), result.end());
}

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_RULES_HPP
