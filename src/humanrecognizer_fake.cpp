#include <unordered_map>
#include <qi/os.hpp>
#include <qi/log.hpp>
#include "humanrecognizer_fake.hpp"

qiLogCategory("SemanticAgent.HumanRecognizer");

namespace qi
{
namespace semantic
{
namespace agent
{
/// Trivial implementation relying on the Human object IDs only.
struct HumanRecognizerFake::Private
{
  std::unordered_map<ObjectUid, std::string> knownHumans;
};


HumanRecognizerFake::HumanRecognizerFake():
  HumanRecognizer(),
  _p(std::make_unique<Private>())
{}


HumanRecognizerFake::~HumanRecognizerFake() = default;


Future<bool> HumanRecognizerFake::isInitialized()
{
  return Future<bool>(true);
}


Future<std::string> HumanRecognizerFake::identify(qi::human::HumanPtr humanPtr)
{
  auto it = _p->knownHumans.find(humanPtr.uid());
  if (it == _p->knownHumans.end())
    return {};
  return Future<std::string>(it->second);
}


Future<bool> HumanRecognizerFake::learn(qi::human::HumanPtr humanPtr, std::string id)
{
  _p->knownHumans[humanPtr.uid()] = id;
  return Future<bool>(true);
}

} // agent
} // semantic
} // qi
