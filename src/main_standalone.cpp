#include <boost/make_shared.hpp>
#include <qi/applicationsession.hpp>
#include "agent.hpp"
#include "client.hpp"

qiLogCategory("SemanticAgent");

int main(int argc, char** argv)
{
  qi::ApplicationSession app{argc, argv};
  int retCode = EXIT_SUCCESS;

  auto session = app.session();
  session->setClientAuthenticatorFactory(
        boost::make_shared<qi::semantic::agent::UserPassAuthProviderFactory>());

  app.atRun([&]
  {
    try
    {
      auto agent = boost::make_shared<qi::semantic::agent::AgentImpl>(session);
      auto chatting = qi::semantic::agent::chatPreemptFocus(session, agent).then([&](qi::Future<void> f)
      {
        if (f.hasError())
          qiLogError() << "Chat failed: " << f.error();
        if (!f.isCanceled())
          app.stop();
      });
      qiLogInfo() << "Chatting...";

      app.atStop([chatting]() mutable
      {
        chatting.cancel();
      });
    }
    catch (const std::exception& e)
    {
      qiLogError() << e.what();
      retCode = EXIT_FAILURE;
      app.stop();
    }
  });

  app.run();
  return retCode;
}
