#include "knowledge_pruning.hpp"
#include <boost/filesystem/path.hpp>
#include <qi/log.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "utility/knowledge.hpp"

qiLogCategory("SemanticAgent.KnowledgePruning");

namespace qi
{
namespace semantic
{
namespace agent
{
boost::posix_time::ptime utcNow()
{
  return boost::posix_time::microsec_clock::universal_time();
}

/// Applies the given function to a node if it is found as a binding result.
/// The node is guaranteed to be valid.
static void applyIfBinding(
    const std::map<std::string, knowledge::Node>& binding,
    const std::string& key,
    std::function<void(const knowledge::Node&)> func)
{
  auto it = binding.find(key);
  if (it != binding.end())
  {
    const auto& node = it->second;
    if (node.value.isValid())
    { // blank bindings are expressed as invalid values
      func(node);
    }
  }
};


/// Helper to select triples for removal.
/// Give a query, process the results, return the triples.
/// We'll take care of the rest.
struct QueryAndProcess
{
  QueryAndProcess(
      std::string n,
      std::string q,
      std::function<std::vector<knowledge::Triple>(
        const std::vector<std::map<std::string, knowledge::Node>>&)> p):
    name(std::move(n)),
    query(std::move(q)),
    process(std::move(p))
  {}

  std::string name;
  std::string query;
  std::function<std::vector<knowledge::Triple>(
      const std::vector<std::map<std::string, knowledge::Node>>&)> process;
};


Future<void> pruneOldCommunicationActs(
    knowledge::EditableKnowledgeBasePtr knowledge,
    std::function<boost::posix_time::ptime()> now)
{
  static unsigned queryCount = 0;
  ++queryCount;

  auto queries = std::make_shared<std::vector<QueryAndProcess>>();

  {
    // Find all communication acts, and their potential response.
    std::stringstream query;
    query << "SELECT DISTINCT ?act ?time ?response ?addressee ?performer WHERE {\n"
          << "  ?act <" << teaching::rdfType() << "> <" << teaching::communicationActType() << "> .\n"
          << "  ?act <" << teaching::occurredAt() << "> ?time .\n"
          << "  OPTIONAL {?response <" << teaching::respondsTo() << "> ?act} .\n"
          << "  OPTIONAL {?act <" << teaching::communicationAddressee() << "> ?addressee} .\n"
          << "  OPTIONAL {?act <" << teaching::performedBy() << "> ?performer} .\n"
          << "}"; // TODO: is it possible to do the time filter in SPARQL?

    queries->emplace_back(
          "prune old communication acts",
          query.str(),
          [=](const std::vector<std::map<std::string, knowledge::Node>>& bindings)
    {
      auto oldLimit = now() - boost::posix_time::minutes(10);

      std::vector<knowledge::Triple> toPrune;
      for (const auto& binding: bindings)
      {
        try
        {
          const auto& act = binding.at("act");
          const auto actResource = act.value.to<knowledge::ResourceNode>();

          // We select only acts that are old enough.
          const auto& time = binding.at("time");
          auto actTime = timeFromDateTimeNode(time);
          if (actTime > oldLimit)
            continue;

          toPrune.push_back(
                knowledge::makeTripleFromNodes(
                  actResource,
                  teaching::occurredAtResource(),
                  time));

          toPrune.push_back(
                knowledge::makeTripleFromNodes(
                  actResource,
                  teaching::rdfTypeResource(),
                  teaching::communicationActTypeNode()));

          toPrune.push_back(
                knowledge::makeTripleFromNodes(
                  actResource,
                  teaching::rdfTypeResource(),
                  teaching::eventTypeNode()));

          applyIfBinding(binding, "response", [&](const knowledge::Node& response)
          {
            toPrune.push_back(
                  knowledge::makeTripleFromNodes(
                    response.value.to<knowledge::ResourceNode>(),
                    teaching::respondsToResource(),
                    act));
          });

          applyIfBinding(binding, "addressee", [&](const knowledge::Node& addressee)
          {
            toPrune.push_back(
                  knowledge::makeTripleFromNodes(
                    actResource,
                    teaching::communicationAddresseeResource(),
                    addressee));
          });

          applyIfBinding(binding, "performer", [&](const knowledge::Node& performer)
          {
            toPrune.push_back(
                  knowledge::makeTripleFromNodes(
                    actResource,
                    teaching::performedByResource(),
                    performer));
          });
        }
        catch (const std::exception& e)
        {
          qiLogError()
              << "Error processing binding " << binding << ": " << e.what();
        }
      }

      return toPrune;
    });
  }

  qi::Future<void> pruningAll(nullptr);
  auto firstError = std::make_shared<bool>(true);
  auto errors = std::make_shared<std::stringstream>();
  auto toPrune = std::make_shared<std::vector<knowledge::Triple>>();

  for (const auto& queryAndProcess: *queries)
  {
    const auto& query = queryAndProcess.query;
    auto querying = pruningAll.then([=](Future<void>)
    {
      qiLogDebug()
          << "SPARQL query #" << queryCount << ": " << query;
      ++queryCount;
      return knowledge.async<std::vector<std::map<std::string, knowledge::Node>>>("querySPARQLBindings", query);
    });

    auto processing = querying.andThen([=](const std::vector<std::map<std::string, knowledge::Node>>& bindings)
    {
      qiLogDebug()
          << "Query results for " << queryAndProcess.name << ": " << std::endl << bindings;
      return queryAndProcess.process(bindings);
    });

    auto pruning = processing.andThen([=](const std::vector<knowledge::Triple>& triples)
    {
      qiLogDebug() << "Pruning communication act triples:" << std::endl << triples;
      return knowledge::removeTriples(knowledge->editableKnowledgeGraph(""), triples);
    });

    pruningAll = pruning.then([=](Future<void> future)
    {
      if (!future.hasValue())
      {
        if (!*firstError)
          *errors << std::endl;
        else
          *firstError = false;

        if (future.hasError())
          *errors << future.error();
        else if (future.isCanceled())
          *errors << "canceled";
      }
    });
  }

  return pruningAll.then([=](Future<void>)
  {
    auto errorStr = errors->str();
    if (!errorStr.empty())
      qiLogError() << "Failed to prune old communication acts:" << std::endl << errorStr;
  });
}

} // agent
} // semantic
} // qi
