#include <boost/make_shared.hpp>
#include <qi/anymodule.hpp>
#include "agent.hpp"

void registerSemanticAgentModule(qi::ModuleBuilder* mb) {
  mb->advertiseFactory<qi::semantic::agent::Agent, qi::SessionPtr>("SemanticAgent");
}

QI_REGISTER_MODULE("semanticagent_module", &registerSemanticAgentModule)
