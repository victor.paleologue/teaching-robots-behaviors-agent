#ifndef SEMANTICAGENT_HUMANRECOGNITION_HELPERS_HPP
#define SEMANTICAGENT_HUMANRECOGNITION_HELPERS_HPP

#include <string>
#include <qi/type/typeinterface.hpp>

/**
 * Some helpers for using the service called "HumanRecognition"
 * providing a Bayesian network complementing OKAO's face recognition.
 */
namespace qi
{
namespace semantic
{
namespace agent
{

struct AgeEstimation
{
  float age;
  float confidence;
};


struct GenderEstimation
{
  std::string gender;
  float confidence;
};


struct HeightEstimation
{
  float height;
  float confidence;
};

} // agent
} // semantic
} // qi

QI_TYPE_STRUCT(::qi::semantic::agent::AgeEstimation, age, confidence);
QI_TYPE_STRUCT(::qi::semantic::agent::GenderEstimation, gender, confidence);
QI_TYPE_STRUCT(::qi::semantic::agent::HeightEstimation, height, confidence);

#endif // SEMANTICAGENT_HUMANRECOGNITION_HELPERS_HPP
