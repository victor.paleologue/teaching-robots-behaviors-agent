#include "humanmanager.hpp"
#include <unordered_map>
#include <qi/log.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "utility/knowledge.hpp"
#include "utility/incontext.hpp"

qiLogCategory("SemanticAgent.HumanManager");

using namespace qi::human;
using namespace qi::knowledge;
using namespace qi::teaching;

namespace qi
{
namespace semantic
{
namespace agent
{
class HumanManagerImpl: public HumanManager, public SafeContext
{
public:
  HumanManagerImpl(
      std::shared_ptr<Strand> strand,
      context::ContextPtr contextPtr,
      SessionPtr sessionPtr,
      humanawareness::HumanAwarenessPtr humanAwarenessPtr,
      EditableKnowledgeBasePtr editableKnowledgeBasePtr,
      std::shared_ptr<HumanRecognizer> humanRecognizer):
    HumanManager(),
    SafeContext(strand),
    sessionPtr(sessionPtr),
    contextPtr(contextPtr),
    humanAwarenessPtr(humanAwarenessPtr),
    _knowledge(editableKnowledgeBasePtr),
    graph(editableKnowledgeBasePtr->editableKnowledgeGraph("human_graph")),
    humanRecognizer(humanRecognizer),
    _nodeForHuman()
  {}


  virtual ~HumanManagerImpl()
  {
    removeLostHumans();
    for (auto &el : _nodeForHuman)
    {
      setTimestamp(el.second, graph, "last_seen");
    }
  }


  void removeLostHumans() override
  {
    inContext([=]
    {
      auto humansAround = humanAwarenessPtr->humansAround.get().value();
      for (auto it = _nodeForHuman.begin(); it != _nodeForHuman.end();)
      {
        const auto& humanAndNode = *it;
        auto jt = std::find(humansAround.begin(), humansAround.end(), humanAndNode.first);
        if (jt == humansAround.end())
        {
          setTimestamp(humanAndNode.second, graph, "last_seen");
          qiLogDebug() << "Human " << humanAndNode.first.uid() << " disappeared";
          it = _nodeForHuman.erase(it);
        }
        else
        {
          ++it;
        }
      }
    })().value();
  }

  ResourceNode getResNode(const HumanPtr& humanPtr) override
  {
    return inContext([=]
    {
      if (!humanPtr)
        throw std::invalid_argument("empty human object");

      // Look if the human is already associated to a node.
      auto node = getResNodeFromMap(humanPtr);
      if (!node.url.empty())
      {
        return node;
      }

      else
      {
        // Otherwise try to identify the human,
        // and recover a node from the knowledge.
        node = getResNodeFromKnowledge(humanPtr);
        if (!node.url.empty())
        {
          _nodeForHuman.insert(std::make_pair(humanPtr, node));
          return node;
        }

        // If the human was not recognized, learn it.
        else
        {
          auto socialAgentResource = makeUniqueResource(socialAgentPrefix());
          auto learned = learn(humanPtr, socialAgentResource);
          if (learned)
            return addToKnowledge(humanPtr, socialAgentResource);
        }
      }

      return ResourceNode();
    })().value();
  }


  HumanPtr getHumanPtr(const ResourceNode& r) override
  {
    return inContext([=]
    {
      removeLostHumans();

      auto res = std::find_if(
            _nodeForHuman.begin(), _nodeForHuman.end(),
            [&r](const std::pair<HumanPtr, ResourceNode>& p)
      {
        return p.second == r;
      });

      if (res != _nodeForHuman.end())
        return res->first;
      return HumanPtr();
    })().value();
  }


  ResourceNode resourceFromIRI(const std::string& iri) override
  {
    return inContext([=]
    {
      auto resource = ResourceNode{iri};
      if (_knowledge->knowledgeBase()->contains(
            makeTripleFromResources(resource, rdfTypeResource(), socialAgentTypeResource()), ""))
        return resource;
      qiLogDebug() << "No social agent in knowledge for <" << iri << ">";
      return ResourceNode();
    })().value();
  }


  ResourceNode getResNodeFromName(const std::string& name) override
  {
    return inContext([=]
    {
      using namespace qi::knowledge;
      using namespace qi::teaching;

      std::string sparQLnodeQuery = "SELECT ?node\n"
                                    "WHERE {\n"
                                    " ?node <" + rdfsLabel() + "> " + name + " .\n"
                                    "}";

      // might require KnowledgeBasePtr instead of KnwoledgeGraphPtr
      auto queryResults =
          _knowledge.call<std::vector<std::map<std::string, Node>>>(
            "querySPARQLBindings", sparQLnodeQuery);
      if (not queryResults.empty() && not queryResults.front().empty())
      {
        try
        {
          return queryResults.front()["node_url"].value.to<ResourceNode>();
        } catch (std::exception& e)
        {
          qiLogInfo("Cannot get node from query, ", e.what());
        }
      }
      return ResourceNode();
    })().value();
  }


  void addName(
      const ResourceNode& agentNode,
      const std::string& name,
      const qi::locale::Locale& locale) override
  {
    inContext([=]
    {
      graph->add(
            makeTripleFromNodes(
                   agentNode, rdfsLabelResource(), makeLocalizedStringNode(name, locale)));
    })().value();
  }


  void colocalize(const ResourceNode& r1, const ResourceNode& r2) override
  {
    inContext([=]
    {
      using namespace qi::teaching;
      graph->add(makeTripleFromResources(r1, colocalizedWithResource(), r2));
    })().value();
  }


  bool changeResNode(const HumanPtr& humanPtr, const ResourceNode& r) override
  {
    return inContext([=]
    {
      ResourceNode resnode = r;
      if (r.url.empty()) // if no ResNode given, get it from knowledge
      {
        auto resnode = getResNodeFromKnowledge(humanPtr);
        if (!resnode.url.empty())
          resnode = addToKnowledge(humanPtr);
      }
      auto it = std::find_if(_nodeForHuman.begin(), _nodeForHuman.end(), [&humanPtr](const std::pair<HumanPtr, ResourceNode>& p)
                                                                  {return p.first == humanPtr;});
      if (it != _nodeForHuman.end())
      {
        auto old_r = it->second;
        it->second = resnode; // change node in map
        return true;
      }
      return false;
    })().value();
  }


  Future<void> engage(const HumanPtr& humanPtr) override
  {
    return inContext([=]
    {
      auto engageHuman = humanAwarenessPtr->makeEngageHuman(contextPtr, humanPtr);
      auto f = engageHuman->async().run();
      engageHuman->humanIsEngaged.connect([f, engageHuman]() mutable { f.cancel(); });
      return f;
    })();
  }


  boost::posix_time::ptime getTimestamp(
      const ResourceNode& target,
      const EditableKnowledgeGraphPtr& graph) override
  {
    return inContext([=]
    {
      auto node = getTimestampNode(target, graph);
      if (node.value.isValue())
        return timeFromDateTimeNode(node);

      return boost::posix_time::ptime();
    })().value();
  }


  void setTimestamp(
      const ResourceNode& target,
      EditableKnowledgeGraphPtr graph,
      const std::string& eventName) override
  {
    inContext([=]
    {
      // TODO: fix this event to match the ontology
      using namespace teaching;

      const auto timestampNode = makeNowNode();
      const auto eventResourceNode = makeUniqueResource(eventPrefix());

      if (!getTimestamp(target, graph).is_not_a_date_time())
      {
        graph->remove(
              makeTripleFromNodes(
                eventTypeResource(),
                occurredAtResource(),
                getTimestampNode(target, graph)));
      }
      else
      {
        graph->add(makeTripleFromResources(eventResourceNode, rdfTypeResource(), eventTypeResource()));
        graph->add(makeTripleFromResources(eventResourceNode, rdfSubjectResource(), target));
        graph->add(makeTripleFromNodes(eventResourceNode, rdfObjectResource(), makeNodeFromName(eventName)));
      }
      graph->add(makeTripleFromNodes(eventResourceNode, occurredAtResource(), timestampNode));
    })().value();
  }


private:
  Node getTimestampNode(const ResourceNode& target, const EditableKnowledgeGraphPtr& graph)
  {
    std::string query =
        "SELECT ?timestamp\n"
        "WHERE {\n"
        "  ?event <" + rdfSubject() + "> <" + target.url + "> .\n"
        "  ?event <" + occurredAt() + "> ?timestamp.\n"
        "}";

    auto queryResults =
        _knowledge.call<std::vector<std::map<std::string, Node>>>(
          "querySPARQLBindings", query);
    if (!queryResults.empty() && !queryResults.front().empty())
      return queryResults.front()["timestamp"];

    return Node();
  }

  // ... because this seems to do it all.
  bool learn(const HumanPtr& humanPtr, const ResourceNode& socialAgentResource = {})
  {
    // TODO: this automatic resolving of the resource node is bad design, clean this.
    // node's url is used as identifier for the recognition service
    return humanRecognizer->learn(
          humanPtr,
          socialAgentResource.url.empty()
          ? getResNode(humanPtr).url
          : socialAgentResource.url).value();
  }

  /// Add the human to the map, associated to the given node.
  /// If no node is provided, a unique one is generated.
  ResourceNode addToKnowledge(const HumanPtr& human, const ResourceNode& node = {})
  {
    using namespace qi::teaching;

    removeLostHumans();
    const auto personResourceNode =
        node.url.empty() ? makeUniqueResource(socialAgentPrefix()) : node;

    graph->add(makeTripleFromResources(personResourceNode, rdfTypeResource(), socialAgentTypeResource()));
    _nodeForHuman.insert(std::make_pair(human, personResourceNode));
    return personResourceNode;
  }


  ResourceNode getResNodeFromMap(const HumanPtr& humanPtr)
  {
    removeLostHumans();
    try { // TODO: avoid using an exception
      return _nodeForHuman.at(humanPtr);
    } catch (std::out_of_range& e) {
      qiLogDebug()
          << "No node registered for given human";
    }
    return ResourceNode();
  }


  /// Identifies a human, and use the identification to find
  /// the social agent node from the knowledge.
  /// The node is associated with the human object in the map.
  ResourceNode getResNodeFromKnowledge(const HumanPtr& humanPtr)
  {
    // Normally, this ID is an IRI
    try
    {
      auto id = humanRecognizer->identify(humanPtr).value();
      if (id.empty())
        return {};
      return resourceFromIRI(id);
    }
    catch (const NoFacePictureException& e)
    {
      qiLogDebug() << "Cannot produce node: " << e.what();
    }
    catch (const std::exception& e)
    {
      qiLogDebug() << "Cannot find node: " << e.what();
    }
    return {};
  }


  qi::SessionPtr sessionPtr;
  qi::context::ContextPtr contextPtr;
  qi::humanawareness::HumanAwarenessPtr humanAwarenessPtr;
  EditableKnowledgeBasePtr _knowledge;
  qi::knowledge::EditableKnowledgeGraphPtr graph;
  std::shared_ptr<HumanRecognizer> humanRecognizer;
  std::unordered_map<HumanPtr, ResourceNode> _nodeForHuman;
};



std::unique_ptr<HumanManager> makeHumanManager(
    std::shared_ptr<Strand> strand,
    context::ContextPtr contextPtr,
    SessionPtr sessionPtr,
    humanawareness::HumanAwarenessPtr humanAwarenessPtr,
    EditableKnowledgeBasePtr editableKnowledgeBasePtr,
    std::shared_ptr<HumanRecognizer> humanRecognizer)
{
  return std::make_unique<HumanManagerImpl>(
        strand,
        contextPtr,
        sessionPtr,
        humanAwarenessPtr,
        editableKnowledgeBasePtr,
        humanRecognizer);
}

} // agent
} // semantic
} // qi
