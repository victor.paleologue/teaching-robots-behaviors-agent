#ifndef SEMANTICAGENT_LANGUAGE_HPP
#define SEMANTICAGENT_LANGUAGE_HPP

#include <qi/session.hpp>
#include <qi/locale/locale.hpp>
#include <qisem/semanticmodel/enum/semanticlanguagetype.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
qisem::SemanticLanguageEnum currentLanguageToSemanticEnum(SessionPtr session);
locale::Locale currentLanguageToLocale(SessionPtr session);

/// Track the locale with this custom property.
struct CurrentLocaleProperty
{
  Property<locale::Locale> locale;
  CurrentLocaleProperty(const SessionPtr& session);
  ~CurrentLocaleProperty();

  /// Gets the current value, as soon as a value is available.
  Future<locale::Locale> value();

private:
  struct Private;
  std::unique_ptr<Private> _p;
};
}
}
}

#endif // SEMANTICAGENT_LANGUAGE_HPP
