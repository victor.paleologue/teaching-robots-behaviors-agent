#ifndef SEMANTICAGENT_HUMANRECOGNITION_OKAO_BN_HPP
#define SEMANTICAGENT_HUMANRECOGNITION_OKAO_BN_HPP

#include <memory>
#include <qi/human/human.hpp>
#include <qi/session.hpp>
#include <qi/strand.hpp>
#include "humanrecognizer.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
/// \brief This implementation of HumanRecognizer allows human recognition
/// using OKAO and a bayesian network.
class HumanRecognizerOkaoBN: public HumanRecognizer
{
public:
  HumanRecognizerOkaoBN(std::shared_ptr<Strand> strand, qi::SessionPtr session);
  ~HumanRecognizerOkaoBN();

  Future<bool> isInitialized() override;
  Future<std::string> identify(qi::human::HumanPtr humanPtr) override;
  Future<bool> learn(qi::human::HumanPtr humanPtr, std::string id) override;

private:
  struct Private;
  std::unique_ptr<Private> _p;
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_HUMANRECOGNITION_OKAO_BN_HPP
