#include "rules.hpp"
#include <qi/log.hpp>
#include <qi/teaching/ontology.hpp>
#include "pddl/pddl_io.hpp"
#include "pddl/pddl_read.hpp"

qiLogCategory("SemanticAgent.Rules");

using namespace pddl::ast;
using namespace qi::teaching;

namespace qi
{
namespace semantic
{
namespace agent
{

std::vector<const pddl::ast::Goal*> preferRespondingToCommunicationActs(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Description& description)
{
  std::vector<const Goal*> filteredGoals;
  if (!description.problem)
    return filteredGoals;

  for (const auto& goal: goals)
  {
    if (!goal->is<AtomicFormula>())
      continue;
    const auto& asFormula = goal->get<AtomicFormula>();

    if (!asFormula.is<PredicatePointer>())
      continue;
    const auto& asPredicate = asFormula.get<PredicatePointer>();

    if (asPredicate->declaration->name != "was_responded_to")
      continue;
    filteredGoals.push_back(goal);
  }
  return filteredGoals;
}


std::vector<const Goal*> preferNotMisunderstoodCommunicationActs(
    const std::set<const Goal*>& goals,
    Description& description)
{
  std::vector<const Goal*> filteredGoals;
  if (!description.problem)
    return filteredGoals;

  for (const auto& goal: goals)
  {
    if (!goal->is<NotPointer<Precondition>>())
      continue;
    const auto& asNot = goal->get<NotPointer<Precondition>>();
    const auto& negatedGoal = asNot->argument;

    if (!negatedGoal.is<AtomicFormula>())
      continue;
    const auto& asFormula = negatedGoal.get<AtomicFormula>();

    if (!asFormula.is<PredicatePointer>())
      continue;
    const auto& asPredicate = asFormula.get<PredicatePointer>();

    if (asPredicate->declaration->name != "was_misunderstood")
      continue;

    filteredGoals.push_back(goal);
  }
  return filteredGoals;
}

std::vector<const pddl::ast::Goal*> preferRemainingInContext(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Description& description)
{
  std::vector<const Goal*> filteredGoals;
  if (!description.problem)
    return filteredGoals;

  for (const auto& goal: goals)
  {
    if (!goal->is<AtomicFormula>())
      continue;
    const auto& asFormula = goal->get<AtomicFormula>();

    if (!asFormula.is<PredicatePointer>())
      continue;
    const auto& asPredicate = asFormula.get<PredicatePointer>();

    if (asPredicate->declaration->name != "in_context")
      continue;
    filteredGoals.push_back(goal);
  }
  return filteredGoals;
}
} // agent
} // semantic
} // qi
