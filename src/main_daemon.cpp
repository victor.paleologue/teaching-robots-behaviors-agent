#include <boost/make_shared.hpp>
#include <qi/applicationsession.hpp>
#include <qi/log.hpp>

#include "agent.hpp"
#include "client.hpp"

qiLogCategory("SemanticAgent.Daemon");

int main(int argc, char** argv)
{
  qi::startEventLoop(8);
  qi::ApplicationSession app{argc, argv};
  int retCode = EXIT_SUCCESS;

  auto session = app.session();
  session->setClientAuthenticatorFactory(
        boost::make_shared<qi::semantic::agent::UserPassAuthProviderFactory>());

  app.atRun([&]
  {
    try
    {
      qiLogDebug() << "Running running, as fast as I can...";
      auto agent = boost::make_shared<qi::semantic::agent::AgentImpl>(session);
      qiLogDebug() << "Semantic agent created, registering...";
      session->registerService("SemanticAgent", agent);
      qiLogInfo() << "Registered as \"SemanticAgent\"";
    }
    catch (const std::exception& e)
    {
      qiLogError() << e.what();
      retCode = EXIT_FAILURE;
      app.stop();
    }
  });
  app.run();
  return retCode;
}
