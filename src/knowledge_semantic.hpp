#pragma once
#ifndef SEMANTICAGENT_KNOWLEDGE_SEMANTIC_HPP
#define SEMANTICAGENT_KNOWLEDGE_SEMANTIC_HPP

#include <memory>
#include <qi/knowledge/knowledge.hpp>
#include "utility/semantic_helpers.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
struct KnowledgeSemanticBridge
{
  /// Provide the knowledge base on which will rely the extraction.
  KnowledgeSemanticBridge(
      knowledge::KnowledgeBasePtr knowledgeBase,
      std::shared_ptr<SemanticMemoryContext> semanticContext);

  /// Destroys the object and stops all extraction.
  ~KnowledgeSemanticBridge();

  /// Runs the extractor, starts updating the problem.
  Future<void> run();

  /// Emitted whenever the internal data changed after an update of the knowledge.
  Signal<> updated;

  /// Find back a node from the given semantic label.
  Future<knowledge::ResourceNode>
  findResourceFromSemanticLabel(
      const std::shared_ptr<qisem::SemanticExpression> semanticLabel,
      const locale::Locale& locale,
      const knowledge::ResourceNode& rdfType = {});

private:
  struct Private;
  std::unique_ptr<Private> _p;
};
} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_KNOWLEDGE_SEMANTIC_HPP
