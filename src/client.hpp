#ifndef SEMANTICAGENT_CLIENT_HPP
#define SEMANTICAGENT_CLIENT_HPP

#include <qi/future.hpp>
#include <qi/session.hpp>
#include <qi/context/context.hpp>
#include <qi/semantic/agent/agent.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{

/// The authentication provider setup with a default password.
struct UserPassAuthProvider: public qi::ClientAuthenticator
{
  CapabilityMap initialAuthData() override;
};

/// A factory for the authentication provider.
struct UserPassAuthProviderFactory: public qi::ClientAuthenticatorFactory
{
  ClientAuthenticatorPtr newAuthenticator() override;
};

/// Runs a normal chat given some context and a semantic agent.
Future<void> chatWithContext(context::ContextPtr context, AgentPtr agent);

/// Preempts the focus and then runs a normal chat, given a semantic agent.
Future<void> chatPreemptFocus(SessionPtr session, AgentPtr agent);

/// Gently tries to take the focus and then runs a normal chat, given a semantic agent.
qi::Future<void> chatTakeFocus(SessionPtr session, AgentPtr agent);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_CLIENT_HPP
