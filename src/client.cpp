#include "client.hpp"
#include <boost/make_shared.hpp>
#include <qi/context/utils.hpp>

qiLogCategory("SemanticAgent");

namespace qi
{
namespace semantic
{
namespace agent
{

CapabilityMap UserPassAuthProvider::initialAuthData()
{
  CapabilityMap capabilitiesWithAuth;
  capabilitiesWithAuth["user"] = qi::AnyValue::from("nao");
  capabilitiesWithAuth["token"] = qi::AnyValue::from("nao");
  return capabilitiesWithAuth;
}


ClientAuthenticatorPtr UserPassAuthProviderFactory::newAuthenticator()
{
  return boost::make_shared<UserPassAuthProvider>();
}


Future<void> chatWithContext(context::ContextPtr context, AgentPtr agent)
{
  // make the chat
  auto speechExtractor = [&agent]
  {
    try
    {
      return agent->makeALDialogSpeechExtractor();
//      return agent->makeMicrosoftSpeechExtractor();
    }
    catch (const std::exception& e)
    {
      qiLogWarning() << "Could not make ALDialog speech extractor ("
                     << e.what() << ") falling back to a fake one";
      return agent->makeFakeSpeechExtractor();
    }
  }();

  auto chat = agent->makeChatWithoutDefaultContent(context, speechExtractor);
  auto running = chat->async().run();
  return running.andThen([agent, chat](void*){});
}


Future<void> chatPreemptFocus(SessionPtr session, AgentPtr agent)
{
  auto life = session->service("ALAutonomousLife").value();
  auto focusOwner = life.call<AnyObject>("_preemptFocusForRemote");
  auto context = context::makeContext();
  context->focus.set(focusOwner);
  return chatWithContext(std::move(context), std::move(agent));
}


Future<void> chatTakeFocus(SessionPtr session, AgentPtr agent)
{
  // take the focus, prepare a context
  auto focus = session->service("Focus").value();
  auto focusOwner = focus.call<AnyObject>("take");
  auto context = context::makeContext();
  context->focus.set(focusOwner);
  return chatWithContext(std::move(context), std::move(agent));
}

} // agent
} // semantic
} // qi
