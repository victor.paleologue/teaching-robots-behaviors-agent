#pragma once
#ifndef SEMANTICAGENT_KNOWLEDGE_PRUNING_HPP
#define SEMANTICAGENT_KNOWLEDGE_PRUNING_HPP

#include <qi/knowledge/knowledge.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
boost::posix_time::ptime utcNow();

/// Remove communication acts that seem too old,
/// including inputs and responses.
/// @note prefer something more subtle whenever some communication acts should be kept.
Future<void> pruneOldCommunicationActs(
    knowledge::EditableKnowledgeBasePtr knowledge,
    std::function<boost::posix_time::ptime()> now = utcNow);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_KNOWLEDGE_PRUNING_HPP
