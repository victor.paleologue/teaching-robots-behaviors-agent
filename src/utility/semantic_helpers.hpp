#ifndef SEMANTICAGENT_BEHAVIOR_SEMANTIC_HELPERS_HPP
#define SEMANTICAGENT_BEHAVIOR_SEMANTIC_HELPERS_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/strand.hpp>
#include <qi/trackable.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpressions.hpp>

namespace qisem
{
  struct ChatbotMemory;
  struct SemanticMemory;
  struct SemanticStatementGrounding;
  namespace linguistics
  {
    struct LinguisticDatabase;
  } // linguistics
} // qisem


namespace qi
{
namespace semantic
{
namespace agent
{
/// A context object to serve as an interface for a semantic memory and
/// a linguistic database.
struct SemanticMemoryContext: Trackable<SemanticMemoryContext>
{
  virtual ~SemanticMemoryContext();
  virtual std::shared_ptr<Strand> strand() = 0;
  virtual std::shared_ptr<const qisem::linguistics::LinguisticDatabase> linguisticDatabase() const = 0;
  virtual std::shared_ptr<qisem::linguistics::LinguisticDatabase> linguisticDatabase() = 0;
  virtual std::shared_ptr<const qisem::SemanticMemory> semanticMemory() const = 0;
  virtual std::shared_ptr<qisem::SemanticMemory> semanticMemory() = 0;

  /// Wraps a function so that it is called in the context of the semantic memory,
  /// but also avoid calling it after the given trackable's death.
  template <typename Function, typename Trackable>
  auto wrap(Trackable&& t, Function&& f)
      -> decltype(strand()->unwrappedSchedulerFor(track(std::forward<Function>(f), std::forward<Trackable>(t))))
  { return strand()->unwrappedSchedulerFor(track(std::forward<Function>(f), std::forward<Trackable>(t))); }

  /// Wraps a function so that it is called in the context of the semantic memory.
  template <typename Function>
  auto wrap(Function&& f) -> decltype(wrap(this, std::forward<Function>(f)))
  { return wrap(this, std::forward<Function>(f)); }
};


/// Make a semantic memory context from separate components.
std::shared_ptr<SemanticMemoryContext> makeSemanticMemoryContext(
    std::shared_ptr<Strand> strandPtr,
    std::shared_ptr<qisem::linguistics::LinguisticDatabase> lingDbPtr,
    std::shared_ptr<qisem::SemanticMemory> semMemPtr);

/// Shortcut to make everything from scratch.
std::shared_ptr<SemanticMemoryContext> makeSemanticMemoryContext();


/// Make a semantic memory context from a chatbot memory.
// KLUDGE: the chatbot memory may outlive its own linguistic
// database! We make sure we keep the a pointer that keeps it alive.
std::shared_ptr<SemanticMemoryContext> makeSemanticMemoryContext(
    std::shared_ptr<qisem::ChatbotMemory> chatbotMemory,
    std::shared_ptr<qisem::linguistics::LinguisticDatabase> lingDbPtr);

qisem::SemanticLanguageEnum localeToSemanticEnum(locale::Locale locale);

/// Communicative act direction, applied to semantic objects.
enum class SemanticDirection
{
  EngagedUserToRobot,
  RobotToEngagedUser
};

/// Shortcut to make a semantic expression from a phrase.
std::shared_ptr<qisem::SemanticExpression> phraseToSemanticExpression(
    const SemanticMemoryContext& semanticMemoryContext,
    const PhraseAndLocale& phrase,
    SemanticDirection direction = SemanticDirection::EngagedUserToRobot);

/// Shortcut to make a phrase from a semantic expression.
/// If it is a template, the children are replaced by
PhraseAndLocale phraseFromSemanticExpression(
    const SemanticMemoryContext& semanticMemoryContext,
        const qisem::SemanticExpression& semanticExpression,
        const locale::Locale& locale,
        SemanticDirection direction = SemanticDirection::EngagedUserToRobot);

/// Shortcut to serialize semantic expressions.
std::ostream& operator<<(std::ostream& out, const qisem::SemanticExpression& expression);


/// Produce a statement centered on the provided verb.
std::unique_ptr<qisem::GroundedExpression> statementExpressionFromEnglishVerbLemma(
    const std::string& verb);


/// Tells whether the given expression has the given verbal group as the head.
/// May match sentences like "{<subject>} <verbal group> {<complements>}".
/// @param semanticExpression An expression to check for the verbal group.
/// @param verbalGroup An expression grounded on a statement with at least a verb in any tense.
/// @param linguisticDatabase A linguistic database for text synthesis.
/// @note Can serve as a predicate for various algorithms.
bool matchVerbalGroup(
    const qisem::SemanticExpression& semanticExpression,
    const qisem::SemanticExpression& verbalGroupTemplate,
    const qisem::SemanticMemory& semanticMemory,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase);


/// If the given expression is an action declaration, return it. Return nullptr otherwise.
const qisem::SemanticStatementGrounding* asActionDeclaration(const qisem::SemanticExpression& exp);

/// If the given expression is a coreference, return it. Return nullptr otherwise.
const qisem::SemanticGenericGrounding* asCoreference(const qisem::SemanticExpression& exp);

/// If the given expression is a list of action declarations, return it. Return nullptr otherwise.
/// A list of action declarations may include coreferences as the first or the last element.
/// A single action is not a list of action.
const qisem::ListExpression* asActionDeclarationList(const qisem::SemanticExpression& exp);

/// Returns non-nullptr if the expression is a statement with the verb "be" in the present.
const qisem::GroundedExpression* asEquivalenceStatementExpression(const qisem::SemanticExpression& exp);

/// Return a new expression of agreement.
/// @todo Make a "isAgreement" function.
std::unique_ptr<qisem::SemanticExpression> makeAgreementExpression();

/// Return an expression of agreement about a given expression.
std::unique_ptr<qisem::SemanticExpression> makeAgreementExpression(std::unique_ptr<qisem::SemanticExpression> exp);

/// Tells whether the given expression could be understood as a closure.
bool isClosure(const qisem::SemanticExpression& exp);

/// Tells whether the given expression could be understood as an enumeration closure.
/// An enumeration is a closure also.
bool isEnumerationClosure(const qisem::SemanticExpression& exp);

/// Make a simple expression grounded on the given text.
/// If provided text is empty, returns a null expression.
std::unique_ptr<qisem::GroundedExpression> makeTextExpression(const std::string& text);
} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_BEHAVIOR_SEMANTIC_HELPERS_HPP
