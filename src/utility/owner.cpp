#include "owner.hpp"
#include <qi/anyobject.hpp>
#include <qi/log.hpp>
#include <qi/strand.hpp>

qiLogCategory("SemanticAgent.CustomOwner");

namespace qi
{
namespace semantic
{
namespace agent
{
struct CustomOwner::Private
{
  ReleaseFunction releaseFunction;
  Future<void> releasing;
  Promise<void> waitingForReleasedPromise;
  Strand strand;
};

CustomOwner::CustomOwner(ReleaseFunction releaseFunction)
  : _p(new Private)
{
  _p->releaseFunction = std::move(releaseFunction);
}

CustomOwner::CustomOwner(ReleaseFunctionSync releaseFunction)
  : _p(new Private)
{
  _p->releaseFunction = [=]
  {
    releaseFunction();
    return Future<void>(nullptr);
  };
}

CustomOwner::~CustomOwner()
{
  _p->strand.join();

  bool releaseWasRequested = _p->releasing.isValid();
  if (!releaseWasRequested)
  { // if the release has not been called yet, call it
    try
    {
      _p->releasing = _p->releaseFunction();
    }
    catch (const std::exception& e)
    {
      _p->releasing = makeFutureError<void>(e.what());
    }
  }

  _p->releasing.then([&](Future<void> f)
  {
    if (f.hasError())
    {
      qiLogDebug()
          << "Error calling the release function at destruction: "
          << f.error();
      // debug level, because it is normal that release functions throw if they're dead.
    }
  });

  if (!_p->waitingForReleasedPromise.future().isRunning())
    return; // release has already been reported

  // since the strand was joined, the callbacks on release are disabled
  // so this destructor takes the relay on reporting the release.
  _p->waitingForReleasedPromise.setValue(nullptr);
  QI_EMIT released();
}


Future<void> CustomOwner::release()
{
  return _p->strand.async([=]
  {
    // if releasing was already asked, hook to its future
    if (_p->releasing.isValid())
      return _p->releasing;

    _p->releasing = _p->releaseFunction();
    _p->releasing.then(_p->strand.unwrappedSchedulerFor([=](Future<void>)
    { // report the release when it is over
      _p->waitingForReleasedPromise.setValue(nullptr);
      QI_EMIT released();
    })).unwrap();

    return _p->releasing;
  }).unwrap();
}

Future<void> CustomOwner::waitForReleased()
{
  return _p->waitingForReleasedPromise.future();
}
} // agent
} // semantic
} // qi

QI_REGISTER_OBJECT(qi::semantic::agent::CustomOwner, release, waitForReleased, released)
REGISTER_OWNER(qi::semantic::agent::CustomOwner);
