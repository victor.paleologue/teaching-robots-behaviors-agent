#ifndef QI_SEMANTIC_AGENT_DISPOSABLE_HPP
#define QI_SEMANTIC_AGENT_DISPOSABLE_HPP

#include <functional>
#include <qi/future.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
/// A simple implementation of the disposable concept.
/// It is not thread-safe, but it supports asynchronous disposal functions.
struct Disposable
{
  /// Destroy and dispose if not already disposed.
  /// Waits synchronously for the disposal, and therefore may throw.
  virtual ~Disposable();

  /// Dispose. Calls the disposal function.
  /// This is asynchronous, you must wait for the future to ensure the disposal is effective.
  virtual Future<void> dispose() = 0;
};

/// Make a disposable that calls an asynchronous function when disposed.
using DisposalFunction = std::function<Future<void>()>;
std::unique_ptr<Disposable> makeDisposable(DisposalFunction disposal);

/// Make a disposable that calls a synchronous function when disposed.
using DisposalFunctionSync = std::function<void()>;
std::unique_ptr<Disposable> makeDisposable(DisposalFunctionSync disposal);

/// Make a disposable from a future to cancel.
/// The result of the dispose mirrors the future's.
std::unique_ptr<Disposable> makeDisposable(Future<void> futureToCancel);

/// A container of disposables that can be disposed of.
/// Note that you can indefinitely compose disposables.
struct Disposables: Disposable
{
  /// Constructor does not need a disposal function, it already defines one.
  Disposables();
  virtual ~Disposables();

  Future<void> dispose() override;

  /// Disposables can be added to the container.
  /// They will be disposed when the container is disposed.
  void add(std::unique_ptr<Disposable> disposable);

private:
  struct Private;
  std::unique_ptr<Private> _p;
};

} // agent
} // semantic
} // qi

#endif // QI_SEMANTIC_AGENT_DISPOSABLE_HPP
