#include "incontext.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
SafeContext::SafeContext(std::shared_ptr<Strand> strand)
  : strand(strand)
{}

SafeContext::~SafeContext()
{
  destroy();
}

} // agent
} // semantic
} // qi
