#pragma once
#ifndef SEMANTICAGENT_INCONTEXT_HPP
#define SEMANTICAGENT_INCONTEXT_HPP

#include <memory>
#include <qi/strand.hpp>
#include <qi/trackable.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
/// Transforms the given function so that it is executed in the context of
/// the given object. The object is expected to be a standard smart pointer
/// to an object that is trackable, and owns a pointer to a strand.
/// This is the pattern found regularly in this project.
template <typename TrackablePtrWithStrand, typename Function>
auto inContext(TrackablePtrWithStrand& p, Function&& f)
    -> decltype(p->strand->unwrappedSchedulerFor(trackSilent(std::forward<Function>(f), p.get())))
{ return p->strand->unwrappedSchedulerFor(trackSilent(std::forward<Function>(f), p.get())); }


struct SafeContext: Trackable<SafeContext>
{
  SafeContext(std::shared_ptr<Strand> strand = std::make_shared<Strand>());
  virtual ~SafeContext();
  using Trackable<SafeContext>::destroy;

  std::shared_ptr<Strand> strand;

  template <typename Function>
  auto inContext(Function&& f)
  {
    return this->strand->unwrappedSchedulerFor(trackSilent(std::forward<Function>(f), this));
  }

  template <typename Function, typename... Args>
  auto runInContext(Function&& f, Args&&... args)
  {
    return inContext(std::forward<Function>(f))(std::forward<Args...>(args...));
  }

  template <typename Function>
  auto runInContext(Function&& f)
  {
    return inContext(std::forward<Function>(f))();
  }

  auto inContextTransform() { return [=](auto f){ return inContext(f); }; }
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_INCONTEXT_HPP
