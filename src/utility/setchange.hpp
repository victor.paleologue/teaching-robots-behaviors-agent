#ifndef QI_SEMANTIC_AGENT_SETCHANGE_HPP
#define QI_SEMANTIC_AGENT_SETCHANGE_HPP

#include <algorithm>
#include <set>
#include <vector>

namespace qi
{
namespace semantic
{
namespace agent
{
/// A view of a set value change.
template <typename ElementType>
struct SetChange
{
  std::set<ElementType> value;
  std::set<ElementType> inserted;
  std::set<ElementType> removed;
};

/// An object to track the changes in a vector,
/// so that to be able to reason on deltas easily.
template <typename ElementType>
struct ChangeTracker
{
  /// Construct and initialize the tracker with a value.
  ChangeTracker(const std::vector<ElementType>& value = {});

  /// Check the change between the current value a a new one.
  /// The new value is then stored as the last value.
  SetChange<ElementType> check(const std::vector<ElementType>& value);

  /// Last value.
  std::set<ElementType> lastValue;
};


template <typename ElementType>
ChangeTracker<ElementType>::ChangeTracker(const std::vector<ElementType>& value)
  : lastValue(value.begin(), value.end()) {}


template <typename ElementType>
SetChange<ElementType> ChangeTracker<ElementType>::check(const std::vector<ElementType>& value)
{
  SetChange<ElementType> change{{value.begin(), value.end()}};

  std::set_difference(
        change.value.begin(), change.value.end(),
        lastValue.begin(), lastValue.end(),
        std::inserter(change.inserted, change.inserted.begin()));

  std::set_difference(
        lastValue.begin(), lastValue.end(),
        change.value.begin(), change.value.end(),
        std::inserter(change.removed, change.removed.begin()));

  lastValue = change.value;
  return change;
}
} // agent
} // semantic
} // qi

#endif // QI_SEMANTIC_AGENT_SETCHANGE_HPP
