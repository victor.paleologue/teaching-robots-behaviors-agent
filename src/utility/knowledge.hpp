#ifndef SEMANTICAGENT_KNOWLEDGE_HPP
#define SEMANTICAGENT_KNOWLEDGE_HPP

#include <memory>
#include <string>
#include <qi/signal.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/knowledge/util.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace qi
{
namespace knowledge
{
// TODO: move all this to the knowledge project.
bool operator<(const Node& lhs, const Node& rhs);
bool operator<(const ResourceNode& lhs, const ResourceNode& rhs);
bool operator<(const Triple& lhs, const Triple& rhs);

std::ostream& operator<<(std::ostream& out, const ResourceNode& node);
std::ostream& operator<<(std::ostream& out, const Node& node);
std::ostream& operator<<(std::ostream& out, const std::vector<Node>& nodes);
std::ostream& operator<<(std::ostream& out, const Triple& triple);
std::ostream& operator<<(std::ostream& out, const std::vector<Triple>& triples);
std::ostream& operator<<(std::ostream& out, const std::map<std::string, Node>& binding);
std::ostream& operator<<(std::ostream& out, const std::vector<std::map<std::string, Node>>& bindings);
} // knowledge

namespace semantic
{
namespace agent
{
using Triples = std::vector<knowledge::Triple>;

/// Make a datetime node representing the given time.
knowledge::Node makeDateTimeNode(const boost::posix_time::ptime& time);

/// Make a datetime node representing "now".
boost::posix_time::ptime makeNow();
knowledge::Node makeNowNode();

boost::posix_time::ptime timeFromDateTimeString(std::string extendedISOTime);
boost::posix_time::ptime timeFromDateTimeString(const knowledge::DateTimeString& dateTimeString);
boost::posix_time::ptime timeFromDateTimeNode(const knowledge::Node& dateTimeNode);

/// Produces triples that represents a new communication act
/// addessed to self, happening right now.
Triples makeCommunicationActAddressedToSelf(
    const knowledge::ResourceNode& eventResource);

/// Produces triples that represent a new communication act
/// performed by self and responding to the given communication act.
Triples makeRobotResponseToCommunicationAct(
    const knowledge::ResourceNode& inResponseTo,
    const knowledge::ResourceNode& responseResource);

/// The domain for beliefs.
const std::string& beliefsDomain();

/// Returns the first label for the given node.
/// String is empty on error.
// TODO: throw.
Future<knowledge::LocalizedString> firstLabelOf(
    knowledge::EditableKnowledgeBasePtr editableKb,
    const knowledge::ResourceNode& node);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_KNOWLEDGE_HPP
