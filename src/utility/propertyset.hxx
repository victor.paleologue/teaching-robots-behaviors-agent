#pragma once
#ifndef SEMANTICAGENT_PROPERTYSET_HXX
#define SEMANTICAGENT_PROPERTYSET_HXX

// This code is supposed to be included from propertyset.hpp

namespace qi
{
namespace semantic
{
namespace agent
{
} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PROPERTYSET_HXX
