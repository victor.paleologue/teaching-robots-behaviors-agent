#include "semantic_helpers.hpp"
#include <boost/algorithm/string/erase.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qi/semantic/engine/chatbotmemory.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase/conceptset.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/groundedexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticconceptualgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticgenericgrouding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticstatementgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>
#include <qisem/linguisticanalyzer/printer/expressionprinter.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/semanticcontroller/tool/semexpcomparator.hpp>
#include <qisem/semanticdebugger/printer/semexplinestostr.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include <semanticnaoqilinker/qisemantictypeswrapper.hpp>

qiLogCategory("SemanticAgent.SemanticHelpers");

namespace qi
{
namespace semantic
{
namespace agent
{
using namespace qisem;


SemanticMemoryContext::~SemanticMemoryContext() { destroy(); }

// A semantic memory context directly from pointers.
struct DirectMemoryContext: SemanticMemoryContext
{
  std::shared_ptr<Strand> strandPtr;
  std::shared_ptr<linguistics::LinguisticDatabase> lingDbPtr;
  std::shared_ptr<SemanticMemory> semMemPtr;

  DirectMemoryContext(
      std::shared_ptr<Strand> pStrand,
      std::shared_ptr<linguistics::LinguisticDatabase> pLingDb,
      std::shared_ptr<SemanticMemory> pSemMem)
    : strandPtr(std::move(pStrand))
    , lingDbPtr(std::move(pLingDb))
    , semMemPtr(std::move(pSemMem)) {
    qiLogVerbose()
        << "Semantic memory context " << static_cast<void*>(this)
        << " created, using memory " << static_cast<void*>(this->semMemPtr.get());
  }

  ~DirectMemoryContext() override {
    qiLogVerbose() << "Semantic memory context " << static_cast<void*>(this) << " dropped";
  }

  std::shared_ptr<Strand> strand() override {
    return strandPtr;
  }

  std::shared_ptr<const linguistics::LinguisticDatabase> linguisticDatabase() const override {
    return lingDbPtr;
  }

  std::shared_ptr<linguistics::LinguisticDatabase> linguisticDatabase() override {
    return lingDbPtr;
  }

  std::shared_ptr<SemanticMemory> semanticMemory() override {
    return semMemPtr;
  }

  std::shared_ptr<const SemanticMemory> semanticMemory() const override {
    return semMemPtr;
  }
};


std::shared_ptr<SemanticMemoryContext> makeSemanticMemoryContext(
    std::shared_ptr<Strand> strandPtr,
    std::shared_ptr<linguistics::LinguisticDatabase> lingDbPtr,
    std::shared_ptr<SemanticMemory> semMemPtr)
{
  return std::make_shared<DirectMemoryContext>(
        std::move(strandPtr), std::move(lingDbPtr), std::move(semMemPtr));
}


std::shared_ptr<SemanticMemoryContext> makeSemanticMemoryContext()
{
  return std::make_shared<DirectMemoryContext>(
        std::make_shared<Strand>(),
        semanticNaoqiLinker::makeLinguisticDatabase(),
        std::make_unique<qisem::SemanticMemory>());
}


// A semantic memory context based on a chatbot memory.
struct ChatbotMemoryContext: SemanticMemoryContext
{
  const std::shared_ptr<ChatbotMemory> chatbotMemory;
  const std::shared_ptr<linguistics::LinguisticDatabase> lingDb;

  ChatbotMemoryContext(
      std::shared_ptr<ChatbotMemory> cbm,
      std::shared_ptr<linguistics::LinguisticDatabase> linguisticDb)
    : chatbotMemory(std::move(cbm))
    , lingDb(linguisticDb)
  {}

  std::shared_ptr<Strand> strand() override {
    return chatbotMemory->strand;
  }

  std::shared_ptr<const linguistics::LinguisticDatabase> linguisticDatabase() const override {
    return lingDb;
  }

  std::shared_ptr<linguistics::LinguisticDatabase> linguisticDatabase() override {
    return lingDb;
  }

  std::shared_ptr<const SemanticMemory> semanticMemory() const override {
    return chatbotMemory->semMemory;
  }

  std::shared_ptr<SemanticMemory> semanticMemory() override {
    return chatbotMemory->semMemory;
  }
};


std::shared_ptr<SemanticMemoryContext> makeSemanticMemoryContext(
    std::shared_ptr<ChatbotMemory> chatbotMemory,
    std::shared_ptr<linguistics::LinguisticDatabase> linguisticDatabase)
{
  return std::make_shared<ChatbotMemoryContext>(
        std::move(chatbotMemory), std::move(linguisticDatabase));
}


qisem::SemanticLanguageEnum localeToSemanticEnum(locale::Locale locale)
{
  return qisem::QiSemanticTypesWrapper::qiLanguageToLanguage(locale.language);
}


static qisem::TextProcessingContext textProcessingContextForDirection(
    SemanticDirection direction,
    const qi::locale::Locale& locale)
{
  auto semanticLanguage = QiSemanticTypesWrapper::qiLanguageToLanguage(locale.language);
  switch (direction)
  {
  case SemanticDirection::EngagedUserToRobot:
    return ::qisem::TextProcessingContext(
          SemanticAgentGrounding::currentUser, // if we want to store it per-user, customize this ID.
          SemanticAgentGrounding::me,
          semanticLanguage);

  case SemanticDirection::RobotToEngagedUser:
    return ::qisem::TextProcessingContext(
          SemanticAgentGrounding::me,
          SemanticAgentGrounding::currentUser,
          semanticLanguage);
  }
  throw std::invalid_argument("unsupported enum provided");
}

std::shared_ptr<SemanticExpression> phraseToSemanticExpression(
    const SemanticMemoryContext& semanticMemoryContext,
    const PhraseAndLocale& phrase,
    SemanticDirection direction)
{
  try
  {
    // TODO: make sure WHO SAID IT.
    auto textContext = textProcessingContextForDirection(direction, phrase.locale);

    // KLUDGE: ASR is usually unreliable regarding to punctuation, let's ignore it.
    auto curatedPhrase = phrase.text;
    boost::algorithm::erase_all(curatedPhrase, ",");
    boost::algorithm::erase_all(curatedPhrase, ".");

    auto expression =
        converter::textToContextualSemExp( // TODO: allow different value than unknown
          curatedPhrase, textContext, SemanticSourceEnum::ASR,
          *semanticMemoryContext.linguisticDatabase());

    // improve text with robot's knowledge
    memoryOperation::mergeWithContext(
          expression,
          *semanticMemoryContext.semanticMemory(),
          *semanticMemoryContext.linguisticDatabase());

    qiLogDebug()
        << "Extracted expression from \"" << phrase.text
        << "\": " << std::endl << *expression;

    return expression.getSharedPtr();
  }
  catch (const std::exception& e)
  {
    qiLogDebug() << "Failed to perform semantic extraction on \""
                 << phrase.text << "\": " << e.what();
    return {};
  }
}


PhraseAndLocale phraseFromSemanticExpression(
    const SemanticMemoryContext& semanticMemoryContext,
    const SemanticExpression& semanticExpression,
    const locale::Locale& locale,
    SemanticDirection direction)
{
  // TODO: make sure WHO SAID IT.
  auto textContext = textProcessingContextForDirection(direction, locale);

  std::string text;
  converter::semExpToText(
        text, semanticExpression.clone(), textContext, false,
        *semanticMemoryContext.semanticMemory(),
        *semanticMemoryContext.linguisticDatabase(), nullptr);
  return {text, locale};
}


std::ostream& operator<<(std::ostream& out, const qisem::SemanticExpression& expression)
{
  std::string prettyPrintedStr;
  std::list<ALSemLineToPrint> semExpStrs;
  printer::prettyPrintSemExp(semExpStrs, expression);
  const auto& consolePrinter =
      SemExpLinesToStr::getInstance(PrintSemExpDiffsOutPutFormat::CONSOLE);
  consolePrinter.printLines(prettyPrintedStr, semExpStrs);
  out << prettyPrintedStr;
  return out;
}


std::unique_ptr<GroundedExpression> statementExpressionFromEnglishVerbLemma(
    const std::string& verb)
{
  auto statementGrounding = mystd::make_unique<SemanticStatementGrounding>();
  statementGrounding->word = SemanticWord();
  statementGrounding->word.language = SemanticLanguageEnum::ENGLISH;
  statementGrounding->word.lemma = verb;
  statementGrounding->word.partOfSpeech = PartOfSpeech::VERB;
  statementGrounding->verbTense = SemanticVerbTense::UNKNOWN;
  return mystd::make_unique<GroundedExpression>(std::move(statementGrounding));
}


bool matchVerbalGroup(
    const qisem::SemanticExpression& semanticExpression,
    const qisem::SemanticExpression& verbalGroupTemplate,
    const qisem::SemanticMemory& semanticMemory,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase)
{
  auto semanticGroundedExpressionPtr = semanticExpression.getGrdExpPtr();
  if (!semanticGroundedExpressionPtr)
    return false;
  auto statementPtr = semanticGroundedExpressionPtr->grounding().getStatementGroundingPtr();
  if (!statementPtr)
    return false;

  auto templateExpressionPtr = verbalGroupTemplate.getGrdExpPtr();
  if (!templateExpressionPtr)
    return false;
  auto templateStatementPtr = templateExpressionPtr->grounding().getStatementGroundingPtr();
  if (!templateStatementPtr)
    return false;

  if (templateStatementPtr->word.lemma != statementPtr->word.lemma
      || templateStatementPtr->word.partOfSpeech != statementPtr->word.partOfSpeech)
    return false;

  for (auto& child: templateExpressionPtr->children)
  {
    auto matchingChildIt = semanticGroundedExpressionPtr->children.find(child.first);
    if (matchingChildIt == semanticGroundedExpressionPtr->children.end())
      return false;
    if (!SemExpComparator::semExpsAreEqual(
          *matchingChildIt->second, *child.second, semanticMemory, linguisticDatabase))
      return false;
  }

  return true;
}


const SemanticStatementGrounding* asActionDeclaration(const SemanticExpression& exp)
{
  // it must be a semantic expression containing an unrecognized grounding (not a list, a condition...)
  auto groundedExpressionPtr = exp.getGrdExpPtr_SkipWrapperPtrs();
  if (!groundedExpressionPtr)
    return nullptr;
  auto& groundedExpression = *groundedExpressionPtr;

  // grounded expression must not have a subject
  if (groundedExpression.children.count(GrammaticalType::SUBJECT))
    return nullptr;

  // grounded expression must be a verb-centered statement
  auto statementPtr = groundedExpression->getStatementGroundingPtr();
  if (!statementPtr)
    return nullptr;
  auto& statement = *statementPtr;

  // subject statement must be an infinitive or a present participle (interpreted similarly!)
  // (it must not be a request)
  if (!(statement.requestTypes.empty() && statement.isAtInfinitive()))
    return nullptr;

  return statementPtr;
}

const SemanticGenericGrounding* asCoreference(const SemanticExpression& exp)
{
  // it must be a semantic expression containing an unrecognized grounding (not a list, a condition...)
  auto groundedExpressionPtr = exp.getGrdExpPtr_SkipWrapperPtrs();
  if (!groundedExpressionPtr)
    return nullptr;
  auto& groundedExpression = *groundedExpressionPtr;

  // grounded expression must be a verb-centered statement
  auto genericGroundingPtr = groundedExpression->getGenericGroundingPtr();
  if (!genericGroundingPtr)
    return nullptr;
  auto& genericGrounding = *genericGroundingPtr;

  if (!genericGrounding.coreference)
    return nullptr;

  return genericGroundingPtr;
}

const ListExpression* asActionDeclarationList(const SemanticExpression& exp)
{
  auto listPtr = exp.getListExpPtr_SkipWrapperPtrs();
  if (!listPtr)
    return nullptr;

  auto& list = *listPtr;
  if (list.listType == ListExpressionType::OR)
    return nullptr; // OR lists are not supported yet.

  size_t lastIndex = list.elts.size() - 1;
  size_t index = 0;
  for (const auto& subExp: list.elts)
  {
    bool isFirstOrLast = index == 0 || index == lastIndex;
    if (!(asActionDeclaration(*subExp) || (isFirstOrLast && asCoreference(*subExp))))
      return nullptr;
  }

  return listPtr;
}


const GroundedExpression* asEquivalenceStatementExpression(const SemanticExpression& exp)
{
  // it must be a semantic expression containing an unrecognized grounding (not a list, a condition...)
  auto groundedExpressionPtr = exp.getGrdExpPtr_SkipWrapperPtrs();
  if (!groundedExpressionPtr)
    return nullptr;

  auto& groundedExpression = *groundedExpressionPtr;

  // grounded expression must be a verb-centered statement
  auto statementPtr = groundedExpression->getStatementGroundingPtr();
  if (!statementPtr)
    return nullptr;
  auto& statement = *statementPtr;

  // statement must be an affirmation (not a question), in the present
  if (!statement.requestTypes.empty()
      || statement.verbTense != SemanticVerbTense::PRESENT)
    return nullptr;

  // statement must involve the equality concept
  if (statement.concepts.find(ConceptSet::conceptVerbEquality) == statement.concepts.end())
    return nullptr;

  return groundedExpressionPtr;
}


std::unique_ptr<SemanticExpression> makeAgreementExpression()
{
  auto agreementGrouding = mystd::make_unique<SemanticConceptualGrounding>();
  agreementGrouding->concepts.emplace(ConceptSet::conceptAccordanceAgreementOk, 5);
  return mystd::make_unique<GroundedExpression>(std::move(agreementGrouding));
}


std::unique_ptr<SemanticExpression> makeAgreementExpression(std::unique_ptr<SemanticExpression> exp)
{
  auto feedbackGrounding = mystd::make_unique<SemanticConceptualGrounding>();
  feedbackGrounding->concepts.emplace(ConceptSet::conceptAccordanceAgreementOk, 5);
  auto feedback = mystd::make_unique<GroundedExpression>(std::move(feedbackGrounding));
  return mystd::make_unique<FeedbackExpression>(std::move(feedback), std::move(exp));
}


bool isClosure(const SemanticExpression& exp)
{
  return isEnumerationClosure(exp); // TODO: || is "ok"
}


bool isEnumerationClosure(const SemanticExpression& exp)
{
  // ... _is_ ...
  if (auto equivalenceGrounded = asEquivalenceStatementExpression(exp))
  {
    // _that_ is ...
    auto subjectIt = equivalenceGrounded->children.find(GrammaticalType::SUBJECT);
    if (subjectIt == equivalenceGrounded->children.end())
      return false;
    if (!asCoreference(*subjectIt->second))
      return false;

    // that is _all_
    auto objectIt = equivalenceGrounded->children.find(GrammaticalType::OBJECT);
    if (objectIt == equivalenceGrounded->children.end())
      return false;
    if (auto objectGrounded = objectIt->second->getGrdExpPtr())
    {
      if (auto objectGrounding = objectGrounded->grounding().getGenericGroundingPtr())
      {
        if (objectGrounding->quantity.type == SemanticQuantityType::EVERYTHING)
          return true;
      }
    }
  }
  return false;
}


std::unique_ptr<GroundedExpression> makeTextExpression(const std::string& text)
{
  if (!text.empty())
  {
    return std::make_unique<GroundedExpression>(
        std::make_unique<SemanticTextGrounding>(text));
  }
  else
  {
    return {};
  }
}

} // agent
} // semantic
} // qi
