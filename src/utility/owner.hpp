#ifndef SEMANTICAGENT_OWNER_HPP
#define SEMANTICAGENT_OWNER_HPP

#include <functional>
#include <qi/future.hpp>
#include <qi/signal.hpp>
#include <src/agent/agent_p.hpp>
#include <qi/knowledge/util.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
/**
 * An owner which release function can be customized at construction.
 */
struct CustomOwner
{
  using ReleaseFunction = std::function<Future<void>()>;
  using ReleaseFunctionSync = std::function<void()>;

  CustomOwner(ReleaseFunction releaseFunction);
  CustomOwner(ReleaseFunctionSync releaseFunction);

  ~CustomOwner();
  Future<void> release();
  Future<void> waitForReleased();
  Signal<> released;

private:
  struct Private;
  std::unique_ptr<Private> _p;
};
} // agent
} // semantic
} // qi

QI_REGISTER_IMPLEMENTATION_H(
    qi::semantic::agent::Owner,
    qi::semantic::agent::CustomOwner);

#endif // SEMANTICAGENT_OWNER_HPP
