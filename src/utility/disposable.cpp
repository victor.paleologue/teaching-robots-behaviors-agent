#include "disposable.hpp"

qiLogCategory("SemanticAgent.Disposable");

namespace qi
{
namespace semantic
{
namespace agent
{
Disposable::~Disposable() = default;

struct FunctionDisposable: Disposable
{
  Strand strand;
  DisposalFunction disposalFunction;
  Future<void> disposing;

  FunctionDisposable(DisposalFunction disposal)
  {
    disposalFunction = std::move(disposal);
  }

  ~FunctionDisposable()
  {
    strand.join();

    if (!disposalFunction) // only happens if this was moved from.
      return;

    if (!disposing.isValid())
      disposing = disposalFunction();

    disposing.then([](Future<void> f)
    {
      if (f.hasError())
      {
        qiLogError()
            << "Error in disposal " << f.error();
      }
    });
  }

  Future<void> dispose() override
  {
    return doDispose();
  }

  Future<void> doDispose()
  {
    return strand.unwrappedSchedulerFor([=]
    {
      if (disposing.isValid())
        throw std::runtime_error("disposal was already requested");
      disposing = disposalFunction();
      return disposing;
    })();
  }
};


std::unique_ptr<Disposable> makeDisposable(DisposalFunction disposal)
{
  return std::make_unique<FunctionDisposable>(std::move(disposal));
}


std::unique_ptr<Disposable> makeDisposable(DisposalFunctionSync disposal)
{
  return std::make_unique<FunctionDisposable>(
        [disposal = std::move(disposal)]
  {
    disposal();
    return Future<void>{nullptr};
  });
}


std::unique_ptr<Disposable> makeDisposable(Future<void> futureToCancel)
{
  return std::make_unique<FunctionDisposable>(
        [futureToCancel = std::move(futureToCancel)]() mutable
  {
    futureToCancel.cancel();
    return futureToCancel;
  });
}


struct Disposables::Private
{
  /// The disposables accumulated.
  std::vector<std::unique_ptr<Disposable>> disposables;

  /// The underlying disposable implementation.
  std::unique_ptr<Disposable> disposable;
};


Disposables::Disposables()
  : _p(std::make_unique<Private>())
{
  _p->disposable = makeDisposable(DisposalFunction([this]
  {
    Future<void> disposing(nullptr);
    auto i = _p->disposables.size();
    auto errors = std::make_shared<std::stringstream>();
    auto reportIfError = [=](size_t index, Future<void> f)
    {
      if (f.hasError())
        *errors << "Error in disposable #" << index << ": " << f.error() << std::endl;
    };

    for (auto it = _p->disposables.rbegin(); it != _p->disposables.rend(); ++it)
    {
      qiLogDebug() << "Preparing disposal #" << i;
      // KLUDGE: future continuations must be copyable, unique pointers cannot be captured.
      std::shared_ptr<Disposable> disposable = std::move(*it);
      disposing = disposing.then([=](Future<void> f) mutable
      {
        reportIfError(i+1, f);
        qiLogDebug() << "Disposing #" << i;
        auto disposing = disposable->dispose();
        disposing.then([disposable](Future<void>){});
        return disposing;
      }).unwrap();
      --i;
    }

    disposing = disposing.then([=](Future<void> f)
    {
      reportIfError(i+1, f);
      auto errorMessage = errors->str();
      if (!errorMessage.empty())
        throw std::runtime_error(errorMessage);
    });

    return disposing;
  }));
}

Disposables::~Disposables() = default;

Future<void> Disposables::dispose()
{
  return _p->disposable->dispose();
}

void Disposables::add(std::unique_ptr<Disposable> disposable)
{
  _p->disposables.push_back(std::move(disposable));
}

} // agent
} // semantic
} // qi
