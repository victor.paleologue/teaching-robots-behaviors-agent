#pragma once
#ifndef SEMANTICAGENT_PROPERTYSET_HPP
#define SEMANTICAGENT_PROPERTYSET_HPP

#include <set>
#include <boost/signals2.hpp>
#include "property.hpp"
#include "setchange.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
/// A set that we can read.
template <typename ElementType>
using ReadableSet = Readable<std::set<ElementType>>;

/// A set which changes can be observed.
template <typename ElementType>
using ObservableSet = boost::signals2::signal<void(const SetChange<ElementType>&)>;

/// A set that can be changed.
template <typename ElementType>
struct MutableSet: Mutable<std::set<ElementType>>
{
  /// Inserts an element.
  /// If the element was already present, does nothing.
  virtual void insert(ElementType&& element) = 0;

  /// Removes an element.
  /// If the element was already absent, does nothing.
  virtual void remove(const ElementType& element) = 0;

  /// Clears the set.
  virtual void clear() = 0;

  /// Applies the delta, as represented in changes.
  /// If an inserted element was already present, it is not inserted.
  /// If a removed element was already absent, it is not removed.
  /// If an element is both inserted and removed, it ends up removed.
  virtual void applyDelta(
      const std::set<ElementType>& toInsert,
      const std::set<ElementType>& toRemove) = 0;
};


/// A property is readable and observable.
template <typename ElementType>
struct PropertySet: ReadableSet<ElementType>, ObservableSet<ElementType>{};

/// A mutable property is readable, observable and mutable.
template <typename ElementType>
struct MutablePropertySet: PropertySet<ElementType>, MutableSet<ElementType>{};

/// An implementation storing the set.
template <typename ElementType>
class StoredPropertySet: public MutablePropertySet<ElementType>
{
public:
  const std::set<ElementType>& get() const override;
  void set(std::set<ElementType> value) override;
  void insert(ElementType&& element) override;
  void remove(const ElementType& element) override;
  void clear() override;
  void applyDelta(
      const std::set<ElementType>& toInsert,
      const std::set<ElementType>& toRemove) override;

private:
  std::set<ElementType> _storage;
};


//======================================================================
// Implementation
//======================================================================
template<typename ElementType>
const std::set<ElementType>& StoredPropertySet<ElementType>::get() const
{
  return _storage;
}

template<typename ElementType>
void StoredPropertySet<ElementType>::set(std::set<ElementType> value)
{
  if(_storage == value)
    return;

  std::swap(_storage, value);
  auto& current = _storage;
  auto& previous = value;

  std::set<ElementType> added;
  for (const auto& elt: current)
  {
    if (!previous.count(elt))
      added.insert(elt);
  }

  std::set<ElementType> removed;
  for (const auto& elt: previous)
  {
    if (!current.count(elt))
      removed.insert(elt);
  }

  // Emit signal: "this" is a signal.
  (*this)(SetChange<ElementType>{current, std::move(added), std::move(removed)});
}


template<typename ElementType>
void StoredPropertySet<ElementType>::insert(ElementType&& element)
{
  auto insertion = _storage.insert(std::forward<ElementType>(element));
  if (insertion.second)
  {
    std::set<ElementType> added;
    std::set<ElementType> removed;
    added.insert(*insertion.first);
    // Emit signal: "this" is a signal.
    (*this)(SetChange<ElementType>{_storage, std::move(added), std::move(removed)});
  }
}


template<typename ElementType>
void StoredPropertySet<ElementType>::remove(const ElementType& element)
{
  if (_storage.erase(element))
  {
    std::set<ElementType> added;
    std::set<ElementType> removed;
    removed.insert(element);
    (*this)(SetChange<ElementType>{_storage, std::move(added), std::move(removed)});
  }
}


template<typename ElementType>
void StoredPropertySet<ElementType>::clear()
{
  if (!_storage.empty())
  {
    std::set<ElementType> added;
    std::set<ElementType> removed;
    std::set<ElementType> tmp;
    std::swap(tmp, _storage);
    for (const auto& elt: tmp)
      removed.insert(elt);
    (*this)(SetChange<ElementType>{_storage, std::move(added), std::move(removed)});
  }
}


template<typename ElementType>
void StoredPropertySet<ElementType>::applyDelta(
    const std::set<ElementType>& toInsert,
    const std::set<ElementType>& toRemove)
{
  std::set<ElementType> inserted;
  std::set<ElementType> removed;

  for (const auto& elt: toInsert)
  {
    bool conflicting = false;
    for (const auto& rmElt: toRemove)
    {
      if (elt == rmElt)
      {
        conflicting = true;
        break;
      }
    }

    if (conflicting)
      continue;

    if (_storage.insert(elt).second)
      inserted.insert(elt);
  }

  for (const auto& elt: toRemove)
  {
    if (_storage.erase(elt))
      removed.insert(elt);
  }

  if (inserted.empty() && removed.empty())
    return; // do not report if no actual change was made

  (*this)(SetChange<ElementType>{_storage, std::move(inserted), std::move(removed)});
}

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PROPERTYSET_HPP
