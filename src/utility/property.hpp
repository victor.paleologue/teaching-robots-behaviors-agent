#pragma once
#ifndef SEMANTICAGENT_PROPERTY_HPP
#define SEMANTICAGENT_PROPERTY_HPP

#include <boost/signals2.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
/// Something we can get.
template <typename Type>
struct Readable
{
  virtual const Type& get() const = 0;
};

/// Something that can be observed.
template <typename Type>
struct Observable
    : public boost::signals2::signal<void(const Type&)>
{};

/// Something that can be changed.
template <typename Type>
struct Mutable
{
  /// Sets a new value.
  virtual void set(Type value) = 0;
};

/// A property is readable and observable.
// KLUDGE: it is named "Prop" to not confuse it with qi::Property.
template <typename Type>
struct Prop: Readable<Type>, Observable<Type>{};

/// A mutable property is readable, observable and mutable.
template <typename Type>
struct MutableProp: Prop<Type>, Mutable<Type>{};

/// An implementation storing the set.
template <typename Type>
class StoredProp: public MutableProp<Type>
{
public:
  const Type& get() const override;
  void set(Type value) override;

private:
  Type _storage;
};

//===========================================================================
// Implementation
//===========================================================================
template<typename Type>
const Type& StoredProp<Type>::get() const
{
  return _storage;
}

template<typename Type>
void StoredProp<Type>::set(Type value)
{
  if(_storage == value)
    return;

  std::swap(_storage, value);

  // Emit signal: "this" is a signal.
  (*this)(_storage);
}

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PROPERTY_HPP
