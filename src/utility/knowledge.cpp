#include "knowledge.hpp"
#include <cmath>
#include <map>
#include <boost/date_time/iso_format.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_duration.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <qi/jsoncodec.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <qi/teaching/ontology.hpp>

qiLogCategory("SemanticAgent.Knowledge");

namespace qi
{
namespace knowledge
{
bool operator<(const Node& lhs, const Node& rhs)
{ // KLUDGE: comparing anyvalues does not seem to work, here is a costly trick
  return encodeJSON(lhs.value) < encodeJSON(rhs.value);
}

bool operator<(const ResourceNode& lhs, const ResourceNode& rhs)
{
  return lhs.url < rhs.url;
}

bool operator<(const Triple& lhs, const Triple& rhs)
{
  return lhs.object < rhs.object
      || lhs.predicate < rhs.predicate
      || lhs.subject < rhs.subject;
}

class NodePrinter: public NodeVisitor
{
  std::ostream& out;

public:
  NodePrinter(std::ostream& out) : out(out) {}


  virtual void accept(const ResourceNode& node)
  {
    out << "<" << node.url << ">";
  }


  virtual void accept(const ::qi::int32_t& literal)
  {
    out << literal;
  }


  virtual void accept(const ::qi::int64_t& literal)
  {
    out << literal;
  }


  virtual void accept(const float& literal)
  {
    out << literal;
  }


  virtual void accept(const double& literal)
  {
    out << literal;
  }


  virtual void accept(const std::string& literal)
  {
    out << "\"" << literal << "\"";
  }


  virtual void accept(const LocalizedString& literal)
  {
    out << "\"" << literal.string << "\"@" << locale::localeToStr(literal.locale);
  }


  virtual void accept(const DateTimeString& literal)
  {
    out << "📅⌚" << literal.dateTimeString;
  }


  virtual void accept(const DateString& literal)
  {
    out << "📅" << literal.dateString;
  }


  virtual void accept(const TimeString& literal)
  {
    out << "⌚" << literal.timeString;
  }
};


std::ostream& operator<<(std::ostream& out, const ResourceNode& node)
{
  NodePrinter printer(out);
  printer.accept(node);
  return out;
}


std::ostream& operator<<(std::ostream& out, const Node& node)
{
  if (!node.value.isValid())
  {
    out << "∅";
  }
  else
  {
    NodePrinter printer(out);
    printer.visit(node);
  }
  return out;
}


std::ostream& operator<<(std::ostream& out, const std::vector<Node>& nodes)
{
  bool first = true;
  out << "[";
  for (const auto& node: nodes)
  {
    if (first)
      first = false;
    else
      out << ", ";
    out << node;
  }
  out << "]";
  return out;
}


std::ostream& operator<<(std::ostream& out, const Triple& triple)
{
  out << triple.subject << " " << triple.predicate << " " << triple.object;
  return out;
}


std::ostream& operator<<(std::ostream& out, const std::vector<Triple>& triples)
{
  bool first = true;
  for (const auto& triple: triples)
  {
    if (first)
      first = false;
    else
      out << std::endl;
    out << triple;
  }
  return out;
}


std::ostream& operator<<(std::ostream& out, const std::map<std::string, Node>& binding)
{
  bool first = true;
  out << "{";
  for (const auto& pair: binding)
  {
    if (first)
      first = false;
    else
      out << ", ";
    out << pair.first << ": " << pair.second;
  }
  out << "}";
  return out;
}


std::ostream& operator<<(std::ostream& out, const std::vector<std::map<std::string, Node>>& bindings)
{
  bool first = true;
  for (const auto& binding: bindings)
  {
    if (first)
      first = false;
    else
      out << std::endl;
    out << binding;
  }
  return out;
}

} // knowledge


using namespace knowledge;


namespace semantic
{
namespace agent
{
Node makeDateTimeNode(const boost::posix_time::ptime& time)
{
  // Knowledge resolution is limited to the milliseconds.
  // Check that the time provided is not more precise.
  static auto nofExtraDigits = boost::posix_time::time_duration::num_fractional_digits() - 3;
  static auto resolutionCeil = std::lround(std::pow(10, nofExtraDigits));
  auto extraFractionalSeconds = time.time_of_day().fractional_seconds() % resolutionCeil;
  if (extraFractionalSeconds > 0)
  {
    std::stringstream ss;
    ss << "provided date time \"" << boost::posix_time::to_iso_extended_string(time)
       << "\" is too precise, millisecond is the highest precision supported.";
    throw std::runtime_error(ss.str());
  }

  // Converting the time to an ISO string with a millisecond precision.
  // First we get it with the microsecond precision.
  auto timeString = boost::posix_time::to_iso_extended_string(time);

  // Then we ignore microseconds and add the "Z" for UTC.
  timeString = timeString.substr(0, timeString.size() - 3) + 'Z';
  return makeStringNode(timeString);
}

boost::posix_time::ptime makeNow()
{
  auto now = boost::posix_time::microsec_clock::universal_time();
  auto tod = now.time_of_day();
  tod = boost::posix_time::milliseconds{tod.total_milliseconds()};
  return boost::posix_time::ptime{now.date(), tod};
}

Node makeNowNode()
{
  return makeDateTimeNode(makeNow());
}


Triples makeCommunicationActAddressedToSelf(
    const ResourceNode& eventResource)
{
  auto eventOccurredNow =
      makeTripleFromNodes(
        eventResource, teaching::occurredAtResource(), makeNowNode());

  auto eventIsAnEvent =
      makeTripleFromNodes(
        eventResource, teaching::rdfTypeResource(), teaching::eventTypeNode());

  auto eventIsACommunicationAct =
      makeTripleFromNodes(
        eventResource, teaching::rdfTypeResource(), teaching::communicationActTypeNode());

  auto communicationActIsAddressedToSelf =
      makeTripleFromNodes(
        eventResource, teaching::communicationAddresseeResource(), teaching::agentSelfNode());

  return Triples{
        std::move(eventOccurredNow),
        std::move(eventIsAnEvent),
        std::move(eventIsACommunicationAct),
        std::move(communicationActIsAddressedToSelf)
  };
}


Triples makeRobotResponseToCommunicationAct(
    const ResourceNode& inResponseTo,
    const ResourceNode& responseResource)
{
  if (!isNodeValid(inResponseTo))
    throw std::runtime_error("invalid communication act to respond to");
  if (isNodeEmpty(inResponseTo))
    throw std::runtime_error("empty communication act to respond to");

  auto eventOccurredNow =
      makeTripleFromNodes(
        responseResource, teaching::occurredAtResource(), makeNowNode());

  auto eventIsAnEvent =
      makeTripleFromNodes(
        responseResource, teaching::rdfTypeResource(), teaching::eventTypeNode());

  auto eventIsACommunicationAct =
      makeTripleFromNodes(
        responseResource, teaching::rdfTypeResource(), teaching::communicationActTypeNode());

  auto communicationActIsPerformedBySelf =
      makeTripleFromNodes(
        responseResource, teaching::performedByResource(), teaching::agentSelfNode());

  auto communicationActIsAResponseTo =
      makeTripleFromNodes(
        responseResource, teaching::respondsToResource(),
        makeNodeFromResourceNode(inResponseTo));

  return {eventOccurredNow, communicationActIsPerformedBySelf,
          communicationActIsAResponseTo, eventIsAnEvent, eventIsACommunicationAct};
}


const std::string& beliefsDomain()
{
  static const std::string r = "beliefs";
  return r;
}


boost::posix_time::ptime timeFromDateTimeString(std::string extendedISOTime)
{
  if (extendedISOTime.empty())
    throw std::runtime_error("date time string is empty");
  if (extendedISOTime[extendedISOTime.size() - 1] != 'Z')
    throw std::runtime_error("date time string is not UTC");

  extendedISOTime = extendedISOTime.substr(0, extendedISOTime.size() - 1) + "000"; // removes the trailing 'Z', adds extra decimals

  // TODO: with boost 1.62, use from_iso_extended_string.
  auto condensedISOTime = extendedISOTime;
  auto removeIt = std::remove_if(
        condensedISOTime.begin(), condensedISOTime.end(),
        [](const char& c){ return c == '-' || c == ':'  || c == '.'; });
  condensedISOTime.erase(removeIt, condensedISOTime.end());

  return boost::posix_time::from_iso_string(condensedISOTime.substr(0, condensedISOTime.size() - 1));
}


boost::posix_time::ptime timeFromDateTimeString(const DateTimeString& dateTimeString)
{
  return timeFromDateTimeString(dateTimeString.dateTimeString);
}


boost::posix_time::ptime timeFromDateTimeNode(const Node& dateTimeNode)
{
  try
  {
    return timeFromDateTimeString(dateTimeNode.value.to<DateTimeString>().dateTimeString);
  }
  catch (const std::exception& e)
  {
    // KLUDGE: libqi-java cannot express DateTimeString, so we'll just read strings
    return timeFromDateTimeString(dateTimeNode.value.to<std::string>());
  }
}


Future<LocalizedString> firstLabelOf(
    EditableKnowledgeBasePtr editableKb,
    const ResourceNode& node)
{
  std::string sparqlQuery =
      "SELECT ?label\n"
      "WHERE {\n"
      "  <" + node.url + "> <" + teaching::rdfsLabel() + "> ?label .\n"
      "}";

  // might require KnowledgeBasePtr instead of KnwoledgeGraphPtr
  auto querying =
      editableKb.async<std::vector<std::map<std::string, Node>>>(
        "querySPARQLBindings", sparqlQuery);
  return querying.andThen(
        [=](const std::vector<std::map<std::string, Node>>& results)
              -> LocalizedString
  {
    if (results.empty())
      return {};
    try
    {
      return results.front().at("label").value.to<LocalizedString>();
    }
    catch (const std::out_of_range& e)
    {
      qiLogError() << "Variable mismatch between sparql query and processing its result";
      QI_ASSERT(false && "variable mismatch between sparql query and processing its result");
      throw;
    }
    catch (const std::exception& e)
    {
      qiLogError() << "Invalid label \"" << qi::encodeJSON(results.front().at("label"));
      return {};
    }
  });
}

} // agent
} // semantic
} // qi
