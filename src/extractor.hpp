#ifndef SEMANTICAGENT_EXTRACTOR_HPP
#define SEMANTICAGENT_EXTRACTOR_HPP

#include <qi/future.hpp>
#include <qi/property.hpp>
#include <qi/session.hpp>
#include <qi/trackable.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/semantic/agent/agent.hpp> // for SpeechExtractor

namespace qi
{
class Strand;

namespace semantic
{
namespace agent
{
/// Translates NAOqi inputs in knowledge data.
/// @todo Do not use a shared semantic memory. Instead, identify the data sources.
class Extractor
{
public:
  /// @param strand The strand to schedule tasks.
  /// @param session A connected session.
  /// @param linguisticDatabase A linguistic database for text synthesis.
  /// @param memory A memory to bind NAOqi events directly to.
  Extractor(
      std::shared_ptr<Strand> strand,
      const SessionPtr& session,
      Object<knowledge::EditableKnowledgeBase> knowledgeBase,
      SpeechExtractorPtr speechExtractor);

  ~Extractor();

  /// Runs speech extractor. Cancel this future to interrupt.
  /// @todo Hearing words should be a passive feature.
  Future<void> extractSpeech();

  /// Retrieve the read-only speech source managed by this extractor.
  Future<SpeechSourcePtr> speechSource();

  /// Get a future for the setup of the speech extraction.
  Future<void> waitForNextStartOfSpeechExtraction();

  /// The confidence threshold used to filter out bad ASR results.
  Property<float>& confidenceThreshold;

private:
  struct Private;
  std::unique_ptr<Private> _p;

public:
  Property<bool>& hearing;
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_EXTRACTOR_HPP
