#include "language.hpp"
#include <qi/locale/util.hpp>
#include <audiolanguage/languagecodes.hpp>
#include <qisem/semanticmodel/enum/semanticlanguagetype.hpp>
#include "utility/semantic_helpers.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{

// TODO: compute the language in a different way, to avoid this costly call?
qisem::SemanticLanguageEnum currentLanguageToSemanticEnum(SessionPtr session)
{
  std::string languageName = session->service("ALDialog").value().call<std::string>("getLanguage");
  qisem::SemanticLanguageEnum language = qisem::SemanticLanguageEnum::UNKNOWN;
  if (!qisem::semanticLanguageEnum_fromTtsStrIfExist(language, languageName)
      && language == qisem::SemanticLanguageEnum::UNKNOWN)
  {
    std::stringstream ss;
    ss << "unrecognized current language: " << languageName;
    throw std::runtime_error(ss.str());
  }

  return language;
}


locale::Locale currentLanguageToLocale(SessionPtr session)
{
  std::string languageName = session->service("ALDialog").value().call<std::string>("getLanguage");
  auto languageCode = AL::languageFromLegacyName(languageName);
  return locale::localeFromStr(languageCode);
}


struct CurrentLocaleProperty::Private
{
  AnyObject tts;
  Future<SignalLink> settingUp;
};


CurrentLocaleProperty::CurrentLocaleProperty(const SessionPtr& session)
  : _p(new Private)
{
  locale.set(locale::Locale{locale::Language::English, locale::Region::United_States});
  auto setLocaleFromLanguageName = [=](const std::string& languageName)
  {
    auto languageCode = AL::languageFromLegacyName(languageName);
    locale.set(locale::localeFromStr(languageCode)).value();
  };

  _p->settingUp = session->service("ALTextToSpeech").async().andThen([=](AnyObject tts)
  {
    _p->tts = tts;
    return _p->tts.connect(
          "languageTTS",
          boost::function<void(const std::string&)>(setLocaleFromLanguageName)).async();
  }).unwrap()

  .andThen([=](SignalLink link)
  {
    return _p->tts.async<std::string>("getLanguage")
        .andThen(setLocaleFromLanguageName)
        .andThen([=](void*){ return link; });
  }).unwrap();
}


CurrentLocaleProperty::~CurrentLocaleProperty()
{
  _p->settingUp.cancel();
  auto unsubscribing = _p->settingUp.andThen([this](SignalLink link)
  {
    QI_ASSERT(_p->tts);
    return _p->tts.disconnect(link).async();
  }).unwrap();
  unsubscribing.wait();
}

Future<locale::Locale> CurrentLocaleProperty::value()
{
  return _p->settingUp.andThen([this](SignalLink)
  {
    return locale.get().async();
  }).unwrap();
}
} // agent
} // semantic
} // qi
