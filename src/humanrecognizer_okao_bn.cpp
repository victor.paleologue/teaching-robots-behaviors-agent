#include <ctime>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <thread>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/bimap.hpp>
#include <boost/filesystem.hpp>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include <okaowrapper/face.hpp>
#include <okaowrapper/okaofacedetection.hpp>
#include <okaowrapper/okaofacerecognition.hpp>
#include <okaowrapper/okaowrapper.hpp>

#include <qi/jsoncodec.hpp>
#include <qi/log.hpp>
#include <qi/actuation/actuation.hpp>

#include "humanrecognition_helpers.hpp"
#include "humanrecognizer_okao_bn.hpp"
#include "utility/incontext.hpp"


qiLogCategory("SemanticAgent.HumanRecognition");


namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
/// The FormattedFace struct gathers all the data needed
/// for the HumanRecognition (bayesian network) API.
struct FormattedFace
{
  const AL::FaceAnalysis::Face f;
  const qi::actuation::FramePtr hf;
  qi::SessionPtr ss;

  float height;
  float heightConfidence;


  std::vector<std::string> getDate()
  {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::stringstream ss;
    ss << std::put_time(&tm, "%T %u %d %B %Y");
    auto dateStr = ss.str();

    std::vector<std::string> dateArray(5);
    boost::algorithm::split(dateArray, dateStr, boost::is_any_of(" "));
    return dateArray;
  }


  FormattedFace(
      const AL::FaceAnalysis::Face& face,
      const qi::actuation::FramePtr headframe,
      const qi::SessionPtr sessionPtr):
      f(face), hf(headframe), ss(sessionPtr)
  {}


  AgeEstimation ageEstimation()
  {
    return {
        static_cast<float>(f.ageEstimation.age),
        static_cast<float>(f.ageEstimation.confidence)};
  }


  HeightEstimation heightEstimation()
  {
    qi::actuation::ActuationPtr actuation = ss->service("Actuation").value();
    auto height =
        static_cast<float>(
          hf->computeTransform(actuation->robotFrame()).transform.translation.z + 10.);

    return {height, 100.f}; // TODO: fix confidence, but now no means to evaluate it
  }


  /// ID used with OKAO, here the IRI of the social agent in knowledge.
  std::string faceId() {
    return f.nameEstimation.name;
  }


  /// Accuracy as estimated by OKAO.
  float faceAccuracy() {
    return f.nameEstimation.confidence;
  }


  /// Confidence as estimated by OKAO.
  // TODO: get to know what is the difference with the accuracy.
  float faceConfidence() {
    return f.recognitionResults.begin()->confidence;
  }


  GenderEstimation genderEstimation()
  {
    return {gender(), f.genderEstimation.confidence};
  }


  std::string gender() {
    if (f.genderEstimation.gender == AL::FaceAnalysis::Face::GENDER::GENDER_MALE)
      return "Male";
    else
      return "Female";
  }


  std::vector<std::string> getFaceIds() {
    auto faceIds = std::vector<std::string>();
    for(auto& nameEstimation : f.recognitionResults)
    {
      faceIds.emplace_back(nameEstimation.name);
    }
    return faceIds;
  }


  std::vector<float> getFaceConfidences()
  {
    auto faceConfidences = std::vector<float>();
    for(auto& nameEstimation : f.recognitionResults)
    {
      faceConfidences.emplace_back(nameEstimation.confidence);
    }
    return faceConfidences;
  }
};


///
/// \brief The HumanData struct: is a collection of the human's face image and face vector needed for face analysis.
///
class HumanData: SafeContext
{
public:
  /// The human associated with this data.
  human::HumanPtr human;

  /// The human's head frame.
  qi::actuation::FramePtr headFrame;

  /// The current face picture.
  cv::Mat faceRaw;

  /// OKAO face information on the current face picture.
  std::vector<AL::FaceAnalysis::Face> faceInfos;

  /// The final identifier after Bayesian estimation.
  std::string finalId;

  /// Constructor takes a strand, because retrieving the face picture is asynchronous.
  HumanData(
      std::shared_ptr<Strand> strand,
      qi::human::HumanPtr pHuman):
    SafeContext(strand),
    human(std::move(pHuman)),
    headFrame(human->headFrame.get().value())
  {
    _gettingFirstFace = loadNextFace().andThen([](void*)
    {
      qiLogDebug() << "First face found for current human";
    });

    auto checkAndAssignFacePicture = inContext([=](const image::TimestampedImage& i)
    {
      // We were waiting for this face picture to load it.
      if (_gettingNextFace.future().isRunning())
      {
        qiLogDebug() << "Expected face picture change, loading it";
        if (_setFaceImage(i))
        {
          qiLogDebug() << "Face image is set";
          _gettingNextFace.setValue(nullptr);
        }
        else
        {
          qiLogDebug() << "Still waiting for a good face image";
        }
      }
      else
      {
        qiLogDebug() << "Unexpected face picture change, ignoring it";
      }
    });

    // Faces are updated regularly, we can wait for them, rather than getting them directly.
    _facePictureLink = human->facePicture.connect(checkAndAssignFacePicture);
    human->facePicture.get().async().andThen(checkAndAssignFacePicture);
  }


  ~HumanData()
  {
    destroy();

    try
    {
      human->facePicture.disconnect(_facePictureLink);
    }
    catch (const std::exception& e)
    {
      qiLogDebug() << "Error disconnecting from human's face picture: " << e.what();
    }

    if (_gettingNextFace.future().isRunning())
      _gettingNextFace.setCanceled();
  }


  /// Wait for the next face.
  Future<void> loadNextFace()
  {
    return inContext([&]
    {
      if (!_gettingNextFace.future().isRunning())
        _gettingNextFace = Promise<void>();
      return _gettingNextFace.future();
    })();
  }


  /// Tells whenever there is a face available.
  /// Finishes immediately if a face is already available.
  Future<void> ensureFace()
  {
    return _gettingFirstFace;
  }

  FormattedFace getFormattedFace(const qi::SessionPtr& sessionPtr)
  {
    return FormattedFace(faceInfos.front(), headFrame, sessionPtr);
  }

  /// Get the name estimated using OKAO.
  /// Must not be called before name estimation was performed.
  std::string okaoId()
  {
    QI_ASSERT(!faceInfos.empty());
    return faceInfos.front().nameEstimation.name;
  }

  /// Tells whether the final identification took place.
  bool isIdentified()
  {
    return !finalId.empty();
  }

private:
  /// Sets the current face image for the human.
  /// If the face is invalid, it fails and returns false.
  bool _setFaceImage(const image::TimestampedImage& faceImage)
  {
    faceInfos.clear(); // Clears prior OKAO detection and identification.
    finalId.clear();

    try
    {
      const image::EncodedImage& facePicture = faceImage.image;
      if (!faceImage.image.data.isValid())
        throw std::runtime_error("invalid face image");

      const auto faceBuffer = facePicture.data.asRaw();
      const auto faceData = faceBuffer.first;
      const auto faceDataSize = faceBuffer.second;
      cv::_InputArray facePng(reinterpret_cast<const qi::uint8_t*>(faceData), faceDataSize);
      if (facePng.empty())
      {
        qiLogVerbose() << "Face discarded because it is an empty PNG";
        return false;
      }

      faceRaw = cv::imdecode(facePng, cv::IMREAD_UNCHANGED);
      if (faceRaw.empty())
      {
        qiLogVerbose() << "Face discarded because it was empty after decoding";
        return false;
      }

      if (faceRaw.cols == 1 && faceRaw.rows == 1)
      {
        qiLogVerbose() << "Face discarded because it was the \"no face\" face";
        return false;
      }

      qiLogVerbose() << "Human face was accepted (dims: "
                     << faceRaw.cols << "x" << faceRaw.rows << ")";

      // OKAO works on 8-bit grayscale images
      cv::Mat gray(faceRaw.rows, faceRaw.cols, CV_8UC1);
      cv::cvtColor(faceRaw, gray, cv::COLOR_RGB2GRAY);
      faceRaw = gray;

      if (AL::FaceAnalysis::scaleFacePictureUpIfTooSmallInPlace(faceRaw))
        qiLogVerbose() << "Face picture was scaled up automagically";

      return true;
    }
    catch (const std::exception& e)
    {
      qiLogVerbose() << "Human face set to invalid picture";
      faceRaw = cv::Mat::zeros(cv::Size(0, 0), CV_8U);
      return false;
    }

    // TODO: set isIdentified to false?
  }


private:
  /// Tracks our waiting for the next face picture.
  Promise<void> _gettingNextFace;

  /// Tells when the first adequate face was received.
  Future<void> _gettingFirstFace;

  /// Tracking face picture changes.
  SignalLink _facePictureLink;
};


/// Get OKAO to estimate the identity of the human.
bool identifyHumanWithOkao(
    HumanData& humanData,
    const AL::FaceAnalysis::OKAOWrapper& okao)
{
  QI_ASSERT(!humanData.faceInfos.empty());

  // Skip if human was already identified using OKAO.
  if (!humanData.okaoId().empty())
    return true;

  // recognize faces with FR. Fills in face infos.
  if (not okao.identifyFace(humanData.faceRaw, humanData.faceInfos.front()))
  {
    qiLogInfo("No face identified by OKAO.");
    return false;
  }

  if (humanData.faceInfos.front().nameEstimation.name.empty())
    return false;

  return true;
}


/// Get OKAO to estimate the age and gender of the human.
bool estimateAgeAndGender(
    HumanData& humanData,
    const AL::FaceAnalysis::OKAOWrapper& okao)
{
  QI_ASSERT(!humanData.faceInfos.empty());
  return okao.detectAgeGender(humanData.faceRaw, &humanData.faceInfos);
}


} // anonymous namespace


struct HumanRecognizerOkaoBN::Private: SafeContext
{
  Private(std::shared_ptr<Strand> strand):
    SafeContext(strand)
  {}

  /// Session to connect other services.
  qi::SessionPtr session;

  /// There is only one human being analyzed at a time.
  // TODO: support several humans.
  std::unique_ptr<HumanData> humanData;

  /// Whether BN is initialized.
  bool initialized;

  /// OKAO facial detector/recognizer/analyzer.
  std::unique_ptr<AL::FaceAnalysis::OKAOWrapper> okaoWrapper;

  /// OKAO current album.
  HALBUM handleAlbum;

  /// Service helping recognition (using a Bayesian network).
  qi::AnyObject bayesNetReco;

  /// Mapping OKAO IDs with Bayesian recognizer IDs.
  boost::bimap<std::string, std::string> okaoToBNids;


  bool saveData()
  {
    // save Okao data
    try
    {
      auto path = qi::path::userWritableDataPath("SemanticAgent", "okao_known_faces");
      okaoWrapper->saveAlbum(path);
    }
    catch (std::exception& e)
    {
      qiLogError() << "Cannot save OKAO album file: " << e.what();
      return false;
    }

    // save Map data (okao id : BN's id)
    try
    {
      auto path = qi::path::userWritableDataPath("SemanticAgent", "okao_to_bn_ids.json");

      boost::filesystem::fstream file;
      file.open(qi::Path(path).bfsPath(), std::ios::out);
      if (!file.is_open())
        throw std::runtime_error(
            std::string("failed to open file \"") + path + "\" for writing");

      std::map<std::string, std::string> idMap;
      for (const auto& okaoToBNid: okaoToBNids.left)
        idMap.insert(std::make_pair(okaoToBNid.first, okaoToBNid.second));

      auto data = qi::encodeJSON(idMap);
      file << data;
      file.close();
      qiLogDebug() << "Saved ID association data to " << path << ": " << data;
    }
    catch (std::exception& e)
    {
      qiLogError() << "Cannot save ID associations: " << e.what();
      return false;
    }

    return true;
  }


  bool loadData()
  {
    // save Okao data
    try
    {
      auto path = qi::path::userWritableDataPath("SemanticAgent", "okao_known_faces");
      if (boost::filesystem::is_regular_file(qi::Path(path).bfsPath()))
        okaoWrapper->loadAlbum(path);
      else
        return false;
      qiLogVerbose() << "Loaded OKAO faces from " << path;
    }
    catch (std::exception& e)
    {
      qiLogError() << "Cannot load OKAO album file: " << e.what();
      return false;
    }

    // save Map data (okao id : BN's id)
    try
    {
      auto path = qi::path::userWritableDataPath("SemanticAgent", "okao_to_bn_ids.json");

      boost::filesystem::fstream file;
      file.open(qi::Path(path).bfsPath(), std::ios::in);
      if (!file.is_open())
        throw std::runtime_error(
            std::string("failed to open file \"") + path + "\" for reading");
      std::string data;
      file >> data;
      file.close();
      qiLogDebug() << "ID association data from " << path << ": " << data;

      auto idMap = qi::decodeJSON(data).to<std::map<std::string, std::string>>();
      for (const auto& idPair: idMap)
        okaoToBNids.left.insert(idPair);
      qiLogVerbose() << "Loaded OKAO <-> BN ID associations from " << path;
    }
    catch (std::exception& e)
    {
      qiLogError() << "Cannot load ID associations: " << e.what();
      return false;
    }

    return true;
  }


  /// Loads the human data if required, including its first face picture,
  /// and the associated face features, producing "face infos".
  void ensureHumanIsLoaded(const human::HumanPtr& human)
  {
    // TODO: do not immediately remove old human data
    if (!humanData || human != humanData->human)
    {
      humanData = std::make_unique<HumanData>(strand, human);
      if (humanData->ensureFace().waitFor(qi::Seconds(3)) == qi::FutureState_Running)
      {
        humanData.reset();
        throw NoFacePictureException();
      }

      // initialize face infos
      if (!okaoWrapper->detectFaces(humanData->faceRaw, &humanData->faceInfos))
      {
        qiLogError() << "Error gathering face information from face picture";
        QI_ASSERT(false && "Face picture must always carry over face information");
        throw std::runtime_error("no face information for human's face picture");
      }

      QI_ASSERT(!humanData->faceInfos.empty());
      if (humanData->faceInfos.size() > 1)
        qiLogError() << "There are two faces in the face!";
    }
  }


  /// Helper to get OKAO to identify current human.
  bool identifyCurrentHumanWithOkao()
  {
    return identifyHumanWithOkao(*humanData, *okaoWrapper);
  }


  /// \brief Get OKOA ids from BN ids.
  /// OKAO ids may be person node URIs. BN ids are undetermined.
  /// \param okaoIds are the ids of the person nodes
  /// \return a vector of bayesian network ids
  std::vector<std::string> idsToBNIds(const std::vector<std::string>& okaoIds)
  {
    auto BNids = std::vector<std::string>();
    for (const auto& id: okaoIds)
    {
      try
      {
        BNids.emplace_back(okaoToBNids.left.at(id));
      }
      catch (std::out_of_range& e)
      {
        qiLogError() << "The id " << id << " does not exist: " << e.what();
      }
    }
    return BNids;
  }


  /// Actually learn the current human data in OKAO and in the Bayes Network.
  /// Requires faces to have been analyzed first.
  bool learnCurrentHumanInOkaoAndBayesNet(const std::string& id)
  {
    QI_ASSERT(!id.empty());
    if (id.empty())
      throw std::invalid_argument("id cannot be empty");

    const auto& analyzedFaces = humanData->faceInfos;
    QI_ASSERT(!analyzedFaces.empty());
    if (analyzedFaces.empty()) // TODO: shan't there be only one face?
      throw std::runtime_error("learn called before faces were analyzed");

    // Is that id already in the database?
    bool isNewFace = !okaoWrapper->getAlbumContent().count(id);

    if (not okaoWrapper->learnFace(
          humanData->faceRaw,
          humanData->faceInfos.front(),
          id, isNewFace))
    {
      qiLogVerbose() << "OKAO failed to learn face for ID: " << id;
      return false; // if OKAO doesn't learn any face, stop there
    }

    qiLogDebug() << "OKAO learned " << id;
    humanData->faceInfos.front().nameEstimation.name = id;
    humanData->faceInfos.front().nameEstimation.confidence = 100;

    // Get age, gender and format data for bayesian network API
    if (!estimateAgeAndGender(*humanData, *okaoWrapper))
      qiLogVerbose() << "OKAO failed to estimate age and gender";

    auto ff = humanData->getFormattedFace(session);
    qiLogDebug() << "OKAO's face name: " << ff.faceId();

    // Learn person in the bayesian network
    auto BNid = bayesNetReco.call<std::string>(
          "learn",
          ff.faceId(), // TODO: how is this related to the id?
          idsToBNIds(ff.getFaceIds()),
          ff.getFaceConfidences(),
          ff.faceAccuracy(),
          ff.ageEstimation(),
          ff.genderEstimation(),
          ff.heightEstimation(),
          ff.getDate(),
          isNewFace);

    qiLogDebug() << "BN learned " << id << " (as " << BNid << ")";

    // Add to map to remember id / BNid association
    if (isNewFace)
    {
      qiLogDebug() << id << " (OKAO) <-> " << BNid << " (BN)";
      okaoToBNids.left.insert(std::make_pair(id, BNid));
    }
    else
    {
      QI_ASSERT(okaoToBNids.left.at(id) == BNid);
      qiLogError() << "OKAO and BN ids are desynchronized";
    }

    if (!saveData())
      qiLogError() << "Failed to save freshly learned recognition data";
    return true;
  }
};


HumanRecognizerOkaoBN::HumanRecognizerOkaoBN(
    std::shared_ptr<Strand> strand,
    qi::SessionPtr session):
  _p(std::make_unique<Private>(strand))
{
  _p->session = std::move(session);
  _p->initialized = false;

  _p->okaoWrapper = AL::FaceAnalysis::OKAOWrapper::makeOKAOWrapperReal();

  _p->bayesNetReco = _p->session->service("HumanRecognition").value();
  _p->bayesNetReco.call<void>("initSystem");

  if(_p->loadData())
  {
    qiLogVerbose() << "Facial recognition data was found, recognizer is ready.";
    _p->initialized = true;
  }
  else
  {
    qiLogVerbose() << "Facial recognition data was not found, recognizer is not initialized.";
    _p->initialized = false;
    _p->bayesNetReco.call<void>("cleanDB");
  }
}


HumanRecognizerOkaoBN::~HumanRecognizerOkaoBN() = default;


Future<bool> HumanRecognizerOkaoBN::isInitialized()
{
  return _p->runInContext([=]{ return _p->initialized; });
}


Future<std::string> HumanRecognizerOkaoBN::identify(qi::human::HumanPtr human)
{
  return _p->runInContext([=]
  {
    _p->ensureHumanIsLoaded(human);

    // recognize faces with FR
    if (not _p->identifyCurrentHumanWithOkao())
      return std::string();

    // Gather extra info. TODO: prevent doing it twice on the same data.
    estimateAgeAndGender(*_p->humanData, *_p->okaoWrapper);

    // Identify the face using the bayesian network
    // We use the FormattedFace structure as a helper.
    auto ff = _p->humanData->getFormattedFace(_p->session);
    auto BNidConf = _p->bayesNetReco.call<std::map<std::string, float>>(
          "get_identity",
          ff.faceId(),
          _p->idsToBNIds(ff.getFaceIds()),
          ff.getFaceConfidences(),
          ff.faceAccuracy(),
          ff.ageEstimation(),
          ff.genderEstimation(),
          ff.heightEstimation(),
          ff.getDate());

    // Recover the OKAO ID from the BN one.
    auto id = [&]() -> std::string
    {
      if (BNidConf.begin()->first.empty())
      {
        qiLogError() << "bayesian network returned an empty estimation, "
                     << "falling back on OKAO's result.";
        return _p->humanData->okaoId();
      }
      else
      {
        return _p->okaoToBNids.right.at(BNidConf.begin()->first);
      }
    }();

    // Update data of the human, necessary for future learning.
    _p->humanData->finalId = id;
    return id;
  });
}


Future<bool> HumanRecognizerOkaoBN::learn(qi::human::HumanPtr human, std::string id)
{
  return _p->runInContext([=]
  {
    _p->ensureHumanIsLoaded(human);

    // Initialize if needed by taking several pictures.
    if (!_p->initialized)
    {
      qiLogVerbose() << "Initialization of OKAO and the Bayesian Network...";
      static const auto timeout = std::chrono::milliseconds(3000);
      auto start = std::chrono::steady_clock::now();
      auto attemptCount = 0u;

      while (!_p->initialized && (std::chrono::steady_clock::now() - start) < timeout)
      {
        ++attemptCount;
        qiLogVerbose() << "Learning face #" << attemptCount << " (" << id << ")...";

        if (!_p->learnCurrentHumanInOkaoAndBayesNet(id))
        {
          qiLogVerbose() << "Initialization attempt #" << attemptCount
                         << " failed, while learning id " << id;
        }

        // Check that OKAO is able to perform an identification (regardless the result)
        bool okaoIsInitialized = _p->identifyCurrentHumanWithOkao();

        // Check that BN's initialization is over.
        bool bnIsInitialized = !_p->bayesNetReco.call<bool>("isInitializing");

        if (okaoIsInitialized && bnIsInitialized)
        {
          _p->initialized = true;
          break;
        }

        // TODO: do this in a non-blocking manner
        _p->humanData->loadNextFace().value();
      }

      if (!_p->initialized) // Still not initialized after timeout.
      { // Reset the bayesian network and OKAO.
        _p->okaoWrapper->forgetAllFaces();
        _p->bayesNetReco.call<void>("initSystem");
        _p->bayesNetReco.call<void>("cleanDB");
        qiLogError() << "Face recognition initialization failed";
        throw std::runtime_error("face recognition initialization failed");
      }

      qiLogVerbose() << "Recognizer is now initialized";
      return true;
    }

    // Else it was already properly initialized.
    else
    {
      if (!_p->learnCurrentHumanInOkaoAndBayesNet(id))
      {
        qiLogVerbose() << "Failed to learn face for id " << id;
        return false;
      }
      return true;
    }
  });
}

} // agent
} // semantic
} // qi
