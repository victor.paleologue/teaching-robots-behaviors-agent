#pragma once
#ifndef SEMANTICAGENT_KNOWLEDGE_PDDL_HPP
#define SEMANTICAGENT_KNOWLEDGE_PDDL_HPP

#include <memory>
#include <qi/knowledge/knowledge.hpp>
#include "behaviormodel/action_pool.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
struct KnowledgeProblemExtractor
{
  /// Provide the knowledge base on which will rely the extraction.
  KnowledgeProblemExtractor(
      knowledge::KnowledgeBasePtr knowledgeBase,
      std::shared_ptr<Strand> strand = std::make_shared<Strand>());

  /// Destroys the object and stops all extraction.
  ~KnowledgeProblemExtractor();

  /// Runs the extractor, starts updating the problem.
  Future<void> run();

  /// Problem defining the state of the world.
  std::shared_ptr<ProblemSource> worldState();

  /// Find back a node from the given PDDL name.
  Future<knowledge::ResourceNode>
  findKnowledgeFromPDDLName(
      const std::string& name);

  /// Find back a PDDL name from a node.
  /// This method is rather inefficient and is most useful for testing.
  Future<std::string>
  findPDDLNameFromKnowledge(
      const knowledge::ResourceNode& node);

  /// Goal stating that the robot should respond to communication acts,
  /// and that communication acts should be well understood.
  std::shared_ptr<ProblemSource> respondToUnderstoodCommunicationActsGoal();

private:
  struct Private;
  std::unique_ptr<Private> _p;
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_KNOWLEDGE_PDDL_HPP
