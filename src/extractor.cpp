#include "extractor.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/core/ignore_unused.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <qi/jsoncodec.hpp>
#include <qi/signalspy.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <qi/teaching/ontology.hpp>
#include <src/agent/agent_p.hpp>
#include "utility/knowledge.hpp"
#include "language.hpp"
#include "utility/incontext.hpp"

qiLogCategory("SemanticAgent.Extractor");

// First we implement a simple speech source.
//===========================================
namespace qi
{
namespace semantic
{
namespace agent
{
static std::string generateUuid()
{
  static boost::uuids::random_generator genUuid;
  return boost::uuids::to_string(genUuid());
}

struct SimpleSpeechSource
{
  SimpleSpeechSource();
  Property<bool> hearing;
  Signal<Utterance> heard;
  const std::string& _id();

private:
  const std::string _uuid;
};

} // agent
} // semantic
} // qi

QI_REGISTER_IMPLEMENTATION_H(
    qi::semantic::agent::SpeechSource,
    qi::semantic::agent::SimpleSpeechSource)

REGISTER_SPEECHSOURCE(qi::semantic::agent::SimpleSpeechSource)

namespace qi
{
namespace semantic
{
namespace agent
{
SimpleSpeechSource::SimpleSpeechSource()
  : _uuid(generateUuid())
{
  static auto unused = initTypeSpeechSource();
  boost::ignore_unused(unused);
  hearing.set(false).value();
}

const std::string& SimpleSpeechSource::_id()
{
  return _uuid;
}

// Then, we implement the general extractor, that provides a speech source.
//=========================================================================
struct Extractor::Private: SafeContext
{
  Private(std::shared_ptr<Strand> strand)
    : SafeContext(std::move(strand))
  {}

  SessionPtr session;
  Object<knowledge::EditableKnowledgeBase> knowledge;
  Object<knowledge::EditableKnowledgeGraph> beliefs;
  SpeechExtractorPtr speechExtractor;
  Promise<void> startingSpeechExtraction;
  Future<void> speechExtractionStartTimeout = Future<void>(nullptr);
  Future<void> extractingSpeech;
  AnyObject saidSubscriber;
  boost::shared_ptr<SimpleSpeechSource> speechSource = boost::make_shared<SimpleSpeechSource>();
};


Extractor::Extractor(
    std::shared_ptr<Strand> strand,
    const SessionPtr &session,
    Object<knowledge::EditableKnowledgeBase> knowledgeBase,
    SpeechExtractorPtr speechExtractor)
  : confidenceThreshold(speechExtractor->confidenceThreshold)
  , _p(std::make_unique<Private>(std::move(strand)))
  , hearing(speechExtractor->hearing)
{
  _p->session = session;
  _p->knowledge = knowledgeBase;
  _p->beliefs = _p->knowledge->editableKnowledgeGraph(beliefsDomain());
  _p->speechExtractor = speechExtractor;
  _p->speechExtractor->hearing.connect(_p->inContext([this](bool value){ hearing.set(value); }));

  // Disable automatic timestamps, metadata is handled by this agent.
  _p->beliefs->timestampEnabled.set(false);

  // Translate speech recognition events to knowledge events.
  _p->speechExtractor->heard.connect(_p->inContext([=](std::vector<PhraseAndLocale> phrases)
  {
    if (phrases.empty() || phrases.front().text.empty())
    { // This can happen when speech extractors aren't properly implemented.
      qiLogDebug() << "Empty user input phrase";
      return;
    }

    // We produce a communication act event related to that utterance.
    auto eventResource = teaching::makeUniqueResource(teaching::eventPrefix());
    auto eventTriples = makeCommunicationActAddressedToSelf(eventResource);

    const auto& phrase = phrases.front();
    qiLogInfo() << "User may have said: " << phrase.text
                << "(event " << eventResource.url << ")";

    knowledge::addTriples(_p->beliefs, std::move(eventTriples))
        .then(_p->inContext([=](Future<void> addingTriples)
    {
      if (addingTriples.hasError())
        qiLogError()
            << "Error occurred registering user input in knowledge: "
            << addingTriples.error();

      QI_EMIT _p->speechSource->heard(
            Utterance{std::move(eventResource), std::move(phrases)});
    }));
  }));
}

Extractor::~Extractor()
{
  // KLUDGE: remove when strand won't crash when callback happens after its death.
  _p->speechExtractionStartTimeout.cancel();
  _p->speechExtractionStartTimeout.wait();
  _p->destroy();
  _p->extractingSpeech.cancel(); // Do not wait, a third-party may have provided that
  _p->speechExtractor.reset();
}

Future<void> Extractor::extractSpeech()
{
  return _p->inContext([this]
  {
    if (_p->extractingSpeech.isRunning())
      throw std::runtime_error("already running"); // TODO: use the standard error

    auto setupSaidSubscriber = _p->inContext([this]
    {
      _p->saidSubscriber = _p->session->service("ALMemory").value().call<AnyObject>("subscriber", "ALTextToSpeech/CurrentSentence");
      _p->saidSubscriber.connect(
            "signal", boost::function<void(AnyValue)>(
              _p->inContext([this](AnyValue v) -> void
      {
        auto phrase = v.toString();
        if (!phrase.empty())
          qiLogInfo() << "Robot said: " << phrase;
      })));
    });

    auto cleanupSubscription = _p->inContext([this]
    {
      _p->startingSpeechExtraction = Promise<void>();
      _p->session->service("ALDialog").value().setProperty("listening", false); // for feedback
      _p->saidSubscriber.reset();
    });

    auto session = _p->session;
    auto startingSpeechExtractionPromise = _p->startingSpeechExtraction;
    SignalLink startedLink = _p->speechExtractor->started.connect([=]() mutable
    {
      qiLogInfo() << "Speech extraction has started";
      setupSaidSubscriber().value();
      try
      {
        session->service("ALDialog").value().setProperty("listening", true); // for feedback
      }
      catch (const std::exception& e)
      {
        qiLogError() << "Failed to produce feedbacks for listening: " << e.what();
      }
      startingSpeechExtractionPromise.setValue(nullptr);
    });

    _p->speechExtractionStartTimeout = _p->strand->asyncDelay(track([=]() mutable
    {
       _p->speechExtractor->started.disconnect(startedLink);
      try {
        startingSpeechExtractionPromise.setError("speech extractor took too long to start, something went wrong");
        qiLogInfo() << "Speech extraction start timed out";
      } catch (const std::exception&) {}
    }, this), Minutes{1});

    qiLogInfo() << "Speech extractor is about to start...";
    _p->extractingSpeech = _p->speechExtractor->async().run();
    qi::WeakObject<SpeechExtractor> weakSpeechExtractor{_p->speechExtractor};
    auto cancelListening = [weakSpeechExtractor](Promise<void> p) mutable
    {
      qiLogVerbose() << "Stopping speech extractor";
      auto speechExtractor = weakSpeechExtractor.lock();
      if (!speechExtractor)
      {
        qiLogDebug() << "Speech extractor was already stopped";
        return;
      }

      // else
      speechExtractor->async().stop().then([speechExtractor, p](Future<void> f) mutable
      {
        qiLogDebug() << "Speech extractor was stopped";
        if (f.hasError())
        {
          qiLogWarning() << "Error stopping the speech extraction: " << f.error();
        }
        // else the state of listening should change, so it is already taken care of
      });
    };

    Promise<void> listeningAdapted{cancelListening};
    _p->extractingSpeech.then([cleanupSubscription, listeningAdapted](Future<void> f) mutable
    {
      cleanupSubscription().value();

      switch (f.waitFor(qi::MilliSeconds{0}))
      {
      case FutureState_FinishedWithValue:
        qiLogInfo() << "Speech extractor has finished listening.";
        listeningAdapted.setValue(nullptr);
        break;

      case FutureState_FinishedWithError:
        listeningAdapted.setError(f.error());
        qiLogError() << "Speech extractor stopped with an error: " << f.error();
        break;

      case FutureState_Canceled:
        listeningAdapted.setCanceled();
        qiLogInfo() << "Speech extractor was stopped as requested";
        break;

      case FutureState_Running:
      case FutureState_None:
        QI_ASSERT(false && "future should be finished");
      }
    });
    return listeningAdapted.future();
  })();
}

Future<SpeechSourcePtr> Extractor::speechSource()
{
  return _p->inContext([=]{ return SpeechSourcePtr(_p->speechSource); })();
}

Future<void> Extractor::waitForNextStartOfSpeechExtraction()
{
  return _p->startingSpeechExtraction.future();
}

SpeechSourcePtr sourceForSpeechExtractor(SpeechExtractorPtr speechExtractor)
{
  return speechExtractor;
}

} // agent
} // semantic
} // qi
