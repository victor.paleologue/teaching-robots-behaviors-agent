#ifndef QI_SEMANTICAGENT_AGENT_HPP
#define QI_SEMANTICAGENT_AGENT_HPP

#include <memory>

#include <qi/anyobject.hpp>
#include <qi/session.hpp>
#include <qi/signal.hpp>

#include <qi/conversation/conversation.hpp>
#include <qi/semantic/agent/agent.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
/**
 * An agent accumulating semantic data out of semantic extraction.
 * Check the corresponding IDL file for documentation in
 * <qi/semantic/agent/agent.idl.qi>
 */
class AgentImpl
{
  struct Private;
  std::unique_ptr<Private> _p;

public:
  AgentImpl(const SessionPtr& session);
  ~AgentImpl();

  /// Makes a chat with a mix of the agent's content and the default one,
  /// provided by MainConversation's default chatbots.
  Future<Object<Chat>> makeChat(
      context::ContextPtr context,
      SpeechExtractorPtr speechExtractor);

  /// Makes a chat with only the agent's content.
  Future<Object<Chat>> makeChatWithoutDefaultContent(
      context::ContextPtr context,
      SpeechExtractorPtr speechExtractor);

  /// Makes a chat with only the agent's content.
  Future<Object<Chat>> makeChatWithoutDefaultContent(
      context::ContextPtr context,
      SpeechExtractorPtr speechExtractor,
      const std::vector<Object<conversation::Chatbot>>& additionalChatbots,
      const locale::Locale&);

  /// Makes a chat with the agent's content and
  /// additional content provided explicitly.
  Future<Object<Chat>> makeMainChat(
      context::ContextPtr context,
      const std::vector<Object<conversation::Chatbot>>& additionalChatbots,
      const locale::Locale&);

  /// Makes a chat with the agent's content and
  /// additional content provided explicitly.
  Future<Object<Chat>> makeMainChat(
      context::ContextPtr context,
      const std::vector<Object<conversation::Chatbot>>& additionalChatbots,
      const std::vector<Object<PDDLAwareQiBehavior>>& additionalBehaviors);

  Future<OwnerPtr> registerFactories(
      const std::vector<PDDLAwareQiActionFactoryPtr>& factories);

  Future<OwnerPtr> registerBehavior(PDDLAwareQiBehaviorPtr behavior);

  SpeechExtractorPtr makeALDialogSpeechExtractor();
  SpeechExtractorPtr makeFakeSpeechExtractor();
  SpeechExtractorPtr makeMicrosoftSpeechExtractor();

  qi::Property<qi::knowledge::EditableKnowledgeBasePtr> editableKnowledge;
  std::string dumpKnowledge();

  Signal<std::string, std::vector<std::string>>& behaviorLearned;
};
} // agent
} // semantic
} // qi

#include <src/agent/agent_p.hpp>
QI_REGISTER_IMPLEMENTATION_H(qi::semantic::agent::Agent, qi::semantic::agent::AgentImpl)

#endif // QI_SEMANTICAGENT_AGENT_HPP
