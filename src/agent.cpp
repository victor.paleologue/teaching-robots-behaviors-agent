#include "agent.hpp"

#include <unordered_set>

#include <boost/core/ignore_unused.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>

#include <qi/anymodule.hpp>
#include <qi/jsoncodec.hpp>
#include <qi/os.hpp>
#include <qi/path.hpp>

#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <semanticnaoqilinker/instancesmanager.hpp>
#include <qi/semantic/engine/chatbotmemory.hpp>

#include <qi/conversation/conversation.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/context/commonerrormessages.hpp>

#include "behaviormodel/action_pool.hpp"
#include "behaviors/chatbot.hpp"
#include "behaviors/greet_behavior.hpp"
#include "behaviors/learn_behavior.hpp"
#include "behaviors/not_understood_fallback.hpp"
#include "behaviors/obey.hpp"
#include "behaviors/semantic_behaviors.hpp"
#include "utility/owner.hpp"
#include "utility/semantic_helpers.hpp"
#include "chat.hpp"
#include "extractor.hpp"
#include "knowledge_semantic.hpp"

#include "speechextractors/aldialog_speechextractor.hpp"
#include "speechextractors/fake_speechextractor.hpp"
#include "speechextractors/microsoft_speechextractor.hpp"

qiLogCategory("SemanticAgent.Agent");

// KLUDGE: see Chat for more info
REGISTER_AGENT(qi::semantic::agent::AgentImpl)

/*
 * The semantic agent is a background service analyzing NAOqi events
 * semantically, thanks to the semantic engine. It also provides an
 * implementation of an active chat and some basic reactions that
 * demonstrate the semantic reasoning.
 *
 * Since this may be used in stand-alone applications, it is designed to be
 * as much stand-alone as it can, by linking directly with the semantic
 * engine compiled in static, instead of using the semantic module, which
 * could be confused with the one already present in the system.
 */
namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
std::vector<PDDLAwareQiBehaviorPtr> makeBehaviorTeachingBehaviors(
      SpeechSourcePtr speechSource,
      conversation::SpeechEnginePtr speechEngine,
      knowledge::EditableKnowledgeBasePtr knowledgeBase,
      SessionPtr session,
      context::ContextPtr context,
      std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
      std::shared_ptr<std::list<qi::semantic::agent::SemanticExpressionAssociation>> taughtBehaviors,
      std::shared_ptr<Signal<std::string, std::vector<std::string>>> behaviorLearned,
      std::shared_ptr<ActionPool> actionPool)
{
  std::vector<PDDLAwareQiBehaviorPtr> behaviors;

  auto knowledgeSemanticBridge =
      std::make_shared<KnowledgeSemanticBridge>(
        knowledgeBase->knowledgeBase(), semanticMemoryContext);
  knowledgeSemanticBridge->run();

  behaviors.push_back(
        makeObey(
          speechSource, speechEngine, knowledgeBase, session, context,
          semanticMemoryContext, taughtBehaviors, actionPool,
          knowledgeSemanticBridge).value());

  behaviors.push_back(
        makeIntrospectBehavior(
          speechSource, speechEngine, knowledgeBase, session, context,
          semanticMemoryContext, taughtBehaviors).value());

  behaviors.push_back(
        makeLearnBehavior(
          speechSource, speechEngine, knowledgeBase, session, context,
          semanticMemoryContext, taughtBehaviors, behaviorLearned).value());

  behaviors.push_back(
        makeNotUnderstoodFallbackBehavior(
          speechSource, speechEngine, session, context,
          semanticMemoryContext, knowledgeBase->knowledgeBase(),
          knowledgeBase->editableKnowledgeGraph(beliefsDomain())).value());

  return behaviors;
}
} // anonymous

struct AgentImpl::Private
{
  std::shared_ptr<Signal<std::string, std::vector<std::string>>> behaviorLearned
      = std::make_shared<Signal<std::string, std::vector<std::string>>>();

  std::shared_ptr<Strand> strand;
  SessionPtr session;
  std::shared_ptr<qisem::linguistics::LinguisticDatabase> linguisticDatabase;
  std::shared_ptr<qisem::SemanticMemory> memory;
  Object<knowledge::EditableKnowledgeBase> knowledge;
  Promise<void> chatting;
  std::unordered_set<AnyObject> registeredFactories;
  std::unordered_set<AnyObject> registeredBehaviors;

  Future<Object<Chat>> makeChatCommon(
        context::ContextPtr context,
        std::function<SpeechExtractorPtr()> provideSpeechExtractor,
        std::function<std::vector<Object<conversation::Chatbot>>()> provideAdditionalChatbots,
        std::function<std::vector<Object<PDDLAwareQiBehavior>>()> provideAdditionalBehaviors)
  {
    return strand->schedulerFor([=]
    {
      try
      {
        if (!context)
          throw std::runtime_error("context was null");

        // The means for receiving events.
        auto speechExtractor = provideSpeechExtractor();
        auto extractor = std::make_shared<Extractor>(
              strand, session, knowledge, std::move(speechExtractor));
        auto speechSource = extractor->speechSource().value();

        // To make the robot speak.
        auto conversation = session->service("Conversation").value();
        auto speechEngine = conversation.call<conversation::SpeechEnginePtr>("makeSpeechEngine", context);

        // The behaviors made for the teaching
        // TODO: represent the taught behaviors the same way as other behaviors
        auto taughtBehaviors = std::make_shared<std::list<SemanticExpressionAssociation>>();

        // KLUDGE: it looks like we're giving a separate database but in fact it isn't.
        auto chatbotMemory = qisem::ensureChatbotMemory(context, session, *linguisticDatabase);
        auto semanticMemoryContext = makeSemanticMemoryContext(std::move(chatbotMemory), linguisticDatabase);
        auto actionPool = std::make_shared<ActionPool>(semanticMemoryContext);
        auto behaviors =
            makeBehaviorTeachingBehaviors(
              speechSource, speechEngine, knowledge, session, context, semanticMemoryContext,
              std::move(taughtBehaviors), behaviorLearned, actionPool);

        for (const auto& behavior: registeredBehaviors)
        {
          qiLogInfo()
              << "Adding behavior \""
              << behavior.property<std::string>("name").value()
              << "\" to chat";
          behaviors.push_back(behavior);
        }

        // Make a behavior for each chatbot provided.
        auto knowledgeGraph = knowledge->editableKnowledgeGraph(beliefsDomain());

        const auto& additionalChatbots = provideAdditionalChatbots();
        if (!additionalChatbots.empty())
        {
          auto combinedChatbot = combineChatbots(additionalChatbots);
          behaviors.push_back(
                makeChatbotBehavior(session, speechSource, speechEngine,
                                    combinedChatbot, knowledgeGraph).value());
        }

        auto additionalBehaviors = provideAdditionalBehaviors();
        for (const auto& additionalBehavior: additionalBehaviors)
        {
          behaviors.push_back(additionalBehavior);
        }

        // Making the actions running those behaviors.
        return Object<Chat>(
              boost::make_shared<ChatImpl>(
                context, strand, session, semanticMemoryContext,
                knowledge, extractor, speechSource, speechEngine,
                chatting, std::move(behaviors),
                registeredFactories,
                actionPool));
      }
      catch (const std::exception& e)
      {
        qiLogInfo() << "Chat action could not be provided: " << e.what();
        throw;
      }
    })();
  }
};

AgentImpl::AgentImpl(const qi::SessionPtr& session)
  : _p(new Private)
  , editableKnowledge()
  , behaviorLearned(*_p->behaviorLearned)
{
  _p->strand = std::make_shared<Strand>();
  _p->session = session;
  _p->linguisticDatabase = qisem::semanticNaoqiLinker::makeLinguisticDatabase();
  _p->memory = makeMemoryKnowingCommandsAndInfos(*_p->linguisticDatabase);

  auto knowledgeModule = import("knowledge_module");
  qi::Path knowledgePath{qi::path::userWritableDataPath("SemanticAgent", "knowledge")};
  boost::filesystem::create_directory(knowledgePath.bfsPath());
  auto anyKb = knowledgeModule.call<qi::AnyObject>("EditableKnowledgeBase", knowledgePath.str());
  auto kb = qi::knowledge::EditableKnowledgeBasePtr{std::move(anyKb)};
  kb->editableKnowledgeGraph("beliefs")->timestampEnabled.set(false).value();
  _p->knowledge = std::move(kb);
  editableKnowledge.set(_p->knowledge);

  _p->chatting.setValue(nullptr);

  static const auto unused = initTypeAgent();
  boost::ignore_unused(unused);
}


AgentImpl::~AgentImpl()
{ // Make sure all processing is finished before destroying
  // the memory, the linguistic database, etc...
  _p->chatting.future().cancel();
  _p->strand->join();
  _p->chatting.future().wait();
}


Future<Object<Chat>> AgentImpl::makeChat(
    context::ContextPtr context,
    SpeechExtractorPtr speechExtractor)
{
  auto provideSpeechExtractor = [=] { return speechExtractor; };

  auto provideAdditionalChatbots = [=]() mutable {
    std::vector<conversation::ChatbotPtr> additionalChatbots;
    try {
      conversation::ConversationPtr conversation{_p->session->service("Conversation").value()};
      auto speechEngine = conversation->makeSpeechEngine(context);
      auto mainConversation = _p->session->service("MainConversation").value();
      additionalChatbots = mainConversation.call<std::vector<conversation::ChatbotPtr>>("makeMainChatbots", context);
    } catch (const std::exception& e) {
      qiLogVerbose() << "Could not make behaviors from main chatbots: " << e.what();
    }
    return additionalChatbots;
  };

  auto provideAdditionalBehaviors = []{ return std::vector<Object<PDDLAwareQiBehavior>>(); };

  return _p->makeChatCommon(
        std::move(context),
        std::move(provideSpeechExtractor),
        std::move(provideAdditionalChatbots),
        std::move(provideAdditionalBehaviors));
}


Future<Object<Chat>> AgentImpl::makeChatWithoutDefaultContent(
    context::ContextPtr context,
    SpeechExtractorPtr speechExtractor)
{
  auto provideSpeechExtractor = [=]{ return speechExtractor; };

  std::vector<conversation::ChatbotPtr> noChatbot;
  auto provideAdditionalChatbots = [=]{ return noChatbot; };
  auto provideAdditionalBehaviors = []{ return std::vector<Object<PDDLAwareQiBehavior>>(); };

  return _p->makeChatCommon(
        std::move(context),
        std::move(provideSpeechExtractor),
        std::move(provideAdditionalChatbots),
        std::move(provideAdditionalBehaviors));
}

Future<Object<Chat>> AgentImpl::makeChatWithoutDefaultContent(
      context::ContextPtr context,
      SpeechExtractorPtr speechExtractor,
      const std::vector<Object<conversation::Chatbot>>& additionalChatbots,
      const locale::Locale&)
{
  auto provideSpeechExtractor = [=]{ return speechExtractor; };
  auto provideAdditionalChatbots = [=]{ return additionalChatbots; };
  auto provideAdditionalBehaviors = []{ return std::vector<Object<PDDLAwareQiBehavior>>(); };

  return _p->makeChatCommon(
        std::move(context),
        std::move(provideSpeechExtractor),
        std::move(provideAdditionalChatbots),
        std::move(provideAdditionalBehaviors));
}

Future<Object<Chat>> AgentImpl::makeMainChat(
    context::ContextPtr context,
    const std::vector<Object<conversation::Chatbot>>& additionalChatbots,
    const locale::Locale&)
{
  auto provideSpeechExtractor = [this] {
    try {
      return makeALDialogSpeechExtractor();
    }
    catch (const std::exception& e) {
      qiLogWarning() << "Falling back to Microsoft speech extractor because NAOqi's failed: "
                     << e.what();
      return makeMicrosoftSpeechExtractor();
    }
  };

  auto provideAdditionalChatbots = [=]{ return additionalChatbots; };
  auto provideAdditionalBehaviors = []{ return std::vector<Object<PDDLAwareQiBehavior>>(); };

  return _p->makeChatCommon(
        std::move(context),
        std::move(provideSpeechExtractor),
        std::move(provideAdditionalChatbots),
        std::move(provideAdditionalBehaviors));
}


Future<Object<Chat>> AgentImpl::makeMainChat(
      context::ContextPtr context,
      const std::vector<Object<conversation::Chatbot>>& additionalChatbots,
      const std::vector<Object<PDDLAwareQiBehavior>>& additionalBehaviors)
{
  auto provideSpeechExtractor = [this] {
    try {
      return makeALDialogSpeechExtractor();
    }
    catch (const std::exception& e) {
      qiLogWarning() << "Falling back to Microsoft speech extractor because NAOqi's failed: "
                     << e.what();
      return makeMicrosoftSpeechExtractor();
    }
  };

  auto provideAdditionalChatbots = [=]{ return additionalChatbots; };
  auto provideAdditionalBehaviors = [=]{ return additionalBehaviors; };

  return _p->makeChatCommon(
        std::move(context),
        std::move(provideSpeechExtractor),
        std::move(provideAdditionalChatbots),
        std::move(provideAdditionalBehaviors));
}

Future<OwnerPtr> AgentImpl::registerFactories(
      const std::vector<PDDLAwareQiActionFactoryPtr>& factories)
{
  for (const auto& factory: factories)
  {
    if (!factory)
      throw std::invalid_argument(qi::error::invalidObject);
  }

  return _p->strand->schedulerFor([=]{
    for (const auto& factory: factories)
    {
      _p->registeredFactories.insert(factory);
      auto name = factory->name.get().value();
      qiLogInfo()
          << "Factory registered: \"" << name << "\"";
    }
    CustomOwner::ReleaseFunction release = _p->strand->schedulerFor([=]{
      for (const auto& factory: factories)
        _p->registeredFactories.erase(factory);
    });
    return OwnerPtr(boost::make_shared<CustomOwner>(std::move(release)));
  })();
};

Future<OwnerPtr> AgentImpl::registerBehavior(PDDLAwareQiBehaviorPtr behavior)
{
  if (!behavior)
    throw std::invalid_argument(qi::error::invalidObject);

  return _p->strand->schedulerFor([=]{
    _p->registeredBehaviors.insert(behavior);
    auto name = behavior->name.get().value();
    qiLogInfo()
        << "Behavior registered: \"" << name << "\"";
    CustomOwner::ReleaseFunction release = _p->strand->schedulerFor([=]{
      _p->registeredBehaviors.erase(behavior);
    });
    return OwnerPtr(boost::make_shared<CustomOwner>(std::move(release)));
  })();
};

SpeechExtractorPtr AgentImpl::makeALDialogSpeechExtractor()
{
  try
  {
    return AnyObject{boost::make_shared<ALDialogSpeechExtractor>(_p->strand, _p->session)};
  }
  catch (const std::exception& e)
  {
    qiLogError() << "ALDialogSpeechExtractor could not be provided: " << e.what();
    throw;
  }
}


SpeechExtractorPtr AgentImpl::makeFakeSpeechExtractor()
{
  return AnyObject{boost::make_shared<FakeSpeechExtractor>(_p->strand, _p->session)};
}


SpeechExtractorPtr AgentImpl::makeMicrosoftSpeechExtractor()
{
  return AnyObject{boost::make_shared<MicrosoftSpeechExtractor>(_p->strand, _p->session)};
}


std::string AgentImpl::dumpKnowledge()
{
  auto triples = _p->knowledge->knowledgeBase()->queryTriples(
        knowledge::makeTripleFromNodes(
          knowledge::makeEmptyResourceNode(),
          knowledge::makeEmptyResourceNode(),
          knowledge::makeEmptyNode()), beliefsDomain());
  return qi::encodeJSON(triples, JsonOption_PrettyPrint);
}

} // agent
} // semantic
} // qi
