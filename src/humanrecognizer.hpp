#ifndef SEMANTICAGENT_HUMANRECOGNITION_HPP
#define SEMANTICAGENT_HUMANRECOGNITION_HPP

#include <qi/session.hpp>
#include <qi/human/human.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{
struct NoFacePictureException: std::exception
{
  const char* what() const noexcept override
  {
    return "no good face picture of the human";
  }
};

/// \brief A class embedding human recognition features.
class HumanRecognizer
{
public:
  /// Returns true if at least one human was learned.
  virtual Future<bool> isInitialized() = 0;

  /// \brief identify a human from its HumanPtr using the humanrecognition service.
  /// \param human qi::HumanPtr of the human
  /// \return the ID given during the learning phase, empty string if not recognised.
  /// \throws NoFacePictureException if the face of the human is not quickly available.
  virtual Future<std::string> identify(qi::human::HumanPtr humanPtr) = 0;

  /// \brief learn a human object and an ID using the humanrecognition service.
  /// \attention must be called after identify()
  /// The first time several pictures are taken
  /// to initialize the system properly.
  /// In that case this function returns when all pictures are used,
  /// and isInitialized() will return true.
  /// \param human qi::HumanPtr of the human.
  /// Can be empty if it is the same than given previously to identify() member.
  /// \param id A string identifier, e.g. a node uri.
  /// \return true if the person was learned
  /// \throws NoFacePictureException if the face of the human is not quickly available.
  virtual Future<bool> learn(qi::human::HumanPtr humanPtr, std::string id) = 0;
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_HUMANRECOGNITION_HPP
