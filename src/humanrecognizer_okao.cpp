#include <ctime>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <thread>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/bimap.hpp>
#include <boost/filesystem.hpp>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include <okaowrapper/face.hpp>
#include <okaowrapper/okaofacedetection.hpp>
#include <okaowrapper/okaofacerecognition.hpp>
#include <okaowrapper/okaowrapper.hpp>

#include <qi/jsoncodec.hpp>
#include <qi/log.hpp>
#include <qi/actuation/actuation.hpp>

#include "humanrecognition_helpers.hpp"
#include "humanrecognizer_okao.hpp"
#include "utility/incontext.hpp"


qiLogCategory("SemanticAgent.HumanRecognition");


namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
size_t picNumber = 0u;

void saveFacePic(const cv::Mat& pic, const std::string& category, const std::string& id)
{
  std::stringstream ss;
  ss << "face_pics/pic_" << ++picNumber << "_id_" << id << "_" << category;
  auto path = path::userWritableDataPath("SemanticAgent", ss.str());
  const std::vector<int> encodingParams{cv::IMWRITE_PNG_COMPRESSION, 0};
  try
  {
    if (cv::imwrite(Path(path).bfsPath().native(), pic, encodingParams))
      qiLogInfo()
          << "Saved face pic to: " << path;
    else
      qiLogError()
          << "Failed to save face file to " << path;
  }
  catch (const std::exception& e)
  {
    qiLogError()
        << "Failed to save face file to " << path
        << ", error was: " << e.what();
  }
}

/// A human's face image and face vector needed for face analysis.
struct HumanData: SafeContext
{
  /// The human associated with this data.
  human::HumanPtr human;

  /// The current face picture.
  cv::Mat faceRaw;

  /// The wrapper for OKAO.
  std::shared_ptr<AL::FaceAnalysis::OKAOWrapper> okao;

  /// OKAO face information on the current face picture.
  std::vector<AL::FaceAnalysis::Face> faceInfos;

  /// Constructor takes a strand, because retrieving the face picture is asynchronous.
  HumanData(
      std::shared_ptr<Strand> strand,
      qi::human::HumanPtr pHuman,
      std::shared_ptr<AL::FaceAnalysis::OKAOWrapper> pOkao):
    SafeContext(strand),
    human(std::move(pHuman)),
    okao(pOkao)
  {
    _gettingFirstFace = loadNextFace().andThen([](void*)
    {
      qiLogInfo() << "First face found for current human";
    });

    auto checkAndAssignFacePicture = inContext([=](const image::TimestampedImage& i)
    {
      // We were waiting for this face picture to load it.
      if (_gettingNextFace.future().isRunning())
      {
        qiLogInfo() << "Expected face picture change, loading it";
        if (_setFaceImage(i))
        {
          qiLogInfo() << "Face image is set";
          _gettingNextFace.setValue(nullptr);
        }
        else
        {
          qiLogInfo() << "Still waiting for a good face image";
        }
      }
      else
      {
        qiLogInfo() << "Unexpected face picture change, ignoring it";
      }
    });

    // Faces are updated regularly, we can wait for them, rather than getting them directly.
    _facePictureLink = human->facePicture.connect(checkAndAssignFacePicture);
    human->facePicture.get().async().andThen(checkAndAssignFacePicture);
  }


  ~HumanData()
  {
    destroy();

    try
    {
      human->facePicture.disconnect(_facePictureLink);
    }
    catch (const std::exception& e)
    {
      qiLogInfo() << "Error disconnecting from human's face picture: " << e.what();
    }

    if (_gettingNextFace.future().isRunning())
      _gettingNextFace.setCanceled();
  }


  /// Wait for the next face.
  Future<void> loadNextFace()
  {
    return inContext([&]
    {
      if (!_gettingNextFace.future().isRunning())
        _gettingNextFace = Promise<void>();
      return _gettingNextFace.future();
    })();
  }


  /// Tells whenever there is a face available.
  /// Finishes immediately if a face is already available.
  Future<void> ensureFace()
  {
    return _gettingFirstFace;
  }

  /// Get the name estimated using OKAO.
  /// Must not be called before name estimation was performed.
  std::string okaoId()
  {
    QI_ASSERT(!faceInfos.empty());
    return faceInfos.front().nameEstimation.name;
  }

  /// Get OKAO to estimate the identity of the human.
  bool identifyHuman()
  {
    QI_ASSERT(!faceInfos.empty());

    // Skip if human was already identified using OKAO.
    if (!okaoId().empty())
      return true;

    // recognize faces with FR. Fills in face infos.
    if (not okao->identifyFace(faceRaw, faceInfos.front()))
    {
      qiLogInfo("No face identified by OKAO.");
      return false;
    }

    if (faceInfos.front().nameEstimation.name.empty())
      return false;

    return true;
  }

  /// Tells whether the final identification took place.
  bool isIdentified()
  {
    return faceInfos.front().nameEstimation.confidence > 0.f;
  }

private:
  /// Sets the current face image for the human.
  /// If the face is invalid, it fails and returns false.
  bool _setFaceImage(const image::TimestampedImage& faceImage)
  {
    faceInfos.clear(); // Clears prior OKAO detection and identification.

    try
    {
      const image::EncodedImage& facePicture = faceImage.image;
      if (!faceImage.image.data.isValid())
        throw std::runtime_error("invalid face image");

      const auto faceBuffer = facePicture.data.asRaw();
      const auto faceData = faceBuffer.first;
      const auto faceDataSize = faceBuffer.second;
      cv::_InputArray facePng(reinterpret_cast<const qi::uint8_t*>(faceData), faceDataSize);
      if (facePng.empty())
      {
        qiLogInfo() << "Face discarded because it is an empty PNG";
        return false;
      }

      faceRaw = cv::imdecode(facePng, cv::IMREAD_UNCHANGED);
      if (faceRaw.empty())
      {
        qiLogInfo() << "Face discarded because it was empty after decoding";
        return false;
      }

      if (faceRaw.cols == 1 && faceRaw.rows == 1)
      {
        qiLogInfo() << "Face discarded because it was the \"no face\" face";
        return false;
      }

      qiLogInfo() << "Human face was accepted (dims: "
                     << faceRaw.cols << "x" << faceRaw.rows << ")";

      // OKAO works on 8-bit grayscale images
      cv::Mat gray(faceRaw.rows, faceRaw.cols, CV_8UC1);
      cv::cvtColor(faceRaw, gray, cv::COLOR_RGB2GRAY);
      faceRaw = gray;

      if (AL::FaceAnalysis::scaleFacePictureUpIfTooSmallInPlace(faceRaw))
        qiLogInfo() << "Face picture was scaled up automagically";

      // initialize face infos
      if (!okao->detectFaces(faceRaw, &faceInfos))
      {
        qiLogError() << "Error gathering face information from face picture";
        QI_ASSERT(false && "Face picture must always carry over face information");
        throw std::runtime_error("no face information for human's face picture");
      }

      if (faceInfos.empty()) // This should never happen, but OKAO makes it possible!
        throw std::runtime_error("face had no face");

      if (faceInfos.size() > 1)
        qiLogError() << "There are two faces in the face!";

      return true;
    }
    catch (const std::exception& e)
    {
      qiLogInfo() << "Human face set to invalid picture after error: " << e.what();
      faceRaw = cv::Mat::zeros(cv::Size(0, 0), CV_8U);
      return false;
    }
  }

  /// Tracks our waiting for the next face picture.
  Promise<void> _gettingNextFace;

  /// Tells when the first adequate face was received.
  Future<void> _gettingFirstFace;

  /// Tracking face picture changes.
  SignalLink _facePictureLink;
};

} // anonymous namespace


struct HumanRecognizerOkao::Private: SafeContext
{
  Private(std::shared_ptr<Strand> strand):
    SafeContext(strand)
  {}

  /// Session to connect other services.
  qi::SessionPtr session;

  /// There is only one human being analyzed at a time.
  // TODO: support several humans.
  std::unique_ptr<HumanData> humanData;

  /// Whether BN is initialized.
  bool initialized;

  /// OKAO facial detector/recognizer/analyzer.
  std::shared_ptr<AL::FaceAnalysis::OKAOWrapper> okaoWrapper;

  /// OKAO current album.
  HALBUM handleAlbum;


  bool saveData()
  {
    // save Okao data
    try
    {
      auto path = qi::path::userWritableDataPath("SemanticAgent", "okao_known_faces");
      okaoWrapper->saveAlbum(path);
    }
    catch (std::exception& e)
    {
      qiLogError() << "Cannot save OKAO album file: " << e.what();
      return false;
    }

    return true;
  }


  bool loadData()
  {
    // save Okao data
    try
    {
      auto path = qi::path::userWritableDataPath("SemanticAgent", "okao_known_faces");
      if (boost::filesystem::is_regular_file(qi::Path(path).bfsPath()))
        okaoWrapper->loadAlbum(path);
      else
        return false;
      qiLogInfo() << "Loaded OKAO faces from " << path;
    }
    catch (std::exception& e)
    {
      qiLogError() << "Cannot load OKAO album file: " << e.what();
      return false;
    }

    return true;
  }


  /// Loads the human data if required, including its first face picture,
  /// and the associated face features, producing "face infos".
  Future<void> ensureHumanIsLoaded(const human::HumanPtr& human)
  {
    // TODO: do not immediately remove old human data
    if (!humanData || human != humanData->human)
    {
      humanData = std::make_unique<HumanData>(strand, human, okaoWrapper);
      qiLogInfo() << "Trying to see human properly...";
    }

    auto waitingForFace = humanData->ensureFace();
    asyncDelay([=]() mutable { waitingForFace.cancel(); }, Seconds(10));
    return waitingForFace.then([](Future<void> f)
    {
      if (f.hasError())
        f.value();
      if (f.isCanceled())
        throw NoFacePictureException();
    });
  }


  /// Actually learn the current human data in OKAO.
  /// Requires faces to have been analyzed first.
  bool learnCurrentHumanInOkao(const std::string& id)
  {
    QI_ASSERT(!id.empty());
    if (id.empty())
      throw std::invalid_argument("id cannot be empty");

    const auto& analyzedFaces = humanData->faceInfos;
    QI_ASSERT(!analyzedFaces.empty());
    if (analyzedFaces.empty()) // TODO: shan't there be only one face?
      throw std::runtime_error("learn called before faces were analyzed");

    // Is that id already in the database?
    bool isNewFace = !okaoWrapper->getAlbumContent().count(id);

    if (not okaoWrapper->learnFace(
          humanData->faceRaw,
          humanData->faceInfos.front(),
          id, isNewFace))
    {
      qiLogInfo() << "OKAO failed to learn face for ID: " << id;
      return false; // if OKAO doesn't learn any face, stop there
    }

    qiLogInfo() << "OKAO learned " << id;
    saveFacePic(humanData->faceRaw, "learn", id);

    if (!saveData())
      qiLogError() << "Failed to save freshly learned recognition data";
    return true;
  }
};


HumanRecognizerOkao::HumanRecognizerOkao(
    std::shared_ptr<Strand> strand,
    qi::SessionPtr session):
  _p(std::make_unique<Private>(strand))
{
  _p->session = std::move(session);
  _p->initialized = false;

  _p->okaoWrapper = AL::FaceAnalysis::OKAOWrapper::makeOKAOWrapperReal();

  if(_p->loadData())
  {
    qiLogInfo() << "Facial recognition data was found, recognizer is ready.";
    _p->initialized = true;
  }
  else
  {
    qiLogInfo() << "Facial recognition data was not found, recognizer is not initialized.";
    _p->initialized = false;
  }
}


HumanRecognizerOkao::~HumanRecognizerOkao() = default;


Future<bool> HumanRecognizerOkao::isInitialized()
{
  return _p->runInContext([=]() mutable { return _p->initialized; });
}


Future<std::string> HumanRecognizerOkao::identify(qi::human::HumanPtr human)
{
  return _p->runInContext([=](human::HumanPtr human)
  {
    return _p->ensureHumanIsLoaded(human);
  }, std::move(human))

      .andThen(_p->inContext([=](void*)
  {
    // recognize faces with FR
    if (not _p->humanData->identifyHuman())
      return std::string();
    qiLogInfo()
        << "Identified " << _p->humanData->okaoId()
        << " with confidence " << _p->humanData->faceInfos.front().nameEstimation.confidence;
    saveFacePic(_p->humanData->faceRaw, "recognize", _p->humanData->okaoId());
    return _p->humanData->okaoId();
  })).unwrap();
}


Future<bool> HumanRecognizerOkao::learn(qi::human::HumanPtr human, std::string id)
{

  return _p->runInContext([=](human::HumanPtr human)
  {
    return _p->ensureHumanIsLoaded(human);
  }, human)

      .andThen(_p->inContext([=](void*)
  {
    // Initialize if needed by taking several pictures.
    qiLogInfo() << "Initialization of OKAO...";
    static const auto timeout = std::chrono::milliseconds(3000);
    auto start = std::chrono::steady_clock::now();
    auto attemptCount = 0u;
    bool gotFace = true;
    std::string idEstimation;

    while ((!_p->initialized &&
            idEstimation != id &&
            (std::chrono::steady_clock::now() - start) < timeout) ||
            attemptCount < 3u)
    {
      // TODO: do this in a non-blocking manner
      if (!gotFace)
        _p->humanData->loadNextFace().value();

      ++attemptCount;
      qiLogInfo() << "Learn attempt #" << attemptCount << " (" << id << ")...";
      bool learnSucceeded = _p->learnCurrentHumanInOkao(id);

      // This face was processed.
      gotFace = false;

      if (!learnSucceeded)
      {
        qiLogInfo() << "Learn attempt #" << attemptCount
                       << " failed, while learning id " << id;
        continue;
      }

      // Check that OKAO is able to perform an identification (regardless the result)
      bool okaoIsInitialized = _p->humanData->identifyHuman();

      if (okaoIsInitialized)
        _p->initialized = true;
      else
        continue;

      // Check that identification now works.
      idEstimation = identify(human).value();
      qiLogInfo() << "After learning attempt, " << id
                     << " is recognized as " << idEstimation;
    }

    if (!_p->initialized) // Still not initialized after timeout.
    { // Reset OKAO.
      _p->okaoWrapper->forgetAllFaces();
      qiLogError() << "Face recognition initialization failed";
      throw std::runtime_error("face recognition initialization failed");
    }
    else
    {
      qiLogInfo() << "Recognizer is now initialized";
    }

    if (idEstimation != id)
    {
      qiLogInfo() << "Failed to learn face for id " << id;
      return false;
    }
    else
    {
      qiLogInfo() << "OKAO learned " << id;
      return true;
    }
  })).unwrap();
}


} // agent
} // semantic
} // qi
