#pragma once
#ifndef SEMANTICAGENT_AUTONOMOUS_PLANNING_HPP
#define SEMANTICAGENT_AUTONOMOUS_PLANNING_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/context/context.hpp>
#include <qi/knowledge/knowledge.hpp>
#include "utility/disposable.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
class ActionPool;

std::unique_ptr<qi::semantic::agent::Disposable> runAutonomousPlanning(
    std::shared_ptr<Strand> strand,
    SessionPtr session,
    context::ContextPtr context,
    knowledge::EditableKnowledgeBasePtr knowledge,
    std::shared_ptr<ActionPool> actionPool);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_AUTONOMOUS_PLANNING_HPP
