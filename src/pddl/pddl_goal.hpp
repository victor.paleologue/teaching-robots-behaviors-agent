#ifndef SEMANTICAGENT_PDDL_PDDL_GOAL_HPP
#define SEMANTICAGENT_PDDL_PDDL_GOAL_HPP

#include <set>
#include <experimental/optional>
#include <pddl/AST.h>

namespace qi
{
namespace semantic
{
namespace agent
{

/// Combines a bunch of goals with an "and" statement.
std::experimental::optional<pddl::ast::Goal> mergeGoals(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Domain& context);

/// Take a goal and split it into an array of goals.
std::set<const pddl::ast::Goal*> splitGoal(
    const std::experimental::optional<pddl::ast::Goal>& goal);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PDDL_PDDL_GOAL_HPP
