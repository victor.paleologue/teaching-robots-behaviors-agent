#include "trivial_lisp_expression.hpp"
#include <boost/algorithm/string/trim_all.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{

TrivialLispExpression::TrivialLispExpression() = default;


TrivialLispExpression::TrivialLispExpression(
        const std::string& l,
        const std::vector<TrivialLispExpression>& a)
    : literal(l),
      args(a)
{}


TrivialLispExpression::TrivialLispExpression(
        const std::initializer_list<std::string> init)
{
  if (init.size() == 0u)
    return;
  auto it = init.begin();
  literal = *it;
  ++it;
  while (it != init.end())
  {
    args.push_back(TrivialLispExpression{*it});
    ++it;
  }
}


bool TrivialLispExpression::empty() const
{
  return literal.empty() && args.empty();
}


bool operator==(const TrivialLispExpression& lhs, const TrivialLispExpression& rhs)
{
  return lhs.literal == rhs.literal
      && lhs.args == rhs.args;
}


std::ostream& operator<<(std::ostream& out, const TrivialLispExpression& expr)
{
  if (!expr.args.empty())
    out << "(";
  out << expr.literal;
  bool first = true;
  for (const auto& arg: expr.args)
  {
    if (first)
      first = false;
    else
      out << " ";
    out << arg;
  }
  if (!expr.args.empty())
    out << ")";
  return out;
}

} // agent
} // semantic
} // qi
