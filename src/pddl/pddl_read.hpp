#ifndef SEMANTICAGENT_PDDL_PDDL_READ_HPP
#define SEMANTICAGENT_PDDL_PDDL_READ_HPP

#include <pddl/AST.h>
#include <algorithm>

namespace qi
{
namespace semantic
{
namespace agent
{

/// A generic functor representing the equivalence function.
struct isEquivalentFunctor
{
  template<typename T>
  bool operator()(const T& lhs, const T& rhs) const;
};

// Below, comparator for every type found in the PDDL library.
template<typename T>
bool isEquivalent(
    const std::unique_ptr<T>& lhs,
    const std::unique_ptr<T>& rhs);

template<typename T>
bool isEquivalent(
    const std::experimental::optional<T>& lhs,
    const std::experimental::optional<T>& rhs);

template<typename T>
bool isEquivalent(
    const std::vector<T>& lhs,
    const std::vector<T>& rhs);

template<typename... T>
bool isEquivalent(
    const pddl::Variant<T...>& lhs,
    const pddl::Variant<T...>& rhs);

template<typename Argument>
bool isEquivalent(
    const pddl::ast::At<Argument>& lhs,
    const pddl::ast::At<Argument>& rhs);

template<typename Argument>
bool isEquivalent(
    const pddl::ast::Not<Argument>& lhs,
    const pddl::ast::Not<Argument>& rhs);

template<typename Derived, typename Argument>
bool isEquivalent(
    const pddl::ast::NAry<Derived, Argument>& lhs,
    const pddl::ast::NAry<Derived, Argument>& rhs);

template<typename Derived, typename ArgumentLeft, typename ArgumentRight = ArgumentLeft>
bool isEquivalent(
    const pddl::ast::Binary<Derived, ArgumentLeft, ArgumentRight>& lhs,
    const pddl::ast::Binary<Derived, ArgumentLeft, ArgumentRight>& rhs);

template<typename Derived, typename Argument>
bool isEquivalent(
    const pddl::ast::Quantified<Derived, Argument>& lhs,
    const pddl::ast::Quantified<Derived, Argument>& rhs);

bool isEquivalent(
    const pddl::ast::PrimitiveType& lhs,
    const pddl::ast::PrimitiveType& rhs);

bool isEquivalent(
    const pddl::ast::PrimitiveTypeDeclaration& lhs,
    const pddl::ast::PrimitiveTypeDeclaration& rhs);

bool isEquivalent(
    const pddl::ast::ConstantDeclaration& lhs,
    const pddl::ast::ConstantDeclaration& rhs);

bool isEquivalent(
    const pddl::ast::VariableDeclaration& lhs,
    const pddl::ast::VariableDeclaration& rhs);

bool isEquivalent(
    const pddl::ast::PredicateDeclaration& lhs,
    const pddl::ast::PredicateDeclaration& rhs);

bool isEquivalent(
    const pddl::ast::Constant& lhs,
    const pddl::ast::Constant& rhs);

bool isEquivalent(
    const pddl::ast::Variable& lhs,
    const pddl::ast::Variable& rhs);

bool isEquivalent(
    const pddl::ast::Predicate& lhs,
    const pddl::ast::Predicate& rhs);

bool isEquivalent(
    const pddl::ast::Action& lhs,
    const pddl::ast::Action& rhs);


/// Returns true if an equivalent element could be found in the container.
/// Relies on the function "isEquivalent" between two elements.
/// @throws if an element of the same name is found but is not equivalent.
template <typename T>
bool hasEquivalentElement(
    const std::vector<T>& container,
    const T& element);


/// Returns true if an equivalent element could be found in the container of pointers.
/// Relies on the function "isEquivalent" between two elements.
/// @throws if an element of the same name is found but is not equivalent.
template <typename T>
bool hasEquivalentElement(
    const std::vector<std::unique_ptr<T>>& container,
    const std::unique_ptr<T>& element);


// Definitions
//----------------------------------------------------------------------------
template<typename T>
bool isEquivalentFunctor::operator()(const T& lhs, const T& rhs) const
{
  return isEquivalent(lhs, rhs);
}

template<typename T>
bool isEquivalent(
    const std::unique_ptr<T>& lhs,
    const std::unique_ptr<T>& rhs)
{
  if (bool(lhs) != bool(rhs))
    return false;
  if (!lhs)
  {
    assert(!rhs);
    return true;
  }
  return isEquivalent(*lhs, *rhs);
}


template<typename T>
bool isEquivalent(
    const std::experimental::optional<T>& lhs,
    const std::experimental::optional<T>& rhs)
{
  if (bool(lhs) != bool(rhs))
    return false;
  if (!lhs)
  {
    assert(!rhs);
    return true;
  }
  return isEquivalent(lhs.value(), rhs.value());
}


template<typename... T>
bool isEquivalent(
    const pddl::Variant<T...>& lhs,
    const pddl::Variant<T...>& rhs)
{
  if (lhs.which() != rhs.which())
    return false;
  mapbox::util::detail::comparer<
      pddl::Variant<T...>,
      isEquivalentFunctor> comparer(lhs);
  return lhs.visit(rhs, comparer);
}


template<typename T>
bool isEquivalent(
    const std::vector<T>& lhs,
    const std::vector<T>& rhs)
{
  if (lhs.size() != rhs.size())
    return false;
  for (size_t i = 0u; i < lhs.size(); ++i)
  {
    if (!isEquivalent(lhs[i], rhs[i]))
      return false;
  }
  return true;
}


template<typename Argument>
bool isEquivalent(
    const pddl::ast::At<Argument>& lhs,
    const pddl::ast::At<Argument>& rhs)
{
  return lhs.timePoint == rhs.timePoint
      && isEquivalent(lhs.argument, rhs.argument);
}


template<typename Argument>
bool isEquivalent(
    const pddl::ast::Not<Argument>& lhs,
    const pddl::ast::Not<Argument>& rhs)
{
  return isEquivalent(lhs.argument, rhs.argument);
}


template<typename Derived, typename Argument>
bool isEquivalent(
    const pddl::ast::NAry<Derived, Argument>& lhs,
    const pddl::ast::NAry<Derived, Argument>& rhs)
{
  if (lhs.arguments.size() != rhs.arguments.size())
    return false;
  for (size_t i = 0u; i < lhs.arguments.size(); ++i)
  {
    if (!isEquivalent(lhs.arguments[i], rhs.arguments[i]))
      return false;
  }
  return true;
}


template<typename Derived, typename ArgumentLeft, typename ArgumentRight>
bool isEquivalent(
    const pddl::ast::Binary<Derived, ArgumentLeft, ArgumentRight>& lhs,
    const pddl::ast::Binary<Derived, ArgumentLeft, ArgumentRight>& rhs)
{
  return isEquivalent(lhs.argumentLeft, rhs.argumentLeft)
      && isEquivalent(lhs.argumentRight, rhs.argumentRight);
}


template<typename Derived, typename Argument>
bool isEquivalent(
    const pddl::ast::Quantified<Derived, Argument>& lhs,
    const pddl::ast::Quantified<Derived, Argument>& rhs)
{
  if (!isEquivalent(lhs.parameters, rhs.parameters))
    return false;
  return isEquivalent(lhs.argument, rhs.argument);
}


template <typename T>
bool hasEquivalentElement(
    const std::vector<T>& container,
    const T& element)
{
  // Find the element by its name.
  auto found = std::find_if(
    container.begin(), container.end(),
    [&](const T& existingElement)
  {
    return isEquivalent(existingElement, element);
  });

  return found != container.end();
}


template <typename T>
bool hasEquivalentElement(
    const std::vector<std::unique_ptr<T>>& container,
    const std::unique_ptr<T>& element)
{
  // Check find the element by its name.
  const auto& elementName = element->name;
  auto found = std::find_if(
    container.begin(), container.end(),
    [&](const std::unique_ptr<T>& existingElement)
  {
    return elementName == existingElement->name;
  });

  // Always accept if the type is encountered for the first time.
  if (found == container.end())
    return false;

  // Oherwise, simply skip the type if it is equivalent to a known one.
  if (isEquivalent(**found, *element))
    return true;

  // If they are not equivalent, we do not know what to do.
  throw std::runtime_error(
      std::string{"conflicting element \"" + elementName + "\""});
}

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PDDL_PDDL_READ_HPP
