#include "pddl_goal.hpp"
#include <qi/log.hpp>
#include <pddl/detail/ASTCopy.h>

qiLogCategory("SemanticAgent.PDDL");

using namespace pddl::ast;

namespace qi
{
namespace semantic
{
namespace agent
{

/// Combines a bunch of goals with an "and" statement.
std::experimental::optional<pddl::ast::Goal> mergeGoals(
    const std::set<const pddl::ast::Goal*>& goals,
    pddl::ast::Domain& context)
{
  if (goals.empty())
    return {};

  pddl::ast::CopyContext copyContext;
  copyContext.types = &context.types;
  copyContext.constants = &context.constants;
  copyContext.predicates = &context.predicates;

  auto result = std::make_unique<And<Precondition>>(Preconditions());
  for (const auto& goal: goals)
  {
    if (!goal->valid())
      qiLogError()
          << "Encountered invalid goal value";
    result->arguments.push_back(deepCopy(*goal, copyContext));
  }
  return Goal(std::move(result));
}


std::set<const pddl::ast::Goal*> splitGoal(
    const std::experimental::optional<pddl::ast::Goal>& goal)
{
  if (!goal)
    return {};

  std::set<const pddl::ast::Goal*> goals;
  if (goal.value().is<AndPointer<Precondition>>())
  {
    const auto& conditions = goal.value().get<AndPointer<Precondition>>()->arguments;
    for (const auto& condition: conditions)
      goals.insert(&condition);
  }
  else
  {
    goals.insert(&goal.value());
  }
  return goals;
}

} // agent
} // semantic
} // qi
