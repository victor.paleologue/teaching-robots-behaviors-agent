#include "pddl_io.hpp"
#include <boost/algorithm/string/join.hpp>
#include <qi/log.hpp>
#include <pddl/Parse.h>

qiLogCategory("SemanticAgent.PDDL");

using namespace pddl::ast;

namespace qi
{
namespace semantic
{
namespace agent
{
DomainPointer parsePddlDomain(const std::string& pddl)
{
  return std::move(parsePddlDomainAndProblem(pddl)->domain);
}


DescriptionPointer parsePddlDomainAndProblem(const std::string& pddl)
{
  std::stringstream pddlStream(pddl, std::ios_base::in);
  tokenize::Tokenizer<tokenize::CaseSensitiveTokenizerPolicy> tokenizer;
  tokenizer.read("from-string", pddlStream);

  pddl::Context context(
        std::move(tokenizer),
        [&](tokenize::Location&& l, const std::string& what)
  {
    qiLogDebug() << "warning in PDDL content line " << l.rowStart
                 << ", column " << l.columnStart << ": " << what;
  }, pddl::Mode::Compatibility);

  try
  {
    return std::make_unique<Description>(pddl::parseDescription(context));
  }
  catch (const tokenize::TokenizerException& e)
  {
    qiLogDebug() << "PDDL content could not be parsed: " << pddl;
    const auto& location = e.location();
    std::stringstream ss;
    ss << "syntax error in PDDL at "
       << location.sectionStart << ":" << location.rowStart
       << ":" << location.columnStart << ":" << " " << e.message();
    throw std::runtime_error(ss.str());
  }
}


std::string pddlNameFromPhrase(const std::string& phrase)
{
  std::string pddlName;
  pddlName.reserve(phrase.size());
  auto previousChar = '_';
  for (const auto& c: phrase)
  {
    if (std::isalnum(c))
    {
      pddlName.push_back(std::tolower(c));
      previousChar = c;
    }
    else if (previousChar != '_')
    {
      pddlName.push_back('_');
      previousChar = '_';
    }
  }
  if (!pddlName.empty() && pddlName.back() == '_')
    return pddlName.substr(0, pddlName.size() - 1);
  return pddlName;
}


std::string pddlNameFromIRI(const std::string& iri)
{
  std::string pddlName;
  pddlName.reserve(iri.size());
  auto previousChar = '_';
  for (const auto& c: iri)
  {
    if (std::isalnum(c))
    {
      pddlName.push_back(c);
      previousChar = c;
    }
    else if (previousChar != '_')
    {
      pddlName.push_back('_');
      previousChar = '_';
    }
  }
  return pddlName;
}


std::string goalToString(const pddl::ast::Goal* goal)
{
  return serializePddl(*goal);
}


std::string goalsToString(const std::set<const pddl::ast::Goal*>& goals)
{
  std::vector<std::string> goalStrings;
  std::transform(
        goals.begin(), goals.end(),
        std::back_inserter(goalStrings),
        goalToString);
  return std::string("{") + boost::algorithm::join(goalStrings, ",") + "}";
}


std::string goalsToString(const std::vector<const pddl::ast::Goal*>& goals)
{
  std::vector<std::string> goalStrings;
  std::transform(
        goals.begin(), goals.end(),
        std::back_inserter(goalStrings),
        goalToString);
  return std::string("[") + boost::algorithm::join(goalStrings, ",") + "]";
}


std::string goalsToString(const std::vector<std::set<const pddl::ast::Goal*>>& goals)
{
  std::vector<std::string> goalStrings;
  std::transform(
        goals.begin(), goals.end(),
        std::back_inserter(goalStrings),
        [](const auto& goals){ return goalsToString(goals); });
  return std::string("[") + boost::algorithm::join(goalStrings, ",") + "]";
}

} // agent
} // semantic
} // qi
