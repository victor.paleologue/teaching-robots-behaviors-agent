#ifndef SEMANTICAGENT_PDDL_TRIVIAL_LISP_EXPRESSION_HPP
#define SEMANTICAGENT_PDDL_TRIVIAL_LISP_EXPRESSION_HPP

#include <iostream>
#include <string>
#include <vector>

namespace qi
{
namespace semantic
{
namespace agent
{

/// A simple model of lisp-like statement.
/// It is always started with a literal, but may have no args.
/// It can express arbitrary combinations of PDDL facts.
struct TrivialLispExpression
{
  TrivialLispExpression();

  TrivialLispExpression(
          const std::string& l,
          const std::vector<TrivialLispExpression>& a);

  TrivialLispExpression(const std::initializer_list<std::string> init);

  std::string literal;
  std::vector<TrivialLispExpression> args;

  bool empty() const;
};


bool operator==(
        const TrivialLispExpression& lhs,
        const TrivialLispExpression& rhs);


std::ostream& operator<<(std::ostream& out, const TrivialLispExpression& expr);


TrivialLispExpression parse(const std::string& lispStr);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PDDL_TRIVIAL_LISP_EXPRESSION_HPP
