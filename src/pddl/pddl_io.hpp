#ifndef SEMANTICAGENT_PDDL_PDDL_IO_HPP
#define SEMANTICAGENT_PDDL_PDDL_IO_HPP

#include <string>
#include <sstream>
#include <iostream>
#include <pddl/AST.h>
#include <pddl/ASTOutput.h>

namespace qi
{
namespace semantic
{
namespace agent
{
/// Parse a PDDL domain into a data structure for a direct manipulation.
pddl::ast::DomainPointer parsePddlDomain(const std::string& pddl);

/// Parse a PDDL description (domain + problem) into a data structure.
pddl::ast::DescriptionPointer parsePddlDomainAndProblem(const std::string& pddl);

/// Serialize a PDDL entity to a human-readable string.
template <typename T>
std::string serializePddl(const T& entity);

/// Generate a PDDL-compliant name from a human-readable phrase.
std::string pddlNameFromPhrase(const std::string& phrase);

/// Generate a PDDL-compliant from an IRI.
std::string pddlNameFromIRI(const std::string& iri);

// Serialize goal pointers, that we use extensively.
std::string goalToString(const pddl::ast::Goal* goal);
std::string goalsToString(const std::set<const pddl::ast::Goal*>& goals);
std::string goalsToString(const std::vector<const pddl::ast::Goal*>& goals);
std::string goalsToString(const std::vector<std::set<const pddl::ast::Goal*>>& goals);

// Template implementation
//----------------------------------------------------------------------------
template <typename T>
std::string serializePddl(const T& entity)
{
  std::stringstream stringStream;
  colorlog::ColorStream colorStream(stringStream);
  pddl::detail::PrintContext printContext;
  pddl::ast::print(colorStream, entity, printContext);
  return stringStream.str();
}

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PDDL_PDDL_IO_HPP
