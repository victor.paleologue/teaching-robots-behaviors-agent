#ifndef SEMANTICAGENT_PDDL_PDDL_EDIT_HPP
#define SEMANTICAGENT_PDDL_PDDL_EDIT_HPP

#include <algorithm>
#include <string>
#include <boost/range/adaptor/transformed.hpp>
#include <qi/log.hpp>
#include <pddl/AST.h>
#include <pddl/detail/ASTCopy.h>
#include "pddl_io.hpp"
#include "pddl_read.hpp"


namespace qi
{
namespace semantic
{
namespace agent
{
/// Ensures there is a domain in the description.
pddl::ast::Domain& ensureDomain(
    pddl::ast::Description& description,
    const std::string& name = "");

/// Ensure there is a problem in the description and returns it.
/// Also ensures a domain.
pddl::ast::Problem& ensureProblem(
    pddl::ast::Description& description,
    const std::string& name = "");

/// Ensure there is a predicate declaration in the description and returns it.
pddl::ast::PredicateDeclaration* ensurePredicate(
    pddl::ast::Description& description,
    const std::string& predicateName,
    const std::vector<std::pair<std::string, std::string>>& varNamesAndTypes);

/// Ensure there is an object declaration in the description and returns it.
pddl::ast::ConstantDeclaration* ensureObject(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type = "");

/// Ensure there is an object declaration in the description and returns it.
pddl::ast::ConstantDeclaration* ensureConstant(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type = "");

/// Add an object to the given PDDL description.
/// @throws if the object already exists, or if the name is empty.
std::pair<pddl::ast::ConstantDeclaration*, pddl::ast::PrimitiveTypeDeclaration*>
addObject(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type);

/// Add an object to the given PDDL description, with no type.
/// @throws if the object already exists, or if the name is empty.
pddl::ast::ConstantDeclaration*
addObject(
    pddl::ast::Description& description,
    const std::string& name);


template<typename DomainIterator>
pddl::ast::DomainPointer mergeDomains(DomainIterator begin, DomainIterator end);

template<typename DescriptionIterator>
pddl::ast::DescriptionPointer mergeDescriptions(DescriptionIterator begin, DescriptionIterator end);

/// Combine two goals with an "and" statement.
/// May perform copies of the contents of the given goals to merge,
/// in the provided domain context, which may get enriched.
std::experimental::optional<pddl::ast::Goal> mergeGoals(
    const std::experimental::optional<pddl::ast::Goal>& goal1,
    const std::experimental::optional<pddl::ast::Goal>& goal2,
    pddl::ast::Domain& context);

// Definitions
//----------------------------------------------------------------------------

/// Always return true.
/// Used as a default predicate.
template<typename T>
bool truePredicate(const std::unique_ptr<T>&)
{
  return true;
}

/// Perform deep copies of the original elements
/// and put them in the destination
/// if and only if the predicate is true.
template<typename T>
void appendDeepCopy(
    std::vector<T>& destination,
    const std::vector<T>& origin,
    pddl::ast::CopyContext& context,
    std::function<bool(const T&)> predicate = truePredicate<T>)
{
  for (const auto& element: origin)
  {
    if (predicate(element))
      destination.push_back(pddl::ast::deepCopy(element, context));
  };
}

/// Perform deep copies of the original elements
/// and put them in the destination
/// if and only if the predicate is true.
template<typename T>
void appendDeepCopyIfNoEquivalent(
    std::vector<T>& destination,
    const std::vector<T>& origin,
    pddl::ast::CopyContext& context)
{
  appendDeepCopy<T>(
        destination, origin, context,
        [&](const T& type)
  {
    return !hasEquivalentElement(destination, type);
  });
}

template<typename T>
void appendIfNoEquivalent(
    std::vector<T>& destination,
    const std::vector<T>& origin)
{
  for (const auto& element: origin)
  {
    auto found = std::find(destination.begin(), destination.end(), element);
    if (found == destination.end())
      destination.push_back(element);
  }
}

/// Merge a range of domains together into a new domain.
/// Duplicate requirements, types and predicates will be omitted.
template<typename DomainIterator>
pddl::ast::DomainPointer mergeDomains(DomainIterator begin, DomainIterator end)
{
  if (begin == end)
    return {};

  pddl::ast::DomainPointer result = std::make_unique<pddl::ast::Domain>();
  result->name = "merged_domain";

  pddl::ast::CopyContext context;
  context.types = &result->types;
  context.constants = &result->constants;
  context.predicates = &result->predicates;

  unsigned i = 0u;
  for (auto current = begin; current != end; ++current)
  {
    if (!*current)
    {
      qiLogDebug("SemanticAgent.PDDL") << "Merging domain #" << i++
                                       << ": (empty)";
      continue;
    }
    qiLogDebug("SemanticAgent.PDDL") << "Merging domain #" << i++
                                     << ": " << serializePddl(**current);

    const pddl::ast::Domain& domain = **current;

    // Merge contents while avoiding duplicates.
    appendIfNoEquivalent(result->requirements, domain.requirements);
    appendDeepCopyIfNoEquivalent<pddl::ast::PrimitiveTypeDeclarationPointer>(
          result->types, domain.types, context);
    appendDeepCopyIfNoEquivalent<pddl::ast::ConstantDeclarationPointer>(
          result->constants, domain.constants, context);
    appendDeepCopyIfNoEquivalent<pddl::ast::PredicateDeclarationPointer>(
          result->predicates, domain.predicates, context);
    appendDeepCopyIfNoEquivalent<pddl::ast::ActionPointer>(
          result->actions, domain.actions, context);
  }
  return result;
}

template<typename DescriptionIterator>
pddl::ast::DescriptionPointer mergeDescriptions(
    DescriptionIterator begin,
    DescriptionIterator end)
{
  if (begin == end)
    return {};

  auto result = std::make_unique<pddl::ast::Description>();

  // Merge domains first.
  auto domainRange = boost::adaptors::transform(
        boost::make_iterator_range(begin, end),
        [](auto descriptionPtr)
            -> const pddl::ast::Domain*
  {
    if (!descriptionPtr || !descriptionPtr->domain)
      return nullptr;
    return descriptionPtr->domain.get();
  });

  auto domain = mergeDomains(boost::begin(domainRange), boost::end(domainRange));
  std::swap(domain, result->domain);

  // Then merge problems.
  pddl::ast::CopyContext context;
  context.types = &result->domain->types;
  context.constants = &result->domain->constants;
  context.predicates = &result->domain->predicates;

  unsigned i = 0u;
  for (auto current = begin; current != end; ++current)
  {
    if (!*current)
    {
      qiLogDebug("SemanticAgent.PDDL") << "Merging problem #" << i++
                                       << ": (empty)";
      continue;
    }

    const pddl::ast::Description& currentDescription = **current;
    if (currentDescription.problem)
    {
      if (!result->problem)
      {
        auto problem = std::make_unique<pddl::ast::Problem>(result->domain.get());
        problem->name = "merged_problem";
        result->problem = std::move(problem);
      }
      qiLogDebug("SemanticAgent.PDDL")
          << "Merging problem #" << i++ << ": "
          << serializePddl(*currentDescription.problem.value());

      const auto& currentProblem = *currentDescription.problem.value();
      auto& mergedProblem = *result->problem.value();

      // Merge contents while avoiding duplicates.
      appendIfNoEquivalent(mergedProblem.requirements, currentProblem.requirements);
      appendDeepCopyIfNoEquivalent<pddl::ast::ConstantDeclarationPointer>(
            mergedProblem.objects, currentProblem.objects, context);
      appendDeepCopyIfNoEquivalent<pddl::ast::Fact>(
            mergedProblem.initialState.facts, currentProblem.initialState.facts, context);
      mergedProblem.goal = mergeGoals(
            mergedProblem.goal, currentProblem.goal,
            *mergedProblem.domain);
    }
    else
    {
      qiLogDebug("SemanticAgent.PDDL") << "Merging problem #" << i++
                                       << ": (empty)";
    }
  }

  return result;
}

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_PDDL_PDDL_EDIT_HPP
