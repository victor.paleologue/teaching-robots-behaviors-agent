#include "pddl_read.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{

bool isEquivalent(
    const pddl::ast::PrimitiveType& lhsType,
    const pddl::ast::PrimitiveType& rhsType)
{
  return isEquivalent(*lhsType.declaration, *rhsType.declaration);
}


bool isEquivalent(
    const pddl::ast::PrimitiveTypeDeclaration& lhs,
    const pddl::ast::PrimitiveTypeDeclaration& rhs)
{
  if (lhs.name != rhs.name)
    return false;
  if (lhs.parentTypes.size() != rhs.parentTypes.size())
    return false;
  for (size_t i = 0u; i < lhs.parentTypes.size(); ++i)
  {
    const auto& lhsParentType = *lhs.parentTypes[i];
    const auto& rhsParentType = *rhs.parentTypes[i];
    assert(lhsParentType.declaration);
    assert(rhsParentType.declaration);
    if (!isEquivalent(*lhsParentType.declaration, *rhsParentType.declaration))
      return false;
  }
  return true;
}


bool isEquivalent(
    const pddl::ast::ConstantDeclaration& lhs,
    const pddl::ast::ConstantDeclaration& rhs)
{
  if (lhs.name != rhs.name)
    return false;
  return isEquivalent(lhs.type, rhs.type);
}


bool isEquivalent(
    const pddl::ast::VariableDeclaration& lhs,
    const pddl::ast::VariableDeclaration& rhs)
{
  if (lhs.name != rhs.name)
    return false;
  return isEquivalent(lhs.type, rhs.type);
}


bool isEquivalent(
    const pddl::ast::PredicateDeclaration& lhs,
    const pddl::ast::PredicateDeclaration& rhs)
{
  if (lhs.name != rhs.name)
    return false;
  if (lhs.parameters.size() != rhs.parameters.size())
    return false;
  for (size_t i = 0u; i < lhs.parameters.size(); ++i)
  {
    const auto& lhsParameter = *lhs.parameters[i];
    const auto& rhsParameter = *rhs.parameters[i];
    // Note that names do not make predicates distinct. Only type matters.
    if (!isEquivalent(lhsParameter.type, rhsParameter.type))
      return false;
  }
  return true;
}


bool isEquivalent(
    const pddl::ast::Constant& lhs,
    const pddl::ast::Constant& rhs)
{
  return isEquivalent(*lhs.declaration, *rhs.declaration);
}


bool isEquivalent(
    const pddl::ast::Variable& lhs,
    const pddl::ast::Variable& rhs)
{
  return isEquivalent(*lhs.declaration, *rhs.declaration);
}


bool isEquivalent(
    const pddl::ast::Predicate& lhs,
    const pddl::ast::Predicate& rhs)
{
  if (!isEquivalent(*lhs.declaration, *rhs.declaration))
    return false;
  if (lhs.arguments.size() != rhs.arguments.size())
    return false;
  for (size_t i = 0u; i < lhs.arguments.size(); ++i)
  {
    const auto& lhsArgument = lhs.arguments[i];
    const auto& rhsArgument = rhs.arguments[i];
    if (!isEquivalent(lhsArgument, rhsArgument))
      return false;
  }
  return true;
}


bool isEquivalent(
    const pddl::ast::Action& lhs,
    const pddl::ast::Action& rhs)
{
  if (lhs.name != rhs.name)
    return false;
  if (!isEquivalent(lhs.parameters, rhs.parameters))
    return false;
  if (!isEquivalent(lhs.precondition, rhs.precondition))
    return false;
  if (!isEquivalent(lhs.effect, rhs.effect))
    return false;
  return true;
}

} // agent
} // semantic
} // qi
