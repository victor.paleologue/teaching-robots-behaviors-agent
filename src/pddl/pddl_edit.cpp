#include "pddl_edit.hpp"
#include "pddl_read.hpp"

using namespace pddl::ast;

namespace qi
{
namespace semantic
{
namespace agent
{
/// Find an element from a container according to its name attribute.
template <typename ElementType>
ElementType* findElementByName(
    const std::vector<std::unique_ptr<ElementType>>& container,
    const std::string& name)
{
  auto found = std::find_if(
        container.begin(), container.end(),
        [&](const std::unique_ptr<ElementType>& element)
  {
    return element->name == name;
  });

  if (found != container.end())
    return found->get();
  else
    return nullptr;
}


pddl::ast::Domain& ensureDomain(
    pddl::ast::Description& description,
    const std::string& name)
{
  if (!description.domain)
    description.domain = std::make_unique<Domain>();
  auto& domain = *description.domain;
  domain.name = name.empty() ? "generated_domain" : name;
  return domain;
}


pddl::ast::Problem& ensureProblem(
    pddl::ast::Description& description,
    const std::string& name)
{
  auto& domain = ensureDomain(description);
  if (!description.problem)
    description.problem = std::make_unique<Problem>(&domain);
  auto& problem = *description.problem.value();
  problem.name = name.empty() ? "generated_problem" : name;
  return problem;
}


/// Throws if the name is empty.
static inline void checkName(const std::string& type, const std::string& name)
{
  if (name.empty())
    throw std::invalid_argument(std::string("empty ") + type + " name");
}


/// Ensures a type is declared.
pddl::ast::PrimitiveTypeDeclaration* ensureType(
    pddl::ast::Description& description,
    const std::string& name)
{
  checkName("type", name);
  auto& domain = ensureDomain(description);
  auto& existingTypes = domain.types;
  auto typeDeclaration = findElementByName(existingTypes, name);
  if (typeDeclaration)
    return typeDeclaration;

  existingTypes.emplace_back(new PrimitiveTypeDeclaration(name));
  return existingTypes.back().get();
}


pddl::ast::PredicateDeclaration* ensurePredicate(
    pddl::ast::Description& description,
    const std::string& name,
    const std::vector<std::pair<std::string, std::string> >& varNamesAndTypes)
{
  checkName("predicate", name);
  auto& domain = ensureDomain(description);
  VariableDeclarations vars;
  for (const auto& nameAndType: varNamesAndTypes)
  {
    const auto& name = nameAndType.first;
    const auto& type = nameAndType.second;
    if (type.empty())
    {
      vars.push_back(std::make_unique<VariableDeclaration>(name));
    }
    else
    {
      auto* typeDeclaration = ensureType(description, type);
      vars.emplace_back(
            std::make_unique<VariableDeclaration>(
              name, std::make_unique<PrimitiveType>(typeDeclaration)));
    }
  }

  auto predicate = std::make_unique<PredicateDeclaration>(name, std::move(vars));

  auto found = std::find_if(
    domain.predicates.begin(), domain.predicates.end(),
    [&](const PredicateDeclarationPointer& existingElement)
  {
    return isEquivalent(existingElement, predicate);
  });

  if (found != domain.predicates.end())
    return found->get();

  domain.predicates.emplace_back(std::move(predicate));
  return domain.predicates.back().get();
}


/// Make an entity attached to a description.
/// It can be added as a constant or as an object later.
std::unique_ptr<ConstantDeclaration> _makeObject(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type)
{
  // If a type is assigned, ensure the existence of the type.
  PrimitiveTypeDeclaration* typeDeclaration = nullptr;
  if (!type.empty())
  {
    if (!description.domain)
      description.domain = std::make_unique<Domain>();
    auto& existingTypes = description.domain->types;
    typeDeclaration = findElementByName(existingTypes, type);
    if (!typeDeclaration)
    {
      existingTypes.emplace_back(new PrimitiveTypeDeclaration(type));
      typeDeclaration = existingTypes.back().get();
    }
  }

  // Add the object with the given (optional) type.
  std::experimental::optional<PrimitiveTypePointer> typePtr;
  if (typeDeclaration)
    typePtr = std::make_unique<PrimitiveType>(typeDeclaration);

  return std::make_unique<ConstantDeclaration>(name, std::move(typePtr));
}


/// Check-less version for adding object to a description.
/// Problem must exist, and object should not be already present.
std::pair<ConstantDeclaration*, PrimitiveTypeDeclaration*> _addObject(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type)
{
  ensureProblem(description);
  auto& problem = *description.problem.value();
  problem.objects.push_back(_makeObject(description, name, type));
  auto result = std::make_pair(
        problem.objects.back().get(),
        static_cast<PrimitiveTypeDeclaration*>(nullptr));
  if (!result.first->type)
    return result;
  auto typePtr = result.first->type.value().get<PrimitiveTypePointer>().get();
  result.second = typePtr->declaration;
  return result;
}


pddl::ast::ConstantDeclaration* ensureObject(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type)
{
  checkName("object", name);
  auto& problem = ensureProblem(description);
  auto* element = findElementByName(problem.objects, name);
  if (element)
    return element;
  return _addObject(description, name, type).first;
}


pddl::ast::ConstantDeclaration* ensureConstant(
    pddl::ast::Description& description,
    const std::string& name,
    const std::string& type)
{
  checkName("object", name);
  auto& domain = ensureDomain(description);
  auto* element = findElementByName(domain.constants, name);
  if (element)
    return element;
  domain.constants.push_back(_makeObject(description, name, type));
  return domain.constants.back().get();
}


std::pair<ConstantDeclaration*, PrimitiveTypeDeclaration*>
addObject(
    Description& description,
    const std::string& name,
    const std::string& type)
{
  checkName("object", name);
  auto& problem = ensureProblem(description);
  auto* element = findElementByName(problem.objects, name);
  if (element)
    throw std::runtime_error(
        std::string("object named \"") + name + "\" already exists");
  return _addObject(description, name, type);
}


pddl::ast::ConstantDeclaration*
addObject(
    pddl::ast::Description& description,
    const std::string& name)
{
  return addObject(description, name, "").first;
}


std::experimental::optional<Goal> mergeGoals(
    const std::experimental::optional<Goal>& goal1,
    const std::experimental::optional<Goal>& goal2,
    pddl::ast::Domain& context)
{
  pddl::ast::CopyContext copyContext;
  copyContext.types = &context.types;
  copyContext.constants = &context.constants;
  copyContext.predicates = &context.predicates;

  if (!goal1 && !goal2)
    return {};
  if (goal1 && !goal2)
    return deepCopy(goal1, copyContext);
  if (!goal1 && goal2)
    return deepCopy(goal2, copyContext);
  QI_ASSERT(goal1 && goal2);

  auto result = std::make_unique<And<Precondition>>(Preconditions());

  auto appendIfNoEquivalent = [&](const Precondition& precondition)
  {
    if (!hasEquivalentElement(result->arguments, precondition))
      result->arguments.push_back(deepCopy(precondition, copyContext));
  };

  auto appendGoalToResult = [&](const Goal& goal)
  {
    if (goal.is<AndPointer<Precondition>>())
    {
      const auto& asAndPtr =
          goal.get_unchecked<AndPointer<Precondition>>();
      for (const auto& element: asAndPtr->arguments)
        appendIfNoEquivalent(element);
    }
    else
    {
      appendIfNoEquivalent(goal);
    }
  };

  appendGoalToResult(goal1.value());
  appendGoalToResult(goal2.value());
  return Goal(std::move(result));
}

} // agent
} // semantic
} // qi
