#ifndef SEMANTICAGENT_CHAT_HPP
#define SEMANTICAGENT_CHAT_HPP

#include <memory>
#include <unordered_set>
#include <qi/future.hpp>
#include <qi/session.hpp>
#include <qi/context/context.hpp>
#include <qi/conversation/conversation.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <src/agent/agent_p.hpp>

namespace qi
{
class Strand;

namespace semantic
{
namespace agent
{
class ActionPool;
class Extractor;
struct SemanticMemoryContext;

/**
 * An implementation of a chat action relying on the semantic engine
 * to select what to do. May give the focus away to other actions if requested
 * by the user.
 * @todo Make it runnable only once, and make sure nothing can be emitted before running.
 */
class ChatImpl
{
public:
  /**
   * @param context A Qi Context authorizing the action.
   * @param strand The same strand the extractor works on.
   * @param session A connected session.
   * @param semanticMemoryContext A context for semantic extraction.
   * @param extractor An extractor that is ready.
   * @param chatting The promise to track the chat
   * @param behaviors The list of behaviors to run in the action
   */
  ChatImpl(
      context::ContextPtr context,
      std::shared_ptr<Strand> strand,
      SessionPtr session,
      std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
      qi::Object<knowledge::EditableKnowledgeBase> knowledge,
      std::shared_ptr<Extractor> extractor,
      SpeechSourcePtr speechSource,
      conversation::SpeechEnginePtr speechEngine,
      Promise<void>& chatting,
      std::vector<PDDLAwareQiBehaviorPtr> behaviors,
      std::unordered_set<AnyObject> actionFactories,
      std::shared_ptr<ActionPool> actionPool);

  virtual ~ChatImpl();
  Future<void> run();
  Signal<> started;

  Signal<conversation::Phrase> heard;
  Property<conversation::Phrase> saying;

  Property<std::string> language; // TODO: make it useful
  Property<float>& confidenceThreshold;

  // From Chat interface
  Property<bool> listening;
  Property<bool> hearing;
  Property<conversation::BodyLanguageOption> listeningBodyLanguage; // TODO: support
  Signal<> noPhraseRecognized; // TODO: emit
  Signal<conversation::Phrase> normalReplyFoundFor; // TODO: emit
  Signal<conversation::Phrase> fallbackReplyFoundFor; // TODO: emit
  Signal<conversation::Phrase> noReplyFoundFor; // TODO: emit

  Property<std::vector<BehaviorControlPtr>> behaviorControls;

private:
  struct Private;
  std::unique_ptr<Private> _p;
};
} // agent
} // semantic
} // qi

QI_REGISTER_IMPLEMENTATION_H(qi::semantic::agent::Chat, qi::semantic::agent::ChatImpl)

#endif // SEMANTICAGENT_CHAT_HPP
