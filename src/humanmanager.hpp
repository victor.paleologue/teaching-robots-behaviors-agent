#ifndef SEMANTICAGENT_HUMAN_MANAGER_HPP
#define SEMANTICAGENT_HUMAN_MANAGER_HPP

#include <boost/date_time/posix_time/ptime.hpp>
#include <qi/locale/locale.hpp>
#include <qi/knowledge/knowledge.hpp>
#include <qi/humanawareness/humanawareness.hpp>
#include "humanrecognizer.hpp"


namespace qi
{
namespace semantic
{
namespace agent
{
/// Maintains a cache of information
/// associating humans to nodes in the knowledge,
/// and leverages human recognition to maintain
/// the association.
/// TODO: most API return degenerated values instead of optionals or throwing, fix this
class HumanManager
{
public:
  /// Prune all human objects that are not around anymore.
  /// The humans resources and their recognition model are kept.
  virtual void removeLostHumans() = 0;

  /// Ensure a resource node for the given human object, and return it.
  /// Human recognition is used to find the right node.
  /// If the human is not known, this functions learns the new human.
  /// If the human is already known, no reinforcement happens.
  // TODO: split this function between the resolver and the trainer.
  virtual knowledge::ResourceNode getResNode(const human::HumanPtr&) = 0;

  /// Tries to return the human associated to the given resource node.
  /// \return An human object. May be null if not found.
  virtual human::HumanPtr getHumanPtr(const knowledge::ResourceNode&) = 0;

  /// Looks for a resource node with the given URI.
  /// If it is not found, an invalid resource node is returned.
  virtual knowledge::ResourceNode resourceFromIRI(const std::string& iri) = 0;

  /// Find the resource node associated to the given label.
  /// If several resources have the same label, the first one found is returned.
  /// If it is not found, an invalid resource node is returned.
  virtual knowledge::ResourceNode getResNodeFromName(const std::string& label) = 0;

  /// Register a label for the given resource node.
  virtual void addName(
      const knowledge::ResourceNode& node,
      const std::string& name,
      const qi::locale::Locale& locale) = 0;

  /// Adds in the knowledge a colocation relation between the given resource nodes.
  virtual void colocalize(const knowledge::ResourceNode&, const knowledge::ResourceNode&) = 0;

  /// Update the association between a human object and a resource node.
  /// This removes the previous resource node,
  /// but does not copy all the relations that was directed to it.
  virtual bool changeResNode(const human::HumanPtr&, const knowledge::ResourceNode&) = 0;

  /// Runs a punctual engage action, that stops as soon as the engagement is effective.
  virtual Future<void> engage(const human::HumanPtr& humanPtr) = 0;

  /// Return the time of occurrence an arbitrary event implying the social agent as a subject.
  // TODO: it was meant to be the time of last greeting of the social agent.
  virtual boost::posix_time::ptime getTimestamp(
            const knowledge::ResourceNode& socialAgent,
            const knowledge::EditableKnowledgeGraphPtr&) = 0;

  /// Produce an event that occurs now, implying the social agent as a subject.
  virtual void setTimestamp(
      const knowledge::ResourceNode&,
      knowledge::EditableKnowledgeGraphPtr,
      const std::string&) = 0;
};

std::unique_ptr<HumanManager> makeHumanManager(
    std::shared_ptr<Strand> strand,
    context::ContextPtr contextPtr,
    SessionPtr sessionPtr,
    humanawareness::HumanAwarenessPtr humanAwarenessPtr,
    knowledge::EditableKnowledgeBasePtr editableKnowledgeBasePtr,
    std::shared_ptr<HumanRecognizer> humanRecognizer = {});
} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_HUMAN_MANAGER_HPP
