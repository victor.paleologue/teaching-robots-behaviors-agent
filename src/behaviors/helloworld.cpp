#include "learn_behavior.hpp"

#include <functional>
#include <random>

#include <boost/scope_exit.hpp>

#include <qi/log.hpp>

#include <qisem/semanticdebugger/printer/semexplinestostr.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpsimplifer.hpp>
#include <qisem/semanticcontroller/tool/semexpagreementdetector.hpp>
#include <qisem/linguisticanalyzer/printer/expressionprinter.hpp>
#include <qisem/linguisticanalyzer/tool/semexpmodifier.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticagentgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticstatementgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semantictextgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>
#include <qisem/semanticmodel/utility/uppercasehandler.hpp>

#include "../behaviormodel/behavior.hpp"
#include "../utility/semantic_helpers.hpp"
#include "../language.hpp"
#include "reaction.hpp"
#include "semantic_behaviors.hpp"

qiLogCategory("SemanticAgent.HelloWorld");

namespace qi
{
namespace semantic
{
namespace agent
{
using namespace qisem;


Future<PDDLAwareQiBehaviorPtr> makeHelloworld(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext)
{
    qiLogInfo() << "Building Helloworld behavior!";

    auto action = [=](
            const PhraseAndLocale& /*phrase*/,
            const SemanticExpression& /*k*/)
    {
      auto helloGrounding = mystd::make_unique<SemanticTextGrounding>("Hello World!");
      auto reaction = mystd::make_unique<GroundedExpression>(std::move(helloGrounding));
      return reaction;
    };

    return makeSemanticBehavior(
          "To say Hello World!",
          speechSource, speechEngine, session, context, semanticMemoryContext, std::move(action),
          "");
}

} // agent
} // semantic
} // qi
