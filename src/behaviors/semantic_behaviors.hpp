#ifndef SEMANTICAGENT_SEMANTIC_BEHAVIORS_HPP
#define SEMANTICAGENT_SEMANTIC_BEHAVIORS_HPP

#include <functional>
#include <memory>
#include <qi/session.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <semanticnaoqilinker/qivirtualexecutor.hpp>
#include "../behaviormodel/behavior.hpp"
#include "../utility/incontext.hpp"
#include "semantic_expression_association.hpp"

namespace qisem
{
  struct SemanticExpression;
  struct SemanticMemory;

  namespace linguistics
  {
    struct LinguisticDatabase;
  } // linguistics
} // qisem

namespace qi
{
namespace semantic
{
namespace agent
{
struct SemanticMemoryContext;

/// A semantic event, expressed as a semantic expression associated to the source phrase.
struct SemanticEvent
{
  PhraseAndLocale phrase;
  std::shared_ptr<qisem::SemanticExpression> expression;
  knowledge::ResourceNode communicationActNode;
};


/// Emits a list of possible semantic events, ordered by decreasing estimated probability.
using SemanticExtractor = Signal<const std::vector<SemanticEvent>&>;


/// Look for a an extractor providing semantic expressions from knowledge events.
/// If no extractor can be found for this knowledge, a new one is created.
// Several behaviors may rely on the same result of semantic extraction.
// A result is the same if it comes from the same source.
std::shared_ptr<SemanticExtractor> ensureSemanticExtractor(
    const SpeechSourcePtr& speechSource,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext);


/// A function that "performs" the given semantic knowledge.
Future<void> runSemanticResponse(
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<qisem::SemanticExpression> reactionExpression,
    locale::Locale locale,
    knowledge::ResourceNode inResponseTo = {},
    knowledge::EditableKnowledgeGraphPtr graph = {},
    knowledge::ResourceNode responseResource = {});


/// A function to fill a semantic memory with a stream of inputs.
void informFromConditionalTxt(
    qisem::SemanticMemory& memory,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    const std::string& filepath);


/// A function to prepare a semantic memory that knows all the primitive actions.
std::unique_ptr<qisem::SemanticMemory> makeMemoryKnowingCommands(
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase);


/// A function to prepare a semantic memory that knows all the primitive actions,
/// and some basic information like who the agent is, and what it likes.
std::unique_ptr<qisem::SemanticMemory> makeMemoryKnowingCommandsAndInfos(
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase);


/// Produces a PDDL snippet presenting an action that is a response to some communication.
std::string makePDDLReplyingToCommunicationAct(
    const std::string& phrase,
    const knowledge::ResourceNode& inputAct,
    const knowledge::ResourceNode& outputAct);


/// Produces a PDDL snippet presenting an action that is a response to some communication,
/// but stipulates that the input was not actually understood.
std::string makePDDLReplyingToMisunderstoodCommunicationAct(
    const std::string& phrase,
    const knowledge::ResourceNode& inputAct,
    const knowledge::ResourceNode& outputAct);


static const std::string kQiActionResourceLabel = "QiAction";


/// The agent's semantic executer class.
class SemanticExpressionExecuter:
    public qisem::QiVirtualExecutor,
    public SafeContext
{
public:
  SemanticExpressionExecuter(
      const SessionPtr& session,
      const context::ContextPtr context,
      std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
      conversation::SpeechEnginePtr speechEngine);

  ~SemanticExpressionExecuter() override;

  /// Add an action so that the executer can run it when encountered.
  /// The ptrUid() of the action is used to identify it in resource groundings.
  OwnerPtr addAction(AnyObject action);

protected:
  void _usageOfMemory(std::function<void(qisem::SemanticMemory&)> /*pFunction*/) override;
  void _usageOfMemblock(std::function<void(const qisem::SemanticMemoryBlock&, const std::string&)> pFunction) override;
  void _usageOfLingDb(std::function<void(const qisem::linguistics::LinguisticDatabase&)> pFunction) override;

  /// How words are communicated.
  qisem::FutureVoid _exposeText(
      const std::string &phrase,
      qisem::SemanticLanguageEnum,
      const qisem::FutureVoid& pStopRequest) override;

  /// How arbitrary objects are interpreted.
  qisem::FutureVoid _exposeResource(
      const qisem::SemanticResourceGrounding& pResourceGrd,
      const qisem::FutureVoid&);

private:
  std::shared_ptr<SemanticMemoryContext> _semanticMemoryContext;
  std::unordered_map<std::string, AnyObject> _actionById;
  conversation::SpeechEnginePtr _speechEngine;
};


/// Make a single action encoded in a semantic knowledge.
ActionObj makeSemanticAction(
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<SemanticExpressionExecuter> semanticExecuter,
    std::unique_ptr<qisem::SemanticExpression> reaction,
    locale::Locale locale,
    knowledge::EditableKnowledgeGraphPtr graph = {},
    const knowledge::ResourceNode& inResponseTo = {},
    std::function<Future<void>()> continuation = {});

/// Make a single action encoded in a semantic knowledge.
/// Also supports a custom name.
ActionObj makeSemanticAction(const std::string& name,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<SemanticExpressionExecuter> semanticExecuter,
    std::unique_ptr<qisem::SemanticExpression> reaction,
    locale::Locale locale,
    knowledge::EditableKnowledgeGraphPtr graph,
    const knowledge::ResourceNode& inResponseTo,
    std::function<Future<void>()> continuation);


/// A function that returns an action to execute as
/// a reaction to given phrase, locale and semantic expression.
using SemanticActionResponseFunction =
    std::function<ActionObj(const std::vector<SemanticEvent>&)>;


/// Make a behavior that suggest actions based on the latest semantic input.
/// The semanticReaction function is guaranteed to be called with
/// a non-empty list of events.
Future<PDDLAwareQiBehaviorPtr> makeSemanticBehavior(
    const std::string& name,
    SpeechSourcePtr speechSource,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    SemanticActionResponseFunction semanticReaction,
    const std::string& pddl = "");


/// A function that returns a semantic expression to execute as
/// a reaction to given phrase, locale and semantic expression.
using SemanticExpressionResponseFunction =
    std::function<std::unique_ptr<qisem::SemanticExpression>(
        const PhraseAndLocale&, const qisem::SemanticExpression&)>;


/// Make a behavior that suggest semantic-based actions in reaction semantic events.
/// Takes a function that accepts semantic expression and returns a semantic expression in return.
/// If the return is a nullptr, the no action will be suggested.
/// The function is executed in the context of the semantic memory,
/// so that it is safe to use it.
Future<PDDLAwareQiBehaviorPtr> makeSemanticBehavior(
    const std::string& name,
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    SemanticExpressionResponseFunction semanticReaction,
    const std::string& pddl = "",
    knowledge::EditableKnowledgeGraphPtr graph = {});


/// Creates a behavior that introspects known behaviors.
Future<PDDLAwareQiBehaviorPtr> makeIntrospectBehavior(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<std::list<qi::semantic::agent::SemanticExpressionAssociation> > taughtBehaviors);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_SEMANTIC_BEHAVIORS_HPP
