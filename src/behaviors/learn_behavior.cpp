#include "learn_behavior.hpp"

#include <functional>
#include <random>

#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <ka/scoped.hpp>

#include <qi/log.hpp>
#include <qi/jsoncodec.hpp>

#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <qi/teaching/ontology.hpp>

#include <qisem/semanticdebugger/printer/semexplinestostr.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semexpsimplifer.hpp>
#include <qisem/semanticcontroller/tool/semexpagreementdetector.hpp>
#include <qisem/linguisticanalyzer/printer/expressionprinter.hpp>
#include <qisem/linguisticanalyzer/tool/semexpmodifier.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticagentgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticmetagrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticstatementgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semantictextgrounding.hpp>
#include <qisem/linguisticanalyzer/dbtype/textprocessingcontext.hpp>
#include <qisem/semanticmodel/utility/uppercasehandler.hpp>

#include "../behaviormodel/behavior.hpp"
#include "../utility/knowledge.hpp"
#include "../utility/semantic_helpers.hpp"
#include "../language.hpp"
#include "reaction.hpp"
#include "semantic_behaviors.hpp"

qiLogCategory("SemanticAgent.LearnBehavior");

namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
using namespace qi::knowledge;
using namespace qi::teaching;
using namespace qisem;

std::unique_ptr<SemanticExpressionAssociation> extractActionEquivalence(
    const GroundedExpression& groundedExpression,
    std::list<SemanticExpressionAssociation>& existingActionEquivalences,
    const linguistics::LinguisticDatabase& linguisticDatabase,
    const SemanticMemory& semanticMemory)
{
  // grounded expression must have a subject
  auto childIt = groundedExpression.children.find(GrammaticalType::SUBJECT);
  if (childIt == groundedExpression.children.end())
    return nullptr;
  auto& subject = *childIt->second;

  // subject must be a verb in the infinitive form
  auto subjectStatement = asActionDeclaration(subject);
  if (!subjectStatement)
    return nullptr;

  // grounded expression must have an object
  childIt = groundedExpression.children.find(GrammaticalType::OBJECT);
  if (childIt == groundedExpression.children.end())
    return std::unique_ptr<SemanticExpressionAssociation>{};
  auto& object = *childIt->second;

  // check that the object is a list of actions
  auto maybeListOfActionsPtr = object.clone();
  auto subGroundedExpressionPtrs = SemExpModifier::listTopGroundedExpressionsPtr(*maybeListOfActionsPtr);
  for (auto& subGroundedExpressionPtr: subGroundedExpressionPtrs)
  {
    // check if the expression is a taught action
    auto actionEquivalenceIt = std::find_if(
          existingActionEquivalences.begin(), existingActionEquivalences.end(),
          [&](const SemanticExpressionAssociation& association)
    {
      return matchVerbalGroup(
            *subGroundedExpressionPtr, *association.first,
            semanticMemory, linguisticDatabase);
    });
    if (actionEquivalenceIt != existingActionEquivalences.end())
      continue;

    // else, check if the verb is a primitive action
    SemExpModifier::addChild(
          *subGroundedExpressionPtr,
          GrammaticalType::SUBJECT,
          mystd::make_unique<GroundedExpression>(SemanticAgentGrounding::getRobotAgentPtr()));
    SemExpModifier::addRequest(*subGroundedExpressionPtr, SemanticRequestType::ACTION);

    auto category = memoryOperation::categorize(*subGroundedExpressionPtr);
    if (category == SemanticExpressionCategory::COMMAND)
      continue; // it's ok, let's check next item

    // else, we did not recognize an action, we do not support this teaching
    return std::unique_ptr<SemanticExpressionAssociation>{};
  }

  return std::unique_ptr<SemanticExpressionAssociation>{
    new SemanticExpressionAssociation{subject.clone(), object.clone()}};
}

std::unique_ptr<SemanticExpression> acknowledgeLearnedBehavior(
    const SemanticExpression& behaviorLabel,
    const SemanticExpression& recipeExpression)
{
  // responding "<agreement>, <behavior label> <equivalence> <recipe>"
  // ---------------------------------------------------------------
  // reformulation of the action equivalence
  auto equivalenceStatement = mystd::make_unique<SemanticStatementGrounding>();
  equivalenceStatement->concepts.emplace(ConceptSet::conceptVerbEquality, 5);
  equivalenceStatement->verbTense = SemanticVerbTense::PRESENT;
  auto reformulation = mystd::make_unique<GroundedExpression>(std::move(equivalenceStatement));
  reformulation->children.emplace(GrammaticalType::SUBJECT, behaviorLabel.clone());
  reformulation->children.emplace(GrammaticalType::OBJECT, recipeExpression.clone());

  // wrapping it in an agreement
  return makeAgreementExpression(std::move(reformulation));
}
} // anonymous

std::unique_ptr<SemanticExpression> learnAction(
    const SemanticExpression& expression,
    std::list<SemanticExpressionAssociation>& existingActionEquivalences,
    const linguistics::LinguisticDatabase& linguisticDatabase,
    const SemanticMemory& semanticMemory)
{
  // if we have an enumeration at the top level that we can merge back, let's do it
  // TODO: remove this and do not split the enumeration at extraction level
  UniqueSemanticExpression simplifiedExpression{expression.clone()};
  simplifier::process(simplifiedExpression, semanticMemory, linguisticDatabase, true);

  /*
   * We are looking for the following structure here:
   * {verb(infinitive or present participle) [complements]} is {action description}
   */
  auto groundedExpressionPtr = asEquivalenceStatementExpression(*simplifiedExpression);
  if (!groundedExpressionPtr)
    return nullptr;

  auto actionEquivalence = extractActionEquivalence(
        *groundedExpressionPtr, existingActionEquivalences, linguisticDatabase, semanticMemory);

  if (!actionEquivalence)
    return nullptr;
  existingActionEquivalences.push_front(
        {std::move(actionEquivalence->first),
        std::move(actionEquivalence->second)});
  auto& latestActionEquivalence = existingActionEquivalences.front();

  return acknowledgeLearnedBehavior(
        *latestActionEquivalence.first, *latestActionEquivalence.second);
}

// A recipe is a chain of infinitive statements (for now), aka "task declaration (td)"
// The canonical form is <td>{, <td>}{ and <td}; it is an enumeration of tds.
// Saying a recipe in several sentences can be expected to keep the tds atomic,
// but the enumeration could be splitted.
// The possible splits are:
// - <td>
// - <td>, <td>{, <td>} (recognizable enumeration, suggests it is not over)
// - and <td> (end of enumeration, suggests it is over)
// - {<td>, }<td> and <td> (recognizable enumeration, suggests it is over)
// Note that enumeration items might in fact be separated indiferrently by "and", "then"
// or ",", and it should be supported. Only, "and" suggests it is over.

// A task association should associate a td to a recipe. Note that a recipe with
// a single td is valid. The canonical form of the association is <td> is <recipe>.
// It can be split as following:
// - <td>
// - is
// - <recipe>, and recursively any split, let's call it <recipe_split>
// - <td> is
// - <td> is <recipe_split>
// - is <recipe_split>
// To recognize a task association, the recipe splits must be understood too.
// This should be part of the interface between the recipe recognition algorithm
// and the task association one.
// More trivially, the td recognition would be an important part of the algorithm.

namespace
{

/// All the information about an interaction data->
struct LearnBehaviorData
{
  /// The previous state that lead to the current data.
  std::shared_ptr<LearnBehaviorData> previousData;

  /// The last phrase that lead to this data.
  PhraseAndLocale phrase;

  /// The reaction to this data change.
  std::unique_ptr<SemanticExpression> reaction;

  /// A verb label we recognized, but for which no role was assigned yet.
  std::unique_ptr<SemanticExpression> unassignedTaskDeclaration;

  /// Behavior verb label.
  std::unique_ptr<SemanticExpression> behaviorLabel;

  /// The recipe in terms of a list of semantic expressions.
  std::list<std::unique_ptr<SemanticExpression>> recipe;

  /// Whether the next sentence should complete the last one.
  bool acceptContinuation = true;

  /// Whether the next sentence should complete the last one.
  bool expectContinuation = false;

  /// The next utterance should definitely be a task declaration serving as the label.
  bool expectBehaviorLabel = false;

  /// The next utterance should definitely be a task declaration serving as the next part
  /// of the current recipe.
  bool expectRecipeElement = false;

  /// According to latest utterances, we're listening to the recipe, but we cannot tell
  /// if next utterance will ultimately be a recipe element.
  bool inRecipeEnumeration = false;

  /// When we're asking whether the teaching is finished, this is true.
  bool askingIfFinished = false;

  inline void checkConsistency()
  {
    QI_ASSERT(!(!acceptContinuation && expectContinuation));
    QI_ASSERT(!(expectBehaviorLabel && expectRecipeElement));
    QI_ASSERT(!(expectBehaviorLabel && behaviorLabel));
    QI_ASSERT(!(unassignedTaskDeclaration && (expectRecipeElement || expectBehaviorLabel)));
    QI_ASSERT(!(unassignedTaskDeclaration && !expectContinuation));
    QI_ASSERT(!(unassignedTaskDeclaration && !recipe.empty()));
    QI_ASSERT(!(unassignedTaskDeclaration && inRecipeEnumeration));
    QI_ASSERT(!(inRecipeEnumeration && expectBehaviorLabel));
    QI_ASSERT(!(askingIfFinished && (expectContinuation || expectBehaviorLabel || expectRecipeElement)));
    if (previousData) // Check history consistency, because it can be changed
      previousData->checkConsistency();
  }
};


std::unique_ptr<SemanticExpression> tryLearnBehavior(
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    const linguistics::LinguisticDatabase& linguisticDatabase,
    const SemanticMemory& semanticMemory,
    std::list<SemanticExpressionAssociation>& taughtBehaviors,
    std::shared_ptr<LearnBehaviorData>& data,
    const PhraseAndLocale& phrase,
    const SemanticExpression& exp,
    std::weak_ptr<Signal<std::string, std::vector<std::string>>> behaviorLearnedSignal,
    std::shared_ptr<StoredPropertySet<PDDLAwareQiActionFactoryPtr>> actionFactories)
{
  // specific hackish commands
  std::string lowerCasePhrase;
  std::for_each(
        phrase.text.begin(), phrase.text.end(),
        [&](const char& c) {
      lowerCasePhrase.push_back(static_cast<char>(std::tolower(c)));
    }); // TODO: use a unicode locale

  static const std::set<std::string> stopUtterances = {
    "forget it", "stop it", "stop",
    "oublie ça", "oublie", "arrête"
  };
  if (stopUtterances.count(lowerCasePhrase))
  {
    data.reset();
    return makeAgreementExpression();
  }
  // The state of this processing is derived from the previous data
  auto previousData = std::move(data);
  data.reset(new LearnBehaviorData{});
  data->phrase = std::move(phrase);
  if (previousData)
  {
    if (previousData->unassignedTaskDeclaration)
      data->unassignedTaskDeclaration = previousData->unassignedTaskDeclaration->clone();
    if (previousData->behaviorLabel)
    data->behaviorLabel = previousData->behaviorLabel->clone();
    for (const auto& elt: previousData->recipe)
      data->recipe.push_back(elt->clone());
    data->acceptContinuation = previousData->acceptContinuation;
    data->expectContinuation = previousData->expectContinuation;
    data->expectBehaviorLabel = previousData->expectBehaviorLabel;
    data->expectRecipeElement = previousData->expectRecipeElement;
    data->inRecipeEnumeration = previousData->inRecipeEnumeration;
    data->askingIfFinished = previousData->askingIfFinished;
    data->previousData = std::move(previousData);
  }

  // Check consistency of the end state every time
  ka::scoped([&data]{ if (data) data->checkConsistency(); });

  //===========================================================================
  // Analyzing the utterance
  //===========================================================================
  // If the analysis discovers unsupported cases, it will give up the teaching
  // and return a null response.
  qiLogInfo() << "Analyzing \"" << data->phrase.text << "\"";

  if (exp.isEmpty())
  {
    qiLogInfo() << "This is not a full proposition, but keeping it in mind";
    data->reaction = nullptr;
    return nullptr;
  }

  bool knewLabel = data->behaviorLabel != nullptr;
  bool knewRecipe = !data->recipe.empty();

  // if we have an enumeration at the top level that we can merge back, let's do it
  // TODO: remove this and do not split the enumeration at extraction level
  UniqueSemanticExpression simplifiedExpression{exp.clone()};
  simplifier::process(simplifiedExpression, semanticMemory, linguisticDatabase, true);
  auto agreementValue = semExpAgreementDetector::semExpToAgreementValue(*simplifiedExpression);

  /*
   * We are looking for the following structure here:
   * {verb(infinitive or present participle) [complements]} is {action description}
   */
  // Check if this is an equivalence
  auto equivalenceExpressionPtr = asEquivalenceStatementExpression(*simplifiedExpression);
  if (equivalenceExpressionPtr)
  {
    qiLogInfo() << "This phrase is a \"be\" statement";
    // Check if it is a full action equivalence.
    // TODO: allow unresolved action equivalences.
    auto actionEquivalence = extractActionEquivalence(
          *equivalenceExpressionPtr, taughtBehaviors, linguisticDatabase, semanticMemory);
    if (actionEquivalence)
    { // TODO: allow extra verbs to be added at the end after this.
      qiLogInfo() << "This is a stand-alone behavior teaching!";
      if (data->behaviorLabel)
        qiLogInfo() << "Received an action equivalence during ongoing learning, overriding";
      data->behaviorLabel = actionEquivalence->first->clone();
      data->unassignedTaskDeclaration.reset();

      auto recipe = SemExpModifier::listTopGroundedExpressionsPtr(*actionEquivalence->second);
      QI_ASSERT(!recipe.empty() && "action equivalence extraction yielded something wrong");
      for (const auto& step: recipe)
        data->recipe.emplace_back(step->clone());
      data->acceptContinuation = true;
      data->expectContinuation = false;
      data->inRecipeEnumeration = true; // in that case it always finishes with the recipe
    }

    // TODO: conditional branching in the "extract action equivalence" algorithm rather than here
    else if (auto groundedExpressionPtr = equivalenceExpressionPtr->getGrdExpPtr())
    {
      if (data->expectBehaviorLabel)
      {
        qiLogInfo() << "The behavior label was expected, not a full equivalence, this is not understood";
        data->reaction = nullptr;
        return nullptr;
      }

      // It is not a complete action equivalence, so let's check if it is an incomplete one
      auto& groundedExpression = *groundedExpressionPtr;

      auto subjectIt = groundedExpression.children.find(GrammaticalType::SUBJECT);
      bool hasSubject = [&]
      {
        if (subjectIt == groundedExpression.children.end())
          return false;
        if (asCoreference(*subjectIt->second))
          return false;
        return true;
      }();

      auto objectIt = groundedExpression.children.find(GrammaticalType::OBJECT);
      bool hasObject = objectIt != groundedExpression.children.end();

      if (hasSubject && hasObject)
      {
        qiLogInfo() << "Either the subject or the object is not a task declaration, ignore it";
        data->reaction = nullptr;
        return nullptr;
      }

      // The subject must have preceded, otherwise it cannot be understood.
      // This check works only because it is the verb "to be", that cannot be used in the
      // passive form: the subject can only precede this utterance.
      if (hasObject)
      {
        const auto& object = *objectIt->second;

        // This should therefore be a continuation
        if (!data->expectContinuation)
        {
          qiLogInfo() << "This utterance looks like a continuation (preceding subject missing), but it was not expected, dropped";
          data->reaction = nullptr;
          return nullptr;
        }

        // By construction, a previous statement of a behavior label wouldn't be recognized,
        // but a recipe could (enumeration can be recognized).
        if (data->behaviorLabel)
        {
          qiLogInfo() << "A behavior label was in memory but it is not possible at this point, dropping";
          data.reset();
          return nullptr;
        }

        if (!data->recipe.empty())
        {
          // The object can only be the behavior label of the recipe.
          if (asActionDeclaration(object))
          {
            qiLogInfo() << "The recipe got its behavior label!";
            data->behaviorLabel = object.clone();
            data->acceptContinuation = false;
            data->expectContinuation = false;
          }
          else
          {
            qiLogInfo() << "This was not a piece of teaching, forget it";
            data.reset();
            return nullptr;
          }
        }

        else if (data->unassignedTaskDeclaration)
        { // There was a pending task declaration, and now we have an association.
          // It means it was the behavior label, and the rest should be the recipe.
          qiLogInfo() << "Previous task declaration was the behavior label, a recipe begins...";
          data->behaviorLabel = std::move(data->unassignedTaskDeclaration);

          // Is it a single action object?
          if (asActionDeclaration(object))
          {
            data->recipe.push_back(object.clone());
            data->acceptContinuation = true;
            data->expectContinuation = false;
            data->inRecipeEnumeration = false;
          }

          else if (auto* taskDeclarationList = asActionDeclarationList(object))
          {
            for (const auto& taskDeclaration: taskDeclarationList->elts)
            {
              data->recipe.push_back(taskDeclaration->clone());
            }
            data->acceptContinuation = true;
            data->expectContinuation = false;
            data->inRecipeEnumeration = true;
          }

          else
          { // It is an association with something of another nature.
            // Let us ignore it and hope that next utterance makes sense.
            data->reaction = nullptr;
            return nullptr;
          }
        }
      }

      // Object is lacking, and will be understood in next utterance.
      else if (hasSubject)
      {
        // subject must be a verb in the infinitive form
        const auto& subject = *subjectIt->second;
        auto subjectStatement = asActionDeclaration(subject);
        if (!subjectStatement)
        {
          qiLogInfo() << "Equivalence did not involve an action, dropped";
          data.reset();
          return nullptr;
        }

        // At this point, we have "{task declaration} is". This could occur:
        // a) at the end of a recipe.
        // b) at the beginning of a teaching
        // In any case, it results in expecting a next utterance.
        auto expectContinuation = [&]
        {
          data->expectContinuation = true;
        };

        // a) it could be an end of recipe
        // In these cases, we'll expect the next utterances to provide the label
        auto expectBehaviorLabel = [&]
        {
          expectContinuation();
          data->expectBehaviorLabel = true;
          data->expectRecipeElement = false;
          data->inRecipeEnumeration = false;
        };

        // Trivial case: we knew we were hearing a recipe.
        if (data->expectRecipeElement)
        {
          qiLogDebug() << "End of recipe reached, expecting label...";
          data->recipe.push_back(subject.clone());
          expectBehaviorLabel();
        }
        // Extra case: we did not consider the previous utterance to be a piece of recipe yet
        else if (data->expectContinuation && data->unassignedTaskDeclaration)
        {
          qiLogDebug() << "End of recipe guessed, expecting label...";
          data->recipe.push_back(std::move(data->unassignedTaskDeclaration));
          data->recipe.push_back(subject.clone());
          expectBehaviorLabel();
        }

        // b) this is the beginning of a teaching
        else if (!data->expectContinuation)
        {
          qiLogDebug() << "Label declared, expecting recipe...";
          // Side note: 1:1 assocations should be indifferent of which term is the behavior label
          // Here I chose that the first one will be the label to simplify the code.
          data->behaviorLabel = subject.clone();
          expectContinuation();
          data->expectRecipeElement = true;
          data->inRecipeEnumeration = true;
        }

        // Fallback
        else
        {
          qiLogDebug() << "Incoherent context, forget it.";
          data.reset();
          return nullptr;
        }
      }

      else
      { // No subject or no object: this is the verb "be" all alone.
        // It supposes there is a subject in the context.
        if (data->expectBehaviorLabel || data->expectRecipeElement)
        {
          qiLogDebug() << "Something else than an equivalence was expected, forget it.";
          data.reset();
          return nullptr;
        }

        if (!data->recipe.empty())
        {
          qiLogDebug() << "Association spotted, previous recipe has ended...";
          data->expectBehaviorLabel = true;
          data->expectContinuation = true;
          data->inRecipeEnumeration = false;
        }

        else if (data->behaviorLabel)
        {
          qiLogDebug() << "A behavior label was defined, this equivalence seem unrelated";
        }

        else if (data->unassignedTaskDeclaration)
        {
          qiLogDebug() << "Previous task declaration was the label, expecting recipe now...";
          data->behaviorLabel = std::move(data->unassignedTaskDeclaration);
          data->expectContinuation = true;
          data->expectRecipeElement = true;
          data->inRecipeEnumeration = true;
        }

        // Fallback
        else
        {
          qiLogDebug() << "Not handled case forget it.";
          data.reset();
          return nullptr;
        }
      }
    }
  }

  // Deal with infinitive statements alone.
  else if (const SemanticStatementGrounding* statement = asActionDeclaration(exp))
  {
    qiLogInfo() << "Infinitive verbal label spotted: \"" << statement->word.lemma << "\"";

    // We may expect a behavior label or a recipe element
    QI_ASSERT(!(data->expectBehaviorLabel && data->expectRecipeElement));
    if (data->expectBehaviorLabel)
    {
      qiLogDebug() << "Expected behavior label found";
      data->behaviorLabel = exp.clone();
      data->acceptContinuation = false;
      data->expectContinuation = false;
      QI_ASSERT(!data->inRecipeEnumeration);
    }
    else if (data->expectRecipeElement)
    {
      qiLogDebug() << "Expected recipe element found";
      data->expectContinuation = !data->behaviorLabel;
      data->expectRecipeElement = false;
      data->expectBehaviorLabel = false;
      data->inRecipeEnumeration = true; // it may be the end, but it is still in the enumeration
      data->recipe.emplace_back(exp.clone());
    }
    else
    {
      if (data->unassignedTaskDeclaration)
      { // Putting two labels like that looks like an enumeration.
        // In this model, only the recipe can be an enumeration.
        if (!data->recipe.empty())
        { // But if the enumeration is already there, there's a problem.
          qiLogDebug() << "It looks like a new recipe is being told, but it was already told!";
          data.reset();
          return nullptr;
        }
        qiLogDebug() << "Task declaration is to be considered a step of the recipe";
        data->recipe.emplace_back(std::move(data->unassignedTaskDeclaration));
        data->recipe.emplace_back(exp.clone());
        data->inRecipeEnumeration = true;
      }

      else if (data->inRecipeEnumeration)
      {
        qiLogDebug() << "Task declaration is the next step of the recipe";
        data->recipe.emplace_back(exp.clone());
      }

      else
      {
        qiLogDebug() << "Task declaration's role is not identified yet, let's wait for it...";
        data->unassignedTaskDeclaration = exp.clone();
        data->expectContinuation = true;
      }
    }
    // Use merge with context here.
  }

  // Deal with list of infinitive statements.
  else if (const ListExpression* list = asActionDeclarationList(exp))
  {
    qiLogInfo() << "This is a recipe (list of action declarations)";
    if (data->expectBehaviorLabel)
    {
      data.reset();
      return nullptr;
    }

    // Thanks to asActionDeclarationList, the coreferences can only be initial or terminal.
    if (asCoreference(*list->elts.front()) || data->unassignedTaskDeclaration)
    {
      // It may be the continuation of the recipe.
      if (!data->recipe.empty())
      {
        qiLogInfo() << "Recipe is being continued...";
      }

      // Or we don't know yet it is a recipe
      else if (data->unassignedTaskDeclaration)
      { // The previous task declaration was the first element of the enumeration
        qiLogInfo() << "Previous task declaration was identified as the first element of the recipe";
        data->recipe.emplace_back(std::move(data->unassignedTaskDeclaration));
      }

      // Or this is nonsense to us.
      else
      {
        qiLogInfo() << "Got a continuation of a recipe, but none was started yet, it is not understood";
        data.reset();
        return nullptr;
      }
    }

    for (const auto& element: list->elts)
    {
      if (const SemanticStatementGrounding* statement = asActionDeclaration(*element))
      {
        qiLogInfo() << "Infinitive verbal label spotted: \"" << statement->word.lemma << "\"";
        data->recipe.emplace_back(element->clone());
      } // skip coreferences
    }

    data->expectBehaviorLabel = false;
    data->expectRecipeElement = false;
    data->inRecipeEnumeration = true;
    data->expectContinuation = !data->behaviorLabel;

    if (asCoreference(*list->elts.back()))
    {
      qiLogInfo() << "The recipe looks incomplete, expecting a task declaration now";
      data->expectBehaviorLabel = false;
      data->expectRecipeElement = true;
      data->inRecipeEnumeration = true;
      data->expectContinuation = true;
    }
  }

  else if (agreementValue != TruenessValue::UNKNOWN)
  {
    qiLogDebug() << "Agreement spotted of value " << static_cast<int>(agreementValue);

    if (data->askingIfFinished)
    {
      qiLogDebug() << "It is a response \"is that all?\"";
      if (agreementValue == TruenessValue::VAL_TRUE)
      {
        data->askingIfFinished = false;
        data->acceptContinuation = false;
        data->expectContinuation = false;
        data->inRecipeEnumeration = false;
        qiLogDebug() << "It is a confirmation";
      }
      else if (agreementValue == TruenessValue::VAL_FALSE)
      {
        data->askingIfFinished = false;
        data->expectContinuation = true;
        qiLogDebug() << "It is an infirmation";
      }
    }

    else
    {
      if (agreementValue == TruenessValue::VAL_FALSE)
      { // Rollback to last state, and reproduce the same reaction.
        auto previousData = data->previousData;
        if (!previousData)
        {
          qiLogDebug() << "Disagreement received, nothing to be done about it";
          return nullptr;
        }
        qiLogDebug() << "Rolling back";
        data = data->previousData->previousData;
        return (data && data->reaction) ? data->reaction->clone() : nullptr;
      }
      else
      {
        qiLogDebug() << "Agreement was not expected, is ignored";
      }
    }
    // else ignore. Not sure we should clear the data.
  }

  else
  { // The sentence seems improperly formed, but sometimes it depends on the context
    qiLogInfo() << "Structure was not understood...";

    if (data->expectContinuation)
    {
      qiLogInfo() << "Trying to understand by recomposing with previous utterance";
      auto initialData = data->previousData->previousData;
      auto currentPhrase = data->phrase;
      lowerCaseFirstLetter(currentPhrase.text, 0u);
      auto recomposedPhrase = data->previousData->phrase;
      recomposedPhrase.text += std::string{" "} + currentPhrase.text;

      // TODO: make sure WHO SAID IT.
      // Beware, there are two classes named "TextProcessingContext".
      auto semanticLanguage = localeToSemanticEnum(currentPhrase.locale);
      ::qisem::TextProcessingContext textContext{
        SemanticAgentGrounding::currentUser,
        SemanticAgentGrounding::me,
        semanticLanguage};

      auto expression =
          converter::textToSemExp(recomposedPhrase.text, textContext, linguisticDatabase);

      data->reaction = tryLearnBehavior(
            knowledgeBase, session, linguisticDatabase, semanticMemory,
            taughtBehaviors, initialData, recomposedPhrase, *expression,
            behaviorLearnedSignal, actionFactories);
      return data->reaction ? data->reaction->clone() : nullptr;
    }
  }

  //===========================================================================
  // Preparing a response
  //===========================================================================
  if (data->behaviorLabel && !data->recipe.empty() && !data->expectContinuation)
  { // Teaching is complete enough
    // If we finished with the behavior label, there is no risk the recipe is incomplete
    if (!data->acceptContinuation)
    {
      qiLogInfo() << "Teaching successful, acknowledging";

      // We identify the slots from the label.
      std::unordered_map<SemanticEntityType, std::string> slotIds;
      auto labelGroundedPtr = data->behaviorLabel->getGrdExpPtr_SkipWrapperPtrs();
      assert(labelGroundedPtr);
      for (auto& labelChild: labelGroundedPtr->children)
      {
        auto groundedChildPtr = labelChild.second->getGrdExpPtr();
        if (!groundedChildPtr)
          continue;
        auto& childGrounding = groundedChildPtr->grounding();

        // Things are of grounding "generic".
        // TODO: handle people and places.
        auto childGenericGrd = childGrounding.getGenericGroundingPtr();
        if (!childGenericGrd)
          continue;
        if (SemanticReferenceType::INDEFINITE != childGenericGrd->referenceType)
          continue;
        if (SemanticEntityType::THING != childGenericGrd->entityType)
          continue;
        // TODO: report error if there are two things
        const auto& slotName = slotIds[SemanticEntityType::THING] = "something";

        // Transform the expression into a slot.
        auto metaGrounding =
            std::make_unique<SemanticMetaGrounding>(
              SemanticGroudingType::META, 0, slotName);
        labelChild.second =
            std::make_unique<GroundedExpression>(std::move(metaGrounding));
      }

      auto replaceCoreferencesToSlots = [&](SemanticExpression& statement)
      {
        auto groundedPtr = statement.getGrdExpPtr();
        if (!groundedPtr)
          return;
        for (auto& child: groundedPtr->children)
        {
          // We are looking for a coreference to an entity
          // of a type we encountered in the label.
          auto childCoreference = asCoreference(*child.second);
          if (!childCoreference)
            continue;
          auto slotIt = slotIds.find(childCoreference->entityType);
          if (slotIt == slotIds.end())
            continue;

          // Transform the expression into a slot.
          auto metaGrounding =
              std::make_unique<SemanticMetaGrounding>(
                SemanticGroudingType::META, 0, slotIt->second);
          child.second =
              std::make_unique<GroundedExpression>(std::move(metaGrounding));
        }
      };

      // We recompose a list expression from the recipe expressions.
      bool isSimpleAssociation = data->recipe.size() == 1;
      auto recipeExpression = [&]() -> std::unique_ptr<SemanticExpression>
      {
        if (isSimpleAssociation)
        {
          data->reaction = data->recipe.front()->clone();
          auto recipe = data->reaction->clone();
          replaceCoreferencesToSlots(*recipe);
          return recipe;
        }
        else
        {
          auto recipe = mystd::make_unique<ListExpression>(ListExpressionType::THEN);
          auto recipeWithSlots = recipe->clone();
          for (const auto& step: data->recipe)
          {
            recipe->elts.emplace_back(step->clone());
            auto stepEdit = step->clone();
            replaceCoreferencesToSlots(*stepEdit);
            recipeWithSlots->elts.emplace_back(std::move(stepEdit));
          }
          data->reaction = std::move(recipe);
          return recipeWithSlots;
        }
      }();


      // Pretty print for logs.
      std::string prettyPrintedStr;
      std::list<qisem::ALSemLineToPrint> semExpStrs;
      qisem::printer::prettyPrintSemExp(semExpStrs, *data->behaviorLabel);
      semExpStrs.emplace_back("<=>");
      qisem::printer::prettyPrintSemExp(semExpStrs, *recipeExpression);
      const auto& consolePrinter =
          qisem::SemExpLinesToStr::getInstance(qisem::PrintSemExpDiffsOutPutFormat::CONSOLE);
      consolePrinter.printLines(prettyPrintedStr, semExpStrs);
      qiLogInfo()
          << "Reconstituted learning: " << std::endl << prettyPrintedStr;

      // Formatting for signal.
      auto language = currentLanguageToSemanticEnum(session);
      auto semExpToText = [&](const SemanticExpression& exp)
      {
        ::qisem::TextProcessingContext textContext{
          qisem::SemanticAgentGrounding::currentUser,
          qisem::SemanticAgentGrounding::me,
          language};

        std::string text;
        converter::semExpToText(
              text, exp.clone(), textContext, false, semanticMemory, linguisticDatabase, nullptr);
        return text;
      };

      auto textLabel = semExpToText(*data->behaviorLabel);
      std::vector<std::string> textRecipe;
      for (const auto& step: data->recipe)
      {
        textRecipe.emplace_back(semExpToText(*step));
      }

      if (auto sig = behaviorLearnedSignal.lock())
      {
        QI_EMIT (*sig)(textLabel, textRecipe);
      }

      auto response = acknowledgeLearnedBehavior(*data->behaviorLabel, *recipeExpression);

      // Saving the teaching. Replaces any other teaching with same behavior label.
      // TODO: save both ways if it is a simple assocation (impacts the obey behavior)
      auto toRemoveIt = std::remove_if(
            taughtBehaviors.begin(), taughtBehaviors.end(),
            [&](const SemanticExpressionAssociation& association)
      {
        return qisem::areEquals(association.first, data->behaviorLabel);
      });
      taughtBehaviors.erase(toRemoveIt, taughtBehaviors.end());
      taughtBehaviors.push_back(
          {std::move(data->behaviorLabel),
           std::move(recipeExpression)});

      // Also insert it in the knowledge.
      // 3 triples for type, implementation and label.
      // 3 triples per child for type, composition, label and consecutivity (but for the 1st one).
      std::vector<Triple> triples;
      triples.reserve(3 + 4 * data->recipe.size() - 1);

      const auto behavior = makeUniqueResource(behaviorPrefix());
      triples.push_back(Triple{behavior, rdfTypeResource(), behaviorTypeNode()});
      // TODO: understand why emplace_back is not good enough

      auto composition = makeUniqueResource(compositionPrefix());
      const auto compositionNode = makeNodeFromResourceNode(composition);
      triples.push_back(Triple{behavior, implementedByResource(), compositionNode});

      const auto currentLocale = currentLanguageToLocale(session); // TODO: avoid a call here
      auto label = makeLocalizedStringNode(std::move(textLabel), currentLocale);
      triples.push_back(Triple{behavior, rdfsLabelResource(), std::move(label)});

      bool first = true;
      Node previousBehaviorNode;
      for (const auto& textStep: textRecipe) {
        auto childBehavior = makeUniqueResource(behaviorPrefix());
        auto childBehaviorNode = makeNodeFromResourceNode(childBehavior);
        triples.push_back(Triple{composition, composedOfResource(), childBehaviorNode});
        triples.push_back(Triple{childBehavior, rdfTypeResource(), behaviorTypeNode()});

        auto label = makeLocalizedStringNode(textStep, currentLocale);
        triples.push_back(Triple{childBehavior, rdfsLabelResource(), std::move(label)});

        if (first) {
          first = false;
        } else {
          triples.push_back(Triple{childBehavior, consecutiveToResource(), previousBehaviorNode});
          first = false;
        }

        previousBehaviorNode = std::move(childBehaviorNode);
      }

      auto recipes = knowledgeBase->editableKnowledgeGraph("teaching");
      for (const auto& triple: triples) {
        qiLogDebug() << "Adding to knowledge: " << qi::encodeJSON(triple);
        recipes->add(triple);
      }

      data.reset();
      return response;
    }

    // Otherwise this is unclear: ask if it is over
    else
    {
      data->askingIfFinished = true;

      // The texts for this response.
      static auto initTexts = []
      {
        std::map<locale::Language, std::vector<std::string>> isThatAllTexts;
        isThatAllTexts[locale::Language::English] = std::vector<std::string>{"is that all?"};
        isThatAllTexts[locale::Language::French] = std::vector<std::string>{"c'est tout?"};
        return isThatAllTexts;
      };
      static auto isThatAllTexts = initTexts();

      // Taking one randomly.
      static std::random_device randomDevice;
      static std::default_random_engine randomEngine{randomDevice()};
      const auto& isThatAllInLanguage = isThatAllTexts[data->phrase.locale.language];
      std::uniform_int_distribution<unsigned long> indexDistribution(0u, isThatAllInLanguage.size() - 1);
      auto randomIndex = indexDistribution(randomEngine);

      // The response.
      auto isThatAllGrounding =
          mystd::make_unique<SemanticTextGrounding>(isThatAllInLanguage[randomIndex]);
      data->reaction = mystd::make_unique<GroundedExpression>(std::move(isThatAllGrounding));
      return data->reaction->clone();
    }
  }
  else
  { // TODO: have specific responses for each case
    bool hasLearnedLabel = !knewLabel && data->behaviorLabel;
    bool hasLearnedRecipe = !knewRecipe && !data->recipe.empty();
    if (hasLearnedLabel || hasLearnedRecipe || data->expectContinuation)
    {
      qiLogInfo() << "Teaching is on track...";
      data->reaction = makeAgreementExpression();
      return data->reaction->clone();
    }
    else
    {
      qiLogInfo() << "This phrase does not seem to contribute to a teaching, no response produced";
    }
  }
  data->reaction = nullptr;
  return nullptr;
}
} // anonymous


Future<PDDLAwareQiBehaviorPtr> makeLearnBehavior(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<std::list<SemanticExpressionAssociation>> taughtBehaviors,
    std::shared_ptr<Signal<std::string, std::vector<std::string>>> behaviorLearned)
{
  qiLogInfo() << "Building the behavior to learn behaviors!";
  auto learnData = std::make_shared<LearnBehaviorData>();
  auto behaviorLearnedSignal =
      std::weak_ptr<Signal<std::string, std::vector<std::string>>>(behaviorLearned);
  auto actionFactories = // TODO: receive from the outside? or play with action pool?
      std::make_shared<StoredPropertySet<PDDLAwareQiActionFactoryPtr>>();
  auto reaction = [=](
      const PhraseAndLocale& phrase,
      const SemanticExpression& k) mutable
  {
    return tryLearnBehavior(
          knowledgeBase, session,
          *semanticMemoryContext->linguisticDatabase(),
          *semanticMemoryContext->semanticMemory(),
          *taughtBehaviors, learnData, phrase, k,
          behaviorLearnedSignal, actionFactories);
  };

  // TODO: set real STRIPS properties for each behavior
  // Required Goals:
  // & Understand a recipe
  // & Learn a way to mobilize it
  //   | A verbal label
  //   | An event to trigger it
  //   | An effect to expect from it
  return makeSemanticBehavior(
        "To learn behaviors from explanation",
        speechSource, speechEngine, session, context, semanticMemoryContext, std::move(reaction),
        "", knowledgeBase->editableKnowledgeGraph("beliefs"));
}
} // agent
} // semantic
} // qi
