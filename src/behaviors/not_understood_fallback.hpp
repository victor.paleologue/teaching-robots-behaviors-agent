#ifndef SEMANTICAGENT_BEHAVIOR_NOT_UNDERSTOOD_FALLBACK_HPP
#define SEMANTICAGENT_BEHAVIOR_NOT_UNDERSTOOD_FALLBACK_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "../behaviormodel/behavior.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{
struct SemanticMemoryContext;

/// This behavior tracks what is said to self,
/// and suggest a response if no other response occurred before a given time.
Future<PDDLAwareQiBehaviorPtr> makeNotUnderstoodFallbackBehavior(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    knowledge::KnowledgeBasePtr knowledge,
    knowledge::EditableKnowledgeGraphPtr graph);


constexpr unsigned notUnderstoodFallbackTimeoutMs = 500;
constexpr qi::MilliSeconds notUnderstoodFallbackTimeout{notUnderstoodFallbackTimeoutMs};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_BEHAVIOR_NOT_UNDERSTOOD_FALLBACK_HPP
