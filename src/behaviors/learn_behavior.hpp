#ifndef SEMANTICAGENT_BEHAVIOR_LEARN_BEHAVIOR_HPP
#define SEMANTICAGENT_BEHAVIOR_LEARN_BEHAVIOR_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "../utility/knowledge.hpp"
#include "semantic_expression_association.hpp"

namespace qisem
{
  struct SemanticExpression;
  struct SemanticStatementGrounding;
  struct UniqueSemanticExpression;
  struct SemanticMemory;

  namespace linguistics
  {
    struct LinguisticDatabase;
  } // linguistics
} // qisem

namespace qi
{
namespace semantic
{
namespace agent
{
class Behavior;
struct SemanticMemoryContext;

/// Stores new actions, equivalent to other actions or sequences of action.
/// Works only with one full sentence described in a semantic knowledge.
std::unique_ptr<qisem::SemanticExpression> learnAction(
    const qisem::SemanticExpression& expression,
    std::list<SemanticExpressionAssociation>& existingActionEquivalences,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    const qisem::SemanticMemory& semanticMemory);

/// Creates a semantic behavior that learns behavior compositions.
/// It supports learning across multiple utterances.
Future<PDDLAwareQiBehaviorPtr> makeLearnBehavior(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<std::list<qi::semantic::agent::SemanticExpressionAssociation> > taughtBehaviors,
    std::shared_ptr<Signal<std::string, std::vector<std::string>>> behaviorLearned = {});

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_BEHAVIOR_LEARN_BEHAVIOR_HPP
