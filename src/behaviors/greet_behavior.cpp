#include <functional>
#include <cmath>

#include <qi/log.hpp>
#include <qi/actuation/actuation.hpp>
#include <qi/humanawareness/humanawareness.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <qi/teaching/ontology.hpp>

#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpression/semanticexpression.hpp>
#include <qisem/linguisticanalyzer/languagedetector.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/greet.hpp>

#include "greet_behavior.hpp"
#include "semantic_behaviors.hpp"
#include "../behaviormodel/behavior.hpp"
#include "../pddl/pddl_io.hpp"
#include "../pddl/pddl_read.hpp"
#include "../utility/disposable.hpp"
#include "../utility/incontext.hpp"
#include "../utility/knowledge.hpp"
#include "../utility/semantic_helpers.hpp"
#include "../humanmanager.hpp"
#include "../humanrecognizer.hpp"
#include "../language.hpp"

qiLogCategory("SemanticAgent.GreetBehavior");


namespace qi
{
namespace semantic
{
namespace agent
{
using namespace qi::human;
using namespace qi::knowledge;
using namespace qi::teaching;
using namespace qisem;


GreetCallbacks getGreetCallbacks(
    qi::context::ContextPtr context,
    const qi::locale::Locale& locale,
    humanawareness::HumanAwarenessPtr humanAwareness,
    std::shared_ptr<HumanManager> humanManager,
    EditableKnowledgeBasePtr knowledgeBase,
    EditableKnowledgeGraphPtr graph)
{
  GreetCallbacks callbacks;
  auto getHumanNode = [=]() -> Future<ResourceNode>
  {
    return humanAwareness->engagedHuman.get().async()
        .andThen([=](HumanPtr human)
    {
      if (human)
        return humanManager->getResNode(human);
      return ResourceNode();
    });
   };

  // TODO rename in Callbacks
  callbacks.onHumanIntroduced = [=]() mutable
  {
    getHumanNode().andThen([=](const ResourceNode& socialAgentNode)
    {
      if (!socialAgentNode.url.empty())
      {
        firstLabelOf(knowledgeBase, socialAgentNode)
            .andThen([=](const knowledge::LocalizedString& name)
        {
          if (!name.string.empty())
          { // register greeted event
            humanManager->addName(socialAgentNode, name.string, name.locale);
          }
        });
      }
    });
  };

  callbacks.onHumanHasGivenName = [=](const std::string& name)
  {
    // TODO : For now, just check who is engaged. Might change that in the future
    getHumanNode().andThen([=](const ResourceNode& socialAgentNode)
    {
      if (!socialAgentNode.url.empty())
      {
        humanManager->addName(socialAgentNode, name, locale);
      }
    });
  };

  callbacks.lookAtOtherPerson = [=] (const std::string& name)
  {
    //TODO move this "action" so that it is executed when the actionSuggestion is used
    // not when the action is constructed as it is here
    HumanPtr newTarget;
    if (not name.empty())
    { // get node from target
      auto humanResNode = humanManager->getResNodeFromName(name);
      if (!humanResNode.url.empty())
      {
        auto humanPtr = humanManager->getHumanPtr(humanResNode);
        if (humanPtr)
          newTarget = humanPtr;}
    }
    else
    {
      auto humanPtr = humanAwareness->engagedHuman.get().value();
      auto actives = humanAwareness->humansAround.get().value();
      if (actives.size() > 1)
      {
        auto newTarget = *find_if(actives.begin(), actives.end(), [humanPtr](qi::human::HumanPtr h) {
            return humanPtr != h;
      });
      }
    }

    if (newTarget)
    {
      auto engageHuman = humanManager->engage(newTarget);
    }
  };
  return callbacks;
}

void colocalizeResourceNodes(const qi::knowledge::EditableKnowledgeGraphPtr& graph,
                             qi::knowledge::ResourceNode from,
                             qi::knowledge::ResourceNode to)
{
  using namespace qi::knowledge;
  using namespace qi::teaching;
  graph->add(makeTripleFromResources(from, colocalizedWithResource(), to));
}

void removeColocalizationResourceNodes(const qi::knowledge::EditableKnowledgeGraphPtr& graph,
                                       qi::knowledge::ResourceNode from,
                                       qi::knowledge::ResourceNode to)
{
  using namespace qi::knowledge;
  using namespace qi::teaching;
  graph->remove(makeTripleFromResources(from, colocalizedWithResource(), to));
}


Future<std::shared_ptr<SemanticExpression>> greetFeedback(
    const PhraseAndLocale& pPhrase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticContext,
    std::shared_ptr<const SemanticExpression> exp,
    std::shared_ptr<HumanManager> humanManager,
    EditableKnowledgeBasePtr knowledgeBase,
    EditableKnowledgeGraphPtr graph)
{
  qi::humanawareness::HumanAwarenessPtr humanAwareness = session->service("HumanAwareness").value();
  auto callbacks = getGreetCallbacks(context, pPhrase.locale, humanAwareness, humanManager, knowledgeBase, graph);

  // Try to get the name of the engaged human, that is supposed to be the source of the utterance.
  auto gettingName = [&]
  {
    auto engagedHuman = humanAwareness->engagedHuman.get().value();
    if (!engagedHuman)
      return qi::makeFutureError<LocalizedString>("no human engaged");
    auto socialAgentNode = humanManager->getResNode(engagedHuman);
    return firstLabelOf(knowledgeBase, socialAgentNode);
  }();

  return gettingName.then(
        semanticContext->wrap(
          [=](const Future<LocalizedString>& name) -> std::shared_ptr<SemanticExpression>
  {
    if (name.isCanceled())
      return {}; // don't proceed if it was canceled

    const auto& linguisticDb = *semanticContext->linguisticDatabase();
    const auto& semanticMemory = *semanticContext->semanticMemory();
    auto interlocutorExp = [&]() -> std::unique_ptr<SemanticExpression>
    {
      if (name.hasError())
        return {};
      return makeTextExpression(name.value().string); // TODO: support locale
    }();

    auto res =
        greetInResponseTo(
          *exp, semanticMemory, linguisticDb, callbacks, std::move(interlocutorExp));

    if (res && !res->getSemExp().isEmpty())
      return res->getSemExp().clone();
    return {};

  })).unwrap();
}

/// @brief returns a greeting SemanticExpression if the engaged human is close enough and not greeted recently
std::unique_ptr<SemanticExpression> initHumanInteraction(
    std::shared_ptr<HumanManager> humanManager,
    SessionPtr session,
    context::ContextPtr context,
    HumanPtr engagedHuman,
    const qi::locale::Locale& locale,
    const SemanticMemoryContext& semanticMemoryContext,
    EditableKnowledgeBasePtr knowledgeBase,
    EditableKnowledgeGraphPtr graph)
{
  const auto& linguisticDb = *semanticMemoryContext.linguisticDatabase();
  const auto& semanticMemory = *semanticMemoryContext.semanticMemory();

  qi::humanawareness::HumanAwarenessPtr humanAwareness = session->service("HumanAwareness").value();
  if (engagedHuman)
  {
    auto socialAgentNode = humanManager->getResNode(engagedHuman); // find resNode

    // define distance
    // ---------------
    auto headFrame = engagedHuman->headFrame.get().value();
    qi::actuation::ActuationPtr actuation = session->service("Actuation").value();
    auto transform = headFrame->computeTransform(actuation->robotFrame()).transform.translation;
    auto distance = static_cast<float>(std::sqrt(transform.x * transform.x + transform.y * transform.y));
    qiLogInfo()
        << "Engaged human is " << distance << "m away";

    // check if human was greeted recently
    // ---------------
    auto timeOfLastMeeting = humanManager->getTimestamp(socialAgentNode, graph);
    auto duration = boost::posix_time::time_duration(makeNow() - timeOfLastMeeting);
    qiLogInfo()
        << "Human was last greeted " << duration << " ago";

    // TODO instead or setting a timer, just set an on/off condition from a event "flag" in knowledge
    // that could be then removed (or use 2 types of events : "greeted" and "last_seen" or "haveleft"
    if (distance < 2.f && duration.minutes() < 5)
    {
      qiLogInfo()
          << "Engaged human should be greeted";

      auto callbacks = getGreetCallbacks(context, locale, humanAwareness, humanManager, knowledgeBase, graph);
      auto interlocutorExpression = [&]() -> std::unique_ptr<GroundedExpression>
      {
        // TODO: asynchronize this
        auto name = firstLabelOf(knowledgeBase, socialAgentNode).value().string;
        if (!name.empty())
        {
          qiLogInfo()
              << "Engaged human's name is \"" << name << "\"";
        }
        else
        {
          qiLogInfo()
              << "Engaged human's name is unknown";
        }
        return makeTextExpression(std::move(name));
      }();

      // says  "hello, what is your name ?" or "hello + name" if name is known
      auto greeting =
          greet(semanticMemory, linguisticDb, callbacks, std::move(interlocutorExpression));
      assert(!greeting->isEmpty());
      return greeting->clone();
    }
    else
    {
      qiLogInfo()
          << "No point to greet the human again";
    }
  }
  return std::unique_ptr<SemanticExpression>();
}


Future<PDDLAwareQiBehaviorPtr> makeGreetBehavior(
    SessionPtr session,
    context::ContextPtr context,
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    knowledge::EditableKnowledgeGraphPtr graph,
    const std::string& pddl,
    std::shared_ptr<HumanManager> humanManager)
{
  auto name = "To greet user";
  auto safeContext = std::make_shared<SafeContext>(semanticMemoryContext->strand());
  auto safe = safeContext->inContextTransform();

  auto behaviorFunction = [=](
      PropertyPtr<ActionObjs> suggestions,
      PropertyPtr<Goals>,
      PropertyPtr<ActionFactories>) mutable
  {
    auto localeTracker = std::make_shared<CurrentLocaleProperty>(session);

    auto actionFromSuggestions = std::make_shared<std::unique_ptr<Disposable>>();
    Promise<void> p;

    auto semanticExecuter =
        std::make_shared<SemanticExpressionExecuter>(session, context, semanticMemoryContext, speechEngine);

    // Deals with semanticReaction
    auto semanticExtractor = ensureSemanticExtractor(speechSource, semanticMemoryContext);
    auto signalLink = semanticExtractor->connect(
          safe([=](const std::vector<SemanticEvent>& events) mutable
    {
      // It is safe to run functions using the semantic memory in this context.
      if (events.empty())
        return;

      // TODO: let several inputs be handled
      const auto& event = events.front();
      const auto& phrase = event.phrase;
      const auto& locale = phrase.locale;
      qiLogInfo()
          << "Behavior \"" << name << "\" acknowledges the incoming phrase \"" << phrase.text << "\"";

      // TODO: several reactions could be possible, with various auxiliary goals, not just one
      // When requested to react anew, drop any current suggestions.
      if (!suggestions->get().value().empty())
        suggestions->set(ActionObjs());

      auto reacting = greetFeedback(
            phrase, session, context, semanticMemoryContext,
            event.expression, humanManager, knowledgeBase, graph);

      reacting.andThen(safe([=](const std::shared_ptr<SemanticExpression>& reaction)
      {
        if (reaction)
        {
          qiLogInfo()
              << "Greeting behavior reacts to \"" << phrase.text << "\"";

          auto action = makeSemanticAction(
                session, context, semanticMemoryContext, semanticExecuter,
                reaction->clone(), locale, graph, event.communicationActNode,
                safe([=]()
          {
            if (*actionFromSuggestions)
              return (*actionFromSuggestions)->dispose();
            return Future<void>(nullptr);
          }));

          auto actions = suggestions->get().value();
          actions.push_back(action);
          suggestions->set(std::move(actions));
          *actionFromSuggestions = makeDisposable(
                DisposalFunction(semanticMemoryContext->strand()->schedulerFor([=]
          {
            auto actions = suggestions->get().value();
            auto found = std::find(actions.begin(), actions.end(), action);
            if (found != actions.end())
              actions.erase(found);
            suggestions->set(actions).value();
          })));
        }
        else
        { // No reaction, so clear the current suggestions.
          qiLogInfo()
              << "Greeting behavior does not react to \"" << phrase.text << "\"";
        }
      }));
    }));

    // Deals with humanAwarenessReaction
    // KLUDGE: using the specialized proxy leads to a crash,
    // where the proxy property is called back from the dead #44736.
    // We use a classical proxy instead.
    auto humanAwareness =
        session->service("HumanAwareness").value();

    auto engagedHumanLink =
        humanAwareness.connect(
          "engagedHuman",
          boost::function<Future<void>(HumanPtr)>(
            safe([=](HumanPtr engagedHuman) mutable
    {
      if (!engagedHuman)
      {
        qiLogInfo() << "No human engaged";
        return;
      }
      else
      {
        qiLogInfo() << "Engaged human " << engagedHuman.uid();
      }

      try
      {
        // SemExp reaction to human engaged and close to the robot
        auto reaction = initHumanInteraction(
              humanManager, session, context, engagedHuman,
              localeTracker->value().value(),
              *semanticMemoryContext, knowledgeBase, graph);

        auto engagedHumanResource = humanManager->getResNode(engagedHuman);

        if (reaction)
        {
          qiLogInfo()
              << "Greeting behavior to an incoming human";

          auto action = makeSemanticAction(
                session, context, semanticMemoryContext, semanticExecuter,
                std::move(reaction), localeTracker->value().value(), {}, {},
                safe([=]()
          {
            if (engagedHuman)
            {
              // Unless the action was cancelled, remove the action from suggestions.
              humanManager->setTimestamp(engagedHumanResource, graph, "greeted");
              qiLogInfo() << "Removing action from suggestion";
              if (*actionFromSuggestions)
                return (*actionFromSuggestions)->dispose();
            }
            return Future<void>(nullptr);
          }));

          actionFromSuggestions->reset();

          // KLUDGE: dunno why we leak suggestions otherwise
          if (!suggestions->get().value().empty())
            suggestions->set(ActionObjs());

          {
            const auto socialAgentPDDL = pddlNameFromIRI(teaching::socialAgentType());
            const auto selfPDDL = pddlNameFromIRI(teaching::agentSelf());
            const auto targetHumanPDDL = pddlNameFromIRI(engagedHumanResource.url);

            std::stringstream ss;
            ss << "(define (domain greeting_domain)\n"
                  "  (:requirements :strips :typing :negative-preconditions)\n"
                  "  (:types\n"
                  "    " << socialAgentPDDL << "\n"
                  "  )\n"
                  "  (:constants\n"
                  "    " << targetHumanPDDL << " " << selfPDDL << " - " << socialAgentPDDL << "\n"
                  "  )\n"
                  "  (:predicates\n"
                  "    (was_greeted_by ?greeted - " << socialAgentPDDL << " ?greeter - " << socialAgentPDDL << ")\n"
                  "  )\n"
                  "  (:action greet_" << targetHumanPDDL << "\n"
                  "    :parameters ()\n"
                  "    :precondition (not (was_greeted_by " << targetHumanPDDL << " " << selfPDDL << "))\n"
                  "    :effect (was_greeted_by " << targetHumanPDDL << " " << selfPDDL << ")\n"
                  "  )\n"
                  ")\n"
                  "(define (problem greeting_problem)\n"
                  "  (:domain greeting_domain)\n"
                  "  (:requirements :strips :typing :negative-preconditions)\n"
                  "  (:objects)\n"
                  "  (:init)\n"
                  "  (:goal (was_greeted_by " << targetHumanPDDL << " " << selfPDDL << "))\n"
                  ")";
            action->pddl.set(ss.str());
          }

          auto actions = suggestions->get().value();
          actions.push_back(action);
          suggestions->set(std::move(actions));
          *actionFromSuggestions = makeDisposable(
                DisposalFunction(semanticMemoryContext->wrap([=]
          {
            auto actions = suggestions->get().value();
            auto found = std::find(actions.begin(), actions.end(), action);
            if (found != actions.end())
              actions.erase(found);
            suggestions->set(actions).value();
          })));
        }
        else
        { // No reaction, so clear the current suggestions.
          qiLogInfo()
              << "Greeting behavior does not react to human event";
        }
      }
      catch (const std::exception& e)
      {
        qiLogError() << "Error trying to compute a response: " << e.what();
      }
    }))).value();

    // Manage future cancellation
    p.setOnCancel(safe([safeContext, humanAwareness, engagedHumanLink,
                       semanticExtractor, signalLink, p](Promise<void>) mutable
    {
      qiLogInfo() << "Greet behavior is being cancelled";
      humanAwareness.disconnect(engagedHumanLink).async();
      semanticExtractor->disconnectAsync(signalLink);
      semanticExtractor.reset();
      qiLogInfo() << "Semantic behavior's extractor was dropped";
      p.setCanceled(); // using p here captures the promise and prevents it to be broken
    }));

    return p.future();
  };

  return makeQiBehavior(name, safe(behaviorFunction), pddl);
}

} // agent
} // semantic
} // qi
