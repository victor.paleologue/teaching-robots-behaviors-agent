#ifndef SEMANTICAGENT_BEHAVIOR_OBEY_HPP
#define SEMANTICAGENT_BEHAVIOR_OBEY_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "../utility/knowledge.hpp"
#include "semantic_expression_association.hpp"

namespace qisem
{
  struct SemanticExpression;
} // qisem

namespace qi
{
namespace semantic
{
namespace agent
{
class ActionPool;
struct SemanticMemoryContext;
struct KnowledgeSemanticBridge;


/// Responds with an knowledge producing an action, if it is recognized as a learned action.
std::unique_ptr<qisem::SemanticExpression> obey(
    const qisem::SemanticExpression& expression,
    const locale::Locale& locale,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    const SemanticMemoryContext& semanticContext);


/// Creates a behavior that obeys to spoken orders.
Future<PDDLAwareQiBehaviorPtr> makeObey(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<std::list<SemanticExpressionAssociation>> taughtBehaviors,
    std::shared_ptr<ActionPool> actionPool = {},
    std::shared_ptr<KnowledgeSemanticBridge> knowledgeSemantic = {});

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_BEHAVIOR_OBEY_HPP
