#include "reaction.hpp"
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/semanticcontroller/tool/semexpcomparator.hpp>
#include <qisem/linguisticanalyzer/tool/semexpmodifier.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpressions.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgroundings.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include "../utility/semantic_helpers.hpp"

using namespace qisem;

namespace qi
{
namespace semantic
{
namespace agent
{
std::unique_ptr<qisem::SemanticExpression> notUnderstood()
{
  // responding "<excuse>, I <not> <understand>"
  // ---------------------------------------------------------------
  // agreement
  auto feedbackGrounding = mystd::make_unique<SemanticGenericGrounding>();
  feedbackGrounding->word = SemanticWord{};
  feedbackGrounding->word.setContent(SemanticLanguageEnum::ENGLISH, "sorry", PartOfSpeech::INTERJECTION);
  auto feedback = mystd::make_unique<GroundedExpression>(std::move(feedbackGrounding));

  // reformulation of the action equivalence
  auto understand = mystd::make_unique<SemanticStatementGrounding>();
  understand->word = SemanticWord{};
  understand->word.setContent(SemanticLanguageEnum::ENGLISH, "understand", PartOfSpeech::VERB);
  understand->verbTense = SemanticVerbTense::PRESENT;
  understand->confidence = -1.f;
  auto iDoNotUnderstand = mystd::make_unique<GroundedExpression>(std::move(understand));
  SemExpModifier::addChild(
        *iDoNotUnderstand,
        GrammaticalType::SUBJECT,
        mystd::make_unique<GroundedExpression>(SemanticAgentGrounding::getRobotAgentPtr()));

  // making the response
  return mystd::make_unique<FeedbackExpression>(std::move(feedback), std::move(iDoNotUnderstand));
}

std::unique_ptr<qisem::SemanticExpression> defaultReact(
    const qisem::SemanticExpression& expression,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    qisem::SemanticMemory& semanticMemory)
{
  boost::optional<qisem::UniqueSemanticExpression> reaction;
  memoryOperation::react(reaction, semanticMemory, expression.clone(), linguisticDatabase);
  if (reaction)
    return (*reaction)->clone();
  return nullptr;
}

bool isSemanticAction(const SemanticExpression &semExp)
{
  auto actionSemExpPtr = semExp.clone();
  auto groundedExpressionsPtr = SemExpModifier::listTopGroundedExpressionsPtr(*actionSemExpPtr);
  for (auto& groundedExpressionPtr: groundedExpressionsPtr)
  {
    SemExpModifier::addChild(
          *groundedExpressionPtr,
          GrammaticalType::SUBJECT,
          mystd::make_unique<GroundedExpression>(SemanticAgentGrounding::getRobotAgentPtr()));
    SemExpModifier::addRequest(*groundedExpressionPtr, SemanticRequestType::ACTION);
  }

  auto category = memoryOperation::categorize(*actionSemExpPtr);
  return category == SemanticExpressionCategory::COMMAND;
}

void listGroundedExpressions(const SemanticExpression& semExp, std::list<UniqueSemanticExpression>& result)
{
  switch(semExp.type)
  {
  case SemanticExpressionType::COMMAND:
  case SemanticExpressionType::GROUNDED:
    result.push_back(semExp.clone());
    break;

  case SemanticExpressionType::LIST:
    for (auto& element: semExp.getListExp().elts)
      listGroundedExpressions(*element, result);
    break;

  case SemanticExpressionType::CONDITION:
  {
    auto& condExp = semExp.getCondExp();
    listGroundedExpressions(*condExp.conditionExp, result);
    listGroundedExpressions(*condExp.thenExp, result);
    auto& elseExp = condExp.elseExp;
    if (elseExp)
      listGroundedExpressions(**elseExp, result);
    break;
  }

  case SemanticExpressionType::COMPARISON:
  {
    auto& compExp = semExp.getCompExp();
    listGroundedExpressions(*compExp.leftOperandExp, result);
    listGroundedExpressions(**compExp.rightOperandExp, result);
    break;
  }

  case SemanticExpressionType::INTERPRETATION:
    listGroundedExpressions(*semExp.getIntExp().interpretedExp, result);
    break;

  case SemanticExpressionType::FEEDBACK:
    listGroundedExpressions(*semExp.getFdkExp().concernedExp, result);
    break;

  case SemanticExpressionType::ANNOTATED:
    listGroundedExpressions(*semExp.getAnnExp().semExp, result);
    break;

  case SemanticExpressionType::SETOFFORMS:
  {
    auto originalFromPtr = semExp.getSetOfFormsExp().getOriginalForm();
    if (originalFromPtr)
      listGroundedExpressions(**originalFromPtr, result);
    break;
  }

  case SemanticExpressionType::METADATA:
    listGroundedExpressions(*semExp.getMetadataExp().semExp, result);
    break;

  case SemanticExpressionType::FIXEDSYNTHESIS:
    listGroundedExpressions(semExp.getFSynthExp().getSemExp(), result);
    break;
  } // switch
}


const char* InfiniteDecompositionException::what() const noexcept
{
  return "infinite decomposition recursion detected";
}


void decompose(
    UniqueSemanticExpression& semExp,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    std::list<UniqueSemanticExpression>& alreadyDecomposedSemExps,
    const qisem::SemanticMemory& semanticMemory,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    boost::optional<size_t> depthLimit)
{
  // check depth limit and compute value for next recursion
  if (depthLimit)
  {
    auto& depthLimitValue = depthLimit.get();
    if (depthLimitValue == 0)
      return;
    if (depthLimitValue > 1)
      --depthLimitValue;
    else
      depthLimit = 0;
  }

  auto alreadyDecomposedIt = std::find_if(
        alreadyDecomposedSemExps.begin(), alreadyDecomposedSemExps.end(),
        [&](const UniqueSemanticExpression& alreadyDecomposedSemExp)
  {
    return SemExpComparator::semExpsAreEqual(
          *semExp, *alreadyDecomposedSemExp, semanticMemory, linguisticDatabase);
  });
  if (alreadyDecomposedIt != alreadyDecomposedSemExps.end())
    throw InfiniteDecompositionException();
  alreadyDecomposedSemExps.push_back(semExp->clone());

  // for each grounded expression in the given semantic expression
  std::list<UniqueSemanticExpression> childrenSemExps;
  listGroundedExpressions(*semExp, childrenSemExps);
  for (auto& childSemExp: childrenSemExps)
  {
    // check if it matches the semantic template of a learned composition
    auto& originalExpression = *childSemExp;
    auto actionEquivalenceIt = std::find_if(
          actionEquivalences.begin(), actionEquivalences.end(),
          [&](const SemanticExpressionAssociation& association)
    {
      qisem::SemExpComparator::ComparisonExceptions ignoreOptions;
      ignoreOptions.request = true;
      ignoreOptions.verbTense = true;
      ignoreOptions.grammaticalTypes.insert(qisem::GrammaticalType::SUBJECT);

      qisem::ImbricationType imbrication =
          qisem::SemExpComparator::getSemExpsImbrications(
            originalExpression, *association.first,
            semanticMemory.memBloc, linguisticDatabase,
            &ignoreOptions);

      static const std::set<qisem::ImbricationType> targetImbrications =
          {qisem::ImbricationType::CONTAINS,
           qisem::ImbricationType::ISCONTAINED,
           qisem::ImbricationType::EQUALS};

      return targetImbrications.count(imbrication);
    });

    // if it matches, decompose and propagate the filled slots
    if (actionEquivalenceIt != actionEquivalences.end())
    {
      // first we collect the slot fillings
      std::unordered_map<std::string, std::unique_ptr<SemanticExpression>> slotFillings;
      const auto& groundedTemplate =
          *actionEquivalenceIt->first->getGrdExpPtr_SkipWrapperPtrs();
      const auto& groundedExpression =
          *semExp->getGrdExpPtr_SkipWrapperPtrs();
      for (const auto& child: groundedTemplate.children)
      {
        const auto& groundedChild = child.second->getGrdExpPtr();
        if (!groundedChild)
          continue;
        // child is grounded! That must be a (dad) joke.
        const auto& metaGrounding = groundedChild->grounding().getMetaGroundingPtr();
        if (!metaGrounding)
          continue;
        const auto& slotName = metaGrounding->attibuteName;
        auto filledSlotIt = groundedExpression.children.find(child.first);
        assert(filledSlotIt != groundedExpression.children.end());
        auto filledSlot = filledSlotIt->second->clone();
        slotFillings[slotName] = std::move(filledSlot);
      }

      auto replaceAndApplyFillings = [&](
          SemanticExpression& toReplace,
          const SemanticExpression& templateExpression)
      {
        qiLogInfo("SemanticAgent.Obey")
            << "Applying input:\n" << toReplace
            << "to template:\n" << templateExpression;

        auto toReplaceGroundedPtr = toReplace.getGrdExpPtr_SkipWrapperPtrs();
        if (!toReplaceGroundedPtr)
          throw std::runtime_error("target expression is not grounded");

        auto toReplaceGroundingPtr = toReplaceGroundedPtr->grounding().getStatementGroundingPtr();
        if (!toReplaceGroundingPtr)
          throw std::runtime_error("target expression is not grounded on a statement");

        auto groundedTemplatePtr = templateExpression.getGrdExpPtr();
        if (!groundedTemplatePtr)
          throw std::runtime_error("template is not grounded");

        auto templateStatementPtr = groundedTemplatePtr->grounding().getStatementGroundingPtr();
        if (!templateStatementPtr)
          throw std::runtime_error("template is not grounded on a statement");

        auto requestTypes = toReplaceGroundingPtr->requestTypes;
        auto verbTense = toReplaceGroundingPtr->verbTense;
        toReplaceGroundedPtr->moveGrounding(groundedTemplatePtr->cloneGrounding());
        toReplaceGroundedPtr->grounding().getStatementGrounding().requestTypes = requestTypes;
        toReplaceGroundedPtr->grounding().getStatementGrounding().verbTense = verbTense;

        for (auto& child: groundedTemplatePtr->children)
        {
          const auto& groundedChild = child.second->getGrdExpPtr();
          if (groundedChild)
          {
            const auto& metaGrounding = groundedChild->grounding().getMetaGroundingPtr();
            if (metaGrounding)
            {
              const auto& slotName = metaGrounding->attibuteName;
              auto slotFillingIt = slotFillings.find(slotName);
              assert(slotFillingIt != slotFillings.end());
              if (slotFillingIt == slotFillings.end())
                throw std::runtime_error("invalid semantic expression association");
              toReplaceGroundedPtr->children[child.first] =
                  slotFillingIt->second->clone();
              continue;
              // TODO: remove other children?
            }
          }

          // If it is not a template, but there is something to recollect.
          toReplaceGroundedPtr->children[child.first] = child.second->clone();
        }

        qiLogInfo("SemanticAgent.Obey")
            << "Expression after replacement and filling:\n"
            << toReplace;
      };

      // if yes, replace it by what it is equivalent to
      auto& equivalentAction = actionEquivalenceIt->second;
      switch (equivalentAction->type)
      {
      case SemanticExpressionType::GROUNDED:
        // if it is equivalent to a single statement, just replace it
        replaceAndApplyFillings(*semExp, *equivalentAction);

        // recursively decompose again
        decompose(
              semExp, actionEquivalences, alreadyDecomposedSemExps,
              semanticMemory, linguisticDatabase,
              depthLimit);
        break;

      case SemanticExpressionType::LIST:
      {
        // if it is equivalent to a list of statement (of size n):
        // - duplicate n times the statement to replace
        // - replace each of these statements the respective equivalent
        auto& equivalentActionAsList = equivalentAction->getListExp();
        auto replacementListExpressionPtr = mystd::make_unique<ListExpression>(equivalentActionAsList.listType);
        for (auto& element: equivalentActionAsList.elts)
        {
          replacementListExpressionPtr->elts.emplace_back(originalExpression.clone());
          auto& replacementExpressionElementPtr = replacementListExpressionPtr->elts.back();
          replaceAndApplyFillings(*replacementExpressionElementPtr, *element);

          // recursively decompose again
          decompose(replacementExpressionElementPtr, actionEquivalences, alreadyDecomposedSemExps,
                    semanticMemory, linguisticDatabase,
                    depthLimit);
        }
        // this is ballsy: we replace the original pointer by the newly constructed one
        // this is safe only because we know that we iterate from the top to the bottom
        semExp = std::move(replacementListExpressionPtr);
        break;
      }
      default:
        QI_ASSERT(false && "malformed action equivalence");
        break;
      }
    }
  }
}


std::unique_ptr<qisem::SemanticExpression> introspectAction(
    const qisem::SemanticExpression& expression,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    qisem::SemanticMemory& semanticMemory)
{
  // first, check that it is a "what is to <learned action>?" sentence
  //------------------------------------------------------------------
  auto groundedExpressionPtr = expression.getGrdExpPtr_SkipWrapperPtrs();
  if (!groundedExpressionPtr)
    return std::unique_ptr<SemanticExpression>{};

  auto& groundedExpression = *groundedExpressionPtr;

  // grounded expression must be a verb-centered statement
  auto statementPtr = groundedExpression->getStatementGroundingPtr();
  if (!statementPtr)
    return std::unique_ptr<SemanticExpression>{};
  auto& statement = *statementPtr;

  // statement must involve the equality concept, in the present (is it a "is"?)
  if (statement.concepts.find(ConceptSet::conceptVerbEquality) == statement.concepts.end()
      || statement.verbTense != SemanticVerbTense::PRESENT)
    return std::unique_ptr<SemanticExpression>{};

  const GroundedExpression* ActionObj = nullptr;

  // statement must request either a subject or an object (is it a "what"?),
  // based respectively on an object or a subject, that is a grounded expression
  if (statement.requestTypes.count(SemanticRequestType::OBJECT))
  {
    auto childIt = groundedExpression.children.find(GrammaticalType::SUBJECT);
    if (childIt == groundedExpression.children.end())
      return std::unique_ptr<SemanticExpression>{};
    ActionObj = childIt->second->getGrdExpPtr_SkipWrapperPtrs();
  }
  else if (statement.requestTypes.count(SemanticRequestType::SUBJECT))
  {
    auto childIt = groundedExpression.children.find(GrammaticalType::OBJECT);
    if (childIt == groundedExpression.children.end())
      return std::unique_ptr<SemanticExpression>{};
    ActionObj = childIt->second->getGrdExpPtr_SkipWrapperPtrs();
  }
  else
    return std::unique_ptr<SemanticExpression>{};

  if (!ActionObj)
    return std::unique_ptr<SemanticExpression>{};
  auto& action = *ActionObj;

  // object grounded expression must be a verb-centered statement
  auto* actionStatementPtr = action->getStatementGroundingPtr();
  if (!actionStatementPtr)
    return std::unique_ptr<SemanticExpression>{};
  auto& actionStatement = *actionStatementPtr;

  // object statement must be an infinitive or a present participle: a.k.a. "unknown"
  if (actionStatement.verbTense != SemanticVerbTense::UNKNOWN)
    return std::unique_ptr<SemanticExpression>{};

  // object grounded expression must not have a subject
  auto childIt = action.children.find(GrammaticalType::SUBJECT);
  if (childIt != action.children.end())
    return std::unique_ptr<SemanticExpression>{};

  // then, identify the action
  //--------------------------
  // check if the object is amongst the learned equivalences
  auto actionEquivalenceIt = std::find_if(
        actionEquivalences.begin(), actionEquivalences.end(),
        [&](const SemanticExpressionAssociation& association)
  {
    return matchVerbalGroup(
          action, *association.first, semanticMemory, linguisticDatabase);
  });

  std::unique_ptr<SemanticExpression> actionDescription;

  // if it is an action we learned
  if (actionEquivalenceIt != actionEquivalences.end())
  {
    // decompose it at the first depth, for human readability
    UniqueSemanticExpression decomposedActionExpressionPtr{action.clone()};
    std::list<UniqueSemanticExpression> alreadyDecomposedSemExps;
    try
    {
      decompose(
            decomposedActionExpressionPtr, actionEquivalences,
            alreadyDecomposedSemExps, semanticMemory, linguisticDatabase, 1u);
    }
    catch (...)
    {
      return std::unique_ptr<SemanticExpression>{};
    }

    // currently the decomposition matches the description
    actionDescription = decomposedActionExpressionPtr->clone();
  }

  // else, let's check if it is a primitive action
  else
  {
    auto actionGrdExpPtr = action.clone();
    SemExpModifier::addChild(
          *actionGrdExpPtr,
          GrammaticalType::SUBJECT,
          mystd::make_unique<GroundedExpression>(SemanticAgentGrounding::getRobotAgentPtr()));
    SemExpModifier::addRequest(*actionGrdExpPtr, SemanticRequestType::ACTION);
    auto actionStatementPtr = actionGrdExpPtr->grounding().getStatementGroundingPtr();
    if (actionStatementPtr)
      actionStatementPtr->verbTense = SemanticVerbTense::PRESENT;

    auto command = memoryOperation::resolveCommand(*actionGrdExpPtr, semanticMemory, linguisticDatabase);
    if (command)
    {
      // it is a primitive action, show it by declaring "this"
      auto thisGrounding = mystd::make_unique<SemanticGenericGrounding>(
            SemanticReferenceType::DEFINITE, SemanticEntityType::THING);
      thisGrounding->coreference.emplace(CoreferenceDirectionEnum::AFTER);
      auto thisGroundedExpression = mystd::make_unique<GroundedExpression>(std::move(thisGrounding));
      auto listExpressionPtr = mystd::make_unique<ListExpression>();
      listExpressionPtr->elts.push_back(std::move(thisGroundedExpression));

      // and consecutively show the action
      listExpressionPtr->elts.push_back(std::move(*command));

      actionDescription = std::move(listExpressionPtr);
    }
  }

  // finally, if an action description was found, say it nicely
  //-----------------------------------------------------------
  if (!actionDescription)
    return std::unique_ptr<SemanticExpression>{};

  // producing "<action> <equivalence> <action description>"
  auto equivalenceStatement = mystd::make_unique<SemanticStatementGrounding>();
  equivalenceStatement->concepts.emplace(ConceptSet::conceptVerbEquality, 5);
  equivalenceStatement->verbTense = SemanticVerbTense::PRESENT;
  auto equivalenceExpression = mystd::make_unique<GroundedExpression>(std::move(equivalenceStatement));
  equivalenceExpression->children.emplace(GrammaticalType::SUBJECT, action.clone());
  equivalenceExpression->children.emplace(GrammaticalType::OBJECT, std::move(actionDescription));

  return std::move(equivalenceExpression);
}

std::unique_ptr<qisem::SemanticExpression> enterActionLearning(
    const qisem::SemanticExpression& knowledge,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    const qisem::SemanticMemory& semanticMemory)
{
  return std::unique_ptr<qisem::SemanticExpression>{};
}


void applyVerbalGroup(
    qisem::SemanticExpression& semanticExpression,
    const qisem::SemanticExpression& verbalGroupTemplate)
{
  auto semanticGroundedExpressionPtr = semanticExpression.getGrdExpPtr_SkipWrapperPtrs();
  if (!semanticGroundedExpressionPtr)
    throw std::runtime_error("target expression is not grounded");

  auto statementGroundingPtr = semanticGroundedExpressionPtr->grounding().getStatementGroundingPtr();
  if (!statementGroundingPtr)
    throw std::runtime_error("target expression is not grounded on a statement");

  auto templateExpressionPtr = verbalGroupTemplate.getGrdExpPtr();
  if (!templateExpressionPtr)
    throw std::runtime_error("template is not grounded");

  auto templateStatementPtr = templateExpressionPtr->grounding().getStatementGroundingPtr();
  if (!templateStatementPtr)
    throw std::runtime_error("template is not grounded on a statement");

  statementGroundingPtr->word = templateStatementPtr->word;

  for (auto& child: templateExpressionPtr->children)
  {
    auto matchingChildIt = semanticGroundedExpressionPtr->children.find(child.first);
    if (matchingChildIt == semanticGroundedExpressionPtr->children.end())
      semanticGroundedExpressionPtr->children.emplace(child.first, child.second->clone());
    else
      matchingChildIt->second = child.second->clone();
  }
}
} // agent
} // semantic
} // qi
