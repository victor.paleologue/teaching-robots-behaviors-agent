#include "not_understood_fallback.hpp"
#include <qi/knowledge/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "../pddl/pddl_edit.hpp"
#include "../pddl/pddl_io.hpp"
#include "../pddl/pddl_read.hpp"
#include "../utility/semantic_helpers.hpp"
#include "../language.hpp"
#include "reaction.hpp"
#include "semantic_behaviors.hpp"

qiLogCategory("SemanticAgent.NotUnderstood");

using namespace pddl::ast;

namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
/// The states representing the expectation of responses in this behavior.
enum class ResponseExpectation
{
  NotExpected, //< No response is expected.
  ExpectedSoon, //< A response is expected, but let's wait for other suggestions first.
  ExpectedNow //< A response is expected right now.
};

// For the meta behavior further below
struct SelectOrNotUnderstoodState: Trackable<SelectOrNotUnderstoodState>
{
  SelectOrNotUnderstoodState(
      SessionPtr session,
      std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
      std::shared_ptr<SemanticExpressionExecuter> pSemanticExecuter)
    : semanticMemoryContext(std::move(semanticMemoryContext))
    , locale(session)
    , semanticExecuter(pSemanticExecuter)
  {}

  /// Stops it all.
  ~SelectOrNotUnderstoodState() { destroy(); }

  /// Whether a response is expected.
  ResponseExpectation responseExpected = ResponseExpectation::NotExpected;

  /// Tracks the execution of the timeout before suggesting the fallback.
  Future<void> fallbackResponseTimeout = Future<void>{nullptr};

  /// Chatbot memory might be reused from external sources.
  /// It comes along a dedicated execution context that we must respect.
  std::shared_ptr<SemanticMemoryContext> semanticMemoryContext;

  /// Tracking the current locale so that to access it more efficiently.
  CurrentLocaleProperty locale;

  /// Subscriber object from the knowledge.
  knowledge::KnowledgeSubscriberPtr subscriber;

  /// The signal connection associated to the subscriber.
  SignalLink subscriberLink;

  /// To execute semantic responses.
  std::shared_ptr<SemanticExpressionExecuter> semanticExecuter;

  /// A helper to wrap callbacks in the execution context of this behavior.
  /// KLUDGE: unwrappedSchedulerFor would be preferable, but the detection of
  /// the start of the actions is buggy, and this works it around.
  template <typename Function>
  auto wrap(Function f) -> decltype(semanticMemoryContext->wrap(this, std::move(f)))
  { return semanticMemoryContext->wrap(this, std::move(f)); }
};

} // anonymous


Future<PDDLAwareQiBehaviorPtr> makeNotUnderstoodFallbackBehavior(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    knowledge::KnowledgeBasePtr knowledge,
    knowledge::EditableKnowledgeGraphPtr graph)
{
  auto executer = std::make_shared<SemanticExpressionExecuter>(
        session, context, semanticMemoryContext, speechEngine);
  auto sharedState = std::make_shared<SelectOrNotUnderstoodState>(
        session, semanticMemoryContext, executer);

  auto selectOrNotUnderstoodBehavior =
      makeQiBehavior(
        "Express when it is not understood",
        sharedState->wrap(
          [=](PropertyPtr<ActionObjs> suggestionsOut,
              PropertyPtr<Goals>,
              PropertyPtr<ActionFactories>) mutable
  {
    // When something is heard, we track if a response occurs.
    // If no response occurs before a given time, we suggest an action.

    // Here is how we reset tracking, and the state of the behavior.
    auto reset = sharedState->wrap([=]() mutable
    {
      qiLogDebug() << "Reset state";
      if (sharedState->subscriber)
      {
        try
        {
          sharedState->subscriber->tripleAdded.disconnect(sharedState->subscriberLink);
        }
        catch (const std::exception& e)
        {
          qiLogDebug() << "Error unsubscribing from knowledge: " << e.what();
        }
      }
      sharedState->subscriberLink = SignalBase::invalidSignalLink;
      sharedState->subscriber.reset();
      sharedState->responseExpected = ResponseExpectation::NotExpected;
      suggestionsOut->set(ActionObjs()).async();
      sharedState->fallbackResponseTimeout.cancel();
    });

    // Let us track what is heard.
    SignalLink heardLink = speechSource->heard.connect(
          sharedState->wrap([=](const Utterance& heard) mutable
    {
      try
      {
        // We stop tracking anything, only the last utterance matters.
        reset().value();

        qiLogDebug() << "Something was heard, a response is expected soon";

        // Let us track the responses.
        sharedState->subscriber = knowledge->subscribe(
              knowledge::Triple{
                knowledge::makeEmptyResourceNode(),
                teaching::respondsToResource(),
                knowledge::makeNodeFromResourceNode(heard.node)
              }, "");

        sharedState->subscriberLink =
            sharedState->subscriber->tripleAdded.connect(
              [=](const knowledge::Triple& /*triple*/) mutable
        {
          qiLogDebug() << "A response has just occurred";
          // When the tracked response occurs, reset tracking.
          reset();
        });

        // If it is already responded, reset immediately.
        auto responses = knowledge->querySubjects(
              teaching::respondsToResource(),
              knowledge::makeNodeFromResourceNode(heard.node),
              "");
        if (!responses.empty())
        {
          qiLogDebug() << "A response has already occurred";
          reset();
          return;
        }

        // So now we are waiting for the response.
        sharedState->responseExpected = ResponseExpectation::ExpectedSoon;

        // TODO: write the timeout in pure PDDL
        // KLUDGE: qi::asyncDelay would unwrap the result inadvertedly
        sharedState->fallbackResponseTimeout =
            getEventLoop()->asyncDelay(sharedState->wrap([=]() mutable
        {
          qiLogDebug() << "Timeout, suggesting a fallback response";
          try
          {
            ActionObjs suggestions{
              makeSemanticAction(
                    session, context, semanticMemoryContext,
                    sharedState->semanticExecuter,
                    notUnderstood(), sharedState->locale.value().value(),
                    graph, heard.node)
            };

            // Add add an some effect to the action, and an associated goal.
            // It states that the input communication act was not understood,
            // and that we would not like that to happen.
            // KLUDGE: there is no support for PDDL "preference" statements,
            // so the priorization of that goal is dealt with in our rule system.
            const auto& action = suggestions.front();
            auto description = parsePddlDomainAndProblem(action->pddl.get().value());

            // From the goal, we extract the input communication act and its type.
            QI_ASSERT(description->problem);
            auto& problem = description->problem.value();
            QI_ASSERT(problem->goal);
            auto& goal = problem->goal.value();
            auto& wasRespondedFormula = goal.get<AtomicFormula>();
            const auto& wasRespondedPredicate = wasRespondedFormula.get<PredicatePointer>();
            const auto& inputActConstant =
                wasRespondedPredicate->arguments[0].get<ConstantPointer>();
            QI_ASSERT(inputActConstant->declaration->type);
            const auto& communicationActType =
                inputActConstant->declaration->type.value().get<PrimitiveTypePointer>();

            // To allow deep copies.
            CopyContext copyContext;
            copyContext.types = &description->domain->types;
            copyContext.constants = &description->domain->constants;
            copyContext.predicates = &description->domain->predicates;

            // We create a predicate expressing the misunderstanding.
            auto communicationActParam =
                std::make_unique<VariableDeclaration>(
                  "c", Type(deepCopy(communicationActType, copyContext)));
            VariableDeclarations predicateVars;
            predicateVars.push_back(std::move(communicationActParam));
            description->domain->predicates.push_back(
                  std::make_unique<PredicateDeclaration>("was_misunderstood", std::move(predicateVars)));
            const auto& misunderstoodPredicateDeclaration = description->domain->predicates.back();

            // Statement that the input act is misunderstood.
            Predicate::Arguments args;
            args.push_back(Term(deepCopy(inputActConstant, copyContext)));
            auto misunderstoodCondition =
                std::make_unique<Predicate>(std::move(args), misunderstoodPredicateDeclaration.get());

            // Add it as an effect of the action.
            const auto& effectOpt = description->domain->actions[0]->effect;
            QI_ASSERT(effectOpt);
            auto& effectAsAnd = effectOpt.value().get<AndPointer<Effect>>();
            effectAsAnd->arguments.push_back(deepCopy(misunderstoodCondition, copyContext));

            // Make a goal to avoid this effect.
            auto notMisunderstoodCondition =
                std::make_unique<Not<Precondition>>(std::move(misunderstoodCondition));
            Preconditions goals;
            goals.push_back(std::move(wasRespondedFormula));
            goals.push_back(std::move(notMisunderstoodCondition));
            auto goalAsAnd = std::make_unique<And<Precondition>>(std::move(goals));
            problem->goal = std::move(goalAsAnd);

            // Set the new PDDL on the action.
            action->pddl.set(
                  serializePddl(*description->domain) +
                  serializePddl(problem)).value();

            // Suggest the action.
            suggestionsOut->set(std::move(suggestions)).value();
            sharedState->responseExpected = ResponseExpectation::ExpectedNow;
            qiLogDebug() << "Suggested a fallback response";
            // Everything should reset by itself,
            // because the semantic action is a resonse.
          }
          catch (const std::exception& e)
          {
            qiLogError() << "Error trying to produce a fallback suggestion: " << e.what();
            reset();
          }
        }), notUnderstoodFallbackTimeout).unwrap();
      }
      catch (const std::exception& e)
      {
        qiLogError() << "Unexpected error in behavior: " << e.what();
      }
    }));

    // Reset the state when stopped.
    Promise<void> promise([=](Promise<void> p) mutable
    {
      qiLogDebug() << "Select or not understood is being cancelled";
      speechSource->heard.disconnect(heardLink);
      auto resetting = reset();
      if (resetting.wait() == FutureState_FinishedWithError)
        qiLogError() << "Error resetting shared state: " << resetting.error();
      p.setCanceled();
    });

    // Keep the promise alive.
    auto running = promise.future();
    running.then([promise](Future<void>){});
    return running;
  }), "");

  return selectOrNotUnderstoodBehavior;
}

} // agent
} // semantic
} // qi
