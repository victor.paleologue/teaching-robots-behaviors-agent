#include "semantic_behaviors.hpp"
#include <boost/date_time/local_time/local_date_time.hpp>
#include <qi/jsoncodec.hpp>
#include <qi/log.hpp>
#include <qi/os.hpp>
#include <qi/knowledge/util.hpp>
#include <qi/locale/util.hpp>
#include <qi/teaching/ontology.hpp>
#include <qisem/semanticcontroller/semanticconverter.hpp>
#include <qisem/semanticcontroller/semanticmemory/semanticmemory.hpp>
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticagentgrounding.hpp>
#include <semanticnaoqilinker/resourcelabels.hpp>
#include <semanticnaoqilinker/tool/conditional_txt.hpp>
#include "../pddl/pddl_io.hpp"
#include "../utility/disposable.hpp"
#include "../utility/knowledge.hpp"
#include "../utility/owner.hpp"
#include "../utility/semantic_helpers.hpp"
#include "../language.hpp"
#include "reaction.hpp"

qiLogCategory("SemanticAgent.SemanticBehavior");

using namespace qisem;

namespace qi
{
namespace semantic
{
namespace agent
{
void informFromConditionalTxt(
    SemanticMemory& memory,
    const linguistics::LinguisticDatabase& linguisticDatabase,
    const std::string& filepath)
{
  static const std::map<std::string, std::string> conditions{
        {"RobotType", "Pepper"} // TODO: support NAO too.
  };

  static const TextProcessingContext textProcContext = []
  {
    TextProcessingContext textProcContext(
          SemanticAgentGrounding::me,
          SemanticAgentGrounding::userNotIdentified,
          SemanticLanguageEnum::UNKNOWN);

    // TODO: set this only for the case when informations are commands
    textProcContext.cmdGrdExtractorPtr =
        std::make_shared<ResourceGroundingExtractor>(resourceLabels);

    return textProcContext;
  }();

  const auto phrases = naoqilinker::tool::getLinesMatchingConditions(filepath, conditions);
  for (const auto& phrase: phrases)
  {
    auto expression = converter::textToSemExp(phrase, textProcContext, linguisticDatabase);
    memoryOperation::resolveAgentAccordingToTheContext(expression, memory, linguisticDatabase);
    memoryOperation::inform(std::move(expression), memory, linguisticDatabase);
  }
}


std::unique_ptr<SemanticMemory> makeMemoryKnowingCommands(
    const linguistics::LinguisticDatabase& linguisticDatabase)
{
  auto memory = std::make_unique<SemanticMemory>();
  auto commandsPath = qi::path::findData("semantic", "naoqibindings/commands.txt");
  informFromConditionalTxt(*memory, linguisticDatabase, std::move(commandsPath));
  qisem::memoryOperation::learnSayCommand(*memory, linguisticDatabase);
  return memory;
}


std::unique_ptr<SemanticMemory> makeMemoryKnowingCommandsAndInfos(
    const linguistics::LinguisticDatabase& linguisticDatabase)
{
  auto memory = makeMemoryKnowingCommands(linguisticDatabase);
  auto infoPath = qi::path::findData("semantic", "defaultassertions/informations.txt");
  informFromConditionalTxt(*memory, linguisticDatabase, std::move(infoPath));
  return memory;
}


std::string makePDDLReplyingToCommunicationAct(
    const std::string& phrase,
    const knowledge::ResourceNode& inputAct,
    const knowledge::ResourceNode& outputAct)
{
  const auto name = pddlNameFromPhrase(phrase);
  const auto inputActPDDL = pddlNameFromIRI(inputAct.url);
  const auto outputActPDDL = pddlNameFromIRI(outputAct.url);
  const auto communicationActPDDL = pddlNameFromIRI(teaching::communicationActType());
  const auto respondsToPDDL = pddlNameFromIRI(teaching::respondsTo());

  std::stringstream ss;
  ss << "(define (domain " << name << "_domain)\n"
        "  (:requirements :strips :typing :negative-preconditions)\n"
        "  (:types\n"
        "    " << communicationActPDDL << "\n"
        "  )\n"
        "  (:constants\n"
        "    " << inputActPDDL << " " << outputActPDDL << " - " << communicationActPDDL << "\n"
        "  )\n"
        "  (:predicates\n"
        "    (" << respondsToPDDL << " ?input - " << communicationActPDDL << " ?response - " << communicationActPDDL << ")\n"
        "    (was_responded_to ?c - " << communicationActPDDL << ")\n"
        "  )\n"
        "  (:action action_" << name << "\n"
        "    :parameters ()\n"
        "    :precondition (not (was_responded_to " << inputActPDDL << "))\n"
        "    :effect (and (" << respondsToPDDL << " " << outputActPDDL << " " << inputActPDDL << ")\n"
        "                 (was_responded_to " << inputActPDDL << "))\n"
        "  )\n"
        ")\n"
        "(define (problem " << name << "_problem)\n"
        "  (:domain " << name << "_domain)\n"
        "  (:requirements :strips :typing :negative-preconditions)\n"
        "  (:objects)\n"
        "  (:init)\n"
        "  (:goal (was_responded_to " << inputActPDDL << "))\n"
        ")";
  return ss.str();
}


// TODO: factorize with previous function.
std::string makePDDLReplyingToMisunderstoodCommunicationAct(
    const std::string& phrase,
    const knowledge::ResourceNode& inputAct,
    const knowledge::ResourceNode& outputAct)
{
  const auto name = pddlNameFromPhrase(phrase);
  const auto inputActPDDL = pddlNameFromIRI(inputAct.url);
  const auto outputActPDDL = pddlNameFromIRI(outputAct.url);
  const auto communicationActPDDL = pddlNameFromIRI(teaching::communicationActType());
  const auto respondsToPDDL = pddlNameFromIRI(teaching::respondsTo());

  std::stringstream ss;
  ss << "(define (domain " << name << "_domain)\n"
        "  (:requirements :strips :typing :negative-preconditions)\n"
        "  (:types\n"
        "    " << communicationActPDDL << "\n"
        "  )\n"
        "  (:constants\n"
        "    " << inputActPDDL << " " << outputActPDDL << " - " << communicationActPDDL << "\n"
        "  )\n"
        "  (:predicates\n"
        "    (" << respondsToPDDL << " ?input - " << communicationActPDDL << " ?response - " << communicationActPDDL << ")\n"
        "    (was_responded_to ?c - " << communicationActPDDL << ")\n"
        "    (was_misunderstood ?c - " << communicationActPDDL << ")\n"
        "  )\n"
        "  (:action action_" << name << "\n"
        "    :parameters ()\n"
        "    :precondition (not (was_responded_to " << inputActPDDL << "))\n"
        "    :effect (and (" << respondsToPDDL << " " << outputActPDDL << " " << inputActPDDL << ")\n"
        "                 (was_responded_to " << inputActPDDL << ")\n"
        "                 (was_misunderstood " << inputActPDDL << "))\n"
        "  )\n"
        ")\n"
        "(define (problem " << name << "_problem)\n"
        "  (:domain " << name << "_domain)\n"
        "  (:requirements :strips :typing :negative-preconditions)\n"
        "  (:objects)\n"
        "  (:init)\n"
        "  (:goal (was_responded_to " << inputActPDDL << "))\n"
        ")";
  return ss.str();
}


std::shared_ptr<SemanticExtractor> ensureSemanticExtractor(
    const SpeechSourcePtr& speechSource,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext)
{
  // KLUDGE: ptrUid is not unique in time! We need an alternate ID.
  auto speechSourceId = speechSource.call<std::string>("_id");

  static std::unordered_map<std::string, std::weak_ptr<SemanticExtractor>> semanticExtractors;
  static std::mutex semanticExtractorsMutex;
  std::unique_lock<std::mutex> lock{semanticExtractorsMutex};

  auto& weakExtractorInMap = semanticExtractors[speechSourceId];
  auto extractor = weakExtractorInMap.lock();
  if (!extractor)
  {
    qiLogDebug() << "New semantic extractor required for source: " << speechSourceId;
    extractor = std::make_shared<SemanticExtractor>();
    weakExtractorInMap = extractor;
    SignalLink speechSourceLink = SignalBase::invalidSignalLink;
    extractor->setOnSubscribers(
          semanticMemoryContext->strand()->schedulerFor(
            track([=](bool hasSubscriber) mutable
    {
      if (hasSubscriber)
      {
        speechSourceLink = speechSource->heard.connect(
              semanticMemoryContext->strand()->schedulerFor(
                [=](const Utterance& utterance)
        { // NOTE: after captures by copy, weakExtractorInMap is not
          // a hard reference anymore to the memory address in map.
          const auto& phrases = utterance.possiblePhrases;
          auto extractor = weakExtractorInMap.lock();
          if (!extractor)
            return;
          std::vector<SemanticEvent> results;
          results.reserve(phrases.size());
          for (const auto& phrase: phrases)
          {
            auto semanticExpression = phraseToSemanticExpression(*semanticMemoryContext, phrase);
            qiLogInfo()
                << "Robot may have heard \"" << phrase.text << "\":\n"
                << semanticExpression;
            results.push_back({phrase, std::move(semanticExpression), utterance.node});
          }
          QI_EMIT (*extractor)(std::move(results));
        }));

        qiLogVerbose() << "Semantic extractor for source "
                       << speechSourceId << " is active";
      }
      else
      {
        if (speechSourceLink != SignalBase::invalidSignalLink)
        {
          try {
            speechSource->heard.disconnect(speechSourceLink);
          } catch (const std::exception&) {}
          speechSourceLink = SignalBase::invalidSignalLink;
        }
        qiLogVerbose() << "Semantic extractor for source "
                       << speechSourceId << " is inactive";
      }
    }, extractor)));
  }
  else
  {
    qiLogDebug() << "Reusing existing semantic extractor for source " << speechSourceId;
  }
  return extractor;
}


SemanticExpressionExecuter::SemanticExpressionExecuter(
    const SessionPtr& session,
    const context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    conversation::SpeechEnginePtr speechEngine)
  : QiVirtualExecutor(session, context)
  , SafeContext(semanticMemoryContext->strand())
  , _semanticMemoryContext(std::move(semanticMemoryContext))
  , _speechEngine(speechEngine)
{}


SemanticExpressionExecuter::~SemanticExpressionExecuter()
{
  destroy();
}


void SemanticExpressionExecuter::_usageOfMemory(
    std::function<void(SemanticMemory&)> /*pFunction*/)
{}


void SemanticExpressionExecuter::_usageOfMemblock(
    std::function<void(const SemanticMemoryBlock&,
                       const std::string&)> pFunction)
{
  pFunction(_semanticMemoryContext->semanticMemory()->memBloc, _semanticMemoryContext->semanticMemory()->getCurrUserId());
}


void SemanticExpressionExecuter::_usageOfLingDb(
    std::function<void(const linguistics::LinguisticDatabase&)> pFunction)
{
  pFunction(*_semanticMemoryContext->linguisticDatabase());
}


// TODO: support language?
FutureVoid SemanticExpressionExecuter::_exposeText(
    const std::string &phrase,
    SemanticLanguageEnum,
    const FutureVoid& stopRequest)
{
  auto res = std::make_shared<PromiseVoid>();
  Future<void> future =
      _speechEngine->async().makeSay({phrase})

      .andThen(inContext([phrase](conversation::SayPtr say)
  {
    qiLogInfo() << "Uttering \"" << phrase << "\"";
    return say->async().run().andThen([say](void*){});
  })).unwrap();

  stopRequest.thenVoid([future]() mutable { future.cancel(); });
  future.then(inContext([=](const Future<void>& f)
  {
    if (f.hasError())
      qiLogError() << "Failed to utter \"" << phrase << "\": " << f.error();
    res->setFinished();
  }));
  return FutureVoid(res);
}


FutureVoid SemanticExpressionExecuter::_exposeResource(
    const qisem::SemanticResourceGrounding& resourceGrd,
    const qisem::FutureVoid& stopRequest)
{
  if (resourceGrd.label == kQiActionResourceLabel)
  {
    const auto& actionId = resourceGrd.value;
    auto found = _actionById.find(actionId);
    if (found != _actionById.end())
    {
      qiLogInfo()
          << "Executer " << static_cast<void*>(this)
          << " found known action resource \"" << actionId << "\"";
      auto res = std::make_shared<PromiseVoid>();
      const auto& action = found->second;
      auto future = action.async<void>("run");
      stopRequest.thenVoid([future]() mutable { future.cancel(); });
      future.then([=](const Future<void>& f)
      {
        if (f.hasError())
          qiLogError() << "Run action \"" << actionId << "\": " << f.error();
        res->setFinished();
      });
      return FutureVoid(res);
    }
    else
    {
      qiLogError()
          << "Executer " << static_cast<void*>(this)
          << " found unknown action resource \"" << actionId << "\"";
      return FutureVoid();
    }
  }

  return QiVirtualExecutor::_exposeResource(resourceGrd, stopRequest);
}


OwnerPtr SemanticExpressionExecuter::addAction(AnyObject action)
{
  return inContext([=]
  {
    std::stringstream idSs;
    idSs << action.uid();
    auto id = idSs.str();
    _actionById[id] = action;
    auto owner = boost::make_shared<CustomOwner>(
          CustomOwner::ReleaseFunction(inContext([=]
    {
      _actionById.erase(id);
    })));
    qiLogInfo()
        << "Executer " << static_cast<void*>(this)
        << " acknowledges action " << id;
    return AnyObject(owner);
  })().value();
}


/// A function that "performs" the given semantic knowledge.
Future<void> runSemanticResponse(
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<qisem::SemanticExpression> reactionExpression,
    locale::Locale locale,
    std::shared_ptr<SemanticExpressionExecuter> expressionExecuter,
    knowledge::ResourceNode inResponseTo = {},
    knowledge::EditableKnowledgeGraphPtr graph = {},
    knowledge::ResourceNode responseResource = {})
{
  auto language = localeToSemanticEnum(locale);
  if (language == SemanticLanguageEnum::UNKNOWN)
  {
    std::stringstream ss;
    ss << "semantic engine does not support language currently set on the robot: "
       << encodeJSON(locale);
    throw std::runtime_error(ss.str());
  }

  Promise<void> promise{[expressionExecuter](const Promise<void>&)
  {
    expressionExecuter->stop();
  }};

  ::qisem::TextProcessingContext textContext{
    qisem::SemanticAgentGrounding::me,
    qisem::SemanticAgentGrounding::currentUser,
    language};
  auto executerContext = std::make_shared<ExecutorContext>(textContext);

  qiLogInfo()
      << "Running semantic reaction";
  expressionExecuter->runSemExp(reactionExpression->clone(), executerContext)
      .thenVoid([promise, expressionExecuter]() mutable
  {
    if (expressionExecuter->isStopped())
      promise.setCanceled();
    else
      promise.setValue(nullptr);
  });

  auto runningSemanticExpression = promise.future();

  auto runningAndPublishing = runningSemanticExpression.andThen([=](void*) mutable -> Future<void>
  {
    if (!graph || !knowledge::isNodeValid(inResponseTo)
        || knowledge::isNodeEmpty(makeNodeFromResourceNode(inResponseTo)))
    {
      qiLogInfo() << "Not publishing of the response in the knowledge";
      return Future<void>(nullptr);
    }

    // We produce a communicative act event related to that response.
    qiLogInfo() << "Publishing the response in the knowledge";
    if (responseResource.url.empty())
      responseResource = teaching::makeUniqueResource(teaching::eventPrefix());
    const auto triples = makeRobotResponseToCommunicationAct(inResponseTo, responseResource);
    return addTriples(graph, triples);
    // TODO: remove leftover events, to prevent memory saturation.
  }).unwrap();

  runningAndPublishing.then([](Future<void> f)
  {
    qiLogInfo() << "Finished semantic reaction";
    if (f.hasError())
      qiLogError() << "Error performing a semantic-based response: " << f.error();
  });

  return runningAndPublishing;
}


ActionObj makeSemanticAction(
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<SemanticExpressionExecuter> semanticExecuter,
    std::unique_ptr<qisem::SemanticExpression> reaction,
    locale::Locale locale,
    knowledge::EditableKnowledgeGraphPtr graph,
    const knowledge::ResourceNode& inResponseTo,
    std::function<Future<void>()> continuation)
{
  ::qisem::TextProcessingContext textContext{
    qisem::SemanticAgentGrounding::me,
    qisem::SemanticAgentGrounding::currentUser,
    currentLanguageToSemanticEnum(session)};

  std::string text;
  converter::semExpToText(
        text, reaction->clone(), textContext, false,
        *semanticMemoryContext->semanticMemory(),
        *semanticMemoryContext->linguisticDatabase(),
        nullptr);

  return makeSemanticAction(
        std::move(text), std::move(session), std::move(context),
        std::move(semanticMemoryContext), std::move(semanticExecuter),
        std::move(reaction), std::move(locale),
        std::move(graph), inResponseTo, std::move(continuation));
}

ActionObj makeSemanticAction(
    const std::string& name,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<SemanticExpressionExecuter> semanticExecuter,
    std::unique_ptr<qisem::SemanticExpression> reaction,
    locale::Locale locale,
    knowledge::EditableKnowledgeGraphPtr graph,
    const knowledge::ResourceNode& inResponseTo,
    std::function<Future<void>()> continuation)
{
  qiLogInfo()
      << "Semantic action is starting";

  // TODO: in C++14 use assignment in capture to move this unique pointer
  std::shared_ptr<qisem::SemanticExpression> reactionShared{std::move(reaction)};
  QI_ASSERT(semanticExecuter);

  const auto response = teaching::makeUniqueResource(teaching::eventPrefix());
  auto semanticFunction = [=](context::ContextPtr context)
  {
    return runSemanticResponse(
          session, context, semanticMemoryContext, reactionShared, locale,
          semanticExecuter, inResponseTo, graph, response)
        .andThen([=](void*)
    {
      if (continuation)
      {
        qiLogInfo()
            << "Semantic action finishing, performing continutation...";
        return continuation();
      }
      qiLogInfo()
          << "Semantic action finished";
      return Future<void>(nullptr);
    }).unwrap();
  };

  std::string pddl = [&]
  {
    if (isNodeValid(inResponseTo))
      return makePDDLReplyingToCommunicationAct(name, inResponseTo, response);
    return std::string();
  }();

  auto action = makeQiAction(
        session, name, std::move(semanticFunction), std::make_shared<Strand>(), pddl);
  action->context.set(context).value();
  return action;
}


Future<PDDLAwareQiBehaviorPtr> makeSemanticBehavior(
    const std::string& name,
    SpeechSourcePtr speechSource,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    SemanticActionResponseFunction semanticReaction,
    const std::string& pddl)
{
  // KLUDGE: we use a shared pointer here because stranding makes copies.
  auto sharedReactionFunction =
      std::make_shared<SemanticActionResponseFunction>(std::move(semanticReaction));

  auto behaviorFunction = [=](
      PropertyPtr<ActionObjs> property,
      PropertyPtr<Goals>,
      PropertyPtr<ActionFactories>) mutable
  {
    Promise<void> p;

    auto semanticExtractor = ensureSemanticExtractor(speechSource, semanticMemoryContext);

    auto signalLink = semanticExtractor->connect(
          semanticMemoryContext->strand()->unwrappedSchedulerFor(
            [=](const std::vector<SemanticEvent>& events) mutable
    {
      // It is safe to run functions using the semantic memory in this context.
      if (events.empty())
        return;

      // TODO: let several inputs be handled
      const auto& event = events.front();
      const auto& phrase = event.phrase;
      qiLogDebug()
          << "Behavior \"" << name << "\" acknowledges the incoming phrase \""
          << phrase.text << "\"";

      // TODO: several reactions could be possible, with various auxiliary goals, not just one
      // When requested to react anew, drop any current suggestions.
      if (!property->get().value().empty())
        property->set(ActionObjs());

      try
      {
        auto reaction = (*sharedReactionFunction)(events);
        if (reaction)
        {
          qiLogVerbose()
              << "Behavior \"" << name << "\" reacts to \"" << phrase.text << "\"";

          auto actionFromSuggestions = std::make_shared<std::unique_ptr<Disposable>>();
          auto action = then(session, reaction, [=](Future<void>)
          {
            if (*actionFromSuggestions)
              return (*actionFromSuggestions)->dispose();
            return Future<void>(nullptr);
          });

          property->set({action});

          *actionFromSuggestions = makeDisposable(
                DisposalFunction(semanticMemoryContext->strand()->schedulerFor([=]
          {
            qiLogDebug() << "Removing action from suggestion";
            auto actions = property->get().value();
            auto found = std::find(actions.begin(), actions.end(), action);
            if (found != actions.end())
              actions.erase(found);
            property->set(actions).value();
          })));
        }
        else
        { // No reaction, so clear the current suggestions.
          qiLogVerbose()
              << "Behavior \"" << name << "\" does not react to \"" << phrase.text << "\"";
        }
      }
      catch (const std::exception& e)
      {
        qiLogError() << "Behavior \"" << name << "\" encountered an error "
                     << "when reacting to \"" << phrase.text << "\": " << e.what();
      }
    }));

    p.setOnCancel([=](Promise<void>&) mutable
    {
      qiLogDebug() << "Semantic behavior is being cancelled";
      semanticExtractor->disconnectAsync(signalLink);
      semanticExtractor.reset();
      qiLogDebug() << "Semantic behavior's extractor was dropped";
      p.setCanceled(); // using p here captures the promise and prevents it to be broken
    });
    return p.future();
  };

  return makeQiBehavior(name, behaviorFunction, pddl);
}


Future<PDDLAwareQiBehaviorPtr> makeSemanticBehavior(
    const std::string& name,
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    SemanticExpressionResponseFunction semanticReaction,
    const std::string& pddl,
    knowledge::EditableKnowledgeGraphPtr graph)
{
  // KLUDGE: wrap in shared pointers until we can move functions to strands.
  auto sharedSemanticReaction =
      std::make_shared<SemanticExpressionResponseFunction>(std::move(semanticReaction));

  // Reuse the executer across reactions.
  auto semanticExecuter =
      std::make_shared<SemanticExpressionExecuter>(session, context, semanticMemoryContext, speechEngine);

  SemanticActionResponseFunction reactionFunction =
      [=](const std::vector<SemanticEvent>& possibleEvents)
  {
    // Semantic behaviors guarantee the list of events is not empty.
    QI_ASSERT(!possibleEvents.empty());

    // This is a simplified case where we only use the most probable result.
    const auto& event = possibleEvents.front();
    const auto& phrase = event.phrase;
    const auto& expression = *event.expression;
    const auto& inputActResource = event.communicationActNode;

    auto reaction = (*sharedSemanticReaction)(phrase, expression);
    if (!reaction)
      return ActionObj();

    qiLogVerbose()
        << "Behavior \"" << name << "\" reacts to \"" << phrase.text << "\"";

    return makeSemanticAction(
          session, context, semanticMemoryContext,
          semanticExecuter, std::move(reaction), phrase.locale,
          graph, inputActResource);
  };

  return makeSemanticBehavior(
        name, speechSource, session, context, semanticMemoryContext,
        std::move(reactionFunction), pddl);
}


Future<PDDLAwareQiBehaviorPtr> makeIntrospectBehavior(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<std::list<SemanticExpressionAssociation>> taughtBehaviors)
{
  auto semanticReaction = [=](const PhraseAndLocale&, const qisem::SemanticExpression& k)
  {
    return introspectAction(
          k, *taughtBehaviors,
          *semanticMemoryContext->linguisticDatabase(),
          *semanticMemoryContext->semanticMemory());
  };

  // TODO: set real STRIPS properties for each behavior
  return makeSemanticBehavior(
        "To introspect a behavior",
        speechSource, speechEngine, session, context, semanticMemoryContext, std::move(semanticReaction),
        "", knowledgeBase->editableKnowledgeGraph("beliefs"));
}
} // agent
} // semantic
} // qi
