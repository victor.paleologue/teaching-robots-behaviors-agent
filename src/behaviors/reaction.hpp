#ifndef SEMANTICAGENT_REACTION_HPP
#define SEMANTICAGENT_REACTION_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/locale/locale.hpp>
#include <qisem/linguisticanalyzer/dbtype/linguisticdatabase.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticexpressions.hpp>
#include "semantic_expression_association.hpp"

namespace qisem
{
namespace linguistics
{
struct LinguisticDatabase;
}
struct SemanticMemory;
}

namespace qi
{
namespace semantic
{
namespace agent
{
// TODO: replace these functions by visitors than can visit the same knowledge in parallel
// TODO: get rid of all those memory and linguistic databases from all those functions
// TODO: reaction currently return some semantic knowledge representing a response, but in theory
// the response should not directly be the outcome of the knowledge processing; it should be the
// outcome of a goal-based planification instead, for example, just like next expectations should be.

/// The respond that you don't get it.
std::unique_ptr<qisem::SemanticExpression> notUnderstood();

/// The default reaction of the robot, result can be null.
std::unique_ptr<qisem::SemanticExpression> defaultReact(
    const qisem::SemanticExpression& expression,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    qisem::SemanticMemory& semanticMemory);

/// Tells whether the given expression is an action or an action list defined in the semantic memory.
bool isSemanticAction(const qisem::SemanticExpression& semExp);

/// Alias for a semantic expression holding a statement or a list of statements in the infinitive form.
using InfinitiveStatementExpression = qisem::UniqueSemanticExpression;

/// Represent a decomposition error caused by an infinite recursion in decomposition.
struct InfiniteDecompositionException: std::exception
{
  const char* what() const noexcept override;
  std::string lemma;
};

/// Takes a semantic expression describing an action,
/// and produces a semantic expression describing the action's decomposition.
void decompose(
    qisem::UniqueSemanticExpression& semExp,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    std::list<qisem::UniqueSemanticExpression>& alreadyDecomposedSemExps,
    const qisem::SemanticMemory& semanticMemory,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    boost::optional<size_t> depthLimit = boost::optional<size_t>{});


/// Responds with a description of the requested action.
std::unique_ptr<qisem::SemanticExpression> introspectAction(
    const qisem::SemanticExpression& expression,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    const qisem::linguistics::LinguisticDatabase& linguisticDatabase,
    qisem::SemanticMemory& semanticMemory);


/// Replace the verbal group by the one given in template.
/// Example: f("I run in the room", "walk fast") -> "I run fast in the room".
void applyVerbalGroup(
    qisem::SemanticExpression& semanticExpression,
    const qisem::SemanticExpression& verbalGroupTemplate);
} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_REACTION_HPP
