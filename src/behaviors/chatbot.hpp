#include <memory>
#include <qi/conversation/conversation.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qi/session.hpp>

namespace qi
{
namespace semantic
{
namespace agent
{

class Behavior;

Object<conversation::Chatbot> combineChatbots(
    std::vector<Object<conversation::Chatbot>> chatbots);

Future<PDDLAwareQiBehaviorPtr> makeChatbotBehavior(
    qi::SessionPtr session,
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    Object<conversation::Chatbot> chatbot,
    knowledge::EditableKnowledgeGraphPtr graph = {});

} // agent
} // semantic
} // qi
