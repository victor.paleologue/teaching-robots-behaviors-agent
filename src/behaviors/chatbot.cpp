#include "chatbot.hpp"
#include <qi/log.hpp>
#include <qi/locale/util.hpp>
#include <qi/teaching/ontology.hpp>
#include "../behaviormodel/behavior.hpp"
#include "../behaviors/semantic_behaviors.hpp"
#include "../utility/incontext.hpp"
#include "../utility/knowledge.hpp"

qiLogCategory("SemanticAgent.Chatbot");

namespace qi
{
namespace semantic
{
namespace agent
{
using namespace qi::conversation;
using namespace qi::locale;

namespace
{
// Utility to make asynchronous for loops.
//=========================================
enum class Iteration
{
  Continue = 0,
  Break
};

template <typename T>
using IterationFunction = std::function<Future<Iteration>(const T&)>;

#define CONTINUE_ASYNC return Future<Iteration>{Iteration::Continue}
#define BREAK_ASYNC return Future<Iteration>{Iteration::Break}

template <typename ConstIterator, typename T>
void _foreachAsyncQueue(
    Promise<void> promise,
    std::shared_ptr<Future<Iteration>> currentIteration,
    ConstIterator&& begin, ConstIterator&& end,
    IterationFunction<T> function)
{
  if (begin == end)
  {
    promise.setValue(nullptr);
    return;
  }

  // Call the function for this iteration.
  // If it throws, do as if it returned a future set to error,
  // so we keep track of it uniformly.
  try
  {
    *currentIteration = function(*begin);
  }
  catch (const std::exception& e)
  {
    *currentIteration = makeFutureError<Iteration>(e.what());
  }

  // Handle result. If anything fails stop
  currentIteration->then([=](Future<Iteration> future) mutable
  {
    if (future.hasError())
    {
      promise.setError(future.error());
      return;
    }
    else if (future.isCanceled())
    {
      promise.setCanceled();
      return;
    }

    // else
    switch (future.value())
    {
      case Iteration::Break:
        promise.setValue(nullptr);
      break;

      case Iteration::Continue:
        _foreachAsyncQueue(
              std::move(promise), std::move(currentIteration),
              ++begin, end, std::move(function));
      break;
    }
  });
}


/// Call the given function for each element found in the range.
/// The call is only performed if the previous call has finished,
/// and "returned" Iteration::Continue.
// TODO: improve automatic deduction of T
template <typename T, typename ConstIterator>
Future<void> foreachAsyncQueue(
    ConstIterator&& begin, ConstIterator&& end, IterationFunction<T>&& function)
{
  auto currentIteration = std::make_shared<Future<Iteration>>();
  Promise<void> promise{[=](Promise<void>){ currentIteration->cancel(); }};
  _foreachAsyncQueue(
        promise, currentIteration,
        std::forward<ConstIterator>(begin),
        std::forward<ConstIterator>(end),
        std::forward<IterationFunction<T>>(function));
  return promise.future();
}


/// Call the given function for each element found in the range.
/// All calls are performed immediately. The result is returned
/// when all calls are finished. The result of the first call to fail
/// (in the order of the range) is reported as the overall result.
template <typename T, typename ConstIterator>
Future<void> foreachAsync(
    ConstIterator&& begin, ConstIterator&& end, std::function<Future<void>(const T&)>&& function)
{
  std::vector<Future<void>> futures;
  auto current = begin;
  while (current != end)
  {
    futures.push_back(function(*current));
    ++current;
  }

  return waitForAll(futures).async().andThen([](const std::vector<Future<void>> fs)
  {
    for (const auto& f: fs)
      if (!f.hasValue())
        return f;
    return Future<void>{nullptr};
  }).unwrap();
}


/// Custom classes for a chatbot combining chatbots
///=================================================
/// Custom reply reaction
// TODO: ensure properties are read-only.
struct SimpleReplyReaction
{
  Property<ChatbotReactionPtr> chatbotReaction;
  Property<ReplyPriority> replyPriority;
};

QI_REGISTER_OBJECT(SimpleReplyReaction, chatbotReaction, replyPriority)

// This is what a chatbot reaction boils down to.
using ChatbotReactionFunction =
    std::function<Future<void>(
          std::shared_ptr<StoredProp<ChatbotReactionHandlingStatus>>,
          SpeechEnginePtr)>;

/// Custom chatbot reaction
class SimpleChatbotReaction
{
  std::shared_ptr<StoredProp<ChatbotReactionHandlingStatus>> _chatbotReactionHandlingStatus;
  ChatbotReactionFunction _function;
  Future<void> _running;

public:
  SimpleChatbotReaction(ChatbotReactionFunction function);
  Future<void> runWith(const SpeechEnginePtr& speechEngine);
  Future<void> stop();
  Property<ChatbotReactionHandlingStatus> chatbotReactionHandlingStatus;
};

QI_REGISTER_OBJECT(SimpleChatbotReaction, runWith, stop, chatbotReactionHandlingStatus)

SimpleChatbotReaction::SimpleChatbotReaction(ChatbotReactionFunction function)
  : _chatbotReactionHandlingStatus(std::make_shared<StoredProp<ChatbotReactionHandlingStatus>>())
  , _function(std::move(function))
  , _running()
  , chatbotReactionHandlingStatus(
      Property<ChatbotReactionHandlingStatus>::Getter(
        [=](boost::reference_wrapper<const ChatbotReactionHandlingStatus>)
                -> ChatbotReactionHandlingStatus
    {
      return _chatbotReactionHandlingStatus->get();
    }),
      Property<ChatbotReactionHandlingStatus>::Setter(
        [=](boost::reference_wrapper<ChatbotReactionHandlingStatus>, const ChatbotReactionHandlingStatus& value)
                -> bool
    {
      _chatbotReactionHandlingStatus->set(value);
      return false;
    }))
{
  _chatbotReactionHandlingStatus->connect(
        [this](ChatbotReactionHandlingStatus status)
  {
    QI_EMIT chatbotReactionHandlingStatus(std::move(status));
  });
}

Future<void> SimpleChatbotReaction::runWith(const SpeechEnginePtr& speechEngine)
{
  if (_running.isValid())
    throw std::runtime_error("reaction has already been started once");
  _running = _function(_chatbotReactionHandlingStatus, speechEngine);
  return _running;
}

Future<void> SimpleChatbotReaction::stop()
{
  _running.cancel();
  return _running;
}

/// Custom chatbot class.
class CombinedChatbot
{
public:
  CombinedChatbot(std::vector<ChatbotPtr> chatbots);

  Future<ReplyReactionPtr> replyTo(const Phrase& phrase, const Locale& locale);
  Future<void> acknowledgeHeard(const Phrase& phrase, const Locale& locale);
  Future<void> acknowledgeSaid(const Phrase& phrase, const Locale& locale);

  Property<AutonomousReactionPtr> autonomousReaction;
  Property<int> maxHypothesesPerUtterance;

private:
  const std::vector<ChatbotPtr> _chatbots;
};

QI_REGISTER_OBJECT(
    CombinedChatbot, replyTo, acknowledgeHeard, acknowledgeSaid,
    autonomousReaction, maxHypothesesPerUtterance)


CombinedChatbot::CombinedChatbot(std::vector<ChatbotPtr> chatbots)
  : _chatbots(std::move(chatbots))
{}

/// A selection of a reply, with a cache of the priority value, to help our algorithms.
struct ReplySelection
{
  ReplyReactionPtr reply;
  ReplyPriority priority = ReplyPriority::Fallback;
  ChatbotPtr replyingChatbot;
  std::vector<PhraseAndLocale> input;
  std::vector<ChatbotPtr> chatbots;
  qi::Strand strand;

  /// Wraps a function so that it is called in the context of strand.
  template <typename Function>
  auto wrap(Function&& f)
      -> decltype(strand.unwrappedSchedulerFor(std::forward<Function>(f)))
  { return strand.unwrappedSchedulerFor(std::forward<Function>(f)); }
};


/// Gets the reply of the given chatbot for the given phrase and locale,
/// and compare it to the currently selected reply. If it is better,
/// the new reply is selected. If it is the best (no further reply could be
/// better), it returns an iteration break. If it fails, it returns an iteration
/// continuation.
Future<Iteration> challengeReplyIteration(
    const std::shared_ptr<ReplySelection>& selection,
    const ChatbotPtr& chatbotSync,
    const Phrase& phrase,
    const qi::locale::Locale& locale)
{
  auto replying = [&]
  {
    if (!chatbotSync)
      return Future<ReplyReactionPtr>{ReplyReactionPtr{}};

    try
    {
      return chatbotSync->async().replyTo(phrase, locale);
    }
    catch (const std::exception& e)
    {
      return makeFutureError<ReplyReactionPtr>(e.what());
    }
  }();

  return replying.then([=](Future<ReplyReactionPtr> futureReply)
  {
    // Sanity check: whether there is a reply already selected or not,
    // it cannot be of normal priority (otherwise we wouldn't be here).
    QI_ASSERT(selection->priority == ReplyPriority::Fallback);

    auto reply = [&]
    {
      if (futureReply.hasValue())
        return futureReply.value();
      else
        return ReplyReactionPtr{};
    }();

    if (!reply) // No reply from this chatbot
      CONTINUE_ASYNC; // Try with the next one

    // TODO: what to do with the "handling status"?
    return reply->replyPriority.get().async().andThen(
          selection->wrap([=](ReplyPriority priority) mutable
    {
      switch (priority)
      {
        case ReplyPriority::Normal:
          selection->reply = reply;
          selection->priority = priority;
          selection->replyingChatbot = chatbotSync;
          return Iteration::Break;

        case ReplyPriority::Fallback:
          if (!selection->reply)
          {
            selection->reply = reply;
            QI_ASSERT(selection->priority == ReplyPriority::Fallback);
            selection->replyingChatbot = chatbotSync;
          }
          return Iteration::Continue;
      }
      throw std::runtime_error("unrecognized priority");
    })).unwrap();
  }).unwrap();
}


Future<ReplyReactionPtr> CombinedChatbot::replyTo(
    const Phrase& phrase,
    const Locale& locale)
{
  auto selection = std::make_shared<ReplySelection>();
  selection->chatbots = _chatbots;

  auto gettingReplies =
      foreachAsyncQueue<ChatbotPtr>(
        selection->chatbots.begin(),
        selection->chatbots.end(),
        selection->wrap([=](const ChatbotPtr& chatbotSync) mutable -> Future<Iteration>
  {
    return challengeReplyIteration(selection, chatbotSync, phrase, locale);
  }));

  return gettingReplies.andThen(selection->wrap([=](void*)
  {
    if (!selection->reply)
      throw std::runtime_error("no reply");

    // We build a new reply that acknowledges all the chatbots
    auto replyReaction = boost::make_shared<SimpleReplyReaction>();
    auto chatbotReactionFunction =
        selection->wrap([=](
            std::shared_ptr<StoredProp<ChatbotReactionHandlingStatus>>, // TODO: use it?
            const SpeechEnginePtr& speechEngine) -> Future<void>
    {
      qiLogInfo()
          << "Reaction of combined chatbot " << static_cast<void*>(this);
      for (const auto& chatbot: selection->chatbots)
      {
        if (chatbot != selection->replyingChatbot)
          chatbot->acknowledgeHeard(phrase, locale);
      }

      qiLogInfo()
          << "Acknowledged heard \"" << phrase.text
          << "\" all sub-chatbots of " << static_cast<void*>(this);
      return selection->reply->chatbotReaction.get().async()
          .andThen([selection, speechEngine, this](const ChatbotReactionPtr& reaction)
      {
        qiLogInfo()
            << "Running selected reaction from "
            << static_cast<void*>(this);
        auto running = reaction->async().runWith(speechEngine);
        running.then([reaction, this](Future<void>)
        { // keep alive the reaction
          qiLogInfo()
              << "Reaction from " << static_cast<void*>(this) << " finished";
        });
        return running;
      }).unwrap();
    });

    auto chatbotReaction = boost::make_shared<SimpleChatbotReaction>(std::move(chatbotReactionFunction));
    return replyReaction->chatbotReaction.set(AnyObject{chatbotReaction}).async()

    .andThen(selection->wrap([=](void*)
    {
      return replyReaction->replyPriority.set(selection->priority).async();
    })).unwrap()

    .andThen([=](void*) { return ReplyReactionPtr{AnyObject{replyReaction}}; });
  })).unwrap();
}


Future<void> CombinedChatbot::acknowledgeHeard(const Phrase& phrase, const Locale& locale)
{
  return foreachAsync<ChatbotPtr>(
        _chatbots.begin(), _chatbots.end(),
        [&](const ChatbotPtr& chatbot)
  {
    return chatbot->async().acknowledgeHeard(phrase, locale);
  });
}


Future<void> CombinedChatbot::acknowledgeSaid(const Phrase& phrase, const Locale& locale)
{
  return foreachAsync<ChatbotPtr>(
        _chatbots.begin(), _chatbots.end(),
        [&](const ChatbotPtr& chatbot)
  {
    return chatbot->async().acknowledgeSaid(phrase, locale);
  });
}

/// A function that challenges a list of chatbots the same way the standard Chat does.
qi::Future<ReplyReactionPtr> selectResult(
    const std::vector<PhraseAndLocale>& phrasesAndLocales,
    const ChatbotPtr& chatbotSync)
{
  auto gettingReply = Future<ReplyReactionPtr>{ReplyReactionPtr{}};

  // A shared pointer is used to keep alive the work data,
  // and to carry over the potential result across iterations.
  auto selection = std::make_shared<ReplySelection>();
  selection->input = phrasesAndLocales;

  auto gettingReplies =
      foreachAsyncQueue<PhraseAndLocale>(
        selection->input.begin(),
        selection->input.end(),
        selection->wrap([=](const PhraseAndLocale& phraseAndLocale) mutable -> Future<Iteration>
  {
    Phrase phrase{phraseAndLocale.text};
    const auto& locale = phraseAndLocale.locale;
    return challengeReplyIteration(selection, chatbotSync, phrase, locale);
  }));

  return gettingReplies.andThen(
        selection->wrap([=](void*) { return selection->reply; })).unwrap();
}
} // anonymous namespace


Object<conversation::Chatbot> combineChatbots(
    std::vector<Object<conversation::Chatbot>> chatbots)
{
  return AnyObject{boost::make_shared<CombinedChatbot>(std::move(chatbots))};
}


static std::string toString(const std::vector<PhraseAndLocale>& phrasesAndLocales)
{
  std::stringstream ss;
  ss << "[";
  bool first = true;
  for (const auto& phraseAndLocale: phrasesAndLocales)
  {
    if (!first)
    {
      first = false;
      ss << ", ";
    }
    ss << phraseAndLocale.text << " ("
       << locale::localeToStr(phraseAndLocale.locale) << ")";
  }
  ss << "]";
  return ss.str();
}


Future<PDDLAwareQiBehaviorPtr> makeChatbotBehavior(
    SessionPtr session,
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    Object<conversation::Chatbot> chatbot,
    knowledge::EditableKnowledgeGraphPtr graph)
{
  auto safeContext = std::make_shared<SafeContext>();
  auto safe = safeContext->inContextTransform();
  auto chatbotName = std::make_shared<std::string>();
  auto gettingName = chatbot.property<std::string>("name").async()

      .then(safe([=](const Future<std::string>& f)
  {
    if (f.hasValue())
    {
      *chatbotName = f.value();
    }
    else if (f.hasError())
    {
      qiLogDebug()
          << "Could not get chatbot's custom name: " << f.error();
      std::stringstream ss;
      ss << "chatbot " << chatbot.uid();
      *chatbotName = ss.str();
    }
  })).unwrap();

  // TODO: handle autonomous suggestions
  auto behaviorFunction =
      safe(
        [=](PropertyPtr<ActionObjs> suggestions,
        PropertyPtr<Goals>,
        PropertyPtr<ActionFactories>)
  {
    qiLogDebug()
        << *chatbotName << " is starting...";
    SignalLink link = speechSource->heard.connect(
          safe([=](const Utterance& heard)
    {
      qiLogDebug()
          << chatbotName << " processes inputs: " << toString(heard.possiblePhrases);
      selectResult(heard.possiblePhrases, chatbot)
          .then(safe([=](Future<ReplyReactionPtr> futureReaction)
      {
        ReplyReactionPtr reaction;
        if (futureReaction.hasValue())
            reaction = futureReaction.value();

        if (reaction)
        {
          qiLogDebug()
              << "Found reaction from " << chatbotName;

          auto reactionId = qi::os::generateUuid().substr(0, 8);
          const auto reactionName =
              std::string{"Reaction "} + reactionId +
              " of " + *chatbotName;
          const auto& inputActResource = heard.node;
          const auto outputActResource = teaching::makeUniqueResource(teaching::eventPrefix());
          auto pddl = [&]
          {
            auto replyPriority = reaction->replyPriority.get().value();
            if (replyPriority == ReplyPriority::Normal)
              return makePDDLReplyingToCommunicationAct(
                    reactionName, inputActResource, outputActResource);
            else
              return makePDDLReplyingToMisunderstoodCommunicationAct(
                    reactionName, inputActResource, outputActResource);
          }();

          try
          {
            auto reactionAction = makeQiAction(
                  session,
                  reactionName,
                  safe([=](context::ContextPtr context)
            {
              qiLogDebug()
                  << "Started " << reactionName;

              auto reacting = reaction->chatbotReaction.get().async()
                  .andThen(safe([safe, context, speechEngine](conversation::ChatbotReactionPtr chatbotReaction)
              {
                if (!chatbotReaction)
                  throw std::runtime_error("null chatbot reaction");

                try
                {
                  chatbotReaction.setProperty("context", context).value();
                }
                catch (const std::exception& e)
                {
                  qiLogDebug()
                      << "Chatbot reaction does not accept a custom context";
                }

                try
                {
                  auto running = chatbotReaction->async().runWith(speechEngine);
                  running.then(safe([safe, chatbotReaction](qi::Future<void>){})); // keep it alive
                  return running;
                }
                catch (std::exception& e)
                {
                  throw std::runtime_error(
                        std::string{"broken chatbot reaction: "} + e.what());
                }
              })).unwrap();

              reacting = reacting.then(safe([=](Future<void> f)
              {
                qiLogInfo()
                    << "Chatbot reply has finished";
                try
                {
                  if (f.hasError())
                  {
                    qiLogError()
                        << "Error running " << reactionName << ": " << f.error();
                  }
                  else
                  {
                    if (!graph)
                      return;

                    if (!knowledge::isNodeValid(inputActResource)
                        || !knowledge::isNodeEmpty(makeNodeFromResourceNode(inputActResource)))
                    {
                      qiLogWarning() << "Received an empty node in utterance";
                      return;
                    }

                    // We produce a communicative act event related to that response.
                    qiLogInfo() << "Creating response triples";
                    const auto triples =
                        makeRobotResponseToCommunicationAct(inputActResource, outputActResource);
                    qiLogInfo() << "Adding response triples";
                    addTriples(graph, triples);
                    qiLogInfo() << "Knowledge published";
                    // TODO: remove leftover events, to prevent memory saturation.
                  }
                }
                catch (const std::exception& e)
                {
                  qiLogWarning()
                      << "Caught error in chatbot reply wrap up: "
                      << e.what();
                }
                suggestions->set(ActionObjs()).value();
              })).unwrap();

              return reacting;
            }), safeContext->strand, std::move(pddl));

            qiLogDebug()
                << "Chatbot Behavior \"" << chatbotName
                << "\" suggests reaction object " << reactionAction.uid();
            suggestions->set(ActionObjs{reactionAction});
          }
          catch (const std::exception& e)
          {
            qiLogError()
                << "Error making an action from chatbot reaction: " << e.what();
          }
        }
        else
        {
          qiLogDebug()
              << "No reaction from " << chatbotName
              << ": " << futureReaction.error();
          suggestions->set(ActionObjs());
        }
      })).unwrap();
    }));

    Promise<void> promise(
          safe([speechSource, link, suggestions, safeContext](Promise<void> p)
    {
      speechSource->heard.disconnect(link);
      suggestions->set(ActionObjs());
      p.setCanceled();
    }));

    auto behaviorRunning = promise.future();
    behaviorRunning.then([promise](Future<void>){});
    qiLogInfo()
        << chatbotName << " is now running";
    return behaviorRunning;
  });

  return gettingName.andThen(safe([=](void*) mutable
  {
    return makeQiBehavior(*chatbotName, behaviorFunction, "");
  })).unwrap();

}

} // agent
} // semantic
} // qi
