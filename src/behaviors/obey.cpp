#include "obey.hpp"
#include <qisem/semanticcontroller/semexpoperators.hpp>
#include <qisem/linguisticanalyzer/dbtype/semanticgrounding/semanticstatementgrounding.hpp>
#include <qisem/linguisticanalyzer/tool/semexpmodifier.hpp>
#include "reaction.hpp"
#include "semantic_behaviors.hpp"
#include "../behaviormodel/action_pool.hpp"
#include "../utility/semantic_helpers.hpp"
#include "../utility/disposable.hpp"
#include "../knowledge_semantic.hpp"

qiLogCategory("SemanticAgent.Obey");

using namespace qisem;

namespace qi
{
namespace semantic
{
namespace agent
{
namespace
{
/// To store the full specification of an action to run.
struct ActionSpecification
{
  AnyObject actionFactory;
  std::map<ActionParameter, knowledge::ResourceNode> parameterSpecifications;
};


/// Prepare an action found in the action pool from a semantic expression.
std::tuple<AnyObject, std::unique_ptr<SemanticExpression>> resolveActionFromPool(
    const SemanticExpression& instruction,
    const locale::Locale& locale,
    const SemanticMemoryContext& semanticContext,
    std::shared_ptr<ActionPool> actionPool, // TODO: remove dependency
    std::shared_ptr<KnowledgeSemanticBridge> knowledgeSemantic,
    context::ContextPtr context)
{
  auto factoriesAndSlots = actionPool->findActionFactory(instruction.clone(), locale).value();
  if (factoriesAndSlots.empty())
  {
    qiLogInfo()
        << "Action was not found among the registered factories";
    return {};
  }

  if (factoriesAndSlots.size() > 1)
  {
    // TODO: here we can add something intelligent to learn which action is best
    qiLogWarning() << "Several factories found for statement \""
                   << phraseFromSemanticExpression(semanticContext, instruction, locale).text
                   << "\", the first one is being used";
  }
  else
  {
    qiLogInfo()
        << "A factory was found for\n" << instruction;
  }

  AnyObject action;
  for (const auto& factoryAndSlot: factoriesAndSlots)
  {
    ActionSpecification actionSpec;
    actionSpec.actionFactory = factoryAndSlot.actionFactory;

    try
    {
      for (const auto& semanticSlot: factoryAndSlot.semanticSlots)
      {
        const auto& parameter = semanticSlot.first;
        const auto& expression = semanticSlot.second;
        auto resource =
            knowledgeSemantic->findResourceFromSemanticLabel(
              expression, locale).value();
        qiLogInfo()
            << "Identified " << resource
            << " for parameter \"" << parameter.name << "\"";
        actionSpec.parameterSpecifications[parameter] = std::move(resource);
      }

      action = actionSpec.actionFactory.call<AnyObject>("makeAction", context);
      qiLogInfo()
          << "Made action \"" << action.property<std::string>("name").value()
          << "\"";
      for (const auto& paramSpec: actionSpec.parameterSpecifications)
      {
        action.setProperty<knowledge::ResourceNode>(
              paramSpec.first.name, paramSpec.second).value();
      }
      qiLogInfo()
          << "Made action \"" << action.property<std::string>("name").value()
          << "\" and applied parameters";
    }
    catch (const std::exception& e)
    {
      qiLogWarning() << "Skipped action after error while preparing it: " << e.what();
      action.reset();
    }

    // Stop as soon as one action seems ok.
    if (action)
      break;
  }

  if (!action)
    return {};

  // The action is ready, we associate it to a resource expression.
  std::stringstream ss;
  ss << action.uid();
  auto resourceGrounding =
      std::make_unique<SemanticResourceGrounding>(
        kQiActionResourceLabel, SemanticLanguageEnum::UNKNOWN, ss.str());
  return {action, std::make_unique<GroundedExpression>(std::move(resourceGrounding))};
}


/// Adaptable version of the obey algorithm.
UniqueSemanticExpression decomposeInstructions(
    const SemanticExpression& expression,
    const locale::Locale& locale,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    const SemanticMemoryContext& semanticContext)
{
  auto& semanticMemory = *semanticContext.semanticMemory();
  const auto& linguisticDatabase = *semanticContext.linguisticDatabase();
  UniqueSemanticExpression interpretedExpression{expression.clone()};

  if (actionEquivalences.empty())
  {
    qiLogInfo()
        << "No known expression association, no decomposition is possible";
    return interpretedExpression;
  }
  std::stringstream ss;
  ss << "Decomposing using known expression associations:\n";
  for (const auto& association: actionEquivalences)
  {
    ss << *association.first << "\n<=>\n" << *association.second << "\n";
  }
  qiLogInfo()
      << ss.str();

  // first, recognize actions that were learned to decompose them
  std::list<UniqueSemanticExpression> alreadyDecomposedSemExps;
  try
  {
    decompose(interpretedExpression, actionEquivalences, alreadyDecomposedSemExps,
              semanticMemory, linguisticDatabase);
  }
  catch (const InfiniteDecompositionException& e)
  {
    qiLogInfo() << "Infinite decomposition spotted on verb " << e.lemma;
  }
  catch (const std::exception& e)
  {
    qiLogError() << "Decomposition interrupted by error: " << e.what();
  }

  return interpretedExpression;
}

} // anonymous namespace

std::unique_ptr<SemanticExpression> obey(
    const SemanticExpression& expression,
    const locale::Locale& locale,
    std::list<SemanticExpressionAssociation>& actionEquivalences,
    const SemanticMemoryContext& semanticContext)
{
  auto decomposedInstructions =
      decomposeInstructions(expression, locale, actionEquivalences, semanticContext);
  if (!isSemanticAction(*decomposedInstructions))
  {
    qiLogInfo() << "Expression is not an action";
    return {};
  }

  auto result = qisem::memoryOperation::resolveCommand(
        *decomposedInstructions,
        *semanticContext.semanticMemory(),
        *semanticContext.linguisticDatabase());

  if (result)
  {
    qiLogInfo() << "Resolved command to perform";
    return (*result)->clone();
  }
  else
  {
    qiLogInfo() << "Could not resolve command to perform";
    return nullptr;
  }
}


Future<PDDLAwareQiBehaviorPtr> makeObey(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    std::shared_ptr<std::list<SemanticExpressionAssociation>> taughtBehaviors,
    std::shared_ptr<ActionPool> actionPool,
    std::shared_ptr<KnowledgeSemanticBridge> knowledgeSemantic)
{
  auto localMemory = std::make_shared<SemanticMemory>();
  localMemory->memBloc.subBlockPtr = &semanticMemoryContext->semanticMemory()->memBloc;
  auto localSemanticContext =
      makeSemanticMemoryContext(
        semanticMemoryContext->strand(),
        semanticMemoryContext->linguisticDatabase(),
        localMemory);
  auto executer =
      std::make_shared<SemanticExpressionExecuter>(session, context, semanticMemoryContext, speechEngine);

  auto semanticReaction = [=](const std::vector<SemanticEvent>& events)
  {
    QI_ASSERT(!events.empty());
    const auto& event = events.front();
    const auto& expression = *event.expression;
    const auto& locale = event.phrase.locale;

    auto decomposedInstructions =
        decomposeInstructions(expression, locale, *taughtBehaviors, *semanticMemoryContext);
    qiLogInfo()
        << "Input is decomposed to:\n" << *decomposedInstructions;
    if (!isSemanticAction(*decomposedInstructions))
    {
      qiLogInfo() << "This is not an instruction";
      return ActionObj();
    }

    qiLogInfo()
        << "This is an instruction";
    auto disposables = std::make_shared<Disposables>();
    if (actionPool && knowledgeSemantic)
    {
      qiLogInfo()
          << "Looking in the action pool...";
      auto instructionExpressionPtrs =
          SemExpModifier::listTopGroundedExpressionsPtr(*decomposedInstructions);
      for (auto& instructionExpressionPtr: instructionExpressionPtrs)
      {
        try
        {
          qiLogInfo()
              << "Checking expression:\n" << *instructionExpressionPtr;
          // Prepare an action from the action pool,
          // and prepare an expression that can be executed with our executer.
          auto actionAndExpression =
              resolveActionFromPool(
                *instructionExpressionPtr, locale, *semanticMemoryContext,
                actionPool, knowledgeSemantic, context);

          auto& action = std::get<0>(actionAndExpression);
          auto& expression = std::get<1>(actionAndExpression);

          if (!action || !expression)
          {
            qiLogInfo()
                << "No action found in pool for\n" << *instructionExpressionPtr;
            continue;
          }

          // Associate the instruction expression with the executable one.
          // First transform the instruction to the infinitive form.
          auto instructionInfinitive = instructionExpressionPtr->clone();
          SemExpModifier::clearRequestListOfSemExp(*instructionInfinitive);
          SemExpModifier::removeChildFromSemExp(*instructionInfinitive, GrammaticalType::SUBJECT);
          SemExpModifier::modifyVerbTenseOfSemExp(*instructionInfinitive, SemanticVerbTense::UNKNOWN);

          // Prepare the association statement.
          auto associationGrounding = std::make_unique<SemanticStatementGrounding>();
          associationGrounding->word = SemanticWord();
          associationGrounding->word.partOfSpeech = PartOfSpeech::VERB;
          associationGrounding->verbTense = SemanticVerbTense::PRESENT;
          associationGrounding->concepts["verb_equal_mean"] = 5;

          // Associate the infinitive instruction to the executable expression.
          auto associationExpression =
              std::make_unique<GroundedExpression>(std::move(associationGrounding));
          associationExpression->children[GrammaticalType::SUBJECT] =
              std::move(instructionInfinitive);
          associationExpression->children[GrammaticalType::OBJECT] =
              std::move(expression);

          // Inform memory.
          qiLogInfo()
              << "Hooking in semantic memory:\n" << *associationExpression;
          auto expressionInMemory = memoryOperation::inform(
                std::move(associationExpression),
                *localMemory, *semanticMemoryContext->linguisticDatabase());
          disposables->add(makeDisposable([=]
          {
            localMemory->memBloc.removeExpression(
                  *expressionInMemory, *semanticMemoryContext->linguisticDatabase(), nullptr);
          }));

          auto actionInExecuter = executer->addAction(action);
          disposables->add(makeDisposable(DisposalFunction([=]
          {
            return actionInExecuter->async().release();
          })));
        }
        catch (const std::exception& e)
        {
          qiLogInfo()
              << "Did not resolve action from pool: " << e.what();
        }
      }
    }

    auto result = qisem::memoryOperation::resolveCommand(
          *decomposedInstructions,
          *localMemory,
          *semanticMemoryContext->linguisticDatabase());

    if (!result)
    {
      qiLogInfo()
          << "No response corresponding to this instruction";
      return ActionObj();
    }

    qiLogInfo()
        << "Instruction understood, preparing response action";

    std::stringstream actionName;
    actionName << "Obey to \"" << event.phrase.text << "\"";
    return makeSemanticAction(
          actionName.str(),
          session, context, semanticMemoryContext, executer,
          (*result)->clone(), locale,
          knowledgeBase->editableKnowledgeGraph("beliefs"),
          event.communicationActNode,
          [=]{ return disposables->dispose(); });
    // Note that the disposables will also be disposed when the action is dropped.
  };

  return makeSemanticBehavior(
        "To obey",
        speechSource, session, context,
        semanticMemoryContext, std::move(semanticReaction));
}

} // agent
} // semantic
} // qi
