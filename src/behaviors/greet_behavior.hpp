#ifndef SEMANTICAGENT_BEHAVIOR_GREET_BEHAVIOR_HPP
#define SEMANTICAGENT_BEHAVIOR_GREET_BEHAVIOR_HPP

#include <qi/session.hpp>
#include <qi/semantic/agent/agent.hpp>
#include <qi/knowledge/knowledge.hpp>
#include "../humanmanager.hpp"


namespace qisem
{
  struct SemanticMemory;
  class SemanticExpression;

  namespace linguistics
  {
    struct LinguisticDatabase;
  } // linguistics
} // qisem

namespace qi
{
namespace semantic
{
namespace agent
{
class Behavior;
struct SemanticMemoryContext;

/// @brief Creates a semantic behavior that greet user
/// @verbatim Builds 2 actionSuggestions :
///     - a basic semantic ActionSuggestion taking & returning a SemanticExpression.
///     - an ActionSuggestion returing a SemanticExpression to say, triggered when a Human is engaged.
Future<PDDLAwareQiBehaviorPtr> makeGreetBehavior(SessionPtr session,
    context::ContextPtr context,
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext,
    knowledge::EditableKnowledgeBasePtr knowledgeBase,
    knowledge::EditableKnowledgeGraphPtr graph,
    const std::string& pddl,
    std::shared_ptr<HumanManager> humanManager);


/// @brief takes an utterance and return a greeting response
/// @verbatim calls greet() from SemanticReasoner as well as a list of callback function
Future<std::shared_ptr<qisem::SemanticExpression>> greetFeedback(
    const PhraseAndLocale& phrase,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticContext,
    std::shared_ptr<const qisem::SemanticExpression> exp,
    std::shared_ptr<HumanManager> humanManager,
    qi::knowledge::EditableKnowledgeBasePtr knowledgeBase,
    qi::knowledge::EditableKnowledgeGraphPtr graph);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_BEHAVIOR_GREET_BEHAVIOR_HPP
