#ifndef SEMANTICAGENT_BEHAVIOR_HELLOWORLD_HPP
#define SEMANTICAGENT_BEHAVIOR_HELLOWORLD_HPP

#include <memory>
#include <qi/session.hpp>
#include <qi/context/context.hpp>
#include <qi/semantic/agent/agent.hpp>
#include "../behaviors/semantic_behaviors.hpp"

namespace qi
{
namespace semantic
{
namespace agent
{

Future<PDDLAwareQiBehaviorPtr> makeHelloworld(
    SpeechSourcePtr speechSource,
    conversation::SpeechEnginePtr speechEngine,
    SessionPtr session,
    context::ContextPtr context,
    std::shared_ptr<SemanticMemoryContext> semanticMemoryContext);

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_BEHAVIOR_HELLOWORLD_HPP
