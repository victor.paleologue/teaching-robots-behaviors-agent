#ifndef SEMANTICAGENT_SEMANTIC_EXPRESSION_ASSOCIATION_HPP
#define SEMANTICAGENT_SEMANTIC_EXPRESSION_ASSOCIATION_HPP

#include <memory>
#include <utility>

namespace qisem
{
  struct SemanticExpression;
} // qisem

namespace qi
{
namespace semantic
{
namespace agent
{
/// A behavior alias, associating an expression to its decomposition.
/// Both expressions may contain meta groundings representing slots.
/// All slots found in the label must be present in the recipe,
/// and vice versa.
/// The correspondence of slots is based on their name:
/// if label has the slot "destination",
/// the piece of recipe reusing it
/// should also have a slot "destination".
struct SemanticExpressionAssociation
{
  std::unique_ptr<qisem::SemanticExpression> first;
  std::unique_ptr<qisem::SemanticExpression> second;
};

} // agent
} // semantic
} // qi

#endif // SEMANTICAGENT_SEMANTIC_EXPRESSION_ASSOCIATION_HPP
